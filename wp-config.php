<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'a&>OcAf4-Ri,l7VqKCVm&n0.hN|MVOPl:89mhoY5 ])Ibf?R=zCVG_DR=Gj#*-]U' );
define( 'SECURE_AUTH_KEY',  '#*vQW-?[X$ba0Lw`X8Cz+r@e,q&@wg~uz3UF^xWf[03]/#GnJ6F%@dILr_ZC8 hR' );
define( 'LOGGED_IN_KEY',    '5%0sk?JF?n$XlK~3$+y55Vv#Jb#!P +%I3a|shf7YE|ojWz`zJg:?/TOUw}n|uX(' );
define( 'NONCE_KEY',        '>+`rZk.*$h5T $uw:XWEEhGNl>e!G YHCUAwkCRe<4:J*LDi^YBslm7PdIP~=$zG' );
define( 'AUTH_SALT',        '5c28DT,G3aA!C=Qj`#5URNHHmo/{P+nQ_p:0,oA}PuW)5U>k~?LQ{?_qCIUO2q7_' );
define( 'SECURE_AUTH_SALT', 'G)lK-n4RuUPFL3wP+Tl:U4T(Z!Z&Y[5:7%6e$FDB*w_RFZe_0LTtmt,#QksFQk$1' );
define( 'LOGGED_IN_SALT',   'VP&$v#W]Pi%`3M[+mhYiU!k^=G;DyIYA}hW3>rApm?8R_y,=L`GrC}r#O3S>e.xI' );
define( 'NONCE_SALT',       'xv8V&6JW5hbb4~<6/X~]CnTHMA+}B><;~})(iucW<*k5-$z_1I*ZbcrS;*N/,yxw' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */ 
define( 'WP_DEBUG', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

