<?php

namespace Elementor;
if (!defined('ABSPATH')) exit;

/**
 * @method add_control(string $string, array $array)
 */

class Domain_Price_Table extends Widget_Base {

    private $version;

    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
    }

    /**
     * Get widget name.
     *
     * Retrieve heading widget name.
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */

    public function get_name()
    {
        return 'domain_price_table';
    }

    /**
     * Get widget Title.
     *
     * Retrieve heading widget Title.
     *
     * @return string Widget Title.
     * @since 1.0.0
     * @access public
     *
     */

    public function get_title()
    {
        return 'Domain TLD Table';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the heading widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */


    public function get_categories()
    {
        return ['iq-wp-whmcs-extension'];
    }


    /**
     * Get widget icon.
     *
     * Retrieve heading widget icon.
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */

    public function get_icon()
    {
        return 'fas fa-table';
    }

    protected function _register_controls() {

        /*---------------------- Start Style ---------------------*/

        $this->start_controls_section(
            'iq_wp_whmcs_section_style_switch',
            [
                'label' => esc_html__( 'Static or Dynamic', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
			'iq_wp_whmcs_style_switch',
			[
				'label' => __( 'dynamic data', 'wp-whmcs-lang' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'no',
				'options'    => [
					'yes'    => __( 'Yes', 'wp-whmcs-lang' ),
					'no'     => __( 'No', 'wp-whmcs-lang' ),
					
				],
			]
		);

        $this->end_controls_section();

        /*---------------------- End Style ---------------------*/



        /*---------------------- Start dynamic stucture ---------------------*/

        $this->start_controls_section(
			'section_domain_pricing_heading',
			[
                'label' => __( 'Table Heading', 'wp-whmcs-lang' ),
                'condition' => [
					'iq_wp_whmcs_style_switch' => 'no',
				],
			]
        ); 


        $this->add_control(
			'iq_wp_whmcs_heading_one',
			[
				'label' => __( 'Heading One', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'TLD', 'wp-whmcs-lang' ),
				'placeholder' => __( 'Enter Heading one', 'wp-whmcs-lang' ),
				'label_block' => true,
			]
        );

        $this->add_control(
			'iq_wp_whmcs_heading_two',
			[
				'label' => __( 'Heading Two', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Register', 'wp-whmcs-lang' ),
				'placeholder' => __( 'Enter Heading Two', 'wp-whmcs-lang' ),
				'label_block' => true,
			]
        );

        $this->add_control(
			'iq_wp_whmcs_heading_three',
			[
				'label' => __( 'Heading Three', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Transfer', 'wp-whmcs-lang' ),
				'placeholder' => __( 'Enter Heading Three', 'wp-whmcs-lang' ),
				'label_block' => true,
			]
        );

        $this->add_control(
			'iq_wp_whmcs_heading_four',
			[
				'label' => __( 'Heading Four', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Renew', 'wp-whmcs-lang' ),
				'placeholder' => __( 'Enter Heading Four', 'wp-whmcs-lang' ),
				'label_block' => true,
			]
        );

        $this->end_controls_section();

        /*------------------------------------------------
            Domain Pricing Ling
        ------------------------------------------------*/
        
		$this->start_controls_section(
			'section_domain_pricing',
			[
                'label' => __( 'Domain Pricing Table', 'wp-whmcs-lang' ),
                'condition' => [
					'iq_wp_whmcs_style_switch' => 'no',
				],
			]
        ); 
         
        $repeater = new Repeater();

        $repeater->add_control(
			'iq_wp_whmcs_nameas_switch',
			[
				'label' => __( 'Domain name as', 'wp-whmcs-lang' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'text',
				'options'    => [
					'text'    => __( 'Text', 'wp-whmcs-lang' ),
					'image'     => __( 'Image', 'wp-whmcs-lang' ),
					
				],
			]
		);

        $repeater->add_control(
			'iq_wp_whmcs_name',
			[
				'label' => __( 'Domain Name', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( '.com', 'wp-whmcs-lang' ),
                'placeholder' => __( 'Enter domain name', 'wp-whmcs-lang' ),
                'condition' => [
					'iq_wp_whmcs_nameas_switch' => 'text',
				],
				'label_block' => true,
			]
        );

        $repeater->add_control(
			'iq_wp_whmcs_image',
			[
				'label' => __( 'Choose Domain Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'iq_wp_whmcs_nameas_switch' => 'image',
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				]
			]
		);

        $repeater->add_control(
			'iq_wp_whmcs_name_txt_color',
			[
				'label' => __( 'Domain Name Text Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'label_block' => true,
                'condition' => [
					'iq_wp_whmcs_nameas_switch' => 'text',
				],
			]
		);

        $repeater->add_control(
			'iq_wp_whmcs_register',
			[
				'label' => __( 'Register', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( '$00.00', 'wp-whmcs-lang' ),
				'placeholder' => __( 'Enter register price', 'wp-whmcs-lang' ),
				'label_block' => true,
			]
        );

        $repeater->add_control(
			'iq_wp_whmcs_transfer',
			[
				'label' => __( 'Transfer', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( '$00.00', 'wp-whmcs-lang' ),
				'placeholder' => __( 'Enter transfer price', 'wp-whmcs-lang' ),
				'label_block' => true,
			]
        );

        $repeater->add_control(
			'iq_wp_whmcs_renew',
			[
				'label' => __( 'Renew', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( '$00.00', 'wp-whmcs-lang' ),
				'placeholder' => __( 'Enter renew Price', 'wp-whmcs-lang' ),
				'label_block' => true,
			]
        );
      
        $this->add_control(
			'iq_domain_list',
			[
				'label' => __( 'Domain Lists', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
                        'iq_wp_whmcs_name' => __( '.com', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_register' => __( '$10.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_transfer' => __( '$10.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_renew' => __( '$10.00', 'wp-whmcs-lang' ),
                        
                    ],
                    [
                        'iq_wp_whmcs_name' => __( '.in', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_register' => __( '$20.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_transfer' => __( '$20.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_renew' => __( '$20.00', 'wp-whmcs-lang' ),
                        
                    ],
                    [
                        'iq_wp_whmcs_name' => __( '.co', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_register' => __( '$15.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_transfer' => __( '$15.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_renew' => __( '$15.00', 'wp-whmcs-lang' ),
                        
                    ],
                    [
                        'iq_wp_whmcs_name' => __( '.net', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_register' => __( '$18.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_transfer' => __( '$18.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_renew' => __( '$18.00', 'wp-whmcs-lang' ),
                        
                    ],
                    [
                        'iq_wp_whmcs_name' => __( '.org', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_register' => __( '$25.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_transfer' => __( '$25.00', 'wp-whmcs-lang' ),
                        'iq_wp_whmcs_renew' => __( '$25.00', 'wp-whmcs-lang' ),
                        
					]
					
				],
                'title_field' => '{{{ iq_wp_whmcs_name }}}',
			]
        );

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'wp-whmcs-lang' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'wp-whmcs-lang' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'wp-whmcs-lang' ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);	

		$this->end_controls_section();
		

        /* Table Heading */

		$this->start_controls_section(
			'iq_wp_whmcs_hedsection',
			[
				'label' => __( 'Table Heading', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
					'iq_wp_whmcs_style_switch' => 'no',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'hedsection_text_typography',
				'label' => __( 'Heading text typography', 'wp-whmcs-lang' ),				
				'selector' => '{{WRAPPER}} .iq-domain-table thead tr .iq-heading',
			]
		);

		$this->add_control(
			'hedsection_textcolor',
			[
				'label' => __( 'Text Color', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-domain-table thead tr .iq-heading' => 'color: {{VALUE}};',
		 		],
			]
		);

		$this->add_control(
			'hedsection_bgcolor',
			[
				'label' => __( 'Background Color', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-domain-table thead tr .iq-heading' => 'background-color: {{VALUE}};',
		 		],
			]
		);

		$this->add_responsive_control(
			'hedsection_table_padding',
			[
				'label' => __( 'Padding', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-domain-table thead tr .iq-heading' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->end_controls_section();


		/* Box */

        $this->start_controls_section(
			'box_name',
			[
				'label' => __( 'Box', 'wp-whmcs-lang' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'box_padding',
			[
				'label' => __( 'Padding', 'wp-whmcs-lang' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-table-style td' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],				
			]
		);
		$this->end_controls_section();


        /*---------------------- End dynamic stucture ---------------------*/



        $this->start_controls_section(
            'iq_wp_whmcs_section_content',
            [
                'label' => esc_html__( 'Settings', 'wp-whmcs-lang' ),   //section name for controler view
                'condition' => [
					'iq_wp_whmcs_style_switch' => 'yes',
				],
            ]   
        );

        $this->add_control(
            'iq_wp_whmcs_whmcs_url',
            [
                'label' => __( 'WHMCS URL', 'wp-whmcs-lang' ),
                'description' => __( 'Used to get/send information from any WHMCS establishment!. Try not to add (/). Just info direct URL of your WHMCS territory (not administrator URL). ex: https://yourdomain/whmcs-server. Live see will presently work for this. Just when you see the site you will see that it fetches the information!', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'http://yourdomain/whmcs-server',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_currency_number',
            [
                'label' => __( 'Currency Number', 'wp-whmcs-lang' ),
                'description' => __( 'The currency number from your WHMCS. Generally the default currency numbered is 1', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '1',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_on',
            [
                'label' => __( 'Show Search', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'wp-whmcs-lang' ),
                'label_off' => __( 'Hide', 'wp-whmcs-lang' ),
                'return_value' => '1',
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_search_2',
            [
                'label' => __( 'Allow dot (.) before TLD', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Yes', 'wp-whmcs-lang' ),
                'label_off' => __( 'No', 'wp-whmcs-lang' ),
                'return_value' => 'isDotAllowed',
                'condition' => [
                    'iq_wp_whmcs_search_on' => '1',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_bar_placeholder',
            [
                'label' => __( 'Search Bar Placeholder', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Search TLD',
                'condition' => [
                    'iq_wp_whmcs_search_on' => '1',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'iq_wp_whmcs_section_style',
            [
                'label' => esc_html__( 'Style', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_input_bg_color',
            [
                'label' => __( 'Search Box Background Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'default'	=> '',
                'selectors' => [
                    '{{WRAPPER}} input[type="text"]' => 'background: {{VALUE}};',
                ],
                'condition' => [ 'iq_wp_whmcs_search_on' => '1', ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_input_text_color',
            [
                'label' => __( 'Search Box Input Text Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'default'	=> '',
                'selectors' => [
                    '{{WRAPPER}} input[type="text"]' => 'color: {{VALUE}};',
                ],
                'condition' => [ 'iq_wp_whmcs_search_on' => '1', ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_input_placeholder_color',
            [
                'label' => __( 'Search Box Placeholder Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'default'	=> '',
                'selectors' => [
                    '{{WRAPPER}} input[type="text"]::placeholder' => 'color: {{VALUE}};',
                ],
                'condition' => [ 'iq_wp_whmcs_search_on' => '1', ],
            ]
        );


        $this->add_control(
            'iq_wp_whmcs_search_input_border',
            [
                'label' => __( 'Search Box Border Css', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXTAREA,
                'placeholder'	=> '1px solid red; ',
                'selectors' => [
                    '{{WRAPPER}} input[type="text"]' => 'border: {{VALUE}};',
                ],
                'condition' => [ 'iq_wp_whmcs_search_on' => '1', ],
            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_search_box_alignment',
            [
                'label' => __( 'Search box Alignment', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'wp-whmcs-lang' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'wp-whmcs-lang' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'wp-whmcs-lang' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'condition' => [ 'search_on' => '1', ],
                'selectors' => [
                    '{{WRAPPER}} .iq-wp-whmcs-search-box' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_width',
            [
                'label' => __( 'Search Box Width', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 10000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} input[type="text"]' => 'width: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [ 'iq_wp_whmcs_search_on' => '1', ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_extra1',
            [
                'label' => __( 'Search Box Main Div CSS', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXTAREA,
                'placeholder' => 'background:#000;',
                'selectors' => [
                    '{{WRAPPER}} .iq-wp-whmcs-search-box' => '{{VALUE}};',
                ],
                'condition' => [ 'iq_wp_whmcs_search_on' => '1', ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_extra',
            [
                'label' => __( 'Search Box Input CSS', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXTAREA,
                'placeholder' => 'width:100%;',
                'selectors' => [
                    '{{WRAPPER}} input[type="text"]' => '{{VALUE}};',
                ],
                'condition' => [ 'iq_wp_whmcs_search_on' => '1', ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_table_css1',
            [
                'label' => __( 'Table Heading Background', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'default'	=> '',
                'selectors' => [
                    '{{WRAPPER}} table.domainpricing th' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_table_css2',
            [
                'label' => __( 'Table Heading Text Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'default'	=> '',
                'selectors' => [
                    '{{WRAPPER}} table.domainpricing th' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_label_typography',
                'label' => __( 'Table Heading Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} table.domainpricing th',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_label_typography1',
                'label' => __( 'Table Rows Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} table.domainpricing td',
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        require WP_WHMCS_ROOT . '/elementor/whmcs/render/domain_price_table.php';
    }

}

Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Domain_Price_Table());