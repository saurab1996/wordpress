<?php

namespace Elementor;
if (!defined('ABSPATH')) exit;

if(! function_exists('wp_whmcs_random_strings'))
{
    function wp_whmcs_random_strings($random = '')
    {
        // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.$random;

        // Shufle the $str_result and returns substring
        // of specified length
        return substr(str_shuffle($str_result),0, 15);
    }
}

/**
 * @method add_control(string $string, array $array)
 */
class Pricing_Table_Section extends Widget_Base
{

    private $version;

    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
    }

    /**
     * Get widget name.
     *
     * Retrieve heading widget name.
     *
     * @return string Widget name.
     * @since 1.2.0
     * @access public
     *
     */

    public function get_name()
    {
        return 'pricing_table_section';
    }

    /**
     * Get widget Title.
     *
     * Retrieve heading widget Title.
     *
     * @return string Widget Title.
     * @since 1.2.0
     * @access public
     *
     */

    public function get_title()
    {
        return 'Pricing Table';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the heading widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * @return array Widget categories.
     * @since 1.2.0
     * @access public
     *
     */


    public function get_categories()
    {
        return ['iq-wp-whmcs-extension'];
    }


    /**
     * Get widget icon.
     *
     * Retrieve heading widget icon.
     *
     * @return string Widget icon.
     * @since 1.2.0
     * @access public
     *
     */

    public function get_icon()
    {
        return 'fas fa-table';
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'iq_pricing_table_wp_whmcs_section',
            [
                'label' => __( 'Pricing Plan', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_url',
            [
                'label' => __( 'WHMCS URL', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'placeholder' => __( 'https://your-link.com', 'wp-whmcs-lang' ),
                'label_block' => true,
                'default' => 'http://yourdomain/whmcs-server',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_package_id',
            [
                'label' => esc_html__( 'Package ID from WHMCS', 'wp-whmcs-lang' ),
                'description' => esc_html__('Give the Package ID number of your item so the framework can get the subtleties of the item through the ID from your characterized WHMCS URL above. The Fetched information will be noticeable to the page after you hit spare and check the page', 'wp-whmcs-lang'),
                'type' => Controls_Manager::NUMBER,
                'default' => '1'
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_auto_title',
            [
                'label' => __( 'Get Package Title Directly', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'description' => 'Whenever turned on the Package name is highlighted consequently from your WHMCS URL characterized previously. Give the ID of your result of WHMCS Package in the Package ID field so the information can be brought',
                'return_value' => '1',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_title',
            [
                'label' => __( 'Package Title', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'dynamic' => [
                    'active' => true,
                ],
                'label_block' => true,
                'default' => __( 'Entry Server', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_auto_title!' => '1',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_auto_price',
            [
                'label' => __( 'Get Monthly Package Price Directly', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                //'default' => '0',
                'description' => 'If turned on the package price is fetched automatically from your WHMCS URL defined above. Give the ID of your Package of WHMCS Package so that the data can be fetched in Package id filed',
                'return_value' => '1',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price',
            [
                'label' => __( 'Price', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'dynamic' => [
                    'active' => true,
                ],
                'label_block' => true,
                'default' => __( '10', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_auto_price!' => '1',
                ]
            ]
        );

        $resultarr = [];
        $currrency = $this->get_currency_symbol();
        foreach ($currrency as $key => $value) {
            $resultarr[$value.' '.$key] = $value.' '.$key;
        }

        $this->add_control(
            'iq_wp_whmcs_currency_symbol',
            [
                'label' => __( 'Currency Symbol', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT2,
                'default' => '$ USD',
                'options' => $resultarr,
                'condition' => [
                    'iq_wp_whmcs_auto_price!' => '1',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_billing_cycle',
            [
                'label'       => __( 'Billing Cycle', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT,
                'description' => 'This shows data from WHMCS url as define. If the billing cycle does not exist in the whmcs it will not show up correct data!',
                'default' => 'monthly',
                'options' => [
                    'monthly'  => esc_html__( 'Monthly', 'wp-whmcs-lang' ),
                    'quarterly'  => esc_html__( 'Quarterly', 'wp-whmcs-lang' ),
                    'semiannually'  => esc_html__( 'Semiannually', 'wp-whmcs-lang' ),
                    'annually'  => esc_html__( 'Annually', 'wp-whmcs-lang' ),
                    'biennially'  => esc_html__( 'Biennially', 'wp-whmcs-lang' ),
                    'triennially'  => esc_html__( 'Triennially', 'wp-whmcs-lang' ),
                ],
                'condition' => [
                    'iq_wp_whmcs_auto_price' => '1',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_time_period',
            [
                'label' => __( 'Time Period', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'dynamic' => [
                    'active' => true,
                ],
                'label_block' => true,
                'default' => __( '/month', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_auto_description',
            [
                'label' => __( 'Get Description Directly', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'return_value' => '1',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_description',
            [
                'label' => __( 'Description', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXTAREA,
                'condition' => [
                    'iq_wp_whmcs_auto_description!' => '1',
                ],
                'dynamic' => [
                    'active' => true,
                ],
                'placeholder' => __( 'Enter Description', 'wp-whmcs-lang' )
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_active',
            [
                'label' => __( 'Is Active?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'label_off',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_has_price_shadow',
            [
                'label' => __( 'Use Box Shadow', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'label_off',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $repeater = new Repeater();
        $repeater->add_control(
            'iq_wp_whmcs_tab_title',
            [
                'label' => __( 'Plan info List', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => __( 'Service Item', 'wp-whmcs-lang' ),
                'placeholder' => __( 'Service Item', 'wp-whmcs-lang' ),
                'label_block' => true,
            ]
        );
        
        $repeater->add_control(
            'iq_wp_whmcs_has_service_active',
            [
                'label' => __( 'Active?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'label_off',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $repeater->add_control(
            'iq_wp_whmcs_has_service_icon',
            [
                'label' => __( 'Use Icon?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'label_off',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $repeater->add_control(
            'iq_wp_whmcs_service_icon',
            [
                'label' => __( 'Service Icon', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::ICONS,
                'fa4compatibility' => 'icon',
                'default' => [
                    'value' => 'fas fa-star'

                ],
                'condition' => [
                    'iq_wp_whmcs_has_service_icon' => 'yes',
                ],
                'label_block' => false,
                'skin' => 'inline',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_tabs',
            [
                'label' => __( 'List Items', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'tab_title' => __( 'Service Item', 'wp-whmcs-lang' )
                    ]

                ],
                'title_field' => '{{{ iq_wp_whmcs_tab_title }}}',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_title_tag',
            [
                'label'      => __( 'Price Tag', 'wp-whmcs-lang' ),
                'type'       => Controls_Manager::SELECT,
                'default'    => 'h4',
                'options'    => [
                    'h1'          => __( 'h1', 'wp-whmcs-lang' ),
                    'h2'          => __( 'h2', 'wp-whmcs-lang' ),
                    'h3'          => __( 'h3', 'wp-whmcs-lang' ),
                    'h4'          => __( 'h4', 'wp-whmcs-lang' ),
                    'h5'          => __( 'h5', 'wp-whmcs-lang' ),
                    'h6'          => __( 'h6', 'wp-whmcs-lang' )
                ],
            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_align',
            [
                'label' => __( 'Alignment', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::CHOOSE,
                'default' => 'text-center',
                'options' => [
                    'text-left' => [
                        'title' => __( 'Left', 'wp-whmcs-lang' ),
                        'icon' => 'eicon-text-align-left',
                    ],
                    'text-center' => [
                        'title' => __( 'Center', 'wp-whmcs-lang' ),
                        'icon' => 'eicon-text-align-center',
                    ],
                    'text-right' => [
                        'title' => __( 'Right', 'wp-whmcs-lang' ),
                        'icon' => 'eicon-text-align-right',
                    ]
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_button_text',
            [
                'label' => __( 'Button Text', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'dynamic' => [
                    'active' => true,
                ],
                'label_block' => true,
                'default' => __( 'Order Now', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_get_url_directly',
            [
                'label' => __( 'Get Url Directly', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_button_url',
            [
                'label' => __( 'Button URL', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::URL,
                'condition' => [
                    'iq_wp_whmcs_get_url_directly!' => 'yes'
                ],
                'dynamic' => [
                    'active' => true,
                ],
                'placeholder' => __( 'https://your-link.com', 'wp-whmcs-lang' ),
                'default' => [
                    'url' => 'http://yourdomain/whmcs-server',
                ],
            ]
        );

        $this->end_controls_section();

        /* Price Table Start*/

        $this->start_controls_section(
            'iq_wp_whmcs_section_fyWdp0abeSvr3Mi44LVm',
            [
                'label' => __( 'Price Table', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs( 'price_table_tabs' );

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_yL0o668ZXM4g9zsUSjCi',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_table_background',
                'label' => __( 'Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'iq_price_table_box_shadow',
                'label' => __( 'Box Shadow', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-container',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_section_oqfR22l3fbiF25GTU02H',
            [
                'label' => __( 'Before Background', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_table_before_background',
                'label' => __( 'Before Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-table-6::before',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'condition' => [
                    'iq_wp_whmcs_price_table_has_border' => 'yes',
                ],
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container ' => 'border-style: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_table_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container' => 'border-color: {{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_table_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_table_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_f1pW85xe0693t0fKBV1Y',
            [
                'label' => __( 'Active', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_table_hover_background',
                'label' => __( 'Hover Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container:hover ,{{WRAPPER}} .iq-price-container.active',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'iq_price_table_hover_box_shadow',
                'label' => __( 'Box Shadow', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-container:hover ,{{WRAPPER}} .iq-price-container.active',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_section_C075nlXNgZ03Afe5d0Is',
            [
                'label' => __( 'Before Hover Background', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_table_hover_before_background',
                'label' => __( 'Hover Before Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-table-6.active::before',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_hover_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_hover_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_table_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover ,{{WRAPPER}} .iq-price-container.active' => 'border-style: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_hover_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_table_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover,{{WRAPPER}} .iq-price-container.active' => 'border-color: {{VALUE}};',
                ],


            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_hover_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_table_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover,{{WRAPPER}} .iq-price-container.active' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_table_hover_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_table_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover,{{WRAPPER}} .iq-price-container.active' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_table_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_table_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();

        /* Price table End*/

        /* Price Header Start*/

        $this->start_controls_section(
            'iq_wp_whmcs_section_K570f6fbXo53dcT4m2eN',
            [
                'label' => __( 'Price Header', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /*currency Start*/
        $this->add_control(
            'iq_wp_whmcs_section_mX28erE332RbyLve03nU',
            [
                'label' => __( 'Currency', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_currency_text_typography',
                'label' => __( 'Currency Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}}  .iq-price-header .iq-price small:first-child	',
            ]
        );

        $this->start_controls_tabs( 'iq_wp_whmcs_price_currency_tabs' );

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_Lfkb48DXYs0eBP93241E',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_currency_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-header .iq-price small:first-child' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_fmjB3Off9KF4v53WZ8Xh',
            [
                'label' => __( 'Hover', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_currency_hover_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header .iq-price small:first-child,
					 {{WRAPPER}} .iq-price-container.active .iq-price-header .iq-price small:first-child' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_currency_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price small:first-child' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_currency_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price small:first-child' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );
        /*Currency End*/

        /*Price Start*/
        $this->add_control(
            'iq_wp_whmcs_section_YxSwfrRT69442fI6mbCp',
            [
                'label' => __( 'Price', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_text_typography',
                'label' => __( 'Price Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-header .iq-price',
            ]
        );

        $this->start_controls_tabs( 'iq_wp_whmcs_price_text_tabs' );

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_FqDe9pYKt6UXJnGh82d2',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_text_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-header .iq-price' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_B2b426p9P3hRJ8sxrUTO',
            [
                'label' => __( 'Hover', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_text_hover_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header .iq-price,
					 {{WRAPPER}} .iq-price-container.active .iq-price-header .iq-price' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_text_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_text_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        /*Price End*/

        /*Duration Start*/

        $this->add_control(
            'iq_wp_whmcs_section_60rHB44IC3AdVSnFPgj7',
            [
                'label' => __( 'Duration', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_duration_text_typography',
                'label' => __( 'Duration Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-header .iq-price-desc,{{WRAPPER}}  .iq-price-header .iq-price small ',
            ]
        );

        $this->start_controls_tabs( 'iq_wp_whmcs_price_duration_tabs' );

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_c5Qbq8WLv6d5307l0Uu3',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_duration_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-header .iq-price-desc,{{WRAPPER}}  .iq-price-header .iq-price small ' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_3A639G6mPd309Rl889Yy',
            [
                'label' => __( 'Hover', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_duration_hover_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header .iq-price-desc,
					 {{WRAPPER}} .iq-price-container:hover  .iq-price-header .iq-price small,
					 {{WRAPPER}} .iq-price-container.active .iq-price-header .iq-price-desc,
					 {{WRAPPER}} .iq-price-container.active  .iq-price-header .iq-price small ' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_duration_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price-desc,{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price small' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_duration_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price-desc,{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price small' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );
        /*Duration End*/

        /*Title Start*/

        $this->add_control(
            'iq_wp_whmcs_section_TnSdjH5Zb7wEm8sxQVuG',
            [
                'label' => __( 'Title', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_title_text_typography',
                'label' => __( 'Title Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-header .iq-price-label',
            ]
        );

        $this->start_controls_tabs( 'iq_wp_whmcs_price_title_tabs' );

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_09OPJST046UeIAYb1Kba',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-header .iq-price-label' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_title_background',
                'label' => __( 'Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container .iq-price-header .iq-price-label',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_has_border' => 'yes',
                ],
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header .iq-price-label' => 'border-style: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header .iq-price-label' => 'border-color: {{VALUE}};',
                    '{{WRAPPER}} .iq-price-container .iq-price-header .iq-price-label:before' => 'border-top-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header .iq-price-label' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header .iq-price-label' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_cT0eg16qd070xv7muc2b',
            [
                'label' => __( 'Hover', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_hover_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-header .iq-price-label,
					 {{WRAPPER}}  .iq-price-container.active .iq-price-header .iq-price-label' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_title_hover_background',
                'label' => __( 'Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}}  .iq-price-container:hover .iq-price-header .iq-price-label,
					 {{WRAPPER}}  .iq-price-container.active .iq-price-header .iq-price-label',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_hover_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_hover_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-header .iq-price-label,
					 {{WRAPPER}}  .iq-price-container.active .iq-price-header .iq-price-label' => 'border-style: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_hover_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-header .iq-price-label,
					 {{WRAPPER}}  .iq-price-container.active .iq-price-header .iq-price-label,'=> 'border-color: {{VALUE}};',
                    ' {{WRAPPER}}  .iq-price-container:hover .iq-price-header .iq-price-label:before,
					 {{WRAPPER}}  .iq-price-container.active .iq-price-header .iq-price-label:before' => 'border-top-color: {{VALUE}};',
                ],


            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_hover_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-header .iq-price-label,
					 {{WRAPPER}}  .iq-price-container.active .iq-price-header .iq-price-label' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_title_hover_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_title_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-header .iq-price-label,
					 {{WRAPPER}}  .iq-price-container.active .iq-price-header .iq-price-label' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_title_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price-label' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_title_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price-label' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        /*Title End*/

        /*Sub Title Start*/

        $this->add_control(
            'iq_wp_whmcs_section_cxKaO95c5CZW4Nz3w36M',
            [
                'label' => __( 'Subtitle', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_subtitle_text_typography',
                'label' => __( 'Subtitle Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-header .iq-price-description ',
            ]
        );

        $this->start_controls_tabs( 'price_subtitle_tabs' );

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_Fu3Wm8nztYqbbkbpSc6J',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_subtitle_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-header .iq-price-description ' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_rXQ60813pl6Rn3B1Sx9d',
            [
                'label' => __( 'Hover', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_subtitle_hover_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header .iq-price-description ,{{WRAPPER}} .iq-price-container.active .iq-price-header .iq-price-description ' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_subtitle_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price-description' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_subtitle_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header .iq-price-description' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        /*Sub Title End*/

        /*Header Backgrund Start*/

        $this->add_control(
            'iq_wp_whmcs_section_O75inm3q9CVk7962gh96',
            [
                'label' => __( 'Pricing Header Background', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->start_controls_tabs( 'iq_wp_whmcs_price_header_tabs' );

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_5D6V6NS96w3QI6bdMU23',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_header_background',
                'label' => __( 'Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container .iq-price-header',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT,
                'condition' => [
                    'iq_wp_whmcs_price_header_has_border' => 'yes',
                ],
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header' => 'border-style: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_header_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header' => 'border-color: {{VALUE}};',
                ],


            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_header_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_header_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-header' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_rH655IP80scfKQz9Wnq6',
            [
                'label' => __( 'Active', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_header_hover_background',
                'label' => __( 'Hover Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container:hover .iq-price-header,{{WRAPPER}} .iq-price-container.active .iq-price-header',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_hover_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_hover_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT,
                'condition' => [
                    'iq_wp_whmcs_price_header_hover_has_border' => 'yes',
                ],
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' ),

                ],

                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header,{{WRAPPER}} .iq-price-container.active .iq-price-header' => 'border-style: {{VALUE}};',

                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_hover_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'condition' => [
                    'iq_wp_whmcs_price_header_hover_has_border' => 'yes',
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header,{{WRAPPER}} .iq-price-container.active .iq-price-header' => 'border-color: {{VALUE}};',
                ],


            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_hover_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_header_hover_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header,{{WRAPPER}} .iq-price-container.active .iq-price-header' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_header_hover_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_header_hover_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-header,{{WRAPPER}} .iq-price-container.active .iq-price-header' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_header_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_header_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-header' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );
        $this->end_controls_section();

        /* Price Header End*/

        /* Price Body Start*/

        $this->start_controls_section(
            'iq_wp_whmcs_section_Odd5qY6t9Lnb095371uS',
            [
                'label' => __( 'Price Body', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_body_text_typography',
                'label' => __( 'Body Content Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-container',
            ]
        );

        $this->start_controls_tabs( 'price_body_tabs' );
        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_K6SG9lsU9Ibb5BZHQNdf',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_price_body_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body' => 'color: {{VALUE}};',
                    '{{WRAPPER}}  .iq-price-container .iq-price-body span' => 'color: {{VALUE}};',
                ],

            ]

        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_body_background',
                'label' => __( 'Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container .iq-price-body ul',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_border_style',
            [
                'label' => __( 'Body Border Style', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT,
                'condition' => [
                    'iq_wp_whmcs_price_body_has_border' => 'yes',
                ],
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul' => 'border-style: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_border_color',
            [
                'label' => __( 'Body Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_body_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul' => 'border-color: {{VALUE}};',
                ],


            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_border_width',
            [
                'label' => __( 'Body Border Width', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_body_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_border_radius',
            [
                'label' => __( 'Body Border Radius', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_body_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_37679POVcpZ0xdBrH236',
            [
                'label' => __( 'Active', 'wp-whmcs-lang' ),
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_price_body_hover_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-body, {{WRAPPER}}  .iq-price-container.active .iq-price-body' => 'color: {{VALUE}};',
                ]
            ]
        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_body_hover_background',
                'label' => __( 'Hover Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul,{{WRAPPER}} .iq-price-container.active .iq-price-body ul',
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_price_body_hover_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_price_body_hover_border_style',
            [
                'label' => __( 'Body Border Style', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_body_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul,{{WRAPPER}} .iq-price-container.active .iq-price-body ul' => 'border-style: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_hover_border_color',
            [
                'label' => __( 'Body Border Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'condition' => [
                    'iq_wp_whmcs_price_body_hover_has_border' => 'yes',
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul,{{WRAPPER}} .iq-price-container.active .iq-price-body ul' => 'border-color: {{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_hover_border_width',
            [
                'label' => __( 'Body Border Width', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'condition' => [
                    'iq_wp_whmcs_price_body_hover_has_border' => 'yes',
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul,{{WRAPPER}} .iq-price-container.active .iq-price-body ul' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_body_hover_border_radius',
            [
                'label' => __( 'Body Border Radius', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'condition' => [
                    'iq_wp_whmcs_price_body_hover_has_border' => 'yes',
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul,{{WRAPPER}} .iq-price-container.active .iq-price-body ul' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_price_body_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_body_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        /*Body End*/

        /*Pricing List Start*/

        $this->add_control(
            'iq_wp_whmcs_section_95xiZHF76mKJ1bE9dS6u',
            [
                'label' => __( 'Pricing list', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );


        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_pricing_list_text_typography',
                'label' => __( 'Pricing List Content Typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-price-container .iq-price-body ul li',
            ]
        );

        $this->start_controls_tabs( 'iq_wp_whmcs_pricing_list_tabs' );
        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_jwryR96oG27O78J6HT8b',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_pricing_list_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul li' => 'color: {{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_active_list_color',
            [
                'label' => __( 'Active Icon Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul li.active i' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_inactive_list_color',
            [
                'label' => __( 'Inactive Icon Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul li.inactive i' => 'color: {{VALUE}};',
                ]
            ]
        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_pricing_list_background',
                'label' => __( 'Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container .iq-price-body ul li',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_border_style',
            [
                'label' => __( 'Pricing List Border Style', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_pricing_list_has_border' => 'yes',
                ],
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' ),
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul li' => 'border-style: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_border_color',
            [
                'label' => __( 'Pricing List Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_pricing_list_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul li' => 'border-color: {{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_border_width',
            [
                'label' => __( 'Pricing List Border Width', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_pricing_list_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul li' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_border_radius',
            [
                'label' => __( 'Pricing List Border Radius', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_pricing_list_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-body ul li' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_85j6g91EM579Gcdx6T6b',
            [
                'label' => __( 'Active', 'wp-whmcs-lang' ),
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_pricing_list_hover_color',
            [
                'label' => __( 'Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul li:hover, {{WRAPPER}}  .iq-price-container .iq-price-body ul li.active' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->add_control(
            'iq_wp_whmcs_pricing_active_list_hover_color',
            [
                'label' => __( 'Active Icon Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-body ul li.active i,{{WRAPPER}}  .iq-price-container.active .iq-price-body ul li.active i' => 'color: {{VALUE}};',
                ],

            ]

        );

        $this->add_control(
            'iq_wp_whmcs_pricing_inactive_list_hover_color',
            [
                'label' => __( 'Inactive Icon Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container:hover .iq-price-body ul li.inactive i,{{WRAPPER}}  .iq-price-container.active .iq-price-body ul li.inactive i' => 'color: {{VALUE}};',
                ],

            ]

        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_pricing_list_hover_background',
                'label' => __( 'Hover Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul li,{{WRAPPER}} .iq-price-container.active .iq-price-body ul li',
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_pricing_list_hover_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_pricing_list_hover_border_style',
            [
                'label' => __( 'Pricing List Border Style', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT,
                'condition' => [
                    'iq_wp_whmcs_pricing_list_hover_has_border' => 'yes',
                ],
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' )
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul li,{{WRAPPER}} .iq-price-container.active .iq-price-body ul li' => 'border-style: {{VALUE}};'
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_hover_border_color',
            [
                'label' => __( 'Pricing List Border Color', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_pricing_list_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul li,{{WRAPPER}} .iq-price-container.active .iq-price-body ul li' => 'border-color: {{VALUE}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_hover_border_width',
            [
                'label' => __( 'Pricing List Border Width', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_pricing_list_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul li,{{WRAPPER}} .iq-price-container.active .iq-price-body ul li' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_pricing_list_hover_border_radius',
            [
                'label' => __( 'Pricing List Border Radius', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_pricing_list_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-body ul li,{{WRAPPER}} .iq-price-container.active .iq-price-body ul li' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_responsive_control(
            'iq_wp_whmcs_pricing_list_padding',
            [
                'label' => __( 'Pricing List Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul li ' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_pricing_list_margin',
            [
                'label' => __( 'Pricing List Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-body ul li ' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

        /* Price Body End*/

        /* Price footer Start End*/

        $this->start_controls_section(
            'iq_wp_whmcs_section_fx9Nm3Z6LD6P50r9TFY8',
            [
                'label' => __( 'Price Footer', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs( 'iq_wp_whmcs_price_footer_tabs' );
        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_7SsC2Xby38hDba3850rm',
            [
                'label' => __( 'Normal', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_footer_background',
                'label' => __( 'Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container .iq-price-footer',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SELECT,
                'condition' => [
                    'iq_wp_whmcs_price_footer_has_border' => 'yes',
                ],
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' ),

                ],

                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-footer' => 'border-style: {{VALUE}};',

                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'condition' => [
                    'iq_wp_whmcs_price_footer_has_border' => 'yes',
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-footer' => 'border-color: {{VALUE}};',
                ],


            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_footer_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-footer' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_footer_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container .iq-price-footer' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            'iq_wp_whmcs_tabs_z67bhXB90O5P0cibF3U1',
            [
                'label' => __( 'Active', 'wp-whmcs-lang' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_wp_whmcs_price_footer_hover_background',
                'label' => __( 'Hover Background', 'wp-whmcs-lang' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-price-container:hover .iq-price-footer,{{WRAPPER}} .iq-price-container.active .iq-price-footer',
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_price_footer_hover_has_border',
            [
                'label' => __( 'Set Custom Border?', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'yes' => __( 'yes', 'wp-whmcs-lang' ),
                'no' => __( 'no', 'wp-whmcs-lang' ),
            ]
        );
        $this->add_control(
            'iq_wp_whmcs_price_footer_hover_border_style',
            [
                'label' => __( 'Border Style', 'wp-whmcs-lang' ),
                'condition' => [
                    'iq_wp_whmcs_price_footer_hover_has_border' => 'yes',
                ],
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'options' => [
                    'solid'  => __( 'Solid', 'wp-whmcs-lang' ),
                    'dashed' => __( 'Dashed', 'wp-whmcs-lang' ),
                    'dotted' => __( 'Dotted', 'wp-whmcs-lang' ),
                    'double' => __( 'Double', 'wp-whmcs-lang' ),
                    'outset' => __( 'outset', 'wp-whmcs-lang' ),
                    'groove' => __( 'groove', 'wp-whmcs-lang' ),
                    'ridge' => __( 'ridge', 'wp-whmcs-lang' ),
                    'inset' => __( 'inset', 'wp-whmcs-lang' ),
                    'hidden' => __( 'hidden', 'wp-whmcs-lang' ),
                    'none' => __( 'none', 'wp-whmcs-lang' ),

                ],

                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-footer,{{WRAPPER}} .iq-price-container.active .iq-price-footer' => 'border-style: {{VALUE}};',

                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_hover_border_color',
            [
                'label' => __( 'Border Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'condition' => [
                    'iq_wp_whmcs_price_footer_hover_has_border' => 'yes',
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-footer,{{WRAPPER}} .iq-price-container.active .iq-price-footer' => 'border-color: {{VALUE}};',
                ],


            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_hover_border_width',
            [
                'label' => __( 'Border Width', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_footer_hover_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-footer,{{WRAPPER}} .iq-price-container.active .iq-price-footer' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'iq_wp_whmcs_price_footer_hover_border_radius',
            [
                'label' => __( 'Border Radius', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'condition' => [
                    'iq_wp_whmcs_price_footer_hover_has_border' => 'yes',
                ],
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} .iq-price-container:hover .iq-price-footer,{{WRAPPER}} .iq-price-container.active .iq-price-footer' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();



        $this->add_responsive_control(
            'iq_wp_whmcs_price_footer_padding',
            [
                'label' => __( 'Padding', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-footer' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'iq_wp_whmcs_price_footer_margin',
            [
                'label' => __( 'Margin', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}}  .iq-price-container .iq-price-footer' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],

            ]
        );


        $this->end_controls_section();

        /* Price Footer End*/

    }

    protected function render() {
        $settings = $this->get_settings();
        require WP_WHMCS_ROOT . '/elementor/whmcs/render/pricing_table_section.php';
    }

    public function get_currency_symbol() {
        return $currency_symbols = [
            'none' => '', // ?
            'AED' => '&#1583;.&#1573;', // ?
            'AFN' => '&#65;&#102;',
            'ALL' => '&#76;&#101;&#107;',
            'ANG' => '&#402;',
            'AOA' => '&#75;&#122;', // ?
            'ARS' => '&#36;',
            'AUD' => '&#36;',
            'AWG' => '&#402;',
            'AZN' => '&#1084;&#1072;&#1085;',
            'BAM' => '&#75;&#77;',
            'BBD' => '&#36;',
            'BDT' => '&#2547;', // ?
            'BGN' => '&#1083;&#1074;',
            'BHD' => '.&#1583;.&#1576;', // ?
            'BIF' => '&#70;&#66;&#117;', // ?
            'BMD' => '&#36;',
            'BND' => '&#36;',
            'BOB' => '&#36;&#98;',
            'BRL' => '&#82;&#36;',
            'BSD' => '&#36;',
            'BTN' => '&#78;&#117;&#46;', // ?
            'BWP' => '&#80;',
            'BYR' => '&#112;&#46;',
            'BZD' => '&#66;&#90;&#36;',
            'CAD' => '&#36;',
            'CDF' => '&#70;&#67;',
            'CHF' => '&#67;&#72;&#70;',

            'CLP' => '&#36;',
            'CNY' => '&#165;',
            'COP' => '&#36;',
            'CRC' => '&#8353;',
            'CUP' => '&#8396;',
            'CVE' => '&#36;', // ?
            'CZK' => '&#75;&#269;',
            'DJF' => '&#70;&#100;&#106;', // ?
            'DKK' => '&#107;&#114;',
            'DOP' => '&#82;&#68;&#36;',
            'DZD' => '&#1583;&#1580;', // ?
            'EGP' => '&#163;',
            'ETB' => '&#66;&#114;',
            'EUR' => '&#8364;',
            'FJD' => '&#36;',
            'FKP' => '&#163;',
            'GBP' => '&#163;',
            'GEL' => '&#4314;', // ?
            'GHS' => '&#162;',
            'GIP' => '&#163;',
            'GMD' => '&#68;', // ?
            'GNF' => '&#70;&#71;', // ?
            'GTQ' => '&#81;',
            'GYD' => '&#36;',
            'HKD' => '&#36;',
            'HNL' => '&#76;',
            'HRK' => '&#107;&#110;',
            'HTG' => '&#71;', // ?
            'HUF' => '&#70;&#116;',
            'IDR' => '&#82;&#112;',
            'ILS' => '&#8362;',
            'INR' => '&#8377;',
            'IQD' => '&#1593;.&#1583;', // ?
            'IRR' => '&#65020;',
            'ISK' => '&#107;&#114;',
            'JEP' => '&#163;',
            'JMD' => '&#74;&#36;',
            'JOD' => '&#74;&#68;', // ?
            'JPY' => '&#165;',
            'KES' => '&#75;&#83;&#104;', // ?
            'KGS' => '&#1083;&#1074;',
            'KHR' => '&#6107;',
            'KMF' => '&#67;&#70;', // ?
            'KPW' => '&#8361;',
            'KRW' => '&#8361;',
            'KWD' => '&#1583;.&#1603;', // ?
            'KYD' => '&#36;',
            'KZT' => '&#1083;&#1074;',
            'LAK' => '&#8365;',
            'LBP' => '&#163;',
            'LKR' => '&#8360;',
            'LRD' => '&#36;',
            'LSL' => '&#76;', // ?
            'LTL' => '&#76;&#116;',
            'LVL' => '&#76;&#115;',
            'LYD' => '&#1604;.&#1583;', // ?
            'MAD' => '&#1583;.&#1605;.', //?
            'MDL' => '&#76;',
            'MGA' => '&#65;&#114;', // ?
            'MKD' => '&#1076;&#1077;&#1085;',
            'MMK' => '&#75;',
            'MNT' => '&#8366;',
            'MOP' => '&#77;&#79;&#80;&#36;', // ?
            'MRO' => '&#85;&#77;', // ?
            'MUR' => '&#8360;', // ?
            'MVR' => '.&#1923;', // ?
            'MWK' => '&#77;&#75;',
            'MXN' => '&#36;',
            'MYR' => '&#82;&#77;',
            'MZN' => '&#77;&#84;',
            'NAD' => '&#36;',
            'NGN' => '&#8358;',
            'NIO' => '&#67;&#36;',
            'NOK' => '&#107;&#114;',
            'NPR' => '&#8360;',
            'NZD' => '&#36;',
            'OMR' => '&#65020;',
            'PAB' => '&#66;&#47;&#46;',
            'PEN' => '&#83;&#47;&#46;',
            'PGK' => '&#75;', // ?
            'PHP' => '&#8369;',
            'PKR' => '&#8360;',
            'PLN' => '&#122;&#322;',
            'PYG' => '&#71;&#115;',
            'QAR' => '&#65020;',
            'RON' => '&#108;&#101;&#105;',
            'RSD' => '&#1044;&#1080;&#1085;&#46;',
            'RUB' => '&#1088;&#1091;&#1073;',
            'RWF' => '&#1585;.&#1587;',
            'SAR' => '&#65020;',
            'SBD' => '&#36;',
            'SCR' => '&#8360;',
            'SDG' => '&#163;', // ?
            'SEK' => '&#107;&#114;',
            'SGD' => '&#36;',
            'SHP' => '&#163;',
            'SLL' => '&#76;&#101;', // ?
            'SOS' => '&#83;',
            'SRD' => '&#36;',
            'STD' => '&#68;&#98;', // ?
            'SVC' => '&#36;',
            'SYP' => '&#163;',
            'SZL' => '&#76;', // ?
            'THB' => '&#3647;',
            'TJS' => '&#84;&#74;&#83;', // ? TJS (guess)
            'TMT' => '&#109;',
            'TND' => '&#1583;.&#1578;',
            'TOP' => '&#84;&#36;',
            'TRY' => '&#8356;', // New Turkey Lira (old symbol used)
            'TTD' => '&#36;',
            'TWD' => '&#78;&#84;&#36;',
            'UAH' => '&#8372;',
            'UGX' => '&#85;&#83;&#104;',
            'USD' => '&#36;',
            'UYU' => '&#36;&#85;',
            'UZS' => '&#1083;&#1074;',
            'VEF' => '&#66;&#115;',
            'VND' => '&#8363;',
            'VUV' => '&#86;&#84;',
            'WST' => '&#87;&#83;&#36;',
            'XAF' => '&#70;&#67;&#70;&#65;',
            'XCD' => '&#36;',
            'XPF' => '&#70;',
            'YER' => '&#65020;',
            'ZAR' => '&#82;',
            'ZMK' => '&#90;&#75;', // ?
            'ZWL' => '&#90;&#36;',
        ];
    }
}

Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Pricing_Table_Section());