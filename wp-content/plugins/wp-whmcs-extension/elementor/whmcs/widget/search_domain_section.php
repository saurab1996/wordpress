<?php

namespace Elementor;
if (!defined('ABSPATH')) exit;

/**
 * @method add_control(string $string, array $array)
 */
class Search_Domain_Section extends Widget_Base
{

    private $version;

    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
    }

    /**
     * Get widget name.
     *
     * Retrieve heading widget name.
     *
     * @return string Widget name.
     * @since 1.2.0
     * @access public
     *
     */

    public function get_name()
    {
        return 'search_domain_section';
    }

    /**
     * Get widget Title.
     *
     * Retrieve heading widget Title.
     *
     * @return string Widget Title.
     * @since 1.2.0
     * @access public
     *
     */

    public function get_title()
    {
        return 'Search Domain';
    }

    public function get_script_depends() {
        return [ 'domain-search-ajax'];
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the heading widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * @return array Widget categories.
     * @since 1.2.0
     * @access public
     *
     */


    public function get_categories()
    {
        return ['iq-wp-whmcs-extension'];
    }


    /**
     * Get widget icon.
     *
     * Retrieve heading widget icon.
     *
     * @return string Widget icon.
     * @since 1.2.0
     * @access public
     *
     */

    public function get_icon()
    {
        return 'fas fa-search';
    }

    protected function _register_controls()
    {

        /*-----------------Widget Content Tab Start--------------------*/

        $this->start_controls_section(
            'iq_search_domain_wp_whmcs_section',
            [
                'label' => esc_html__('Search Domain', 'wp-whmcs-lang')
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_url',
            [
                'label' => __( 'URL', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'http://yourdomain/whmcs-server',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_bar_placeholder',
            [
                'label' => __( 'Search Bar Place Holder', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Search Your Domain Name',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_search_button_text',
            [
                'label' => __( 'Search Button Text', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Search',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_live_ajax_search',
            [
                'label' => __( 'Live Ajax Search', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::SWITCHER,
                'return_value' => '1',
            ]
        );

        $this->end_controls_section();

        /*-----------------Widget Content Tab End--------------------*/

        /*-----------------Search Button Style Tab Start--------------------*/

        $this->start_controls_section(
            'iq_wp_whmcs_search_btn',
            [
                'label' => __( 'Search Button', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_head_section_text_typography',
                'label' => __( 'Text typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-domain-search input[type="submit"]',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_head_section_text_color',
            [
                'label' => __( 'Text Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-domain-search input[type="submit"]' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_head_section_bgcolor',
            [
                'label' => __( 'Background Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-domain-search .iq-wp-whmcs-form-input-box input[type="submit"]' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        /*-----------------Search Button Style Tab End--------------------*/

        /*-----------------Search Input Field Tab Start--------------------*/

        $this->start_controls_section(
            'iq_wp_whmcs_search_input',
            [
                'label' => __( 'Search input field', 'wp-whmcs-lang' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'iq_wp_whmcs_input_text_typography',
                'label' => __( 'Text typography', 'wp-whmcs-lang' ),
                'selector' => '{{WRAPPER}} .iq-wp-whmcs-form-input-box input[type="text"]',
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_input_text_color',
            [
                'label' => __( 'Text Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-wp-whmcs-form-input-box input[type="text"]' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'iq_wp_whmcs_input_bgcolor',
            [
                'label' => __( 'Background Color', 'wp-whmcs-lang' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .iq-wp-whmcs-form-input-box input[type="text"]' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        /*-----------------Search Input Field Tab End--------------------*/


    }

    protected function render() {
        $settings = $this->get_settings();
        require WP_WHMCS_ROOT . '/elementor/whmcs/render/search_domain_section.php';
    }
}

Plugin::instance()->widgets_manager->register_widget_type(new \Elementor\Search_Domain_Section());