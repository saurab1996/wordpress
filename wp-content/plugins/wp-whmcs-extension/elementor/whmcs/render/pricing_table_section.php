<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) exit;

$html = '';

//$this->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );
$settings = $this->get_settings();
$tabs = $this->get_settings_for_display( 'iq_wp_whmcs_tabs' );
$align = $settings['iq_wp_whmcs_align'];
$this->add_render_attribute( 'iq_container', 'class', 'iq-btn-container' );

if($settings['iq_wp_whmcs_has_price_shadow'] === 'yes') {
    $this->add_render_attribute( 'iq_price_class', 'class', 'iq-price-shadow') ;
    $align .= ' iq-box-shadow';
}

$active = $settings['iq_wp_whmcs_active'];
$html .= esc_html($settings['iq_wp_whmcs_button_text']);

if ($settings['iq_wp_whmcs_get_url_directly'] !== 'yes') {
    $url = $settings['iq_wp_whmcs_button_url']['url'];
    $this->add_render_attribute( 'iq_class', 'href', esc_url($url) );

    if($settings['iq_wp_whmcs_button_url']['is_external']) {
        $this->add_render_attribute( 'iq_class', 'target', '_blank' );
    }

    if($settings['iq_wp_whmcs_button_url']['nofollow']) {
        $this->add_render_attribute( 'iq_class', 'rel', 'nofollow' );
    }

} else {

    $url = $settings['iq_wp_whmcs_url'].'/cart.php?a=add&pid='.$settings['iq_wp_whmcs_package_id'];
    $this->add_render_attribute( 'iq_class', 'href', esc_url($url) );
}

$this->add_render_attribute( 'iq_class', 'class', 'iq-button' );

$url = '';

if($active === "yes") {
    $align .= ' active';
}

//price header render attribute
$this->add_render_attribute( 'iq-price-header', 'class', 'iq-price-header' );
if(!empty($settings['image']['url'])) {
    $this->add_render_attribute( 'iq-price-header', 'style', 'background:url('.$settings['image']['url'].')' );
}

if($settings['iq_wp_whmcs_has_price_shadow'] == 'yes') {
    $this->add_render_attribute( 'iq_price_class', 'class', 'iq-price-shadow') ;
    $align .= ' iq-box-shadow';
} ?>

<div  class="iq-price-container iq-price-table-6 <?php echo esc_attr($align); ?>">

    <div <?php echo $this->get_render_attribute_string('iq-price-header'); ?>>

    <!--    Title Start   -->
        <span class="iq-price-label">
            <?php if( $settings['iq_wp_whmcs_auto_title'] && $settings['iq_wp_whmcs_url'] !== '' && $settings['iq_wp_whmcs_package_id'] && !\Elementor\Plugin::$instance->editor->is_edit_mode() ) {
                echo '<script language="javascript" src="'.$settings['iq_wp_whmcs_url'].'/feeds/productsinfo.php?pid='. $settings['iq_wp_whmcs_package_id'] .'&get=name"></script>';
            } else {
                echo $settings['iq_wp_whmcs_title'];
            } ?>
        </span>
    <!--   Title End     -->

        <?php
            $ex = explode(' ',$settings['iq_wp_whmcs_currency_symbol']);
        ?>
        <<?php echo $settings['iq_wp_whmcs_title_tag']; ?> class="iq-price">

    <!--    Price Show    -->
        <?php if( $settings['iq_wp_whmcs_auto_price'] && $settings['iq_wp_whmcs_package_id'] && !\Elementor\Plugin::$instance->editor->is_edit_mode() ) {
            echo '<script language="javascript" src="'.$settings['iq_wp_whmcs_url'].'/feeds/productsinfo.php?pid='. $settings['iq_wp_whmcs_package_id'] .'&get=price&billingcycle='.$settings['iq_wp_whmcs_billing_cycle'].' "></script>';
        } else {
            echo $ex[0].' ';
            echo $settings['iq_wp_whmcs_price'];
        } ?>
    <!--     End    -->

    <!--   Time Period Start     -->
        <span class="iq-price-desc"><?php
            echo esc_html($settings['iq_wp_whmcs_time_period']); ?>
        </span>
    <!--   Time Period End   -->

    </<?php echo $settings['iq_wp_whmcs_title_tag']; ?>>

    <!--   Description Start   -->
    <?php
        if ($settings['iq_wp_whmcs_auto_description'] && $settings['iq_wp_whmcs_package_id'] && !\Elementor\Plugin::$instance->editor->is_edit_mode()) {
            echo '<p class="iq-price-description">';
            echo '<script language="javascript" src="' . $settings['iq_wp_whmcs_url'] . '/feeds/productsinfo.php?pid='. $settings['iq_wp_whmcs_package_id'] .'&get=description"></script>';
            echo '</p>';
        } else {
            if(!empty($settings['iq_wp_whmcs_description'])) {
                echo '<p class="iq-price-description">'.esc_html($settings['iq_wp_whmcs_description']).'</p>';
            }
        }
    ?>
    <!--  Description End  -->
</div>

<div class="iq-price-body">
    <ul class="iq-price-service"> <?php
        foreach ( $tabs as $index => $item ) {
            if($item['iq_wp_whmcs_has_service_active'] === 'yes') {
                $class = 'active';
            } else {
                $class='inactive';
            } ?>
            <?php
        } ?>
    </ul>
</div>

<div class="iq-price-footer">
    <div <?php echo $this->get_render_attribute_string( 'iq_container' ) ?> > <?php
        if(!empty($settings['iq_wp_whmcs_button_text'])) { ?>
            <a <?php echo $this->get_render_attribute_string( 'iq_class' ) ?> >   <?php
                echo $html; ?>
            </a> <?php
        } ?>    
    </div>
</div>

</div>
