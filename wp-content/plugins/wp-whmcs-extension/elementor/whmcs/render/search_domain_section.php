<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) exit;

$settings = $this->get_settings();
$message = '';
?>
<div class="iq-domain-search">
<?php if($settings['iq_wp_whmcs_live_ajax_search']) : ?>
    <div class="iq-wp-whmcs-form-area">
        <div class="iq-wp-whmcs-form-input-box">
            <form method="post" action="<?php echo esc_url($settings['iq_wp_whmcs_url'].'/domainchecker.php');  ?>" >
                <input class="wpWhmcsSearchDomainName" type="text" autocomplete="off" required name="domain" placeholder="<?php echo $settings['iq_wp_whmcs_search_bar_placeholder']; ?>">
                <input id="find" type="submit" disabled value="<?php echo $settings['iq_wp_whmcs_search_button_text']; ?>">
            </form>
        </div>
        <div id="iq-domain-search-result" class="iq-wp-whmcs-domain-result"></div>
    </div>

<?php else: ?>
    <div class="iq-wp-whmcs-form-area">
        <form method="post" action="<?php echo esc_url($settings['iq_wp_whmcs_url'].'/domainchecker.php'); ?>">
            <div class="iq-wp-whmcs-form-input-box">
                <input type="text" autocomplete="off" <?php echo $message; ?> required name="domain" placeholder="<?php echo $settings['iq_wp_whmcs_search_bar_placeholder']; ?>">
                <input type="submit" value="<?php echo $settings['iq_wp_whmcs_search_button_text']; ?>">
            </div>
        </form>
    </div>
<?php endif; ?>
</div>
