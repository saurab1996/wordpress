<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 
$settings = $this->get_settings_for_display();
$settings = $this->get_settings();

$style = $settings['iq_wp_whmcs_style_switch'];

if($style == 'no') {
    
    $domain_list = $this->get_settings_for_display( 'iq_domain_list' );
    $align = $settings['align']; ?>

    <div class="iq-domain-table iq-wp-whmcs-domain <?php echo esc_attr($align); ?> table-responsive">
        <table class="iq-table">
            <thead>
                <tr> <?php

                    if(!empty($settings['iq_wp_whmcs_heading_one'])) { ?>
                        <th class="iq-heading"><?php echo esc_html($settings['iq_wp_whmcs_heading_one'], 'wp-whmcs-lang'); ?></th> <?php
                    }

                    if(!empty($settings['iq_wp_whmcs_heading_two'])) { ?>
                        <th class="iq-heading"><?php echo esc_html($settings['iq_wp_whmcs_heading_two'], 'wp-whmcs-lang'); ?></th> <?php
                    }

                    if(!empty($settings['iq_wp_whmcs_heading_three'])) { ?>
                        <th class="iq-heading"><?php echo esc_html($settings['iq_wp_whmcs_heading_three'], 'wp-whmcs-lang'); ?></th> <?php
                    }
                    
                    if(!empty($settings['iq_wp_whmcs_heading_four'])) { ?>
                        <th class="iq-heading"><?php echo esc_html($settings['iq_wp_whmcs_heading_four'], 'wp-whmcs-lang'); ?></th> <?php
                    } ?>
               
                </tr>
            </thead>
            <tbody> <?php
                foreach ( $domain_list as $index => $item ){
                    
                    if(!empty($item['iq_wp_whmcs_name_txt_color'])) {
                        $this->add_render_attribute( 'domain-text', 'style', 'color:'.$item['iq_wp_whmcs_name_txt_color'].'; ' );
                    } else {
                        $this->add_render_attribute( 'domain-text', 'style', 'color:#000; ' );
                    } ?>

                    <tr> <?php
                        if($item['iq_wp_whmcs_nameas_switch'] == 'text' && isset($item['iq_wp_whmcs_name']) && !empty($item['iq_wp_whmcs_name'])){ ?>

                            <td class="iq-row" <?php echo $this->get_render_attribute_string('domain-text'); ?>><?php 
                                echo esc_html($item['iq_wp_whmcs_name'],'wp-whmcs-lang'); ?> 
                            </td> <?php

                        } elseif(isset($item['iq_wp_whmcs_nameas_switch']) && $item['iq_wp_whmcs_nameas_switch'] == 'image') {

                            if ( ! empty( $item['iq_wp_whmcs_image']['url'] ) ) {

                                $this->add_render_attribute( 'iq_wp_whmcs_image', 'src', $item['iq_wp_whmcs_image']['url'] );
                                $this->add_render_attribute( 'iq_wp_whmcs_image', 'srcset', $item['iq_wp_whmcs_image']['url'] );
                                $this->add_render_attribute( 'iq_wp_whmcs_image', 'alt', Control_Media::get_image_alt( $item['iq_wp_whmcs_image'] ) );
                                $this->add_render_attribute( 'iq_wp_whmcs_image', 'title', Control_Media::get_image_title( $item['iq_wp_whmcs_image'] ) );
                                $image_html = Group_Control_Image_Size::get_attachment_image_html( $item, 'thumbnail', 'iq_wp_whmcs_image' ); ?>
                                <td class="iq-row"> <?php
                                    echo $image_html; ?>
                                </td> <?php

                            }

                        } ?>
                        <td class="iq-row"><?php echo esc_html($item['iq_wp_whmcs_register'],'wp-whmcs-lang'); ?> </td>
                        <td class="iq-row"><?php echo esc_html($item['iq_wp_whmcs_transfer'],'wp-whmcs-lang'); ?> </td>
                        <td class="iq-row"><?php echo esc_html($item['iq_wp_whmcs_renew'],'wp-whmcs-lang'); ?> </td>
                    </tr> <?php
                } ?>
            </tbody>
        </table>
    </div> <?php

}

if($style == 'yes') {

    if(empty($settings['iq_wp_whmcs_currency_number'])){
        $settings['iq_wp_whmcs_currency_number']=1;
    }
    ?>

    <?php if(!empty($settings['iq_wp_whmcs_search_on']) ): ?>
        <div>
            <input type="text" class="iq-wp-whmcs-domain-table-search <?php echo $settings['iq_wp_whmcs_search_2']; ?>" autocomplete="off" placeholder="<?php echo $settings['iq_wp_whmcs_search_bar_placeholder']; ?>">
        </div>
    <?php endif; ?>

    <div class="iq-wp-whmcs-domain-tld-<?php echo $this->get_id(); ?> iq-wp-whmcs-domain table-responsive">
        <?php if( $settings['iq_wp_whmcs_whmcs_url']!='' ) {

            if(\Elementor\Plugin::$instance->editor->is_edit_mode()){
                echo 'Dynamic Data will be shown in original view';
            } else {
                echo'<script language="javascript" src="'.$settings['iq_wp_whmcs_whmcs_url'].'/feeds/domainpricing.php?&currency='.$settings['iq_wp_whmcs_currency_number'].'"></script>';
            }
        } ?>
    </div> <?php
}
