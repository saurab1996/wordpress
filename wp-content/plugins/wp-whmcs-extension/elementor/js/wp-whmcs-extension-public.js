( function( $ ) {
	var WidgetDomainSearchHandler = function( $scope, $ ) {

		$(".wpWhmcsSearchDomainName").on("paste input", function(){
			var selector = $(this);
			var domainname = selector.val();
			selector.closest('.iq-wp-whmcs-form-area').find('#iq-domain-search-result').html('');
			if (domainname.indexOf('.') !== -1){
			$.ajax({
					method: "POST",
					url: domain_search_ajax.ajaxurl,
					_ajax_nonce: domain_search_ajax.nonce,
					data: { 'action': 'wp_whmcs_ajax_domain_search', 'domain': domainname,  }
				})
				.done(function( data ) {
					var data = JSON.parse(data);
					if(data['registered?']){
						selector.closest('.iq-wp-whmcs-form-area').find( '#find' ).val(domain_search_ajax.button_unavailable).removeClass('iq-wp-whmcs-btn-available iq-wp-whmcs-btn-domain-info').addClass('iq-wp-whmcs-btn-unavailable').prop('disabled', true);
						selector.closest('.iq-wp-whmcs-form-area').find('#iq-domain-search-result').html('<div class="iq-wp-whmcs-domain-unavailable-div"><span class="iq-wp-whmcs-domain-unavailable">'+ domain_search_ajax.domain_unavailable +'</span></div>');
					}
					else if(data['available?']){
						selector.closest('.iq-wp-whmcs-form-area').find( '#find' ).val(domain_search_ajax.button_available).removeClass('iq-wp-whmcs-btn-unavailable iq-wp-whmcs-btn-domain-info').addClass('iq-wp-whmcs-btn-available').prop('disabled', false);
						selector.closest('.iq-wp-whmcs-form-area').find('#iq-domain-search-result').html('<div class="iq-wp-whmcs-domain-available-div"><span class="iq-wp-whmcs-domain-available">'+ domain_search_ajax.domain_available +'</span></div>');
					}
					else{
						selector.closest('.iq-wp-whmcs-form-area').find( '#find' ).val(domain_search_ajax.button_info).removeClass('iq-wp-whmcs-btn-unavailable iq-wp-whmcs-btn-available').addClass('').prop('disabled', true);
						selector.closest('.iq-wp-whmcs-form-area').find('#iq-domain-search-result').html('<div class="iq-wp-whmcs-domain-info-div"><span class="iq-wp-whmcs-domain-info">Unable to find a WHOIS server for '+ domainname +'</span></div>');
					}
				})
				.fail(function( data ) {
					selector.closest('.iq-wp-whmcs-form-area').find('#iq-domain-search-result').html('');
				});
			}
		});

	};

	var WidgetDomainTableHandler = function( $scope, $ ) {

		$(".iq-wp-whmcs-domain-table-search").on("keyup paste", function(){
			var selector = $(this).parent();
			var rows = selector.next();
			rows = $('tr:not(:first-child)',rows);
			console.log($(this).hasClass("isDotAllowed"))
			if($(this).hasClass("isDotAllowed")){
				var typed_tld = $(this).val();
				console.log(typed_tld)
				if( typed_tld[0] != '.' ){
					typed_tld = '.' + typed_tld;
				}
				var val = '^(?=' +$.trim( typed_tld ).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
					reg = RegExp(val, 'i'),
					text;

			}else{
				var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
					reg = RegExp(val, 'i'),
					text;
			}
			rows.show().filter(function() {
				text = $(this).text().replace(/\s+/g, ' ');
				return !reg.test(text);
			}).hide();
		});
	};

	// Make sure you run this code under Elementor..
	$( window ).on( 'elementor/frontend/init', function() {
		elementorFrontend.hooks.addAction( 'frontend/element_ready/search_domain_section.default', WidgetDomainSearchHandler );
		elementorFrontend.hooks.addAction( 'frontend/element_ready/domain_price_table.default', WidgetDomainTableHandler );
	} );
} )( jQuery );
