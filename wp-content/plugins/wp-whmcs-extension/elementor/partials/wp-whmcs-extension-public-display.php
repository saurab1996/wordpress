<?php

/**
 * Provide a elementor-facing view for the plugin
 *
 * This file is used to markup the elementor-facing aspects of the plugin.
 *
 * @link       https://iqonic.design/
 * @since      1.0.0
 *
 * @package    Wp_Whmcs_Extension
 * @subpackage Wp_Whmcs_Extension/elementor/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
