<?php

/**
 * The elementor-facing functionality of the plugin.
 *
 * @link       https://iqonic.design/
 * @since      1.0.0
 *
 * @package    Wp_Whmcs_Extension
 * @subpackage Wp_Whmcs_Extension/elementor
 */

use Elementor\Plugin;

/**
 * The elementor-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the elementor-facing stylesheet and JavaScript.
 *
 * @package    Wp_Whmcs_Extension
 * @subpackage Wp_Whmcs_Extension/elementor
 * @author     Iqonic Design <hello@iqonic.com>
 */
class Wp_Whmcs_Extension_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the elementor-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Whmcs_Extension_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Whmcs_Extension_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-whmcs-extension-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the elementor-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Whmcs_Extension_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Whmcs_Extension_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-whmcs-extension-public.js', array( 'jquery' ), $this->version, true );
        $our_script_array = array(	'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce(),
            'button_available' => esc_html__( 'Buy Now','wp-whmcs-lang' ),
            'button_unavailable' => esc_html__( 'Not Available','wp-whmcs-lang' ),
            'button_info' => esc_html__( 'Search Again','wp-whmcs-lang' ),
            'domain_available' => esc_html__( 'Congratulations! The domain is available.','wp-whmcs-lang' ),
            'domain_unavailable' => esc_html__( 'Sorry! The Domain is already taken! Search Another one.','wp-whmcs-lang'));
        wp_localize_script( $this->plugin_name, 'domain_search_ajax', $our_script_array );

	}

    public function elementor_init()
    {
        Plugin::$instance->elements_manager->add_category(
            'iq-wp-whmcs-extension',
            [
                'title' => esc_html__('WP WHMCS', 'iq-wp-whmcs-extension'),
                'icon' => 'fa fa-plug',
            ]
        );
    }

    public function include_widgets()
    {
        if (defined('ELEMENTOR_PATH') && class_exists('Elementor\Widget_Base')) {
            require plugin_dir_path(__FILE__) . 'whmcs/widget/search_domain_section.php';
            require plugin_dir_path(__FILE__) . 'whmcs/widget/pricing_table_section.php';
            require plugin_dir_path(__FILE__) . 'whmcs/widget/domain_price_table.php';
        }
    }

}