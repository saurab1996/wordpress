<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://iqonic.design/
 * @since             1.0.0
 * @package           Wp_Whmcs_Extension
 *
 * @wordpress-plugin
 * Plugin Name:       WP WHMCS Extension
 * Plugin URI:        https://iqonic.design/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Iqonic Design
 * Author URI:        https://iqonic.design/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-whmcs-extension
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if( !defined( 'WP_WHMCS_ROOT' ) )
    define('WP_WHMCS_ROOT', plugin_dir_path( __FILE__ ));

if( !defined( 'WP_WHMCS_URL' ) )
    define( 'WP_WHMCS_URL', plugins_url( '', __FILE__ ) );

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WP_WHMCS_EXTENSION_VERSION', '1.0.0' );

/**
 *
 * Load the plugin after Elementor (and other plugins) are loaded.
 *
 * @since 1.0.0
 */
function wp_whmcs_load_elements() {
    require( __DIR__ . '/utils/wp_whmcs_helper.php' );
}
add_action( 'plugins_loaded', 'wp_whmcs_load_elements' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-whmcs-extension-activator.php
 */
function activate_wp_whmcs_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-whmcs-extension-activator.php';
	Wp_Whmcs_Extension_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-whmcs-extension-deactivator.php
 */
function deactivate_wp_whmcs_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-whmcs-extension-deactivator.php';
	Wp_Whmcs_Extension_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_whmcs_extension' );
register_deactivation_hook( __FILE__, 'deactivate_wp_whmcs_extension' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and elementor-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-whmcs-extension.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_whmcs_extension() {

	$plugin = new Wp_Whmcs_Extension();
	$plugin->run();

}
run_wp_whmcs_extension();
