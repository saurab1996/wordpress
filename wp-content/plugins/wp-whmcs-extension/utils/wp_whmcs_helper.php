<?php

function wp_whmcs_ajax_domain_search() {
    $json = file_get_contents('http://whoiz.herokuapp.com/lookup.json?url='.$_POST['domain']);
    $json = json_decode(json_encode($json),true);
    echo $json;
    wp_die();
}

add_action( 'wp_ajax_wp_whmcs_ajax_domain_search', 'wp_whmcs_ajax_domain_search' );
add_action( 'wp_ajax_nopriv_wp_whmcs_ajax_domain_search', 'wp_whmcs_ajax_domain_search' );