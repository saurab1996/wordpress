<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://iqonic.design/
 * @since      1.0.0
 *
 * @package    Wp_Whmcs_Extension
 * @subpackage Wp_Whmcs_Extension/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Whmcs_Extension
 * @subpackage Wp_Whmcs_Extension/includes
 * @author     Iqonic Design <hello@iqonic.com>
 */
class Wp_Whmcs_Extension_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
