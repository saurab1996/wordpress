<?php

/**
 * Fired during plugin activation
 *
 * @link       https://iqonic.design/
 * @since      1.0.0
 *
 * @package    Wp_Whmcs_Extension
 * @subpackage Wp_Whmcs_Extension/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Whmcs_Extension
 * @subpackage Wp_Whmcs_Extension/includes
 * @author     Iqonic Design <hello@iqonic.com>
 */
class Wp_Whmcs_Extension_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
