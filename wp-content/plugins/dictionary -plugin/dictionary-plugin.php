<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wp.test
 * @since             1.0.0
 * @package           Dictionary_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       dictionary plugin
 * Plugin URI:        http://wp.test
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Saurab
 * Author URI:        http://wp.test
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dictionary plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
//exit if accessed directly 
if(!defined('ABSPATH'))
{
    exit();
}
//load scripts
include_once(plugin_dir_path(__FILE__).'includes/dictionary-scripts.php');

add_shortcode('dictionary','dictionary_shortcode');

function dictionary_shortcode()
{   
    include_once(plugin_dir_path(__FILE__).'assests/html/footermenu.php');
} 
?>