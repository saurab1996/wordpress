<?php 
function dictionary_add_scripts()
{
    //add main css file
    wp_enqueue_style('dictionary-main-style', plugin_dir_url( __DIR__ ) . 'assests/css/style.css');
   // wp_enqueue_style('dictionary2-main-style', plugin_dir_url( __DIR__ ) . 'assests/css/style2.css');
    //add main js file
    wp_enqueue_script('dictionary-main-script',plugin_dir_url( __DIR__ ) . 'assests/js/main.js');
    wp_register_style('bootstrap','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.class');
    wp_enqueue_style('bootstrap');
   
}
add_action('wp_enqueue_scripts','dictionary_add_scripts');
?>