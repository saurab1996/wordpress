<?php 
// register custom control
require  IQ_TH_ROOT . '/inc/elementor/register-control/iqonic_register_control.php';

add_action( 'elementor/widgets/widgets_registered', 'iqonic_register_elementor_widgets' );
function iqonic_register_elementor_widgets() {

	
	if ( defined( 'ELEMENTOR_PATH' ) && class_exists('Elementor\Widget_Base') ) {
	
		//Elements
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_button.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_section_title.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_list.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_blockquotes.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_alert_box.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_counter.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_accordion.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_blog.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_fancybox.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_fancybox_list.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_tabs.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_icon_box.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_price.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_process_steps.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_progress.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_radial_progress.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_count_down.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_event_schedule.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_team.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_video_popup.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_testimonial.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_flip_box.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_client.php';
		
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_divider.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_horizontal_timeline.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_vertical_timeline.php';
		
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_price_list.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_slider.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_slider_with_text.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_tooltip.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_gallery.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_image_background_effect.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_feature_circle.php';		
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_map_chart.php';
		
		
		
 	}
}

add_action( 'elementor/init', function() {
	\Elementor\Plugin::$instance->elements_manager->add_category( 
		'iqonic',
		[
			'title' => __( 'Element', 'iqonic' ),
			'icon' => 'fa fa-plug',
		]
	);
});



// Add Custom Icon 

add_filter( 'elementor/fonts/groups', function( $font_groups ) {
	$font_groups['theme_fonts'] = __( 'Theme Fonts' );
	return $font_groups;
} );

add_filter( 'elementor/fonts/additional_fonts', function( $additional_fonts ) {
	// Key/value
	//Font name/font group
	$additional_fonts['TeXGyreAdventor-Bold'] = 'theme_fonts';
	$additional_fonts['TeXGyreAdventor-Regular'] = 'theme_fonts';
	$additional_fonts['TeXGyreAdventor-Italic'] = 'theme_fonts';
	$additional_fonts['TeXGyreAdventor-BoldItalic'] = 'theme_fonts';
	
	return $additional_fonts;
} );

add_action( 'elementor/fonts/print_font_links/theme_fonts', function( $font_name ) {
  // Enqueue your font
} );

//as for Google fonts, Elementor enqueues the Google font in a generic manner to cover 99% of the uses cases. If you need something other than that you can tell Elementor not to include the google fonts:

//add_filter( 'elementor/frontend/print_google_fonts', '__return_false' );

require  IQ_TH_ROOT . '/inc/elementor/icon/custom-icon.php';

add_action('elementor/editor/before_enqueue_scripts', function() {
    wp_enqueue_script('timeline', IQ_TH_URL .'/assest/js/timeline.js', array(), '1.0.0' , true);
    
});

add_action( 'wp_footer', function() {
	if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
		return;
	}
	?>

	<script>
		jQuery( function( $ ) {
			// Add space for Elementor Menu Anchor link
			if ( window.elementorFrontend ) {
				
				jQuery("#load").fadeOut();
				jQuery("#loading").delay(0).fadeOut("slow");
				
				if(jQuery('header').hasClass('has-sticky'))
                {         
                    jQuery(window).on('scroll', function() {
                        if (jQuery(this).scrollTop() > 300) {
                            jQuery('header').addClass('menu-sticky');
                            jQuery('.has-sticky .logo').addClass('logo-display');
                        } else if(jQuery(this).scrollTop() < 20){
                            jQuery('header').removeClass('menu-sticky');
                            jQuery('.has-sticky .logo').removeClass('logo-display');
                        }
                    });

				}
				
				
			}
			
		} );
	</script>
	<?php
} ); ?>
