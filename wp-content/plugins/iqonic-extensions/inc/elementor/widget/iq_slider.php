<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Slider extends Widget_Base {

	public function __construct($data = [], $args = null) {

		parent::__construct($data, $args);
		wp_register_script('owl-carousel-slider', IQ_TH_URL .'/assest/js/owl.carousel.min.js', [ 'elementor-frontend' ], '1.0.0' , true);
		wp_register_script('iq_owl-slider', IQ_TH_URL .'/assest/js/widget/iq_owl.js', [ 'elementor-frontend', 'owl-carousel-slider' ], '1.0.0' , true);
		wp_register_style( 'owl-carousel-slider', IQ_TH_URL .'/assest/css/owl.carousel.min.css');
	}

	public function get_script_depends() {
        return [ 'owl-carousel-slider', 'iq_owl-slider' ];
    }

    public function get_style_depends() {
        return [ 'owl-carousel-slider' ];
    }

	public function get_name() {
		return __( 'Iq_Slider', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Slider', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}
	

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-testimonial-carousel';
	}

	

	protected function _register_controls() {

		

		$this->start_controls_section(
			'section_slider',
			[
				'label' => __( 'Iqonic Slider', 'iqonic' ),
			]
		);
		$this->add_control(
			'iq_carousel',
			[
				'label' => __( 'Add Images', 'iqonic' ),
				'type' => Controls_Manager::GALLERY,
				'default' => [],
				'show_label' => false,
				'dynamic' => [
					'active' => true,
				],
			]
		);
		
        $this->add_control(
			'desk_number',
			[
				'label' => __( 'Desktop view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				
				'label_block' => true,
				'default' => '3',
			]
		);

		$this->add_control(
			'lap_number',
			[
				'label' => __( 'Laptop view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				
				'label_block' => true,
				'default' => '3',
			]
		);

		$this->add_control(
			'tab_number',
			[
				'label' => __( 'Tablet view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				
				'label_block' => true,
				'default' => '2',
			]
		);

		$this->add_control(
			'mob_number',
			[
				'label' => __( 'Mobile view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				
				'label_block' => true,
				'default' => '1',
			]
		);	

		$this->add_control(
			'autoplay',
			[
				'label'      => __( 'Autoplay', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				
			]
		);

		$this->add_control(
			'loop',
			[
				'label'      => __( 'Loop', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				
			]
		);

		$this->add_control(
			'dots',
			[
				'label'      => __( 'Dots', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				
			]
		);

		$this->add_control(
			'nav-arrow',
			[
				'label'      => __( 'Arrow', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
								
				
				'default' => [					
					'size' => 30
				],
				
			]
		);

		$this->add_control(
			'border_radius',
			[
				'label' => __( 'Image Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-slider .iq-slider-img img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );

        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_slider.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

           <script>
				
				jQuery(document).ready(function() {

					jQuery('.owl-carousel').each(function() {
						
						var jQuerycarousel = jQuery(this);
						jQuerycarousel.owlCarousel({
							items: jQuerycarousel.data("items"),                        
							loop: jQuerycarousel.data("loop"),
							margin: jQuerycarousel.data("margin"),
							stagePadding: jQuerycarousel.data("padding"),
							nav: jQuerycarousel.data("nav"),
							dots: jQuerycarousel.data("dots"),
							autoplay: jQuerycarousel.data("autoplay"),
							autoplayTimeout: jQuerycarousel.data("autoplay-timeout"),
							navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
							responsiveClass: true,
							responsive: {
								// breakpoint from 0 up
								0: {
									items: jQuerycarousel.data("items-mobile-sm"),
									nav: false,
									dots: true
								},
								// breakpoint from 480 up
								480: {
									items: jQuerycarousel.data("items-mobile"),
									nav: false,
									dots: true
								},
								// breakpoint from 786 up
								786: {
									items: jQuerycarousel.data("items-tab")
								},
								// breakpoint from 1023 up
								1023: {
									items: jQuerycarousel.data("items-laptop")
								},
								1199: {
									items: jQuerycarousel.data("items")
								}
							}
						});
					});

				});
		
			</script> 

		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Slider() );