<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_image_select extends Widget_Base {

	public function get_name() {
		return __( 'iq_image_select', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Image Select', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}
	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-image-rollover';
	}

	protected function _register_controls() {
		     
        
       

		$this->start_controls_section(
			'section_ts7cN7Gqj08dr9Ou9z8y',
			[
				'label' => __( 'Fancy Box Style', 'iqonic' ),
			]
		);

		$this->add_control(
            'image_select',
            [
                
                'type' => 'iqonic_image_select_control',
                'option' => [
                				'1' => IQ_TH_URL.'/assest/img/fancybox/150.png', 
                				'2' => IQ_TH_URL.'/assest/img/fancybox/150.png', 
                				'3' => IQ_TH_URL.'/assest/img/fancybox/150.png',
                				'4' => IQ_TH_URL.'/assest/img/fancybox/150.png'
                			],
                'description' => __('This is desceiption' , 'iqonic')
            ]
        );

        $this->end_controls_section();
		
		}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_image_select.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) {
			
			?>

		<?php } 
	}	  
	
	
	
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_image_select() );