<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class iqonic_Svg_Animation extends Widget_Base {

	public function get_name() {
		return __( 'svg_animation', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'iqonic Svg Animation', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}
	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-image-rollover';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Svg Animation', 'iqonic' ),
			]
		);


		

		

        
        
       

		$this->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
                    'active' => true,
                    
                ],
                'media_type' => '*/',

                
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		
		
		


		
		
		
		
		
		

		

        $this->end_controls_section();

        


	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_svg_animation.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) {
			
			?>
			<script>
			

			</script>

		<?php } 
	}	  
	
	
	
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\iqonic_Svg_Animation() );