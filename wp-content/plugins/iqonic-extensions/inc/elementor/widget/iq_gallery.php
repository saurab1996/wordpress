<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Gallery extends Widget_Base {

	public function __construct($data = [], $args = null) {

		parent::__construct($data, $args);
		wp_register_script('isotope-galle', IQ_TH_URL .'/assest/js/isotope.pkgd.min.js', [ 'elementor-frontend' ], '1.0.0' , true);
		wp_register_script('iq-isotope-galle', IQ_TH_URL .'/assest/js/widget/iq_isotope.js', [ 'elementor-frontend', 'isotope-galle' ], '1.0.0' , true);

	}

	public function get_script_depends() {
        return [ 'isotope-galle', 'iq-isotope-galle' ];
    }

	public function get_name() {
		return __( 'Iq_Gallery', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Gallery', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}
	

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-testimonial-carousel';
	}

	

	protected function _register_controls() {
		$this->start_controls_section(
			'section_slider',
			[
				'label' => __( 'Iqonic Gallery', 'iqonic' ),
			]
		);
		$this->add_control(
			'design_style',
			[
				'label' => __( 'Gallery Style', 'iqonic' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'iq-columns-1'  => __( 'One Column', 'iqonic' ),
					'iq-columns-2' => __( 'Two Column', 'iqonic' ),
					'iq-columns-3' => __( 'Three Column', 'iqonic' ),
					'iq-columns-4' => __( 'Four Column', 'iqonic' ),
					'iq-columns-5' => __( 'Five Column', 'iqonic' ),
					
				],
			]
        );

         $this->add_control(
			'allow_space',
			[
				'label' => __( 'Allow Space?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );
         $this->add_control(
			'count_down_padding',
			[
				'label' => __( 'Padding', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-masonry .iq-masonry-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'allow_space' => 'yes',
				],
			]
		);
		$this->add_control(
			'iq_carousel',
			[
				'label' => __( 'Add Images', 'iqonic' ),
				'type' => Controls_Manager::GALLERY,
				'default' => [],
				'show_label' => false,
				'dynamic' => [
					'active' => true,
				],
			]
		);
		
       $this->add_control(
			'media_style',
			[
				'label'      => __( 'Before Icon / Image', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'icon',
				'options'    => [
					
					'icon'          => __( 'Icon', 'iqonic' ),
					'image'          => __( 'Image', 'iqonic' ),
					'none'			=> __( 'None', 'iqonic' ),	
				],
			]
		);

		$this->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'iqonic' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'condition' => [
					'media_style' => 'icon',
                ],
                'default' => [
					'value' => 'fas fa-star'
					
				],
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'media_style' => 'image',
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'border_radius',
			[
				'label' => __( 'Image Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-slider .iq-slider-img img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		
		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );


        $this->end_controls_section();


	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_gallery.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

		    
			<script>
			    /*------------------------
				Masonry
				--------------------------*/
				jQuery(document).ready(function() {

					jQuery('.iq-masonry-block .iq-masonry').each(function() {
					
						var jQuerymsnry = jQuery('.iq-masonry-block .iq-masonry');
						if (jQuerymsnry) {
							var jQueryfilter = jQuery('.iq-masonry-block .isotope-filters');
							jQuerymsnry.isotope({
								percentPosition: true,
								resizable: true,
								itemSelector: '.iq-masonry-block .iq-masonry-item',
								masonry: {
									gutterWidth: 0
								}
							});
							// bind filter button click
							jQueryfilter.on('click', 'button', function() {
								var filterValue = jQuery(this).attr('data-filter');
								jQuerymsnry.isotope({
									filter: filterValue
								});
							});

							jQueryfilter.each(function(i, buttonGroup) {
								var jQuerybuttonGroup = jQuery(buttonGroup);
								jQuerybuttonGroup.on('click', 'button', function() {
									jQuerybuttonGroup.find('.active').removeClass('active');
									jQuery(this).addClass('active');
								});
							});
						}
					});		
				});	
		
			</script> 
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Gallery() );