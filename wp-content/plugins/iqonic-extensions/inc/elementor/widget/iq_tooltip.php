<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_ToolTip extends Widget_Base {

	public function get_name() {
		return __( 'iqonic_tooltip', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Tooltip', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve tabs widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-tabs';
	}

	protected function _register_controls() {
			$this->start_controls_section(
			'section_f2h8Vdt1cfBy6DC85gf1',
			[
				'label' => __( 'Tooltip Style', 'iqonic' ),
			]
			);

		$this->add_control(
            'design_style',
            [
                
                'type' => 'iqonic_image_select_control',
                'option' => [
                				'1' => IQ_TH_URL.'/assest/img/fancybox/150.png', 
                				'2' => IQ_TH_URL.'/assest/img/fancybox/150.png', 
                			
                			],
            ]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Tool Tip', 'iqonic' ),
				'condition' => ['design_style' => '1']
				
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'design_style' => '2',
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$repeater = new Repeater();
		
        $repeater->add_control(
			'text_line',
			[
				'label' => __( 'Line', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Loreum isprum', 'iqonic' ),
				'placeholder' => __( 'Tab Title', 'iqonic' ),
				'label_block' => true,

			]
        );

     	$repeater->add_control(
			'tooltip_text',
			[
				'label' => __( 'Tool Tip Text', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( '', 'iqonic' ),
				'placeholder' => __( 'Tab Title', 'iqonic' ),
				'label_block' => true,
			]
        	);

 		$repeater->add_control(
			'media_style',
			[
				'label'      => __( 'Icon / Image / Text', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'none',
				'options'    => [
					
					'icon'          => __( 'Icon', 'iqonic' ),
					'image'          => __( 'Image', 'iqonic' ),
					'text'          => __( 'Text', 'iqonic' ),
					'none'          => __( 'None', 'iqonic' ),
					
				],
			]
		);

		$repeater->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'iqonic' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'condition' => [
					'media_style' => 'icon',
				],
				'default' => [
					'value' => 'fas fa-star'
					
				],
			]
		);

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'media_style' => 'image',
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$repeater->add_control(
			'tooltip_text_popup',
			[
				'label' => __( 'Tool Tip Text', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Tool Tip Popup Text', 'iqonic' ),
				'placeholder' => __( 'Tab Title', 'iqonic' ),
				'label_block' => true,
				'condition' => [
					'media_style' => 'text',
				],
			]
        );   


        
        $this->add_control(
			'tabs',
			[
				'label' => __( 'Tooltip', 'iqonic' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),				
				'title_field' => '{{{ text_line }}}',
			]
		);

		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );


		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);

		$this->end_controls_section();


		/* Markers */
		
		$this->start_controls_section(
			'section_marker',
			[
				'label' => __( 'Tool Tip Markers', 'iqonic' ),
				'condition' => ['design_style' => '2']
				
			]
		);
		

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'flip_back_back',
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .tooltip-up, .tooltip-down',
				'fields_options' => [
					'background' => [
						'frontend_available' => true,
					],
				],
			]
		);

		$this->end_controls_section();
		


        $this->start_controls_section(
			'section_D2SifMLCh967Ac5v3dma',
			[
				'label' => __( 'Tool Tip', 'iqonic' ),
				'condition' => ['design_style' => '2'],

				
			]
		);
		$this->add_control(
			'image_tooltip',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);
		



		$rep = new Repeater();
		  $rep->add_control(
			'x_pos',
			[
				'label' => __( 'X Pos', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( '10', 'iqonic' ),
				'placeholder' => __( 'Only number', 'iqonic' ),
				'label_block' => true,
				
				
			]
        );

        $rep->add_control(
			'y_pos',
			[
				'label' => __( 'Y Pos', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( '10', 'iqonic' ),
				'placeholder' => __( 'Only number', 'iqonic' ),
				'label_block' => true,
				
				
			]
        );
     
 		$rep->add_control(
			'media_style',
			[
				'label'      => __( 'Icon / Image / Text', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'none',
				'options'    => [
					
					'icon'          => __( 'Icon', 'iqonic' ),
					'image'          => __( 'Image', 'iqonic' ),
					'text'          => __( 'Text', 'iqonic' ),
					'none'          => __( 'None', 'iqonic' ),
					
				],
			]
		);

		$rep->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'iqonic' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'condition' => [
					'media_style' => 'icon',
				],
				'default' => [
					'value' => 'fas fa-star'
					
				],
			]
		);

		$rep->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'media_style' => 'image',
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$rep->add_control(
			'tooltip_text_popup',
			[
				'label' => __( 'Tool Tip Text', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Tool Tip Popup Text', 'iqonic' ),
				'placeholder' => __( 'Tab Title', 'iqonic' ),
				'label_block' => true,
				'condition' => [
					'media_style' => 'text',
				],
			]
        );   

        $this->add_control(
			'tabs_1',
			[
				'label' => __( 'Tooltip', 'iqonic' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),				
				'title_field' => '{{{ x_pos }}}',
			]
		);
		$this->end_controls_section();


		/*Content Start*/

         $this->start_controls_section(
			'section_S2uE2H5fWnKkTiaCb8Fe',
			[
				'label' => __( 'Content', 'iqonic' ),
				'condition' => [
					'design_style' => '1',
				],
				'tab' => Controls_Manager::TAB_STYLE,
				
			]
		);

        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'content_text_typography',
				'label' => __( 'Typography', 'iqonic' ),				
				'selector' => '{{WRAPPER}} .iq-tooltip-style-1 p',
			]
		);

		$this->add_control(
			'content_color',
			[
				'label' => __( 'Choose Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tooltip-style-1 p' => 'color: {{VALUE}};',
		 		],
				
			]
			
		);

		$this->end_controls_section();

		/*Tooltip  Start*/

         $this->start_controls_section(
			'section_fAHlceTGM5L7Zfmufn84',
			[
				'label' => __( 'Tooltip ', 'iqonic' ),
				'tab' => Controls_Manager::TAB_STYLE,
				
			]
		);

          $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'tooltip_content_text_typography',
				'label' => __( 'Typography', 'iqonic' ),				
				'selector' => '{{WRAPPER}} .iq-tooltip-style-1  .tooltip-content, {{WRAPPER}} .iq-tooltip-style-1 .tooltip-item,{{WRAPPER}} .iq-tooltip-style-2 .tooltip',
			]
		);

		$this->add_control(
			'tooltip_content_color',
			[
				'label' => __( 'Choose Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tooltip-style-1 .iq-tooltip-text, {{WRAPPER}} .iq-tooltip-style-1 .tooltip-item , {{WRAPPER}} .iq-tooltip-style-2 .tooltip' => 'color: {{VALUE}};',
		 		],
				
			]
			
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'tooltip_background',
				'label' => __( 'Icon Background', 'iqonic' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-tooltip-style-1  .tooltip-content .iq-tooltip-text, {{WRAPPER}} .iq-tooltip-style-1 .tooltip-item, {{WRAPPER}} .iq-tooltip-style-2 .tooltip',

			]
		);
		$this->add_control(
			'tooltip_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-tooltip-style-1 .tooltip-content .iq-tooltip-text , {{WRAPPER}} .iq-tooltip-style-1 .tooltip-item ,{{WRAPPER}} .iq-tooltip-style-2 .tooltip' => 'border-style: {{VALUE}};',
						
					],
				]
		);
			
		$this->add_control(
			'tooltip_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tooltip-style-1  .tooltip-content .iq-tooltip-text , {{WRAPPER}} .iq-tooltip-style-1 .tooltip-item , {{WRAPPER}} .iq-tooltip-style-2 .tooltip' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .iq-tooltip-style-1 .tooltip-content::after , {{WRAPPER}} .iq-tooltip-style-2 .tooltip-down .tooltip::after' => 'border-top-color: {{VALUE}};', 
		 		],
				
				
			]
		);

		$this->add_control(
			'tooltip_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tooltip-style-1  .tooltip-content .iq-tooltip-text , {{WRAPPER}} .iq-tooltip-style-1 .tooltip-item, {{WRAPPER}} .iq-tooltip-style-2 .tooltip' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'tooltip_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tooltip-style-1  .tooltip-content .iq-tooltip-text, {{WRAPPER}} .iq-tooltip-style-1 .tooltip-item, {{WRAPPER}} .iq-tooltip-style-2 .tooltip' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);


		$this->add_responsive_control(
			'tooltip_content_padding',
			[
				'label' => __( 'Padding', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tooltip-style-1  .tooltip-content .iq-tooltip-text, {{WRAPPER}} .iq-tooltip-style-2 .tooltip' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'tooltip_content_margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tooltip-style-1 .tooltip-item' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

        $this->end_controls_section();


	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_tooltip.php'; ?>

        <script type="text/javascript">
        	 /*------------------------
                 Tool tip
            --------------------------*/

                // set the wrapper width and height to match the img size
                jQuery('#iq-tooltip').css({'width':jQuery('#iq-tooltip img').width(),
                    'height':jQuery('#iq-tooltip img').height()
                });

                //   jQuery('.map-marker').tooltip({
                //     animated: 'fade',
                //     placement: 'top',    
                //     html: true,
                //     container: 'body'
                    
				// });
				jQuery('.dot').each(function() {

					jQuery(".dot").popover({ trigger: "manual" , html: true, animation:false})
						.on("mouseenter", function () {
							var _this = this;
							jQuery(this).popover("show");
							jQuery(".popover").on("mouseleave", function () {
							jQuery(_this).popover('hide');
						});
						}).on("mouseleave", function () {
							var _this = this;
							setTimeout(function () {
							if (!jQuery(".popover:hover").length) {
								jQuery(_this).popover("hide");
							}
						}, 300);
					});
				});	
                
                //tooltip direction
                var tooltipDirection;

                             
                for (var i=0; i<jQuery(".pin").length; i++) {               
                    // set tooltip direction type - up or down    


                    if (jQuery(".pin").eq(i).hasClass('pin-down')) {
                        tooltipDirection = 'tooltip-down  tooltip-down-'+i;
                    } else {
                        tooltipDirection = 'tooltip-up';
                    }
                    // append the tooltip
                    jQuery("#iq-tooltip").append(
                        "<div style='left:"+jQuery(".pin").eq(i).data('xpos')+"px;top:"+jQuery(".pin").eq(i).data('ypos')+"px' class='" + tooltipDirection +"'>\
                                <div class='tooltip'>" + jQuery(".pin").eq(i).html() + "</div>\
                        </div>"
                    );
                }    
                
                // show/hide the tooltip
                jQuery('.tooltip-up, .tooltip-down').mouseenter(function() {
                            jQuery(this).children('.tooltip').fadeIn(100);
                        }).mouseleave(function(){
                            jQuery(this).children('.tooltip').fadeOut(100);
                });
        </script> <?php
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_ToolTip() );