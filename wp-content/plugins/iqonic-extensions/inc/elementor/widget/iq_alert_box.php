<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Alert extends Widget_Base {

	public function get_name() {
		return __( 'iq_alert', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Alert Box', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];

		
	} 

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-site-title';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Alert Box', 'iqonic' ),
			]
		);

		


		$this->add_control(
			'alert_style',
			[
				'label'      => __( 'Alert Box Style', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'alert-primary',
				'options'    => [
					
					'alert-primary'          => __( 'primary', 'iqonic' ),
					'alert-secondary'          => __( 'secondary', 'iqonic' ),
					'alert-success'          => __( 'success', 'iqonic' ),
					'alert-danger'          => __( 'danger', 'iqonic' ),
					'alert-warning'          => __( 'warning', 'iqonic' ),
					'alert-info'          => __( 'info', 'iqonic' ),
					'alert-light'          => __( 'light', 'iqonic' ),
					'alert-dark'          => __( 'dark', 'iqonic' ),
					
					
                    
					
				],
			]
        );

        $this->add_control(
			'alert_back',
			[
				'label'      => __( 'Alert Box Background', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'alert-primary',
				'options'    => [
					
					'bg-primary'          => __( 'primary', 'iqonic' ),
					'bg-secondary'          => __( 'secondary', 'iqonic' ),
					'bg-success'          => __( 'success', 'iqonic' ),
					'bg-danger'          => __( 'danger', 'iqonic' ),
					'bg-warning'          => __( 'warning', 'iqonic' ),
					'bg-info'          => __( 'info', 'iqonic' ),
					'bg-light'          => __( 'light', 'iqonic' ),
					'bg-dark'          => __( 'dark', 'iqonic' ),
					'bg-transparent'          => __( 'Transparent', 'iqonic' ),
					
					
                    
					
				],
			]
        );


        $this->add_control(
			'alert_custom_color',
			[
				'label' => __( 'Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-alert-box' => 'color: {{VALUE}};',

				],
				
			]
		);
        
	   
		$this->add_control(
			'section_title',
			[
				'label' => __( 'Message', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
                'label_block' => true,
                'default' => __( 'Section Title', 'iqonic' ),
			]
		);

		$this->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'iqonic' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star'
					
                ],
                
			]
		);
		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Show?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );
		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);

        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
        require  IQ_TH_ROOT . '/inc/elementor/render/iq_alert_box.php';
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Alert() );
