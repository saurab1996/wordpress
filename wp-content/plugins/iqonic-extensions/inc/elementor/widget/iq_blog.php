<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

// Blog Category
if ( !function_exists('iq_by_blog_cat')) {
	function iq_by_blog_cat() {
		$taxonomy = 'category';
		$iq_blog_cat = array();
		$terms = get_terms($taxonomy);

			foreach ( $terms as $term ) {			   
			    $iq_blog_cat[$term->slug] = $term->name;
		    }
		    return $iq_blog_cat;
	}	
}

class Iq_Blog extends Widget_Base {
	
	public function __construct($data = [], $args = null) {

		parent::__construct($data, $args);
		wp_register_script('owl-carousel-blog', IQ_TH_URL .'/assest/js/owl.carousel.min.js', [ 'elementor-frontend' ], '1.0.0' , true);
		wp_register_script('iq-owl-blog', IQ_TH_URL .'/assest/js/widget/iq_owl.js', [ 'elementor-frontend', 'owl-carousel-blog' ], '1.0.0' , true);
		wp_register_style( 'owl-carousel-blog', IQ_TH_URL .'/assest/css/owl.carousel.min.css');

	}

	public function get_script_depends() {
        return [ 'owl-carousel-blog', 'iq-owl-blog' ];
    }

    public function get_style_depends() {
        return [ 'owl-carousel-blog' ];
    }

	public function get_name() {
		return __( 'iqonic_blog', 'iqonic' );
	}
	
	public function get_title() {
        return __( 'iqonic Blog', 'iqonic' );
	}

	public function get_categories() {
        return [ 'iqonic' ];
	}
	

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-slider-push';
	}

	protected function _register_controls() {
        $this->start_controls_section(
			'section_blog',
			[
				'label' => __( 'Blog Post', 'iqonic' ),
				
			]
        );
      
        $this->add_control(
			'blog_style',
			[
				'label'      => __( 'Blog Style', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => '1',
				'options'    => [
					'1'          => __( 'Blog Slider', 'iqonic' ),
					'2'          => __( 'Blog 1 Columns', 'iqonic' ),
					'3'          => __( 'Blog 2 Columns', 'iqonic' ),
					'4'          => __( 'Blog 3 Columns', 'iqonic' )
				],
			]
		);

		$this->add_control(
            'blog_cat',
            [
                'label' => esc_html__( 'Select blog category', 'iqonic' ),
                'type' => Controls_Manager::SELECT2,
                'return_value' => 'true',
                'multiple' => true,
                'options' => iq_by_blog_cat(),
            ]
        );
        
        $this->add_control(
			'desk_number',
			[
				'label' => __( 'Desktop view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_style' => '1',
				],
				'label_block' => true,
				'default'    => '3',
			]
		);

		$this->add_control(
			'lap_number',
			[
				'label' => __( 'Laptop view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_style' => '1',
				],
				'label_block' => true,
				'default'    => '3',
			]
		);

		$this->add_control(
			'tab_number',
			[
				'label' => __( 'Tablet view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_style' => '1',
				],
				'label_block' => true,
				'default'    => '2',
			]
		);

		$this->add_control(
			'mob_number',
			[
				'label' => __( 'Mobile view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_style' => '1',
				],
				'label_block' => true,
				'default'    => '1',
			]
		);	

		$this->add_control(
			'autoplay',
			[
				'label'      => __( 'Autoplay', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'blog_style' => '1',
				]
			]
		);

		$this->add_control(
			'loop',
			[
				'label'      => __( 'Loop', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'blog_style' => '1',
				]
			]
		);

		$this->add_control(
			'dots',
			[
				'label'      => __( 'Dots', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'blog_style' => '1',
				]
			]
		);

		$this->add_control(
			'nav-arrow',
			[
				'label'      => __( 'Arrow', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'blog_style' => '1',
				]
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				
				'condition' => [
					'blog_style' => '1',
				]
				
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order By', 'iqonic' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'ASC',
				'options' => [
						'DESC' => esc_html__('Descending', 'iqonic'), 
						'ASC' => esc_html__('Ascending', 'iqonic') 
				],

			]
		);

		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					],
					'text-justify' => [
						'title' => __( 'Justified', 'iqonic' ),
						'icon' => 'eicon-text-align-justify',
					],
				]
			]
		);

        $this->end_controls_section();
	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_blog.php'; 
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

		   <script>
		        jQuery(document).ready(function() {
           
                    jQuery('.owl-carousel').each(function() {    
                
                        var jQuerycarousel = jQuery(this);
                        jQuerycarousel.owlCarousel({
							items: jQuerycarousel.data("items"),                        
							loop: jQuerycarousel.data("loop"),
							margin: jQuerycarousel.data("margin"),
							stagePadding: jQuerycarousel.data("padding"),
							nav: jQuerycarousel.data("nav"),
							dots: jQuerycarousel.data("dots"),
							autoplay: jQuerycarousel.data("autoplay"),
							autoplayTimeout: jQuerycarousel.data("autoplay-timeout"),
							navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
							responsiveClass: true,
							responsive: {
								// breakpoint from 0 up
								0: {
									items: jQuerycarousel.data("items-mobile-sm"),
									nav: false,
									dots: true
								},
								// breakpoint from 480 up
								480: {
									items: jQuerycarousel.data("items-mobile"),
									nav: false,
									dots: true
								},
								// breakpoint from 786 up
								786: {
									items: jQuerycarousel.data("items-tab")
								},
								// breakpoint from 1023 up 
								1023: {
									items: jQuerycarousel.data("items-laptop")
								},
								1199: {
									items: jQuerycarousel.data("items")
								}
							}
                        });
                    });
                });
		   </script>
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Blog() );