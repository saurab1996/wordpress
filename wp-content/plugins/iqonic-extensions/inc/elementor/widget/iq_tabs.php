<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Tabs extends Widget_Base {

	public function get_name() {
		return __( 'iqonic_Tabs', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Tabs', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve tabs widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-tabs';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_807aSE5Da0ne368h71aR',
			[
				'label' => __( 'Tab Style', 'iqonic' ),
			]
		);

		$this->add_control(
            'design_style',
            [
                
                'type' => 'iqonic_image_select_control',
                'default' => '1',
                'option' => [
                				'1' => IQ_TH_URL.'/assest/img/tab/1.jpg', 
                				'2' => IQ_TH_URL.'/assest/img/tab/2.jpg',
                				'3' => IQ_TH_URL.'/assest/img/tab/3.jpg',
                				
                			]
            ]
        );

        $this->end_controls_section();
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Tabs', 'iqonic' ),
			]
		);

		
		 $this->add_control(
			'row_reverse',
			[
				'label' => __( 'Switch Column', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
				'condition' => [
					'design_style' => '2',
				],

			]
		);

        
        
		$repeater = new Repeater();
		
        $repeater->add_control(
			'tab_title',
			[
				'label' => __( 'Tab Title', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Tab Title', 'iqonic' ),
				'label_block' => true,
			]
        );
         $repeater->add_control(
			'tab_title_desc',
			[
				'label' => __( 'Tab Description', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Tab Description', 'iqonic' ),
				'label_block' => true,
			]
        );
        
        $repeater->add_control(
			'tab_content',
			[
				'label' => __( 'Content', 'iqonic' ),
				'default' => __( 'Tab Content', 'iqonic' ),
				'placeholder' => __( 'Tab Content', 'iqonic' ),
				'type' => Controls_Manager::WYSIWYG,
				'show_label' => false,
			]
		);

		$repeater->add_control(
			'media_style',
			[
				'label'      => __( 'Icon / Image', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'icon',
				'options'    => [
					
					'icon'          => __( 'Icon', 'iqonic' ),
					'image'          => __( 'Image', 'iqonic' ),
					'none'          => __( 'None', 'iqonic' ),
					
				]
			]
		);
		
		
        $repeater->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'iqonic' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star'					
				],
				'condition' => [
					'media_style' => 'icon',
				],
			]
		);

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'media_style' => 'image',
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				]
			]
		);


        
        $this->add_control(
			'tabs',
			[
				'label' => __( 'Tabs Items', 'iqonic' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'tab_title' => __( 'Tab #1', 'iqonic' ),
                        'tab_content' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'iqonic' ),
                        'selected_icon' => __( 'fas fa-star', 'iqonic' ),
					]
					
				],
				'title_field' => '{{{ tab_title }}}',
			]
		);


		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );


		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);

       $this->end_controls_section();

        /* Tablist content start*/

        $this->start_controls_section(
			'section_e93Koa91f1kFaVN6MABU',
			[
				'label' => __( 'Tab list', 'iqonic' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'content_text_typography',
				'label' => __( 'Title Typography', 'iqonic' ),				
				'selector' => '{{WRAPPER}} .iq-tabs .nav-pills .nav-link .tab-title',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'content_desc_typography',
				'label' => __( 'Tital Description Typography', 'iqonic' ),				
				'selector' => '{{WRAPPER}} .iq-tabs .nav-pills .nav-link .tab-title-desc',
			]
		);

		 $this->start_controls_tabs( 'tablist_tabs' );

		$this->start_controls_tab(
			'tabs_M7mf29Ec64y0K03BYo9c',
			[
				'label' => __( 'Normal', 'iqonic' ),
			]
		);

		$this->add_control(
			'content_text_color',
			[
				'label' => __( 'Title Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link .tab-title' => 'color: {{VALUE}};',
					
					
				],			
				
			]
		);

		$this->add_control(
			'content_desc_color_3',
			[
				'label' => __( 'Title Description Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link .tab-title-desc' => 'color: {{VALUE}} !important;',
				],			
				
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'quote_back_color',
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-tabs.iq-tab-horizontal .nav.nav-pills,{{WRAPPER}} .iq-tabs.iq-tab-vertical .nav.nav-pills,{{WRAPPER}} .iq-tabs.iq-tab-vertical-2 .nav.nav-pills .nav-link',
				'fields_options' => [
					'background' => [
						'frontend_available' => true,
					],
					
				],
			]
		);
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'iq_iconbox_box_shadow',
				'label' => __( 'Box Shadow', 'iqonic' ),
				'selector' => '{{WRAPPER}} .iq-tabs .nav.nav-pills',
			]
		);
		$this->add_control(
			'section_19xTa0CnJ6g24YW5zfbV',
			[
				'label' => __( 'Before Background', 'iqonic' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'design_style' => '3',
					],
			]
		);

		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'tab_before_background',
                'label' => __( 'Before Background', 'iqonic' ),
                'condition' => [
					'design_style' => '3',
					],
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-tabs.iq-tab-vertical-2 .nav-pills li .nav-link::before',
            ]
        );


		 $this->end_controls_tab();

        $this->start_controls_tab(
			'tabs_r0c98aZl9VUB07Jah5Ic',
			[
				'label' => __( 'Active', 'iqonic' ),
			]
		);


		$this->add_control(
			'content_active_text_color',
			[
				'label' => __( 'Title  Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active .tab-title, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover .tab-title' => 'color: {{VALUE}};',
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active:before, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover:before' => 'background: {{VALUE}};',
					
					
				],			
				
			]
		);

		$this->add_control(
			'content_active_desc_color',
			[
				'label' => __( 'Title Description Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active .tab-title-desc, 
					 {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover .tab-title-desc' => 'color: {{VALUE}};',
				],			
				
			]
		);


		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'quote_active_back_color',
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-tabs.iq-tab-horizontal .nav-pills .nav-link.active, {{WRAPPER}} .iq-tabs.iq-tab-horizontal .nav-pills .nav-link:hover,{{WRAPPER}}  .iq-tabs.iq-tab-vertical .nav-pills .nav-link.active,{{WRAPPER}}  .iq-tabs.iq-tab-vertical .nav-pills .nav-link:hover{{WRAPPER}}  .iq-tabs.iq-tab-vertical-2 .nav-pills .nav-link.active,{{WRAPPER}}  .iq-tabs.iq-tab-vertical-2 .nav-pills .nav-link:hover',
				'fields_options' => [
					'background' => [
						'frontend_available' => true,
					],
					
				],
			]
		);

		$this->add_control(
			'section_Po9g9rDBEKmYyR4wX0MF',
			[
				'label' => __( 'Before Background', 'iqonic' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'design_style' => '3',
					],
			]
		);

		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'tab_before_hover_background',
                'label' => __( 'Before Background', 'iqonic' ),
                'condition' => [
					'design_style' => '3',
					],
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-tabs.iq-tab-vertical-2 .nav-pills li .nav-link.active::before,{{WRAPPER}} .iq-tabs.iq-tab-vertical-2 .nav-pills li .nav-link:hover::before',
            ]
        );



		 $this->end_controls_tab();
		 $this->end_controls_tabs();


		$this->add_control(
			'content_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-tabs .nav.nav-pills' => 'border-style: {{VALUE}};',
						
					],
				]
		);
			
		$this->add_control(
			'content_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav.nav-pills' => 'border-color: {{VALUE}};',
					
		 		],
				
				
			]
		);

		$this->add_control(
			'content_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav.nav-pills' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'content_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav.nav-pills' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);


		$this->add_responsive_control(
			'content_padding',
			[
				'label' => __( 'Padding', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'content_margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav.nav-pills' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);



		$this->end_controls_section();

		 /* Tablist content end*/


		  /* Tablist Icon start*/

        $this->start_controls_section(
			'section_Ref0O4G1Puvadsngc4dZ',
			[
				'label' => __( 'Tab Icon', 'iqonic' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		 $this->add_control(
			'icon_size',
			[
				'label' => __( 'Icon Size', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'icon_tabs' );
		$this->start_controls_tab(
			'tabs_6NcbEozd6Gj10lF9810a',
			[
				'label' => __( 'Normal', 'iqonic' ),
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Choose Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i' => 'color: {{VALUE}};',
		 		],
				
			]
			
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'icon_background',
				'label' => __( 'Icon Background', 'iqonic' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i',
			]
		);

		$this->add_control(
			'icon_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i' => 'border-style: {{VALUE}};',
						
					],
				]
		);
			
		$this->add_control(
			'icon_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'icon_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'icon_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab(
			'tabs_JtZgbV85U933d41201Xa',
			[
				'label' => __( 'Active', 'iqonic' ),
			]
		);

		$this->add_control(
			'icon_hover_color',
			[
				'label' => __( 'Choose Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active  i, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover  i' => 'color: {{VALUE}};',
		 		],
				
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'icon_hover_background',
				'label' => __( 'Icon Hover Background', 'iqonic' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active  i, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover  i',
			]
		);


		$this->add_control(
			'icon_hover_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active  i, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover  i' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'icon_hover_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active  i, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover  i' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'icon_hover_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active  i, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover  i' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'icon_hover_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link.active  i, {{WRAPPER}} .iq-tabs .nav-pills .nav-link:hover  i' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);		

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->add_responsive_control(
			'icon_margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link  i' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'icon_padding',
			[
				'label' => __( 'Padding', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link i' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

         
        $this->add_responsive_control(
			'icon_width',
			[
				'label' => __( 'Width', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link i' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

        $this->add_responsive_control(
			'icon_height',
			[
				'label' => __( 'Height', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .iq-tabs .nav-pills .nav-link i' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
		 /* Tablist content end*/


             /* Tab content start*/

        $this->start_controls_section(
			'section_1Zh4aod5FaB0xsE1Wq8v',
			[
				'label' => __( 'Tab Content', 'iqonic' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		 $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'tab_content_text_typography',
				'label' => __( 'Typography', 'iqonic' ),				
				'selector' => '{{WRAPPER}} .iq-tabs .tab-content',
			]
		);

		

		$this->add_control(
			'tab_content_text_color',
			[
				'label' => __( 'Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .iq-tabs .tab-content' => 'color: {{VALUE}};',
					
					
				],			
				
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'tab_content_back_color',
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-tabs .tab-content',
				'fields_options' => [
					'background' => [
						'frontend_available' => true,
					],
					
				],
			]
		);



		$this->add_control(
			'tab_content_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}}  .iq-tabs .tab-content' => 'border-style: {{VALUE}};',
						
					],
				]
		);
			
		$this->add_control(
			'tab_content_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .iq-tabs .tab-content' => 'border-color: {{VALUE}};',
					
		 		],
				
				
			]
		);

		$this->add_control(
			'tab_content_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-tabs .tab-content' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'tab_content_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-tabs .tab-content' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);


		$this->add_responsive_control(
			'tab_content_padding',
			[
				'label' => __( 'Padding', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-tabs .tab-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'tab_content_margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-tabs .tab-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);



		$this->end_controls_section();

		 /* Tab content end*/


	}
	
	protected function render() {
		$settings = $this->get_settings();
        require  IQ_TH_ROOT . '/inc/elementor/render/iq_tabs.php';
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Tabs() );