<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Sticky_Nav_Bar extends Widget_Base {

	public function get_name() {
		return __( 'iqonic_sticky_nav_bar', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Sticky Nav Bar', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}

/**
	 * Get widget icon.
	 *
	 * Retrieve Lists widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-countdown';
	}
	

	protected function _register_controls() {
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Stick Nav Bar', 'iqonic' ),
			]
        );
        $this->add_control(
			'section_title',
			[
				'label' => __( 'Title', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
				'default' => __( 'Event Title', 'iqonic' ),
			]
		);

		$this->add_control(
			'description',
			[
				'label' => __( 'Description', 'iqonic' ),
				'type' => Controls_Manager::TEXTAREA,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your Description', 'iqonic' ),
				'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'iqonic' ),
			]
		);




        
        
        $this->add_control(
			'future_date',
			[
				'label' => __( 'Select Date', 'iqonic' ),
				'type' => Controls_Manager::DATE_TIME,
				'dynamic' => [
					'active' => true,
				],
                'label_block' => true,
                'picker_options' => true
				
			]
		);

		$this->add_control(
			'is_count_down',
			[
				'label' => __( 'Show Labels', 'iqonic' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'iqonic' ),
				'label_off' => __( 'Hide', 'iqonic' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);

		
		
		$this->add_control(
			'timer_format',
			[
				'label'      => __( 'Select Style', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'YODHMS',
				'options'    => [
					
					'YODHMS'          => __( 'Year / Month / Day / Hour / Minute / Second', 'iqonic' ),					
					'ODHMS'          => __( 'Month / Day/ Hour / Minute / Second', 'iqonic' ),					
					'DHMS'          => __( 'Day / Hour / Minute / Second', 'iqonic' ),
					'HMS'          => __( ' Hour / Minute / Second', 'iqonic' ),
					'MS'          => __( 'Minute / Second', 'iqonic' ),			
					'S'          => __( ' Second', 'iqonic' ),			
					
					
				],
				'condition' => [
					'is_count_down' => true,
				],
			]
		);

        $this->add_control(
			'show_label',
			[
				'label' => __( 'Show Labels', 'iqonic' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'iqonic' ),
				'label_off' => __( 'Hide', 'iqonic' ),
				'return_value' => 'true',
				'default' => 'true',
				'condition' => [
					'is_count_down' => true,
				],
			]
		);

        
        
       

		
		

		$this->add_control(
			'image',
			[
				'label' => __( 'Event Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

	
		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );

		
		

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);

		
		
		
		
		

		

        $this->end_controls_section();

        


	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_sticky_nav_bar.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) {
			
			?>
	
			<script>	
                   


			</script>

		<?php 
		}
		

		 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Sticky_Nav_Bar() );