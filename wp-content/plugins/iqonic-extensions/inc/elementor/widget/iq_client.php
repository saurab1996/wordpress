<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

/**
 * Elementor Client widget.
 *
 * Elementor widget that displays an eye-catching headlines.
 *
 * @since 1.0.0
 */

class iqonic_Client extends Widget_Base {

    public function __construct($data = [], $args = null) {

		parent::__construct($data, $args);
		wp_register_script('owl-carousel-client', IQ_TH_URL .'/assest/js/owl.carousel.min.js', [ 'elementor-frontend' ], '1.0.0' , true);
		wp_register_script('iq_owl-client', IQ_TH_URL .'/assest/js/widget/iq_owl.js', [ 'elementor-frontend', 'owl-carousel-client' ], '1.0.0' , true);
		wp_register_style( 'owl-carousel-client', IQ_TH_URL .'/assest/css/owl.carousel.min.css');

	}

	public function get_script_depends() {
        return [ 'owl-carousel-client', 'iq_owl-client' ];
    }

    public function get_style_depends() {
        return [ 'owl-carousel-client' ];
	}
	
	/**
	 * Get widget name.
	 *
	 * Retrieve heading widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */

	public function get_name() {
		return __( 'Client', 'iqonic' );
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve heading widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	
	public function get_title() {
		return __( 'Iqonic Client', 'iqonic' );
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the heading widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */

	public function get_categories() {
		return [ 'iqonic' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */

	public function get_icon() {
		return 'eicon-posts-carousel';
	}

	/**
	 * Register heading widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */

	protected function _register_controls() {
		$this->start_controls_section(
			'section_FdQDBHfO5ay2X8xzR30d',
			[
				'label' => __( 'Client Style', 'iqonic' ),
			]
		);

		$this->add_control(
            'design_style',
            [
                
                'type' => 'iqonic_image_select_control',
                'option' => [
                				'1' => IQ_TH_URL.'/assest/img/client/01.jpg', 
                				'2' => IQ_TH_URL.'/assest/img/client/02.jpg', 
                				'3' => IQ_TH_URL.'/assest/img/client/03.jpg', 
                				'4' => IQ_TH_URL.'/assest/img/client/04.jpg', 
                				
                			],
                'description' => __('' , 'iqonic')
            ]
		);

        $this->end_controls_section();

		$this->start_controls_section(
			'section_client',
			[
				'label' => __( 'Client Post', 'iqonic' ),
				
			]
		);

        $this->add_control(
			'client_style',
			[
				'label'      => __( 'Client Style', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'slider',
				'options'    => [
					
					'slider' => __( 'Client Slider', 'iqonic' ),						
					'2' => __( 'Client 2 Columns', 'iqonic' ),
					'3' => __( 'Client 3 Columns', 'iqonic' ),
					'4' => __( 'Client 4 Columns', 'iqonic' ),
					'5' => __( 'Client 5 Columns', 'iqonic' ),
					'6' => __( 'Client 6 Columns', 'iqonic' ),
				],
			]
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'clinet_name',
			[
				'label' => __( 'Clinet Name', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Service Item', 'iqonic' ),
				'placeholder' => __( 'Clinet name', 'iqonic' ),
				'label_block' => true,
			]
		);


		$repeater->add_control(
			'description',
			[
				'label' => __( 'Description', 'iqonic' ),
				'type' => Controls_Manager::TEXTAREA,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => __( 'Enter Title Description', 'iqonic' ),
				'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'iqonic' ),
			]
        );

        $repeater->add_control(
			'image',
			[
				'label' => __( 'Client Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'List Items', 'iqonic' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'clinet_name' => __( 'Client Name', 'iqonic' ),
						
                        
					]
					
				],
				'title_field' => '{{{ clinet_name }}}',
			]
		);

		$this->add_control(
			'iq_client_list_hover_shadow',
			[
				'label' => __( 'Use Shadow On Hover?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );

        $this->add_control(
			'iq_client_list_hover_grascale',
			[
				'label' => __( 'Use grayscale On Hover?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );
        
        $this->add_control(
			'desk_number',
			[
				'label' => __( 'Desktop view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'client_style' => 'slider',
				],
				'label_block' => true,
				'default'    => '3',
			]
		);

		$this->add_control(
			'lap_number',
			[
				'label' => __( 'Laptop view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'client_style' => 'slider',
				],
				'label_block' => true,
				'default'    => '3',
			]
		);

		$this->add_control(
			'tab_number',
			[
				'label' => __( 'Tablet view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'client_style' => 'slider',
				],
				'label_block' => true,
				'default'    => '2',
			]
		);

		$this->add_control(
			'mob_number',
			[
				'label' => __( 'Mobile view', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'client_style' => 'slider',
				],
				'label_block' => true,
				'default'    => '1',
			]
		);	

		$this->add_control(
			'autoplay',
			[
				'label'      => __( 'Autoplay', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'client_style' => 'slider',
				]
			]
		);

		$this->add_control(
			'loop',
			[
				'label'      => __( 'Loop', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'client_style' => 'slider',
				]
			]
		);

		$this->add_control(
			'dots',
			[
				'label'      => __( 'Dots', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'client_style' => 'slider',
				]
			]
		);

		$this->add_control(
			'nav-arrow',
			[
				'label'      => __( 'Arrow', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'iqonic' ),
					'false'      => __( 'False', 'iqonic' ),
					
				],
				'condition' => [
					'client_style' => 'slider',
				]
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				
				'condition' => [
					'client_style' => 'slider',
				]
				
			]
		);

		

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order By', 'iqonic' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'ASC',
				'options' => [
						'DESC' => esc_html__('Descending', 'iqonic'), 
						'ASC' => esc_html__('Ascending', 'iqonic') 
				],

			]
		);

		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					],
					
				]
			]
		);

		
        $this->end_controls_section();


         /* Client Box Start*/

        $this->start_controls_section(
			'section_jpdZo4adyfPfER8bqVQF',
			[
				'label' => __( 'Client Box', 'iqonic' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

        $this->start_controls_tabs( 'iq_client_box_tabs' );
		$this->start_controls_tab(
            'tabs_Ad2RWVvjUsS088PCabdf',
            [
                'label' => __( 'Normal', 'iqonic' ),
            ]
        );
        
		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_client_box_background',
                'label' => __( 'Background', 'iqonic' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-client',
            ]
        );

         
		$this->add_control(
			'iq_client_box_has_border',
			[
				'label' => __( 'Set Custom Border?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );
        $this->add_control(
			'iq_client_box_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'condition' => [
					'iq_client_box_has_border' => 'yes',
					],
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-client, {{WRAPPER}} .iq-client ul ' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'iq_client_box_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'condition' => [
					'iq_client_box_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-client,{{WRAPPER}} .iq-client ul' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'iq_client_box_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'condition' => [
					'iq_client_box_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client,{{WRAPPER}} .iq-client ul' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'iq_client_box_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'condition' => [
					'iq_client_box_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client,{{WRAPPER}} .iq-client ul' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
		
		$this->end_controls_tab();
		$this->start_controls_tab(
            'tabs_m3r8Z6h2ja8xSFVid4z8',
            [
                'label' => __( 'Hover', 'iqonic' ),
            ]
        );
       
		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_client_box_hover_background',
                'label' => __( 'Hover Background', 'iqonic' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-client:hover,{{WRAPPER}} .iq-client.active ,{{WRAPPER}} .iq-client.active,{{WRAPPER}} .iq-client:hover ul,{{WRAPPER}} .iq-client.active ul',
            ]
        );

       
        $this->add_control(
			'iq_client_box_hover_has_border',
			[
				'label' => __( 'Set Custom Border?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );
        $this->add_control(
			'iq_client_box_hover_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'condition' => [
					'iq_client_box_hover_has_border' => 'yes',
					],
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-client:hover,{{WRAPPER}} .iq-client.active,{{WRAPPER}} .iq-client:hover ul,{{WRAPPER}} .iq-client.active ul ' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'iq_client_box_hover_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'condition' => [
					'iq_client_box_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-client:hover,{{WRAPPER}} .iq-client.active,{{WRAPPER}} .iq-client:hover ul,{{WRAPPER}} .iq-client.active ul' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'iq_client_box_hover_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'condition' => [
					'iq_client_box_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client:hover,{{WRAPPER}} .iq-client.active,{{WRAPPER}} .iq-client:hover ul,{{WRAPPER}} .iq-client.active ul' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'iq_client_box_hover_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'condition' => [
					'iq_client_box_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client:hover,{{WRAPPER}} .iq-client.active,{{WRAPPER}} .iq-client:hover ul,{{WRAPPER}} .iq-client.active ul' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
		
		$this->end_controls_tab();
		$this->end_controls_tabs();



		$this->add_responsive_control(
			'iq_client_box_padding',
			[
				'label' => __( 'Padding', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-client' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'iq_client_box_margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-client' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);


		$this->end_controls_section();

		 /* Client Box End*/

        /* Client List Start*/

        $this->start_controls_section( 
			'section_avb386YbL4372yhufPre',
			[
				'label' => __( 'Client List', 'iqonic' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'client_style' => ['2','3','4','5','6'],
				],
			]
		);

        $this->start_controls_tabs( 'iq_client_list_tabs' );
		$this->start_controls_tab(
            'tabs_j4aK4oUfabgwx9Zmv8eC',
            [
                'label' => __( 'Normal', 'iqonic' ),
            ]
        );
        
		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_client_list_background',
                'label' => __( 'Background', 'iqonic' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-client li',
            ]
        );

        $this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'iq_client_list_box_shadow',
				'label' => __( 'Box Shadow', 'iqonic' ),
				'selector' => '{{WRAPPER}} .iq-client li',
			]
		);

         
		$this->add_control(
			'iq_client_list_has_border',
			[
				'label' => __( 'Set Custom Border?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );
        $this->add_control(
			'iq_client_list_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'condition' => [
					'iq_client_list_has_border' => 'yes',
					],
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-client li ' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'iq_client_list_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'condition' => [
					'iq_client_list_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-client li' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'iq_client_list_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'condition' => [
					'iq_client_list_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client li' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'iq_client_list_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'condition' => [
					'iq_client_list_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client li' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
		
		$this->end_controls_tab();
		$this->start_controls_tab(
            'tabs_Xl7FbE1e30MZJaQn08m7',
            [
                'label' => __( 'Hover', 'iqonic' ),
            ]
        );
       
		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'iq_client_list_hover_background',
                'label' => __( 'Hover Background', 'iqonic' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-client li:hover ',
            ]
        );

        
		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'iq_client_list_hover_box_shadow',
				'label' => __( 'Box Shadow', 'iqonic' ),
				'selector' => '{{WRAPPER}} .iq-client li:hover',
			]
		);
		
        $this->add_control(
			'iq_client_list_hover_has_border',
			[
				'label' => __( 'Set Custom Border?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );
        $this->add_control(
			'iq_client_list_hover_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'condition' => [
					'iq_client_list_hover_has_border' => 'yes',
					],
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-client li:hover' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'iq_client_list_hover_border_color',
			[
				'label' => __( 'Border Color', 'iqonic' ),
				'condition' => [
					'iq_client_list_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-client li:hover' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'iq_client_list_hover_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'condition' => [
					'iq_client_list_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client li:hover' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'iq_client_list_hover_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'condition' => [
					'iq_client_list_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-client li:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
		
		$this->end_controls_tab();
		$this->end_controls_tabs();



		$this->add_responsive_control(
			'iq_client_list_padding',
			[
				'label' => __( 'Padding', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-client li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'iq_client_list_margin',
			[
				'label' => __( 'Margin', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-client li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);


		$this->end_controls_section();

		 /* Client list End*/

		


	}
	/**
	 * Render Client widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	protected function render() {
		
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_client.php';
		
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>
		   
		   <script>
				
				jQuery(document).ready(function() {

					jQuery('.owl-carousel').each(function() {
						
						var jQuerycarousel = jQuery(this);
						jQuerycarousel.owlCarousel({
							items: jQuerycarousel.data("items"),                        
							loop: jQuerycarousel.data("loop"),
							margin: jQuerycarousel.data("margin"),
							stagePadding: jQuerycarousel.data("padding"),
							nav: jQuerycarousel.data("nav"),
							dots: jQuerycarousel.data("dots"),
							autoplay: jQuerycarousel.data("autoplay"),
							autoplayTimeout: jQuerycarousel.data("autoplay-timeout"),
							navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
							responsiveClass: true,
							responsive: {
								// breakpoint from 0 up
								0: {
									items: jQuerycarousel.data("items-mobile-sm"),
									nav: false,
									dots: true
								},
								// breakpoint from 480 up
								480: {
									items: jQuerycarousel.data("items-mobile"),
									nav: false,
									dots: true
								},
								// breakpoint from 786 up
								786: {
									items: jQuerycarousel.data("items-tab")
								},
								// breakpoint from 1023 up
								1023: {
									items: jQuerycarousel.data("items-laptop")
								},
								1199: {
									items: jQuerycarousel.data("items")
								}
							}
						});
					});

				});
		
			</script>
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\iqonic_Client() );
