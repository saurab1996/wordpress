<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Event_Schedule extends Widget_Base {

	public function __construct($data = [], $args = null) {

		parent::__construct($data, $args);
		wp_register_script('jQuery-countdownTimer-min-sedu', IQ_TH_URL .'/assest/js/jQuery.countdownTimer.min.js', [ 'elementor-frontend' ], '1.0.0' , true);
	}

	public function get_script_depends() {
        return [ 'jQuery-countdownTimer-min-sedu' ];
    }

	public function get_name() {
		return __( 'iqonic_event_schedule', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Event Schedule', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}

/**
	 * Get widget icon.
	 *
	 * Retrieve Lists widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-calendar';
	}
	

	protected function _register_controls() {
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Event Information', 'iqonic' ),
			]
        );
        
        $this->add_control(
			'section_title',
			[
				'label' => __( 'Title', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
				'default' => __( 'Event Title', 'iqonic' ),
			]
		);

		$this->add_control(
			'description',
			[
				'label' => __( 'Description', 'iqonic' ),
				'type' => Controls_Manager::TEXTAREA,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your Description', 'iqonic' ),
				'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'iqonic' ),
			]
		);

        $this->add_control(
			'future_date',
			[
				'label' => __( 'Select Date', 'iqonic' ),
				'type' => Controls_Manager::DATE_TIME,
				'dynamic' => [
					'active' => true,
				],
                'label_block' => true,
                'picker_options' => true
				
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Event Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'custom_image_size', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
				'default' => 'full',
				'separator' => 'none',
			]
		);
		$this->add_control(
			'user_name',
			[
				'label' => __( 'Name Of Speaker', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
				'default' => __( 'John Deo', 'iqonic' ),
			]
		);

		$this->add_control(
			'user_image',
			[
				'label' => __( 'Speaker Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);
		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'custom_image_1', 
				'default' => 'full',
				'separator' => 'none',
			]
		);

		$this->add_control(
			'user_designation',
			[
				'label' => __( 'Speaker Designation', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
				'default' => __( 'CEO', 'iqonic' ),
			]
		);

		$this->add_control(
			'user_compnay',
			[
				'label' => __( 'User Company', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
				'default' => __( 'Tech Info', 'iqonic' ),
			]

		);

		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );



		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);	





		$this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_event_schedule.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) { ?>

            <script>
			    jQuery(document).ready(function() {
					/*----------------
					Count Down Timer
					---------------------*/
					jQuery('.iq-data-countdown-timer').each(function() {

						var future_date = jQuery(this).attr('data-date') ;
						var label = jQuery(this).attr('data-labels') ;
						var displayFormat = jQuery(this).attr('data-format') ;
						var l=true;
						if(label == "true")
						{
							l= true;
						}
						else
						{
							l = false;
						}
							jQuery(this).countdowntimer({
							dateAndTime : future_date,
							labelsFormat : l,                
							displayFormat : displayFormat,

						});      
					});
				});		
			</script> <?php
			
		}
		

		 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new Iq_Event_Schedule() );
