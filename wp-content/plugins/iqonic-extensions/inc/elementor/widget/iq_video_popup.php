<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Video_Popup extends Widget_Base {

	public function get_name() {
		return __( 'iqonic_popup_video', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Popup Video', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ]; 
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-video-camera';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Popup Video', 'iqonic' ),
			]
		);

	$this->add_control(
			'video_type',
			[
				'label' => __( 'Source', 'iqonic' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'youtube',
				'options' => [
					'video_link' => __( 'Link', 'iqonic' ),					
					'hosted' => __( 'Self Hosted', 'iqonic' ),
				],
			]
		);

	$this->add_control(
			'hosted_url',
			[
				'label' => __( 'Choose File', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				
				'media_type' => 'video',
				'condition' => [
					'video_type' => 'hosted',					
				],
			]
		);

		$this->add_control(
			'link_url',
			[
				'label' => __( 'Link', 'iqonic' ),
				'type' => Controls_Manager::TEXT,				
				'placeholder' => __( 'Enter your URL', 'iqonic' ),
				'default' => 'https://www.youtube.com/watch?v=XHOmBV4js_E',
				'label_block' => true,
				'condition' => [
					'video_type' => 'video_link',
				],
			]
		);     
        
        $this->add_control(
			'media_style',
			[
				'label'      => __( 'Select Style', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'none',
				'options'    => [
					
					'icon'          => __( 'Icon', 'iqonic' ),
					'image'          => __( 'Image', 'iqonic' ),
					'none'          => __( 'none', 'iqonic' ),
					
				],
			]
		);

		$this->add_control(
			'image_icon',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'media_style' => 'image',
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);
		

		$this->add_control(
			'selected_icon',
			[
				'label' => __( 'Play Icon', 'iqonic' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				
                'default' => [
					'value' => 'fas fa-star'
					
				],
				'skin' => 'inline',
				'label_block' => false,
				'condition' => [
					'media_style' => 'icon',
				],

				
			]
		);

	  $this->add_control(
			'image_style',
			[
				'label'      => __( 'Poster Color/Image', 'iqonic' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => '1',
				'options'    => [
					
					'1'          => __( 'color', 'iqonic' ),
					'2'          => __( 'Image', 'iqonic' ),
					'3'			 => __( 'None', 'iqonic' ),
					
				],
			]
		);
                
        $this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => __( 'Background', 'iqonic' ),
				'types' => [ 'classic', 'gradient' ],
				 'condition' => [
					'image_style' => '1',
				], 
				'selector' => '{{WRAPPER}} .iq-video-img',
			]
		);
        
        $this->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
                    'active' => true,
                    
                ],  
                'condition' => [
					'image_style' => '2',
				],          

                
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);	

		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
        );

        $this->end_controls_section();
        $this->start_controls_section(
			'section_NZk6cbrofHfRt2C5bJS9',
			[
				'label' => __( 'Icon', 'iqonic' ),
				'tab' => Controls_Manager::TAB_STYLE,
				/* 'condition' => [
					'media_style' => 'icon',
				],   */
				
			]
		);

		$this->add_control(
			'icon_back_color',
			[
				'label' => __( 'Icon Box Background Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'alpha' => true,
				'selectors' => [
					'{{WRAPPER}} .iq-popup-video .iq-video-icon .iq-video,.iq-popup-video .iq-video-icon .iq-waves .waves' => 'background: {{VALUE}};',
		 		],
				
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Icon Color  <span style="color: #5bc0de"> (Note : Working only for icon) </span>', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-popup-video .iq-video-icon .iq-video' => 'color: {{VALUE}};',
		 		],
				
			]
		);
		 $this->add_control(
			'has_border',
			[
				'label' => __( 'Use Border?', 'iqonic' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'label_off',
				'yes' => __( 'yes', 'iqonic' ),
				'no' => __( 'no', 'iqonic' ),
			]
		);

		$this->add_control(
			'iq_icon_border_color',
			[
				'label' => __( 'Icon Border Color', 'iqonic' ),
				'type' => Controls_Manager::COLOR,
				
				
				'selectors' => [
					'{{WRAPPER}} .iq-popup-video .iq-video-icon .iq-video' => 'border-color: {{VALUE}};',
		 		],

		 		'condition' => ['has_border' => 'yes']
				
			]
		);

		$this->add_control(
			'iq_icon_border_style',
				[
					'label' => __( 'Border Style', 'iqonic' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid', 'iqonic' ),
						'dashed' => __( 'Dashed', 'iqonic' ),
						'dotted' => __( 'Dotted', 'iqonic' ),
						'double' => __( 'Double', 'iqonic' ),
						'outset' => __( 'outset', 'iqonic' ),
						'groove' => __( 'groove', 'iqonic' ),
						'ridge' => __( 'ridge', 'iqonic' ),
						'inset' => __( 'inset', 'iqonic' ),
						'hidden' => __( 'hidden', 'iqonic' ),
						'none' => __( 'none', 'iqonic' ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-popup-video .iq-video-icon .iq-video' => 'border-style: {{VALUE}};',
						
					],
					'condition' => ['has_border' => 'yes']
				]
			);

		$this->add_control(
			'iq_icon_border_width',
			[
				'label' => __( 'Border Width', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-popup-video .iq-video-icon .iq-video' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => ['has_border' => 'yes']
				
			]
		);

		$this->add_control(
			'iq_icon_border_radius',
			[
				'label' => __( 'Border Radius', 'iqonic' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-popup-video .iq-video-icon .iq-video' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => ['has_border' => 'yes']
				
				
			]
		);
		
		$this->add_control(
			'iq_icon_size',
			[
				'label' => __( 'Icon Size  <span style="color: #5bc0de"> (Note : Working only for icon) </span>', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 45,
				],
				'selectors' => [
					'{{WRAPPER}} .iq-popup-video .iq-video-icon .iq-video i' => 'font-size: {{SIZE}}{{UNIT}};',
				],
				'condition' => ['has_border' => 'yes']
			]
		);

		 $this->add_responsive_control(
			'iq_icon_width',
			[
				'label' => __( 'Width', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .iq-video' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

        $this->add_responsive_control(
			'iq_icon_height',
			[
				'label' => __( 'Height', 'iqonic' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .iq-video' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}};',
				],
			]
		);	
		 
		$this->end_controls_section();

        


	}
	
	protected function render() {
		$settings = $this->get_settings();
        require  IQ_TH_ROOT . '/inc/elementor/render/iq_video_popup.php';
        if ( Plugin::$instance->editor->is_edit_mode() ) {
        ?>
        <script type="text/javascript">
        	jQuery('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
                    disableOn: 700,
                    type: 'iframe',
                    mainClass: 'mfp-fade',
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: false
                });
        </script>
        <?php 
    }
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Video_Popup() );