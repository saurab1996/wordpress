<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Extended_Tabs extends Widget_Base {

	public function get_name() {
		return __( 'iqonic_extended_tabs', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Extended Tabs', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve tabs widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-tabs';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Extended Tabs', 'iqonic' ),
			]
		);

		
	

        
        
		$repeater = new Repeater();
		
        $repeater->add_control(
			'tab_title',
			[
				'label' => __( 'Title & Description', 'iqonic' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Tab Title', 'iqonic' ),
				'placeholder' => __( 'Tab Title', 'iqonic' ),
				'label_block' => true,
			]
        );
        $repeater->add_control(
			'selected_icon',
			[
				'label' => __( 'Icon', 'iqonic' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star'
					
				],
			]
		);
        
        $repeater->add_control(
			'tab_content',
			[
				'label' => __( 'Content', 'iqonic' ),
				'default' => __( 'Tab Content', 'iqonic' ),
				'placeholder' => __( 'Tab Content', 'iqonic' ),
				'type' => Controls_Manager::TEXTAREA,
				'show_label' => false,
			]
		);
		
		$repeater->add_control(
			'image',
			[
				'label' => __( 'Choose Image', 'iqonic' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
        );
        
        $repeater->add_control(
			'list',
			[
				'label' => __( 'Tab Content List', 'iqonic' ),
				'type' => Controls_Manager::WYSIWYG,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => __( 'Enter your title', 'iqonic' ),
				'default' => __( '<ul><li>Service List</li></ul>', 'iqonic' ),
			]
		);
        

        
        $this->add_control(
			'tabs',
			[
				'label' => __( 'Tabs Items', 'iqonic' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'tab_title' => __( 'Tab #1', 'iqonic' ),
                        'tab_content' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'iqonic' ),
                        'selected_icon' => __( 'fas fa-star', 'iqonic' ),
					]
					
				],
				'title_field' => '{{{ tab_title }}}',
			]
		);


		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'iqonic' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'iqonic' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'iqonic' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'iqonic' ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);

		
		
		
		

		

        $this->end_controls_section();

        


	}
	
	protected function render() {
		$settings = $this->get_settings();
        require  IQ_TH_ROOT . '/inc/elementor/render/iq_extended_tabs.php';
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Extended_Tabs() );
