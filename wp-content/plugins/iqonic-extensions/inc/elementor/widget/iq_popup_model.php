<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Popup_Model extends Widget_Base {

	public function get_name() {
		return __( 'iq_popup_model', 'iqonic' );
	}
	
	public function get_title() {
		return __( 'Iqonic Popup Model', 'iqonic' );
	}

	public function get_categories() {
		return [ 'iqonic' ];
	}
	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-image-rollover';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_header',
			[
				'label' => __( 'Model Header', 'iqonic' ),
			]
		);


        $this->end_controls_section();

        $this->start_controls_section(
			'section_body',
			[
				'label' => __( 'Model Body', 'iqonic' ),
			]
		);


        $this->end_controls_section();

        $this->start_controls_section(
			'section_footer',
			[
				'label' => __( 'Model Footer', 'iqonic' ),
			]
		);


        $this->end_controls_section();

        


	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_popup_model.php';
		
	}	  
	
	
	
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Popup_Model() );