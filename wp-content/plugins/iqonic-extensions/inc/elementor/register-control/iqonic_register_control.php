<?php
final class Elementor_Test_Extension {

	public function init() {

		// Include plugin files
		$this->includes();

		// Register controls
		add_action( 'elementor/controls/controls_registered', [ $this, 'register_controls' ] );

	}

	public function includes() {
		
		require  IQ_TH_ROOT . '/inc/elementor/custom_control/iqonic_image_select_control.php';
	}

	public function register_controls() {
		
		
		
		$controls_manager = \Elementor\Plugin::$instance->controls_manager;
		$controls_manager->register_control( 'iqonic_image_select_control', new Iqonic_Image_Select_Control );
		
		

	}

}

$obj = new Elementor_Test_Extension;
$obj->init();
