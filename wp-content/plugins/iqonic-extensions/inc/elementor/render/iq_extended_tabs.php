<?php
namespace Elementor;
if (!defined('ABSPATH')) exit;

$tab_nav = '';
$tab_content = '';
$tab_img = '';

$tabs = $this->get_settings_for_display('tabs');
$id_int = rand(10, 100);

$align = $settings['align'];

$i = 0;
foreach ($tabs as $index => $item)
{

    if ($i == 0) {
        $class = "active";
    } else {
        $class = "";
    }

    $tab_nav .= '
    <li class="nav-item">
        <a class="nav-link ' . esc_attr($class) . '" data-toggle="tab" href="#tabs-' . $index . $id_int . '" role="tab">        
            ' . sprintf('<i aria-hidden="true" class="%1$s"></i>', esc_attr($item['selected_icon']['value'], 'iqonic')) . '
            ' . esc_html__($item['tab_title'], 'iqonic') . '
        </a>
    </li>';

    $tab_content .= '
    <section class="tab-pane ' . esc_attr($class) . '" id="tabs-' . $index . $id_int . '" role="tabpanel">
        <h4>'.esc_html($item['tab_title']).'</h4>   
        <p>' . $this->parse_text_editor($item['tab_content']) . '</p>';
         $tab_content .=  $this->parse_text_editor($item['list']) .
    '</section>'; 
                  
    if ( ! empty( $item['image']['url'] ) ) {
        $this->add_render_attribute( 'image', 'src', $item['image']['url'] );
        $this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $item['image'] ) );
        $this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $item['image'] ) );
        $image_html = Group_Control_Image_Size::get_attachment_image_html( $item, 'thumbnail', 'image' );
    }              
                  
    $tab_img .= $image_html;
    $i++;

} ?>

<div class="iq-extended-tabs <?php echo esc_attr($align); ?>">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6">
                <div class="tab-content">
                    <?php echo $tab_content; ?>
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <?php echo $tab_nav; ?>
                </ul>
            </div>

            <div class="col-lg-6">
                <?php echo $tab_img; ?>
            </div>
            
        </div>
    </div>
</div>
