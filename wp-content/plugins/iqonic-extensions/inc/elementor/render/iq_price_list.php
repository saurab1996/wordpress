<?php
namespace Elementor;
if (!defined('ABSPATH')) exit;

$tab_nav = '';
$tab_content = '';

$tabs = $this->get_settings_for_display('tabs');
$id_int = rand(10, 100);

// $align = $settings['align'];

if($settings['iqonic_has_box_shadow'] == 'yes')
{             
    $align .= ' iq-box-shadow';
} 


$i = 0;

foreach ($tabs as $index => $item)
{
    $media = '';
    
    if($item['media_style'] == 'image')
    {
        $media = '<img class="hover-img" src="'.esc_url($item['image']['url']).'" alt="fancybox">';
    }
    if($item['media_style'] == 'icon')
    {
        $media = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($item['selected_icon']['value'],'iqonic'));
    }

    if(!empty($item['item_tag']))
    {
        $tag = '<span class="iq-price-list-tag">'.esc_html($item['item_tag']).'</span>';
    }
    else
    {
        $tag = '';
    }
?>
    

    <div class="iq-price-list">
        <?php 
        if($item['media_style'] != 'none')
        {
        ?>
        <div class="iq-price-list-img">
            <?php echo $media; ?>
        </div>
        <?php } ?>
        <div class="clearfix">
            <?php echo $tag; ?>
            <h5 class="iq-price-list-title">
                <a href="#"><?php echo esc_html($item['item_title']); ?></a>
            </h5>
            <p class="iq-price-list-text"> <?php echo$this->parse_text_editor($item['tab_content']); ?> </p>
            <h2 class="list-price"><?php echo esc_html($item['amount']); ?></h2>
        </div>
    </div>
    

<?php }
