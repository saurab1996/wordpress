<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 
$html = '';
    
	$settings = $this->get_settings_for_display();
	$settings = $this->get_settings();

    $align = $settings['align'];

    if($settings['iqonic_has_box_shadow'] == 'yes')
    {        
            
       $align .= ' iq-box-shadow';
    } 
  
    
    $this->add_render_attribute( 'render_attribute', 'data-date', esc_attr($settings['future_date']) );

    if($settings['show_label'])
    {
        $label = "true";
    }
    else {
        $label = "false";
    }
    $this->add_render_attribute( 'render_attribute', 'data-labels', esc_attr($label) );
	$this->add_render_attribute( 'render_attribute', 'data-format', esc_attr($settings['timer_format']) );

	
	  if ( ! empty( $settings['image']['url'] ) ) {
        $this->add_render_attribute( 'image', 'src', $settings['image']['url'] );
        $this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $settings['image'] ) );
        $this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $settings['image'] ) );
        $image_html = Group_Control_Image_Size::get_attachment_image_html( $settings, 'thumbnail', 'image' );
        //echo sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($settings['selected_icon']['value'],'iqonic')) ;
		}
	
	?>

    <style>
        .our-info.gray-bg.nav-stikcy{
    position: fixed;
    top: 275px;
    display: inline-block;
    width: 100%;
    z-index: 999;
}
    </style>

<div class="iq-nav-sticky ">
<div class="our-info gray-bg">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <ul class="about-us text-center">
                        <li class="list-inline-item"><a href="#Blog" class="active"><i aria-hidden="true" class="ion ion-ios-paperplane-outline mr-3"></i><span> Blog Pages</span></a></li>
                        <li class="list-inline-item"><a href="#Portfolio"><i aria-hidden="true" class="ion ion-ios-paperplane-outline mr-3"></i><span>Portfolio Pages</span></a></li>
                        <li class="list-inline-item"><a href="#Services"><i aria-hidden="true" class="ion ion-ios-paperplane-outline mr-3"></i><span>Services Pages</span></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="blog-info tab-block-pt gray-bg" id="Blog">
            <div class="container">
               <div class="row">
                  <div class="col-lg-7">
                     <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="true" data-dots="false" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" data-margin="30">
                        <div class="item">
                           <img src="images/home/inner-page/13.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/14.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/15.jpg" alt="">
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-5 pl-5">
                     <h3 class="mb-3">The news section has never looked better</h3>
                     <p class="mb-4">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                     <ul class="list-inline list-details">
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Advance Advisory Team</span></li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Professional Consulting Services</span></li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">24/7 Support Help Center</span> </li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Advance Advisory Team</span> </li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2"> Professional Consulting Services</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="blog-info tab-block-pt gray-bg" id="Portfolio">
            <div class="container">
               <div class="row">
                  <div class="col-lg-5 pl-5">
                     <h3 class="mb-3">The news section has never looked better</h3>
                     <p class="mb-4">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                     <ul class="list-inline list-details">
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Advance Advisory Team</span></li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Professional Consulting Services</span></li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">24/7 Support Help Center</span> </li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Advance Advisory Team</span> </li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2"> Professional Consulting Services</span></li>
                     </ul>
                  </div>
                  <div class="col-lg-7">
                     <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="true" data-dots="false" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" data-margin="30">
                        <div class="item">
                           <img src="images/home/inner-page/05.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/06.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/01.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/07.jpg" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="blog-info tab-block-ptb  gray-bg" id="Services">
            <div class="container">
               <div class="row">
                  <div class="col-lg-7">
                     <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="true" data-dots="false" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" data-margin="30">
                        <div class="item">
                           <img src="images/home/inner-page/01.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/02.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/03.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="images/home/inner-page/04.jpg" alt="">
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-5 pl-5">
                     <h3 class="mb-3">The news section has never looked better</h3>
                     <p class="mb-4">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                     <ul class="list-inline list-details">
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Advance Advisory Team</span></li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Professional Consulting Services</span></li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">24/7 Support Help Center</span> </li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2">Advance Advisory Team</span> </li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i><span class="ml-2"> Professional Consulting Services</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
</div>
