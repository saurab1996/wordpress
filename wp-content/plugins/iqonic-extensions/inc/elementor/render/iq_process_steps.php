<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

$html = '';

    //$this->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );
	$settings = $this->get_settings();
    $tabs = $this->get_settings_for_display( 'tabs' );


    $align = $settings['align']; 

     if($settings['iqonic_has_box_shadow'] == 'yes')
    {        
            
       $align .= ' iq-box-shadow';
    } 

   $media = '';
  

    if($settings['media_style'] == 'image')
    {
        $media = '<img class="hover-img" src="'.esc_url($settings['image_icon']['url']).'" alt="fancybox">';
    }
    if($settings['media_style'] == 'icon')
    {
        $media = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($settings['selected_icon']['value'],'iqonic'));
    }

        
           
    
  if($settings['design_style'] == "1")
  {
    
 ?>

<div class="iq-process iq-process-step-style-1 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
          
                   <?php
                        if($settings['is_before_img'] == 'yes') 
                        {
                    ?>
                    <img class="iq-before-img" src="<?php echo esc_url($settings['arrow_img']['url']); ?>" alt="arrow-img">
                <?php } ?>
                    <div class="iq-step-content">
                        <?php echo $media; ?>
                         <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="iq-step-text-area">

                        <?php
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 
                        <?php
                        }
                            if($settings['use_button'] == 'yes')
                            {
                            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php'; 
                            } 
                        ?>
                        
                    </div>
                    
              
        </div>

 </div>
 <?php 
}
if($settings['design_style'] == "2")
{
?>
<div class="iq-process iq-process-step-style-2 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
            <?php
                        if($settings['is_before_img'] == 'yes') 
                        {
                    ?>
                    <img class="iq-before-img" src="<?php echo esc_url($settings['arrow_img']['url']); ?>" alt="arrow-img">
                <?php } ?>
                    <div class="iq-step-content">
                        
                         <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                        </div>
                    <?php } ?>
                    
                    <div class="iq-step-text-area">

                        <?php
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 
                        <?php
                        }
                            if($settings['use_button'] == 'yes')
                            {
                            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
                            } 
                            ?>
                        
                    </div>
                </div>
                    
               
        </div>

 </div>
<?php 
}
if($settings['design_style'] == "3")
  {
    
 ?>

<div class="iq-process iq-process-step-style-3 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
          
                    <?php
                        if($settings['is_before_img'] == 'yes') 
                        {
                    ?>
                    <img class="iq-before-img" src="<?php echo esc_url($settings['arrow_img']['url']); ?>" alt="arrow-img">
                <?php } ?>
                    <div class="iq-step-content">
                        
                          <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                        </div>
                    <?php } ?>
                        <?php  echo $media; ?>
                    </div>
                   
                    <div class="iq-step-text-area">

                        <?php
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 

                        <?php
                        }
                        
                            if($settings['use_button'] == 'yes')
                            { 
                                require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
                             } 
                             ?>
                        
                    </div>
                    
               
        </div>

 </div>
 <?php 
}
if($settings['design_style'] == "4")
  {
    
 ?>

<div class="iq-process iq-process-step-style-4 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
          
                    <?php
                        if($settings['is_before_img'] == 'yes') 
                        {
                    ?>
                    <img class="iq-before-img" src="<?php echo esc_url($settings['arrow_img']['url']); ?>" alt="arrow-img">
                <?php } ?>
                    <div class="iq-step-content">
                        <?php echo $media; ?>
                         <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="iq-step-text-area">

                        <?php
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 

                        <?php
                        }
                        
                            if($settings['use_button'] == 'yes')
                            {
                            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php'; 
                            } ?>
                        
                    </div>
                    
                
        </div>

 </div>
 <?php 
}

if($settings['design_style'] == "5")
  {    
 ?>

<div class="iq-process iq-process-step-style-5 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
          
                   <?php
                        if($settings['is_before_img'] == 'yes') 
                        {
                    ?>
                    <img class="iq-before-img" src="<?php echo esc_url($settings['arrow_img']['url']); ?>" alt="arrow-img">
                <?php } ?>
                    <div class="iq-step-content">
                        
                        <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                        </div>
                    <?php } ?>
                    </div>
                   
                    <div class="iq-step-text-area">
                        <?php  echo $media; ?>
                        <?php
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 

                        <?php
                        }
                        
                            if($settings['use_button'] == 'yes')
                            {
                            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php'; 
                             } ?>
                        
                    </div>
                    
                
        </div>

 </div>
 <?php 
}

if($settings['design_style'] == "6")
  {
    
 ?>

<div class="iq-process iq-process-step-style-6 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
           <?php
           
             

                if($settings['media_style'] == 'image')
                {
                    $media = '<img class="hover-img" src="'.esc_url($settings['image_icon']['url']).'" alt="fancybox">';
                }
                if($settings['media_style'] == 'icon')
                {
                    $media = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($settings['selected_icon']['value'],'iqonic'));
                }
    
        
            ?>
               
                    <?php
                        if($settings['is_before_img'] == 'yes') 
                        {
                    ?>
                    <img class="iq-before-img" src="<?php echo esc_url($settings['arrow_img']['url']); ?>" alt="arrow-img">
                <?php } ?>
                    <div class="iq-step-content">
                        <?php echo $media; ?>
                          <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                        </div>
                    <?php } ?>
                    </div>

                    
                    <div class="iq-step-text-area">
                        
                        <?php
                        
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 

                        <?php
                        }
                        
                            if($settings['use_button'] == 'yes')
                            {
                            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php'; 
                            } ?>
                        
                    </div>
              

               
    
         
        </div>

 </div>
 <?php 
}

if($settings['design_style'] == "7")
  {
    
 ?>

<div class="iq-process iq-process-step-style-7 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
    
                   <?php
                        if($settings['is_before_img'] == 'yes') 
                        {
                    ?>
                    <img class="iq-before-img" src="<?php echo esc_url($settings['arrow_img']['url']); ?>" alt="arrow-img">
                <?php } ?>
                    <div class="iq-step-content">
                        <?php echo $media; ?>
                         <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                        </div>
                    <?php } ?>
                    </div>

                    
                    <div class="iq-step-text-area">
                        
                        <?php
                        
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 

                        <?php
                        }
                        
                            if($settings['use_button'] == 'yes')
                            {
                            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php'; 
                             } ?>
                        
                    </div>
                    
              
    
           
        </div>

 </div>
 <?php 
}
if($settings['design_style'] == "8")
  {
    
 ?>

<div class="iq-process iq-process-step-style-8 <?php echo esc_attr($align) ?>">   
        
        <div class="iq-process-step">
    
                   
                    <div class="iq-step-content">
                        
                         <?php
                        if(!empty($settings['step_number']))
                        { 
                        ?>
                        <div class="step-number">
                            <span><?php echo esc_html($settings['step_number']); ?></span>
                            <div class="step_icon">
                            <?php echo $media; ?>
                            </div>
                        </div>
                    <?php } ?>
                    </div>

                    
                    <div class="iq-step-text-area">
                        
                        <?php
                        
                        if(!empty($settings['section_title']))
                        { 
                        ?>
                            <<?php echo $settings['title_tag'];  ?> class="iq-step-title"><?php echo esc_html($settings['section_title']) ?></<?php echo $settings['title_tag'];  ?>>
                        <?php
                        }
                       
                        if($settings['description']) 
                        {
                        
                        ?>                        
                            <span class="iq-step-desc"><?php echo esc_html($settings['description']) ?></span> 

                        <?php
                        }
                        
                            if($settings['use_button'] == 'yes')
                            {
                            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php'; 
                             } ?>
                        
                    </div>
                    
              
    
           
        </div>

 </div>
 <?php 
}
