<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 
$html ='';
    //$this->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );
	$settings = $this->get_settings_for_display();
	$settings = $this->get_settings();

    $align = '';    
    
    $this->add_render_attribute( 'iq_container', 'class', 'iq-btn-container' ); 
    
    if($settings['btn_has_box_shadow'] == 'yes')
    {        
            
       $this->add_render_attribute( 'iq_class', 'class', 'iq-box-shadow') ;
    } 
    
    $icon = '';
    $this->add_render_attribute( 'iq_class', 'class', 'iq-button' );
    $html .= esc_html($settings['button_text']);

    if($settings['button_size'] != 'default')
    {
        $this->add_render_attribute( 'iq_class', 'class', esc_attr($settings['button_size']) );
    }

    if($settings['button_shape'] != 'default')
    {
        $this->add_render_attribute( 'iq_class', 'class', esc_attr($settings['button_shape']) );
    }

    if($settings['button_style'] != 'default')
    {
        $this->add_render_attribute( 'iq_class', 'class', esc_attr($settings['button_style']) ); 
    }

    if($settings['has_icon'] == 'yes')
    {
        $this->add_render_attribute( 'iq_class', 'class', 'has-icon' );
        $icon = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($settings['button_icon']['value'],'iqonic'));

        if($settings['icon_position'] == 'right')
        {
            $html.=$icon;
            $this->add_render_attribute( 'iq_class', 'class', 'btn-icon-right') ;
        }

        if($settings['icon_position'] == 'left')
        {
        
            $html = $icon.$html;
           $this->add_render_attribute( 'iq_class', 'class', 'btn-icon-left') ;
        }

        
    }
     if($settings['button_width'] == 'iq-inline-block')
    { 
        $this->add_render_attribute( 'iq_class', 'class', esc_attr('d-inline') ); 
      }
      if($settings['button_width'] == 'iq-full')
    { 
        $this->add_render_attribute( 'iq_class', 'class', esc_attr('d-block') ); 
      }
   

    if($settings['button_action'] == 'link')
    {
        if($settings['link']['url'])
        {
            $url = $settings['link']['url'];
            $this->add_render_attribute( 'iq_class', 'href', esc_url($url) );

            if($settings['link']['is_external'])
            {
                $this->add_render_attribute( 'iq_class', 'target', '_blank' );
            }
            
            if($settings['link']['nofollow'])
            {
                $this->add_render_attribute( 'iq_class', 'rel', 'nofollow' );
            }
            $url = '';
        }
    }

    $modalid = '';
    if($settings['button_action'] == 'popup')
    {
        $modalid = 'mymodal'.rand(10,1000);
        
        $this->add_render_attribute( 'iq_class', 'data-toggle', 'modal' );
        $this->add_render_attribute( 'iq_class', 'data-target', '#'.$modalid );
         $this->add_render_attribute( 'iq_class', 'href', '#'.$modalid );
    }

?>
 <div <?php echo $this->get_render_attribute_string( 'iq_container' ) ?> >

     <a <?php echo $this->get_render_attribute_string( 'iq_class' ) ?> >  

         <?php 
            echo $html;        
         ?>   
         
    </a>
</div>
