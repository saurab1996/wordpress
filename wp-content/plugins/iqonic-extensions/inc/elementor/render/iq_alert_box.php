<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 
	
	$settings = $this->get_settings();

	$align = $settings['align'];
	

		$align .= " ".$settings['alert_style'];
	

		$align .= ' '.$settings['alert_back'];

	if($settings['iqonic_has_box_shadow'] == 'yes')
    {        
		$align .= ' iq-box-shadow';            
    }  
	
 
 ?>

 <div class="alert <?php echo esc_attr($align); ?>">
	<div class="iq-alert-box">
		<div class="iq-alert-icon">
			<i class="<?php echo esc_attr($settings['selected_icon']['value'], 'iqonic'); ?>"></i>
		</div>
		<div class="iq-alert-message">
			<p><?php echo esc_html($settings['section_title']); ?></p>
		</div>
		<a class="iq-alert-close">
			<i class="fa fa-times"></i>
		</a>		
	</div>
</div> 
