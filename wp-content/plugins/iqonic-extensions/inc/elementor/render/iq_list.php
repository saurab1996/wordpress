<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

    $tabs = $this->get_settings_for_display( 'tabs' );    
    $align = $settings['align'];
    $align .= ' iq-'.$settings['list_column'].'-column';
       
    if($settings['iqonic_has_box_shadow'] == 'yes')
    {        
            
       $align .= ' iq-box-shadow';
    } 
  
    

if($settings['list_style'] == 'unorder')
{
   
   
?>

<div class="iq-list <?php echo esc_attr($align); ?>">    
    <ul class="iq-unoreder-list">
    <?php 
        
        foreach ( $tabs as $index => $item ){
            ?>
            <li>
                
                    <?php echo esc_html($item['tab_title'],'iqonic'); ?>
            </li>
            
    <?php  }                
    ?>
    </ul>
 </div>

<?php } 
if($settings['list_style'] == 'order')
{
    
    
?>

<div class="iq-list <?php echo esc_attr($align); ?>">    
    <ol class="iq-order-list">
    <?php 
        
        foreach ( $tabs as $index => $item ){
            ?>
            <li>                
                <?php echo esc_html($item['tab_title'],'iqonic'); ?>
            </li>
            
    <?php  }                
    ?>
    </ol>
 </div>

<?php } 
if($settings['list_style'] == 'icon')
{
    
    
?>

<div class="iq-list <?php echo esc_attr($align); ?>">    
    <ul class="iq-list-with-icon">
    <?php 
        
        foreach ( $tabs as $index => $item ){
            ?>
            <li>
                <i class="<?php echo esc_attr($settings['selected_icon']['value'], 'iqonic'); ?>"></i>
                    <?php echo esc_html($item['tab_title'],'iqonic'); ?>
            </li>
            
    <?php  }                
    ?>
    </ul>
 </div>

<?php } 
if($settings['list_style'] == 'image')
{
    
    
?>

<div class="iq-list <?php echo esc_attr($align); ?>">    
    <ul class="iq-list-with-img">
    <?php 
        
        foreach ( $tabs as $index => $item ){
            ?>
            <li>
                <img src="<?php echo esc_url($settings['image']['url']); ?>">
                <?php echo esc_html($item['tab_title'],'iqonic'); ?>
            </li>
            
    <?php  }                
    ?>
    </ul>
 </div>

<?php }
