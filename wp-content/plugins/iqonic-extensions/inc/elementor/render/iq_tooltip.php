<?php
namespace Elementor;
if (!defined('ABSPATH')) exit;

$html = '';

$image_html = '';

$align = $settings['align'];

if($settings['iqonic_has_box_shadow'] == 'yes')
{        
        
   $align .= ' iq-box-shadow';
} 
  

if($settings['design_style'] == 1)
{
$tabs = $this->get_settings_for_display('tabs');
foreach ($tabs as $index => $item)
{
    if($item['media_style'] == 'image')
    {
        if ( ! empty( $item['image']['url'] ) ) 
        {
            $this->add_render_attribute( 'image', 'src', $item['image']['url'] );
            $this->add_render_attribute( 'image', 'srcset', $item['image']['url'] );
            $this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $item['image'] ) );
            $this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $item['image'] ) );
            $image_html = Group_Control_Image_Size::get_attachment_image_html( $item, 'thumbnail', 'image' );
        }
    }

    if($item['media_style'] == 'icon')
    {
        $image_html = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($item['selected_icon']['value'],'iqonic'));
    }

    if($item['media_style'] == 'text')
    {
        $image_html = '<span class="iq-tooltip-text">'.$item['tooltip_text_popup'].'</span>';
    }

   $html .= $item['text_line'] . ' ';
   
   if(!empty($item['tooltip_text']))
   {
    $html .= '<span class="tooltip-iq tooltip-effect-1">';
    $html .='<span class="tooltip-item">'.$item['tooltip_text'].'</span>
                <span class="tooltip-content clearfix">
                        '.$image_html.'
                </span>
            </span>';
   }

}

?>

 <div class="iq-tooltip iq-tooltip-style-1 <?php echo esc_attr($align); ?>">
   
    <p> <?php echo $html; ?> </p>
 </div>

<?php } 
if($settings['design_style'] == "2")
{
    
?>
    <div id="iq-tooltip" class="iq-tooltip iq-tooltip-style-2">
        
        <img class="iq-tooltip-img"  src="<?php echo $settings['image_tooltip']['url'] ?>" alt="World continents">
        
        <?php 
        $idd = 0;
        $i=0;
        $tabs = $this->get_settings_for_display('tabs_1');
        foreach ($tabs as $index => $item)
        {   
        $idd++; 
        if($item['media_style'] == 'image')
        {
        if ( ! empty( $item['image']['url'] ) ) 
        {
            $this->add_render_attribute( 'image', 'src', $item['image']['url'] );
            $this->add_render_attribute( 'image', 'srcset', $item['image']['url'] );
            $this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $item['image'] ) );
            $this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $item['image'] ) );
            $image_html = Group_Control_Image_Size::get_attachment_image_html( $item, 'thumbnail', 'image' );
        }
        }

        if($item['media_style'] == 'icon')
        {
        $image_html = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($item['selected_icon']['value'],'iqonic'));
        }

        if($item['media_style'] == 'text')
        {
        $image_html = $item['tooltip_text_popup'];
        }
        ?>
             
            
            <div class="pin pin-down pin-down-<?php echo $i; ?>" data-xpos="<?php echo $item['x_pos'] ?>" data-ypos="<?php echo $item['y_pos'] ?>">     
                <p><?php echo $image_html; ?></p>
            </div>
          <?php   
             $i++;
        }
        ?>    
    
    </div>

<?php }
