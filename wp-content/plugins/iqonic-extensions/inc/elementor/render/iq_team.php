<?php
namespace Elementor; 
$html = '';
if ( ! defined( 'ABSPATH' ) ) exit; 

    

$settings = $this->get_settings();
$align = '';

if($settings['iqonic_has_box_shadow'] == 'yes')
{       
    $align .= ' iq-box-shadow';
} 

    $args = array(
        'post_type'         => 'iqonicteam',
        'post_status'       => 'publish',  
        'posts_per_page'    => -1,      
        
    );

    $wp_query = new \WP_Query($args);  

			global $post;

			$style = $settings['team_style'];
			$desk = $settings['desk_number'];
			$lap = $settings['lap_number'];
			$tab = $settings['tab_number'];
			$mob = $settings['mob_number'];

		

		$this->add_render_attribute( 'slider', 'data-dots', $settings['dots'] );
		$this->add_render_attribute( 'slider', 'data-nav', $settings['nav-arrow'] );
		$this->add_render_attribute( 'slider', 'data-items', $settings['desk_number'] );
		$this->add_render_attribute( 'slider', 'data-items-laptop', $settings['lap_number'] );
		$this->add_render_attribute( 'slider', 'data-items-tab', $settings['tab_number'] );
		$this->add_render_attribute( 'slider', 'data-items-mobile', $settings['mob_number'] );
		$this->add_render_attribute( 'slider', 'data-items-mobile-sm', $settings['mob_number'] );
		$this->add_render_attribute( 'slider', 'data-autoplay', $settings['autoplay'] );
		$this->add_render_attribute( 'slider', 'data-loop', $settings['loop'] );
		$this->add_render_attribute( 'slider', 'data-margin', $settings['margin']['size'] );
if ($settings['design_style'] == '1') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-1 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );
				
				$li = '';
				if(!function_exists('rwmb_the_value'))
				{
					$designation = rwmb_the_value('iqonic_team_designation');
					$contact = rwmb_the_value('iqonic_team_contact');
					$email = rwmb_the_value('iqonic_team_email');
					
					$facebook   = rwmb_the_value( 'iqonic_team_facebook');
					if(!empty($facebook))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
					}
					$twitter   = rwmb_the_value( 'iqonic_team_twitter' );
					if(!empty($twitter))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
					}
					$google   = rwmb_the_value(  'iqonic_team_google');
					if(!empty($google))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
					}
					$github     = rwmb_the_value( 'iqonic_team_github');
					if(!empty($github))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
					}

					$insta     = rwmb_the_value( 'iqonic_team_insta');
					if(!empty($insta))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
					}

					$linked     = rwmb_the_value( 'iqonic_team_linkedin');
					if(!empty($linked))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>',esc_url($linked));
					}
				}
	    	?>
		    	<div class="iq-team-blog">
		    		<div class="iq-team-img">
		    			<?php echo sprintf('<img src="%1$s" />',esc_url($full_image[0],'iqonic')); ?>
		    		</div>
		    		<div class="iq-team-info">
		    			<h5 class="member-text"><?php echo sprintf("%s",esc_html(get_the_title( $post->ID ),'iqonic'));?></h5>
		    			<span class="team-post designation-text"><?php echo sprintf("%s",esc_html($designation,'iqonic') );?></span>

		    			
		    		
		    		 <div class="share iq-team-social">

		    			<ul>
		    				<?php echo $li; ?>	    				
		    			</ul>
		    			</div>
		    		</div> 		
		    	</div>
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>
</div>


<?php 
} 
?>

<?php 

if ($settings['design_style'] == '2') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-2 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
				$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );
				$designation = '';
				$contact = '';
				$email = '';
				
				if(!function_exists('rwmb_the_value'))
				{
					$designation = rwmb_the_value('iqonic_team_designation');
					$contact = rwmb_the_value('iqonic_team_contact');
					$email = rwmb_the_value('iqonic_team_email');
					
					$facebook   = rwmb_the_value( 'iqonic_team_facebook');
					if(!empty($facebook))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
					}
					$twitter   = rwmb_the_value( 'iqonic_team_twitter' );
					if(!empty($twitter))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
					}
					$google   = rwmb_the_value(  'iqonic_team_google');
					if(!empty($google))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
					}
					$github     = rwmb_the_value( 'iqonic_team_github');
					if(!empty($github))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
					}

					$insta     = rwmb_the_value( 'iqonic_team_insta');
					if(!empty($insta))
					{
						$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
					}
				}
	    
	    	?>
		    	

			<div class="iq-team-blog">
				
				<div class="iq-team-content">
					<div class="iq-team-img">
						<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="">
					</div>

					<div class="iq-team-contact">
						<div class="contact-text"><?php echo esc_html($contact); ?></div>
						<div class="email-text"><?php echo esc_html($email); ?></div>
					</div>
					<div class="iq-team-social">
						<ul>
							<?php echo $li; ?>
						</ul>
					</div>					
				</div>

				<div class="iq-team-info">
						<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
						<p class="designation-text"><?php echo esc_html($designation); ?></p>
				</div>
			</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>
</div>

<?php 
} 
?>

<?php 

if ($settings['design_style'] == '3') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-3 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';
    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?>  	

			

				<div class="iq-team-blog">
					<div class="iq-team-img">
						<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">
					</div>
					
					<div class="iq-team-info">
						<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
						<p class="designation-text"><?php echo esc_html($designation); ?></p>
					</div>
					
						
					<div class="iq-team-social">
						<ul>
							<?php echo $li; ?>
						</ul>
					</div>

				</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
} 
?>


<?php 

if ($settings['design_style'] == '4') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-4 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';

    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?> 		

				

				<div class="iq-team-blog">
					<div class="iq-team-img"> 
						<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">
						<div class="iq-team-social">
							<ul>
								<?php echo $li; ?>
							</ul>
						</div>
					</div>

					<div class="iq-team-info">
						<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
						<p class="designation-text"><?php echo esc_html($designation); ?></p>
					</div>
					
				</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
} 
?>


<?php 

if ($settings['design_style'] == '5') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-5 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';
    			
    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?> 		

				

				

				<div class="iq-team-blog">
					<div class="team-blog">
						<div class="iq-team-img"> 
							<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">
						</div>

						<div class="iq-team-contact">
							<div class="contact-text"><?php echo esc_html($contact); ?></div>
							<div class="email-text"><?php echo esc_html($email); ?></div>
						</div>

						<div class="iq-team-social">
							<ul>
								<?php echo $li; ?>
							</ul>
						</div>
					</div>
					<div class="iq-team-info">
						<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
						<p class="designation-text"><?php echo esc_html($designation); ?></p>
					</div>
				</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
} 
?>


<?php 

if ($settings['design_style'] == '6') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-6 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';
    			
    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?>		

				<div class="iq-team-blog">
					<div class="team-blog">
						<div class="iq-team-img"> 
							<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">
						</div>

						<div class="iq-team-description">
							<div class="line"></div>

							<div class="iq-team-info">
								<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
								<p class="designation-text"><?php echo esc_html($designation); ?></p>
							</div>

							
							<div class="iq-team-social">
								<ul>
									<?php echo $li; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
} 
?>

<?php 

if ($settings['design_style'] == '7') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-7 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';
    			
    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?>				

			<div class="iq-team-blog">
				<div class="iq-team-img"> 
					<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">
					<span class="iq-tooltip-item"></span>
				</div>

				<div class="iq-team-description">
					<div class="iq-team-info">
						<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
						<span class="designation-text"><?php echo esc_html($designation); ?></span>
						<p class="content-text"><?php echo get_the_content(); ?></p>
					</div>

					
					<div class="iq-team-social">
						<ul>
							<?php echo $li; ?>
						</ul>
					</div>
				</div>
			</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
} 
?>
<?php 

if ($settings['design_style'] == '8') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-8 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';
    			
    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?>				

			

		<div class="iq-team-blog">
				<div class="iq-team-img">
					<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">					

					<div class="iq-team-social">
						<ul>
							<?php echo $li; ?>
						</ul>
					</div>
				</div>
				<div class="iq-team-description">
					<div class="iq-team-info">
						<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
						<span class="designation-text"><?php echo esc_html($designation); ?></span>
						<p class="content-text"><?php echo get_the_content(); ?></p>						
					</div>
				</div>
		</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
} 
?>

<?php 

if ($settings['design_style'] == '9') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-9 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';
    			
    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?>				

			

		

		<div class="iq-team-blog">
			<div class="iq-team-img">
				<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">
				<div class="iq-team-social">
						<ul>
							<?php echo $li; ?>
						</ul>
					</div>
			</div>
			<div class="iq-team-description">
				<div class="iq-team-info">
					<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
					<span class="designation-text"><?php echo esc_html($designation); ?></span>		
					
				</div>
				
			</div>
		</div>
				
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
} 
?>

<?php 

if ($settings['design_style'] == '10') 
{	
?>   
 <div class="iq-team iq-team-slider iq-team-style-10 <?php echo esc_attr( $align ); ?>">
	 <div class="owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
	     
		<?php 
		if($wp_query->have_posts()) 
		{   
	    	while ( $wp_query->have_posts() ) 
    		{
    			$wp_query->the_post();
    			$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID  ), "full" );

    			$designation   = get_post_meta( $post->ID, 'iqonic_team_designation', true);
    			$contact   = get_post_meta( $post->ID, 'iqonic_team_contact', true);
    			$email   = get_post_meta( $post->ID, 'iqonic_team_email', true);


    			$li = '';
    			
    			$facebook   = get_post_meta( $post->ID, 'iqonic_team_facebook', true);
                if(!empty($facebook))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-facebook"></i></a></li>',esc_url($facebook));
    			}
    			$twitter   = get_post_meta( $post->ID, 'iqonic_team_twitter', true);
    			if(!empty($twitter))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-twitter"></i></a></li>',esc_url($twitter));
    			}

                

                $google   = get_post_meta( $post->ID, 'iqonic_team_google', true);
                if(!empty($google))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-google"></i></a></li>',esc_url($google));
    			}

                $github     = get_post_meta( $post->ID, 'iqonic_team_github', true);
                if(!empty($github))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-github"></i></a></li>',esc_url($github));
    			}

                $insta     = get_post_meta( $post->ID, 'iqonic_team_insta', true);
                if(!empty($insta))
    			{
    				$li .= sprintf('<li><a href="%s"><i class="fa fa-instagram"></i></a></li>',esc_url($insta));
    			}
	    
	    	?>				

			

		

		

		<div class="iq-team-blog">
			<div class="iq-team-img">
				<img class="img-fluid" src="<?php echo esc_url($full_image[0]); ?>" alt="image">
				
			</div>
			<div class="iq-team-info">
				<h5 class="member-text"><?php echo esc_html(get_the_title( $post->ID )); ?></h5>
				<span class="designation-text"><?php echo esc_html($designation); ?></span>	

				<div class="iq-team-social">
					<span> <i class="fa fa-share-alt" aria-hidden="true"></i>Share With</span>
					<ul>
						<?php echo $li; ?>
					</ul>
				</div>			
			</div>

			
		</div>
	
			<?php
	    
			}
			  wp_reset_postdata();
		    
		}
		?>
		
	</div>

</div>
<?php 
}
