<?php
namespace Elementor;
if (!defined('ABSPATH')) exit;

$nav_content = '';
$time_content = '';

$tabs = $this->get_settings_for_display('tabs');
$id_int = rand(10, 100);

$align = $settings['align'];
if($settings['iqonic_has_box_shadow'] == 'yes')
    {        
            
       $align .= ' iq-box-shadow';
    } 


if($settings['design_style'] == '1')
{
?> 
<div class="iq-timeline iq-timeline-vertical-1 <?php echo esc_attr( $align ); ?>">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="main-timeline-section ">
          <div class="conference-center-line"></div>
          <div class="conference-timeline-content">
             <?php
                foreach ($tabs as $index => $item)
                { 
                    $date=date_create($item['timeline_date']);
                    $d = date_format($date,"d/m/Y");
                ?>
              <div class="timeline-article">
                <div class="meta-date"></div>
                  <div class="content-box">
                 
                  <<?php echo $settings['title_tag']?> class="timeline-title"><?php echo esc_html($item['tab_title']); ?> </<?php echo $settings['title_tag']?>>
                   <h6 class="timeline-date"><?php echo $d; ?></h6>
                  <p class="timeline-content"><?php echo esc_html($item['tab_content']); ?></p>
                </div>
              </div> 
              <?php } ?>  
              </div>     
              </div> 
            </div>
          </div>
</div>
<?php 
}
if($settings['design_style'] == '2')
{
?>
    <div class="iq-timeline iq-timeline-vertical-2 <?php echo esc_attr( $align ); ?>" >
        <div class="timeline" id="iq-timeline-vertical-2">
          <div class="timeline__wrap">
            <div class="timeline__items">
                <?php
                foreach ($tabs as $index => $item)
                { 
                    $date=date_create($item['timeline_date']);
                    $d = date_format($date,"d/m/Y");
                ?>
              <div class="timeline__item">
                <div class="timeline__content">
                  <h2><?php echo $d; ?></h2>
                  <<?php echo $settings['title_tag']?> class="timeline-title"><?php echo esc_html($item['tab_title']); ?> </<?php echo $settings['title_tag']?>>
                  <p class="timeline-content"><?php echo esc_html($item['tab_content']); ?></p>
                </div>
              </div> 
              <?php } ?>        
            </div>
          </div>
        </div>
    </div>
<?php 
}
