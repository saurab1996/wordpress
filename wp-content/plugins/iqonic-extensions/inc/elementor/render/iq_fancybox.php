<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 
$html = '';
    
	$settings = $this->get_settings_for_display();
	$settings = $this->get_settings();

	$align = $settings['align'];

 	if($settings['iqonic_has_box_shadow'] == 'yes')
    {        
            
       $align .= ' iq-box-shadow';
    } 


	 $active = $settings['active_onoff'];
    if($active === "yes")
    {
        $align .= ' active';
    }
	$image_html = '';

	if($settings['media_style'] == 'image')
	{
		if ( ! empty( $settings['image']['url'] ) ) 
		{
			$this->add_render_attribute( 'image', 'src', $settings['image']['url'] );
			$this->add_render_attribute( 'image', 'srcset', $settings['image']['url'] );
			$this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $settings['image'] ) );
			$this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $settings['image'] ) );
			$image_html = Group_Control_Image_Size::get_attachment_image_html( $settings, 'thumbnail', 'image' );
		}
	}

	if($settings['media_style'] == 'icon')
	{
		$image_html = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($settings['selected_icon']['value'],'iqonic'));
	}
if($settings['design_style'] == '1')
{
?>

 <div class="iq-fancy-box iq-fancy-box-style-1 <?php echo esc_attr($align);?>">
 	<div class="iq-fancy-box-content">
        <div class="iq-img-area">
			<?php echo $image_html; ?>
		</div>
        <?php if($settings['section_title']) { ?>
        <<?php echo $settings['title_tag'];  ?> class="iq-fancy-title"> <?php echo sprintf('%1$s',esc_html($settings['section_title'],'iqonic'));?></<?php echo $settings['title_tag'];  ?>>	
    <?php } ?>
     <?php  if($settings['description']){ ?>
        <div class="special-content">
            <p class="fancy-box-content"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        </div>
    <?php } ?>
    </div>
    <?php  if($settings['use_button'] == 'yes'){  
    	require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
     } ?>
    <div class="iq-special-img" style="background: url('<?php echo $settings['image_back']['url']; ?>')">
    	
    </div>
	
 </div>
<?php 
} 
if($settings['design_style'] == '2')
{
?>
 <div class="iq-fancy-box iq-fancy-box-style-2 <?php echo esc_attr($align);?>">	
 	<?php
 		if($settings['media_style'] != 'none')
 		{ 
 	?>
	<div class="iq-img-area">
		<?php echo $image_html; ?>
	</div>
	<?php } ?>
	<div class="iq-fancy-box-content">
		<?php if($settings['section_title']){ ?>
		<<?php echo $settings['title_tag'];  ?> class="iq-fancy-title"> <?php echo sprintf('%1$s',esc_html($settings['section_title'],'iqonic'));?></<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>
		<?php if($settings['description']){ ?>					
		<p class="fancy-box-content"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
	<?php } ?>
	    <?php  if($settings['use_button'] == 'yes'){  
	    		require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
	    } ?>
	</div>
			
		
 </div>
<?php 
} 

if($settings['design_style'] == '3')
{
?>
 <div class="iq-fancy-box iq-fancy-box-style-3 <?php echo esc_attr($align);?>">		
	<div class="iq-img-area">
		<?php echo $image_html; ?>
	</div>
	<div class="overlay"></div> 
	<div class="iq-fancy-box-content">
		<?php
			if($settings['section_title'])
			{
			?>
		<<?php echo $settings['title_tag'];  ?> class="iq-fancy-title"> <?php echo sprintf('%1$s',esc_html($settings['section_title'],'iqonic'));?></<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>
		<?php
			if($settings['description'])
			{
			?>			
		<p class="fancy-box-content"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
	<?php } ?>
		    <?php  if($settings['use_button'] == 'yes'){  
		    	require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
		     } ?>
	</div>
			
		
 </div>
<?php 
} 

if($settings['design_style'] == '4')
{
?>


 <div class="iq-fancy-box iq-fancy-box-style-4 <?php echo esc_attr($align);?>">		
	
	<div class="iq-fancy-box-content">
		<div class="iq-img-area">
			<?php echo $image_html; ?>
		</div>
		<?php
			if($settings['section_title'])
			{
			?>
	
		<<?php echo $settings['title_tag'];  ?> class="iq-fancy-title"> <?php echo sprintf('%1$s',esc_html($settings['section_title'],'iqonic'));?></<?php echo $settings['title_tag'];  ?>>
		<?php } ?>
		<?php
			if($settings['description'])
			{
			?>				
		<p class="fancy-box-content"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
	<?php } ?>
		    <?php  if($settings['use_button'] == 'yes'){  
		    	require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
		     } ?>
	</div>
			
		
 </div>

<?php 
} 
if($settings['design_style'] == '5')
{
?>


 <div class="iq-fancy-box iq-fancy-box-style-5 <?php echo esc_attr($align);?>">		
	
	
	<div class="iq-fancy-box-content">
		<div class="iq-img-area">
			<?php echo $image_html; ?>
		</div>
		<?php
			if($settings['section_title'])
			{
			?>
		<<?php echo $settings['title_tag'];  ?> class="iq-fancy-title"> <?php echo sprintf('%1$s',esc_html($settings['section_title'],'iqonic'));?></<?php echo $settings['title_tag'];  ?>>	
		<?php  } ?>			
		<?php
			if($settings['description'])
			{
			?>
		<p class="fancy-box-content"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
	<?php } ?>

	</div>

	    <?php  if($settings['use_button'] == 'yes'){  
	    	require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';	
	     } ?>		
		
 </div>

<?php 
} 
if($settings['design_style'] == '6')
{
?>
 <div class="iq-fancy-box iq-fancy-box-style-6 <?php echo esc_attr($align);?>">	

 <div class="iq-fancy-box-content media">
        <div class="iq-img-area">
			<?php echo $image_html; ?>
		</div>
        <div class="media-body">
			<?php
			if($settings['section_title'])
			{
			?>
			<<?php echo $settings['title_tag'];  ?> class="iq-fancy-title"> <?php echo sprintf('%1$s',esc_html($settings['section_title'],'iqonic'));?></<?php echo $settings['title_tag'];  ?>>			
	
			<?php } ?>
			<?php
			if($settings['description'])
			{
			?>
			<p class="fancy-box-content"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
			<?php } ?>
			
			  <?php  if($settings['use_button'] == 'yes'){  

			  	require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';

			   } ?>
		
        </div>

      
	
    </div>
</div>
<?php 
} 
if($settings['design_style'] == '7')
{
?>
 <div class="iq-fancy-box iq-fancy-box-style-7 <?php echo esc_attr($align);?>">	
	<div class="iq-img-area">
			<?php echo $image_html; ?>
	</div>
 	<div class="iq-fancy-box-content">
 		<?php
			if($settings['section_title'])
			{
			?>
			<<?php echo $settings['title_tag'];  ?> class="iq-fancy-title"> <?php echo sprintf('%1$s',esc_html($settings['section_title'],'iqonic'));?></<?php echo $settings['title_tag'];  ?>>			
	
			<?php } ?>
			<?php
			if($settings['description'])
			{
			?>
			<p class="fancy-box-content"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
			<?php } ?>
			
			  <?php  if($settings['use_button'] == 'yes'){  

			  	require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';

			   } ?>     
	
    </div>
</div>
<?php 
}
