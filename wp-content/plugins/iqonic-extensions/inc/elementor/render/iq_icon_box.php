<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 
$html = '';
    
	$settings = $this->get_settings_for_display();
	$settings = $this->get_settings();

	$align = $settings['align'];
	$image_html = '';

    if($settings['iqonic_has_box_shadow'] == 'yes') {
       $align .= ' iq-box-shadow';
    }

    $active = $settings['active_onoff'];
    if($active === "yes") {
        $align .= ' active';
    }

	if($settings['media_style'] == 'image') {
		if ( ! empty( $settings['image']['url'] ) ) {
			$this->add_render_attribute( 'image', 'src', $settings['image']['url'] );
			$this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $settings['image'] ) );
			$this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $settings['image'] ) );
			$image_html = Group_Control_Image_Size::get_attachment_image_html( $settings, 'thumbnail', 'image' );
		}
	}

	if($settings['media_style'] == 'icon') {
		$image_html = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($settings['selected_icon']['value'],'iqonic'));
	}
    if($settings['icon_box_link']['url']) {
        $url = $settings['icon_box_link']['url'];
        $this->add_render_attribute( 'link_attr', 'href', esc_url($url) );
        if($settings['icon_box_link']['is_external']) {
            $this->add_render_attribute( 'link_attr', 'target', '_blank' );
        }
        if($settings['icon_box_link']['nofollow']) {
            $this->add_render_attribute( 'link_attr', 'rel', 'nofollow' );
        }
        $url = '';
    }


    if($settings['design_style'] == '1') { ?>
        <div class="iq-icon-box iq-icon-box-style-1 <?php echo esc_attr($align);?>"> <?php
 	        if($settings['has_back_img']) { ?>
 	            <img src="<?php echo esc_url($settings['back_image']['url']); ?>" class="hover-img" alt="image">
 	<?php
	 }
	 ?>

	<div class="icon-box-img">
		<?php echo $image_html; ?>
	</div>
	<div class="icon-box-content">
		 <?php if($settings['section_title']) { ?>
		<<?php echo $settings['title_tag'];  ?> class="icon-box-title"><a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
		</<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>			
		<?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
         <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
	</div>
 </div>
<?php 
} 
if($settings['design_style'] == '2')
{
?>
 <div class="iq-icon-box iq-icon-box-style-2 <?php echo esc_attr($align);?> ">
	
	<div class="icon-box-img">
		<?php echo $image_html; ?>
	</div>
			
	<div class="icon-box-content">
		<?php if($settings['section_title']) { ?>
		<<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
		</<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>				
		<?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
         <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
	</div>
	
 </div>
<?php 
} 

if($settings['design_style'] == '3')
{
?>
 <div class="iq-icon-box iq-icon-box-style-3 <?php echo esc_attr($align);?> ">
	
        <div class="icon-box-img">
        	<div class="img-style-3">
            	<?php echo $image_html; ?>
        	</div>
           <?php if($settings['section_title']) { ?>
		<<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
		</<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>
        </div>
        <div class="icon-box-content">
           <?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
         <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
        </div>
    
 </div>
<?php 
} 
if($settings['design_style'] == '4') { ?>
 <div class="iq-icon-box iq-icon-box-style-4 <?php echo esc_attr($align);?> ">
	
        <div class="icon-box-img">
            <?php echo $image_html; ?> <?php 
            if($settings['section_title']) { ?>
		        <<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
                </<?php echo $settings['title_tag'];  ?>> <?php 
            } ?> <?php 
            if($settings['section_title_1']) { ?>
                <<?php echo $settings['subtitle_tag'];  ?> class="icon-box-subtitle"><?php echo sprintf('%1$s',esc_html($settings['section_title_1'],'iqonic'));?></<?php echo $settings['subtitle_tag'];  ?>>
                <?php 
            } ?>
        </div>

        <div class="icon-box-content effect-box">
        	<div class="effect-btn">
				<i aria-hidden="true" class="<?php echo esc_attr($settings['effect_icon']['value']); ?>"></i>
            </div> <?php  
            if($settings['description']){ ?>
                <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p> <?php 
            } ?> <?php  
            if($settings['use_button'] == 'yes') {
                require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
            } ?>
        </div>
    
 </div>
<?php 
} 
if($settings['design_style'] == '5')
{
?>
 <div class="iq-icon-box iq-icon-box-style-5 <?php echo esc_attr($align);?> ">
	
        <div class="icon-box-img">
            <?php echo $image_html; ?>
           <?php if($settings['section_title']) { ?>
		<<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
		</<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>
        <?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
         <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
        </div>
        <div class="icon-box-content">
        	<?php echo $image_html; ?>            
        </div>
    
 </div>
<?php 
} 

if($settings['design_style'] == '6')
{
?>
 <div class="iq-icon-box iq-icon-box-style-6 <?php echo esc_attr($align);?> ">
	
        <div class="icon-box-content">
           <?php if($settings['section_title']) { ?>
		<<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
		</<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>
        <?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
         <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
        </div>
        <div class="icon-box-img">
        	<?php echo $image_html; ?>            
        </div>
    
 </div>
<?php 
} 
if($settings['design_style'] == '7')
{
?>
 <div class="iq-icon-box iq-icon-box-style-7 <?php echo esc_attr($align);?> ">
 	<?php if($settings['has_loader'] == 'yes'){ ?>
 		<div class="holder">
						<div class="loader"><div></div><div></div><div></div></div>
					</div>
 	<?php } ?>
 	 	<div class="icon-box-img">
 	 		<div class="effect-circle"></div>
 	 		<div class="main-img">
 	 			<div class="img-bg">
        			<?php echo $image_html; ?> 
        		</div>
       		</div>           
       </div>
	
        <div class="icon-box-content">
           <?php if($settings['section_title']) { ?>
		<<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
		</<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>
		 <?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
         <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
        </div>
       
    
 </div>
<?php 
}
if($settings['design_style'] == '8') { ?>

    <div class="iq-icon-box iq-icon-box-style-8  <?php echo esc_attr($align);?>">
        <div class="icon-box-img">
            <?php echo $image_html; ?>
        </div>
	<div class="icon-box-content">
		 <?php if($settings['section_title']) { ?>
		<<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
		</<?php echo $settings['title_tag'];  ?>>	
		<?php } ?>			
		<?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
        <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
	</div>
 </div>
<?php
}
if($settings['design_style'] == '9')
{
?>

 <div class="iq-icon-box iq-icon-box-style-9  <?php echo esc_attr($align);?>">
   
    <div class="icon-box-content">
         <?php if($settings['section_title']) { ?>
        <<?php echo $settings['title_tag'];  ?> class="icon-box-title"> <a <?php echo $this->get_render_attribute_string( 'link_attr' ); ?>><?php echo esc_html($settings['section_title']);?></a>
        </<?php echo $settings['title_tag'];  ?>>   
        <?php } ?>          
        <?php  if($settings['description']){ ?>
            <p class="icon-box-desc"> <?php echo sprintf('%1$s',esc_html($settings['description'],'iqonic'));?> </p>
        <?php } ?>
        <?php  if($settings['use_button'] == 'yes')
    {
            require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
        } ?>
    </div>
     <div class="icon-box-img">
        <?php echo $image_html; ?>
    </div>
 </div>
<?php 
}
