<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) exit;

    
    $settings = $this->get_settings();
    $align = $settings['align'];
    if($settings['iqonic_has_box_shadow'] == 'yes')
    {        
            
       $align .= ' iq-box-shadow';
    } 


$image_html = '';
$user_image = '';

if(!empty($settings['image']['url']))
{
$this->add_render_attribute( 'image_attribute', 'src', $settings['image']['url'] );
$this->add_render_attribute( 'image_attribute', 'srcset', $settings['image']['url'] );
$this->add_render_attribute( 'image_attribute', 'alt', Control_Media::get_image_alt( $settings['image'] ) );
$this->add_render_attribute( 'image_attribute', 'title', Control_Media::get_image_title( $settings['image'] ) );
$image_html = Group_Control_Image_Size::get_attachment_image_html( $settings, 'custom_image_size', 'image' );
}
if(!empty($settings['user_image']['url']))
{
$this->add_render_attribute( 'image_attribute_1', 'src', $settings['user_image']['url'] );
$this->add_render_attribute( 'image_attribute_1', 'srcset', $settings['user_image']['url'] );
$this->add_render_attribute( 'image_attribute_1', 'alt', Control_Media::get_image_alt( $settings['user_image'] ) );
$this->add_render_attribute( 'image_attribute_1', 'title', Control_Media::get_image_title( $settings['user_image'] ) );
$user_image = Group_Control_Image_Size::get_attachment_image_html( $settings, 'custom_image_1', 'user_image' );
}
$timestamp = strtotime($settings['future_date']);
$date = date('d/m/Y', $timestamp);
$time = date('H:i', $timestamp);
$day =  date('l', $timestamp);


?>

<div class="iq-event-schedule <?php echo esc_attr( $align ); ?>">
    <div class="row">        
        <div class="col-lg-12">
            <div class="iq-event-title"> <h5><?php echo $settings['section_title']; ?></h5></div>            
           
           
        </div>
        <div class="col-lg-12">
            <div class="iq-event-info">
                 <div class="iq-event-timeschedule">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <span class="iq-event-day"><?php echo $day; ?></span>
                <span class="iq-event-date"><?php echo $date; ?></span>
                <span class="iq-event-time"><?php echo $time; ?></span>
            </div>
                <div class="iq-event-img">
                    <?php echo $image_html; ?>
                </div>
                <div class="iq-event-text-area">
                   
                    <p><?php echo $settings['description']; ?></p>
                </div>
                 <div class="iq-speaker">
                <div class="iq-speaker-img">
                    <?php echo $user_image; ?>
                </div>
                <div class="iq-speaker-info">
                    <h5><?php echo $settings['user_name']; ?></h5>
                    <span><?php echo $settings['user_designation']; ?></span>
                    <strong>-</strong>
                    <span><?php echo $settings['user_compnay']; ?></span>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
