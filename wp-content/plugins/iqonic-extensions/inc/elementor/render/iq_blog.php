<?php
namespace Elementor;

use Elementor\Plugin;
if ( ! defined( 'ABSPATH' ) ) exit;

  $cat = '';

  $settings = $this->get_settings();
  
  if(isset($settings['blog_cat']) && !empty($settings['blog_cat'])) {
      $cat = implode(',',$settings['blog_cat']);
  }

  $args = array(
        'post_type'         => 'post',
        'post_status'       => 'publish', 
        'category_name'		=> $cat,      
        'suppress_filters'  => 0              
  );
  
  $align = $settings['align'];

    if($settings['iqonic_has_box_shadow'] == 'yes') {       
        $align .= ' iq-box-shadow';
    }

    $wp_query = new \WP_Query($args);
    $out = '';
    $iqonic_option = get_option('qloud_options');
    global $post; ?>
   
    <div class="iq-blog <?php echo esc_attr($align, 'iqonic') ?>">  <?php

    if($settings['blog_style'] === '1') {

      $desk = $settings['desk_number'];
      $lap = $settings['lap_number'];
      $tab = $settings['tab_number'];
      $mob = $settings['mob_number'];

      $this->add_render_attribute( 'slider', 'data-dots', $settings['dots'] );
      $this->add_render_attribute( 'slider', 'data-nav', $settings['nav-arrow'] );
      $this->add_render_attribute( 'slider', 'data-items', $settings['desk_number'] );
      $this->add_render_attribute( 'slider', 'data-items-laptop', $settings['lap_number'] );
      $this->add_render_attribute( 'slider', 'data-items-tab', $settings['tab_number'] );
      $this->add_render_attribute( 'slider', 'data-items-mobile', $settings['mob_number'] );
      $this->add_render_attribute( 'slider', 'data-items-mobile-sm', $settings['mob_number'] );
      $this->add_render_attribute( 'slider', 'data-autoplay', $settings['autoplay'] );
      $this->add_render_attribute( 'slider', 'data-loop', $settings['loop'] );
      $this->add_render_attribute( 'slider', 'data-margin', $settings['margin']['size'] );   ?>
 
          <div id="my-carousel" class="blog-carousel owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ) ?>> <?php

        if($wp_query->have_posts()) {
          while ( $wp_query->have_posts() ) {
            $wp_query->the_post();
            $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $wp_query->ID  ), "full" );  ?>
            <div class="iq-blog-box">
              <div class="iq-blog-image"> <?php 
                echo sprintf('<img src="%1$s"/>',esc_url($full_image[0],'iqonic')); ?> <?php 
                if(has_post_thumbnail()) {
                  $postcat = get_the_category();
                  if ($postcat) { ?>
                    <ul class="iq-blogtag"> <?php 
                      foreach($postcat as $cat) {
                        ?><li><a href= <?php echo sprintf("%s",esc_url(get_category_link($cat->cat_ID ),'iqonic'));?> > <?php echo sprintf("%s",esc_html__($cat->name));?> </a></li><?php
                      } ?>
                    </ul> <?php
                  }
                } ?>
              </div>
              <div class="iq-blog-detail">
                <div class="iq-blog-meta">
                  <ul class="list-inline"> <?php
                    //post date
                    $archive_year  = get_the_time('Y',$wp_query->ID);
                    $archive_month = get_the_time('m',$wp_query->ID); 
                    $archive_day   = get_the_time('d',$wp_query->ID);
                    $date=date_create($wp_query->post_date); ?>
                    <li class="list-inline-item">
                      <i class="fa fa-calendar mr-2" aria-hidden="true"></i>
                      <?php echo sprintf("%s",iqonic_blog_time_link()); ?>  
                    </li>
                    <li class="list-inline-item">
                      <a href="<?php echo  sprintf("%s",get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ),'iqonic'); ?>" class="iq-user">
                      <i class="fa fa-user mr-2" aria-hidden="true"></i>
                        <?php echo  sprintf("%s ",get_the_author(),'iqonic'); ?>
                      </a>
                    </li> <?php 
                    if(is_single()) { ?> <?php
                      $comment_count = $post->comment_count; ?>
                      <li class="list-inline-item">
                        <a href="<?php if($comment_count > 0 ) { echo sprintf("%s",comments_link(),'iqonic'); } else { echo sprintf("%s",esc_html__('javascript:void(0)','iqonic')); } ?>">
                        <i class="fa fa-comments mr-2" aria-hidden="true"></i><?php echo sprintf("%s",esc_html($comment_count),'iqonic'); ?> </a>
                      </li> <?php
                    } ?>
                  </ul>
                </div>
                <div class="blog-title">
                  <a class="button-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>">
                    <h5><?php echo sprintf("%s",esc_html__(get_the_title( $wp_query->ID ),'iqonic')); ?></h5>
                  </a>
                </div>
                <p><?php  echo sprintf("%s",get_the_excerpt( $wp_query->ID ) ); ?></p>
                <div class="blog-button"> <?php            
                  if(!empty($iqonic_option['blog_btn'])) { ?>
                    <a class="button-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr($iqonic_option['blog_btn']) );?>   <i class="fa fa-angle-right" aria-hidden="true"></i></a><?php
                  } else { ?>
                    <a class="button-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr('Read More', 'iqonic'));?>   <i class="fa fa-angle-right" aria-hidden="true"></i></a> <?php
                  } ?>
              </div>
              </div>
            </div> <?php
          }
        }
        wp_reset_postdata(); ?>
      </div> <?php 
    } else {

      echo '<div class="row">';
      if($settings['blog_style'] === "2") {
        $col = 'col-lg-12';   
      }
      if($settings['blog_style'] === "3") {
        $col = 'col-lg-6 col-md-6 ';    
      }
      if($settings['blog_style'] === "4") {
        $col = 'col-lg-4 col-md-6';   
      }
      if($wp_query->have_posts()) {
        while ( $wp_query->have_posts() ) {
          $wp_query->the_post();
          $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $wp_query->ID  ), "full" ); ?>

          <div class="<?php echo esc_attr__($col, 'iqonic') ?>">
            <div class="iq-blog-box">
              <div class="iq-blog-image">
                <?php echo sprintf('<img src="%1$s"/>',esc_url($full_image[0],'iqonic')); ?> <?php 
                if(has_post_thumbnail()) {
                  $postcat = get_the_category();
                  if ($postcat) { ?>
                    <ul class="iq-blogtag"> <?php 
                      foreach($postcat as $cat) { ?>
                        <li>
                          <a href= <?php echo sprintf("%s",esc_url(get_category_link($cat->cat_ID ),'iqonic'));?> >     <?php echo sprintf("%s",esc_html__($cat->name));?> 
                          </a>
                        </li> <?php
                      } ?>
                    </ul> <?php 
                  }
                } ?>
              </div>
              <div class="iq-blog-detail">
                <div class="iq-blog-meta">
                  <ul class="list-inline"> <?php
                    //post date
                    $archive_year  = get_the_time('Y',$wp_query->ID);
                    $archive_month = get_the_time('m',$wp_query->ID); 
                    $archive_day   = get_the_time('d',$wp_query->ID);
                    $date = date_create($wp_query->post_date); ?>
                    <li class="list-inline-item"> 
                      <i class="fa fa-calendar mr-2" aria-hidden="true"></i>
                      <?php echo sprintf("%s",iqonic_blog_time_link()); ?>
                    </li>
                    <li class="list-inline-item">
                      <a href="<?php echo  sprintf("%s",get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ),'iqonic'); ?>" class="iq-user">
                      <i class="fa fa-user mr-2" aria-hidden="true"></i>
                        <?php echo  sprintf("%s ",get_the_author(),'iqonic'); ?>
                      </a>
                    </li> <?php 
                    if(is_single()) { ?> <?php
                      $comment_count = $post->comment_count; ?>
                      <li class="list-inline-item">
                        <a href="<?php if($comment_count > 0 ) { echo sprintf("%s",comments_link(),'iqonic'); }else { echo sprintf("%s",esc_html__('javascript:void(0)','iqonic')); } ?>">
                          <i class="fa fa-comments mr-2" aria-hidden="true"></i> <?php 
                          echo sprintf("%s",esc_html($comment_count),'iqonic'); ?>
                        </a>
                      </li>
                      <?php
                    } ?>
                  </ul>
                </div>
                <div class="blog-title">
                  <a class="button-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>">
                    <h5><?php echo sprintf("%s",esc_html__(get_the_title( $wp_query->ID ),'iqonic')); ?></h5>
                  </a>
                </div>
                <p><?php  echo sprintf("%s",get_the_excerpt( $wp_query->ID ) ); ?></p>
                <div class="blog-button"> <?php 
                  if(!empty($iqonic_option['blog_btn'])) {  ?>
                    <a class="button-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr($iqonic_option['blog_btn']) );?>   <i class="fa fa-angle-right" aria-hidden="true"></i></a><?php
                  } else { ?>
                    <a class="button-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr('Read More', 'iqonic'));?>  <i class="fa fa-angle-right" aria-hidden="true"></i></a> <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <?php
        }
      }
        wp_reset_postdata();
      echo '</div>';
  } ?> 
</div>
