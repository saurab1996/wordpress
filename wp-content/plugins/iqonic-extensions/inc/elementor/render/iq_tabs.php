<?php
namespace Elementor;
if (!defined('ABSPATH')) exit;

$tab_nav = '';
$tab_content = '';
$image_html = '';

$tabs = $this->get_settings_for_display('tabs');
$id_int = rand(10, 100);

$align = $settings['align'];

if($settings['iqonic_has_box_shadow'] == 'yes')
{        
        
   $align .= ' iq-box-shadow';
} 



$i = 0;
foreach ($tabs as $index => $item)
{


    if($item['media_style'] == 'image')
    {
        if ( ! empty( $item['image']['url'] ) ) 
        {
            $this->add_render_attribute( 'image', 'src', $item['image']['url'] );
            $this->add_render_attribute( 'image', 'srcset', $item['image']['url'] );
            $this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $item['image'] ) );
            $this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $item['image'] ) );
            $image_html = Group_Control_Image_Size::get_attachment_image_html( $item, 'thumbnail', 'image' );
        }
    }

    if($item['media_style'] == 'icon')
    {
        $image_html = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($item['selected_icon']['value'],'iqonic'));
    }

    if ($i == 0)
    {
        $class = "active";
        $i++;
    }
    else
    {
        $class = "";
    }

    $tab_nav .= '<li class="nav-item">

                    <a class="nav-link ' . esc_attr($class) . '" data-toggle="pill" href="#tabs-' . $index . $id_int . '" role="tab">
                      <div class="media">'.$image_html;
                      if(!empty($item['tab_title']))
                      {
                         $tab_nav.= '<div class="media-body">
                            <h6 class="tab-title">' .esc_html__($item['tab_title']) . '</h6>';
                            if(!empty($item['tab_title_desc']))
                            {
                                $tab_nav.='<p class="mb-0 tab-title-desc">'.esc_html__($item['tab_title_desc']) . '</p>';
                            }
                         $tab_nav.='</div>';
                     }
                    $tab_nav.='</div>
                    </a>
                </li>';

    $tab_content .= '<div class="tab-pane ' . esc_attr($class) . '" id="tabs-' . $index . $id_int . '" role="tabpanel">
                    <div>' . $this->parse_text_editor($item['tab_content']) . '</div>
                  </div>';
    

}

?>
<?php
    if($settings['design_style'] == '1')
    { 
?>


 <div class="iq-tabs iq-tab-horizontal <?php echo esc_attr($align); ?>">
    <ul class="nav nav-pills" id="pills-tab" role="tablist">
        <?php echo $tab_nav; ?>
    </ul>
    <div class="tab-content">
        <?php echo $tab_content; ?>
    </div>
    
 </div>
<?php } 
if($settings['design_style'] == '2')
{

?>

<div class="iq-tabs iq-tab-vertical <?php echo esc_attr($align); ?>">
    <div class="row <?php if($settings['row_reverse'] =='yes'){echo esc_attr("flex-row-reverse");} ?>">
        <div class="col-lg-4">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <?php echo $tab_nav; ?>
            </ul>
        </div>
        <div class="col-lg-8">
            <div class="tab-content">
                <?php echo $tab_content; ?>
            </div>
        </div>    
 </div>
</div>
<?php 
} 
if($settings['design_style'] == '3')
{

?>

<div class="iq-tabs iq-tab-vertical-2 <?php echo esc_attr($align); ?>">
    <div class="row <?php if($settings['row_reverse'] =='yes'){echo esc_attr("flex-row-reverse");} ?>">
        <div class="col-lg-4">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <?php echo $tab_nav; ?>
            </ul>
        </div>
        <div class="col-lg-8">
            <div class="tab-content">
                <?php echo $tab_content; ?>
            </div>
        </div>    
 </div>
</div>
<?php 
} 
