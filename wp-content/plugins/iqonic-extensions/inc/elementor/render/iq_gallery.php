<?php
namespace Elementor; 

use Elementor\Plugin;
if ( ! defined( 'ABSPATH' ) ) exit;
    
	
    $settings = $this->get_settings();
    //$align = $settings['align'];

    if($settings['iqonic_has_box_shadow'] == 'yes')
    {       
            
       $align .= ' iq-box-shadow';
    } 

    $this->add_render_attribute( 'iq_class', 'class', 'iq-masonry' );
    $this->add_render_attribute( 'iq_class', 'class', $settings['design_style'] );

    if($settings['allow_space'] == 'no')
    {
    	$this->add_render_attribute( 'iq_class', 'class', 'no-padding' );
    }
   
 if($settings['media_style'] == 'image')
					  {
							if ( ! empty( $settings['image']['url'] ) ) 
							{
								$this->add_render_attribute( 'image', 'src', $settings['image']['url'] );
								$this->add_render_attribute( 'image', 'alt', Control_Media::get_image_alt( $settings['image'] ) );
								$this->add_render_attribute( 'image', 'title', Control_Media::get_image_title( $settings['image'] ) );
								$image_html = Group_Control_Image_Size::get_attachment_image_html( $settings, 'thumbnail', 'image' );
							}
					 }

					if($settings['media_style'] == 'icon')
					{
						$image_html = sprintf('<i aria-hidden="true" class="%1$s"></i>',esc_attr($settings['selected_icon']['value'],'iqonic'));
					} 
					if($settings['media_style'] == 'none')
					{
						$image_html = "";
					} 
				
		

    ?>
  
<div class="iq-masonry-block <?php echo esc_attr($align);  ?>">
	
	<div <?php echo $this->get_render_attribute_string('iq_class'); ?>>
		<?php 
        foreach ( $settings['iq_carousel'] as $index => $attachment )  {
			$image_html = ''; ?>
		<div class="iq-masonry-item">
			<div class="iq-gallery">
			<a  class="image-link" href="#"> <?php 
			    if(isset($attachment['url'])) { ?>
				    <div class="iq-gallery-img"><img  src="<?php echo esc_url( $attachment['url'] ); ?>" alt="image" /></div> <?php
				} ?>	
				</a>
				 <div class="iq-overbg">
				    <div class="iq-gallery-content"> <?php
					    if(isset($attachment['url'])) { ?>
					        <a class=" popup-img" href="<?php echo esc_url( $attachment['url'] ); ?>">
				                <?php echo $image_html; ?>
			                </a> <?php
						} ?>	
				    </div>
				</div>
			</div>
		</div>
		<?php } ?>

	</div>

</div>
