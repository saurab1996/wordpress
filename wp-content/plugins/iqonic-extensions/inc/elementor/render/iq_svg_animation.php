<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 
$html = '';
    //$this->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );
	$settings = $this->get_settings_for_display();
	

    $this->add_render_attribute( 'render_attribute', 'class', 'bodymovin' );
    $this->add_render_attribute( 'render_attribute', 'id', 'bodymovin' );
    $this->add_render_attribute( 'render_attribute', 'data-bm-path', esc_url($settings['image']['url']) );
    $this->add_render_attribute( 'render_attribute', 'data-bm-renderer', 'svg');
    $this->add_render_attribute( 'render_attribute', 'data-bm-loop', 'true');

    
?>


<div class="iq-svg-animation text-center">
    <div <?php echo $this->get_render_attribute_string('render_attribute'); ?>></div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-user"></i></span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
