<?php
namespace Elementor;
if (!defined('ABSPATH')) exit;

$nav_content = '';
$time_content = '';

$tabs = $this->get_settings_for_display('tabs');
$id_int = rand(10, 100);

$align = ''; 

 
if($settings['iqonic_has_box_shadow'] == 'yes')
{
   $align .= ' iq-box-shadow';
} 

$this->add_render_attribute( 'iq_container', 'class', 'events-content' ); 

if($settings['design_style'] == '1')
{
?>
<div class="iq-timeline iq-timeline-horizontal-1 <?php echo esc_attr( $align ); ?>">
 <div class="cd-horizontal-timeline ">
    <div class="timeline">
        <div class="events-wrapper">
            <div class="events">
                <ol>
                  <?php 
                   $i=0;
                  foreach ($tabs as $index => $item) {

                    $date=date_create($item['timeline_date']);
                    $d = date_format($date,"d/m/Y");
                    $month = date("M", strtotime($item['timeline_date'])); //May
                    $day = date("d", strtotime($item['timeline_date'])); //May
                    $year = date("y", strtotime($item['timeline_date'])); //May
                   
                   
                    if ($i == 0) {
                        $class = "selected";
                    } else {
                        $class = "";
                    }
                    echo '<li>
                        <a href="#" data-date="'.esc_attr( $d ).'" class="'.esc_attr( $class ).'">'.esc_html($day .' '.$month).'</a>
                    </li> ';             
                    $i++; } ?>
                    
                </ol> 
                <span class="filling-line" aria-hidden="true"></span>
            </div>
        </div>
        <ul class="cd-timeline-navigation">
            <li><a href="#" class="prev">Prev</a>
            </li>
            <li><a href="#" class="next">Next</a>
            </li>
        </ul>
    </div>
    <div class="events-content">
        <ol>
            <?php 
            $i=0;
            foreach ($tabs as $index => $item) {

                $date=date_create($item['timeline_date']);
                $d = date_format($date,"d/m/Y");
                
                if ($i == 0)
                {
                    $class = "selected";                        

                }
                else
                {
                    $class = "";
                } ?>

                <li class="<?php echo esc_attr( $class ) ;?>" data-date="<?php echo esc_attr($d);?> ">
                        <<?php echo $settings['title_tag']?> class="timeline-title mb-3"> <?php echo esc_html($item['tab_title']);?> </<?php echo $settings['title_tag']?>>
                         <p class="timeline-content"><?php echo esc_html($item['tab_content']); ?></p>
                     </li>

           <?php   $i++;  }
                
            ?>
        </ol>
    </div>
</div>
</div>
<?php 
}
if($settings['design_style'] == '2')
{
?>
    <div class="iq-timeline iq-timeline-horizontal-2 <?php echo esc_attr( $align ); ?>">
        <div class="timeline" id="iq-timeline-horizontal-2">
          <div class="timeline__wrap">
            <div class="timeline__items">
                <?php
                foreach ($tabs as $index => $item)
                { 
                    $date=date_create($item['timeline_date']);
                    $d = date_format($date,"d/m/Y");
                ?>
              <div class="timeline__item">
                <div class="timeline__content ">
                  <h2 class="timeline_content_date"><?php echo $d; ?></h2>
                  <<?php echo $settings['title_tag']?>  class="timeline-title"><?php echo esc_html($item['tab_title']); ?> </<?php echo $settings['title_tag']?>>
                  <p class="timeline-content"><?php echo esc_html($item['tab_content']); ?></p>
                </div>
              </div> 
              <?php } ?>
            </div>
          </div>
        </div>
    </div>
<?php 
}
