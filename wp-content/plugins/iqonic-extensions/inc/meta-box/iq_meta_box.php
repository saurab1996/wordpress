<?php
add_filter( 'rwmb_meta_boxes', 'iqonic_meta_boxes' );
function iqonic_meta_boxes( $meta_boxes ) {	

	

	// Team Member Details In Class
	$meta_boxes[] = array(
		'title'			=> esc_html__( 'Team Member Details','iqonic' ),
		'post_types'	=> 'iqonicteam',
		'fields'		=> array(
					
			array(
				'id'		=> 'iqonic_team_contact',
				'name'		=> esc_html__( 'Contact Number :','iqonic' ),				
				'type'		=> 'text'				
			),
			array(
				'type'	=>'divider',
			),

			array(
				'id'		=> 'iqonic_team_email',
				'name'		=> esc_html__( 'Email :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'type'	=>'divider',
			),

			array(
				'id'		=> 'iqonic_team_designation',
				'name'		=> esc_html__( 'Designation :','iqonic' ),				
				'type'		=> 'text'				
			),
			array(
				'type'	=>'divider',
			),
			array(
				'id'		=> 'iqonic_team_facebook',
				'name'		=> esc_html__( 'Facebook Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_team_twitter',
				'name'		=> esc_html__( 'Twitter Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_team_google',
				'name'		=> esc_html__( 'Google Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_team_github',
				'name'		=> esc_html__( 'Github Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_team_insta',
				'name'		=> esc_html__( 'Instagram Url :','iqonic' ),				
				'type'		=> 'text'
			),
			
		),
	);
	// Testimonial Member Details In Class
	$meta_boxes[] = array(
		'title'			=> esc_html__( 'Testimonial Member Details','iqonic' ),
		'post_types'	=> 'testimonial',
		'fields'		=> array(
					
			array(
				'id'		=> 'iqonic_testimonial_designation',
				'name'		=> esc_html__( 'Designation :','iqonic' ),				
				'type'		=> 'text'				
			),
			array(
				'id'		=> 'iqonic_testimonial_company',
				'name'		=> esc_html__( 'Company :','iqonic' ),				
				'type'		=> 'text'				
			),
		),
	);
	// Portfolio Member Details In Class
	$meta_boxes[] = array(
		'title'			=> esc_html__( 'Portfolio Project Details','iqonic' ),
		'post_types'	=> 'portfolio',
		'fields'		=> array(
					
			array(
				'id'		=> 'iqonic_portfolio_client',
				'name'		=> esc_html__( 'Created by:','iqonic' ),				
				'type'		=> 'text'				
			),
			array(
				'id'		=> 'iqonic_portfolio_skill',
				'name'		=> esc_html__( 'Skills :','iqonic' ),				
				'type'		=> 'text'				
			),
			array(
				'id' => 'portfolio_data',
				'type' => 'date',
				'name' => esc_html__( 'Project Completed on:', 'iqonic' ),
				'js_options' => array(
				  'dateFormat' => 'dd/mm/yy',
				),
				'class' => 'data-arrivo',
			),

			array(
				'type'	=>'divider',
			),
			array(
				'id'		=> 'iqonic_portfolio_facebook',
				'name'		=> esc_html__( 'Facebook Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_portfolio_twitter',
				'name'		=> esc_html__( 'Twitter Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_portfolio_google',
				'name'		=> esc_html__( 'Google Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_portfolio_github',
				'name'		=> esc_html__( 'Github Url :','iqonic' ),				
				'type'		=> 'text'
			),
			array(
				'id'		=> 'iqonic_portfolio_insta',
				'name'		=> esc_html__( 'Instagram Url :','iqonic' ),				
				'type'		=> 'text'
			),

			array(
				'type'	=>'divider',
			),

			array(
				'id'		=> 'portfolio_detail',
				'name'		=> esc_html__( 'Project Link :','iqonic' ),				
				'type'		=> 'text'
			),

		),
	);
	return $meta_boxes;
}
