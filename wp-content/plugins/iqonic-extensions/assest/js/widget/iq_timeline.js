(function(jQuery) {
    "use strict";
    jQuery(document).ready(function() {
        jQuery('#iq-timeline-horizontal-2.timeline ').timeline({
            forceVerticalMode: 800,
            mode: 'horizontal',
            visibleItems: 3
        });

        jQuery('#iq-timeline-vertical-2.timeline ').timeline({
            forceVerticalMode: 800,
             mode: 'vertical',
            visibleItems: 2
        });
    });
})(jQuery);