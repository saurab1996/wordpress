/*
Template: Xamin - Data science WordPress landing Page
Author: iqonicthemes.in
Version: 1.0
Design and Developed by: iqonicthemes.in
*/

/*----------------------------------------------
Index Of Script
------------------------------------------------

1.Page Loader
2.Isotope
3.Masonry
4.Slick
5.Swiper
6.Progress Bar
7.Counter
8.Coming soon
6.Timer
7.Back To Top
8.Accordion
9.Magnific Popup
10.Owl Carousel
11.Wow Animation
12.Skrollr
13.Tab

------------------------------------------------
Index Of Script
----------------------------------------------*/
(function(jQuery) {

    "use strict";
    jQuery(document).ready(function() {

        jQuery(window).on('load', function(e) {

             /*------------------------
                 Tool tip
            --------------------------*/

                // set the wrapper width and height to match the img size
                jQuery('#iq-tooltip').css({'width':jQuery('#iq-tooltip img').width(),
                    'height':jQuery('#iq-tooltip img').height()
                });

                //   jQuery('.map-marker').tooltip({
                //     animated: 'fade',
                //     placement: 'top',    
                //     html: true,
                //     container: 'body'
                    
                // });

              jQuery(".dot").popover({ trigger: "manual" , html: true, animation:false})
                .on("mouseenter", function () {
                    var _this = this;
                    jQuery(this).popover("show");
                    jQuery(".popover").on("mouseleave", function () {
                        jQuery(_this).popover('hide');
                    });
                }).on("mouseleave", function () {
                    var _this = this;
                    setTimeout(function () {
                        if (!jQuery(".popover:hover").length) {
                            jQuery(_this).popover("hide");
                        }
                    }, 300);
            });
                
                //tooltip direction
                var tooltipDirection;

                             
                for (var i=0; i<jQuery(".pin").length; i++) {               
                    // set tooltip direction type - up or down    


                    if (jQuery(".pin").eq(i).hasClass('pin-down')) {
                        tooltipDirection = 'tooltip-down  tooltip-down-'+i;
                    } else {
                        tooltipDirection = 'tooltip-up';
                    }
                    // append the tooltip
                    jQuery("#iq-tooltip").append(
                        "<div style='left:"+jQuery(".pin").eq(i).data('xpos')+"px;top:"+jQuery(".pin").eq(i).data('ypos')+"px' class='" + tooltipDirection +"'>\
                                <div class='tooltip'>" + jQuery(".pin").eq(i).html() + "</div>\
                        </div>"
                    );
                }    
                
                // show/hide the tooltip
                jQuery('.tooltip-up, .tooltip-down').mouseenter(function() {
                            jQuery(this).children('.tooltip').fadeIn(100);
                        }).mouseleave(function(){
                            jQuery(this).children('.tooltip').fadeOut(100);
                });
            /*----------------
            Counter
            ---------------------*/
            jQuery('.timer').countTo();


            /*------------------------
            Isotope
            --------------------------*/
            jQuery('.isotope').each(function() {
                jQuery('.isotope').isotope({
                    itemSelector: '.iq-grid-item',
                });
            });    

            /*------------------------------
            filter items on button click
            -------------------------------*/
            jQuery('.isotope-filters').on('click', 'button', function() {
                var filterValue = jQuery(this).attr('data-filter');
                jQuery('.isotope').isotope({
                    resizable: true,
                    filter: filterValue
                });
                jQuery('.isotope-filters button').removeClass('show active');
                jQuery(this).addClass('show active');
            });


            /*------------------------
            Masonry
            --------------------------*/
            jQuery('.iq-masonry-block .iq-masonry').each(function() {
                var jQuerymsnry = jQuery('.iq-masonry-block .iq-masonry');
                if (jQuerymsnry) {
                    var jQueryfilter = jQuery('.iq-masonry-block .isotope-filters');
                    jQuerymsnry.isotope({
                        percentPosition: true,
                        resizable: true,
                        itemSelector: '.iq-masonry-block .iq-masonry-item',
                        masonry: {
                            gutterWidth: 0
                        }
                    });
                    // bind filter button click
                    jQueryfilter.on('click', 'button', function() {
                        var filterValue = jQuery(this).attr('data-filter');
                        jQuerymsnry.isotope({
                            filter: filterValue
                        });
                    });

                    jQueryfilter.each(function(i, buttonGroup) {
                        var jQuerybuttonGroup = jQuery(buttonGroup);
                        jQuerybuttonGroup.on('click', 'button', function() {
                            jQuerybuttonGroup.find('.active').removeClass('active');
                            jQuery(this).addClass('active');
                        });
                    });
                }
            });

            /*------------------------
            Effect Box
            --------------------------*/

           jQuery(".effect-box .effect-btn").click(function() {
                jQuery(this).parent().toggleClass("main");
            });


            /*------------------------
            Progress Bar
            --------------------------*/
            jQuery('.iq-progress-bar > span').each(function() {
                    
                var jQuerythis = jQuery(this);
                var width = jQuery(this).data('percent');
                jQuerythis.css({
                    'transition': 'width 2s'
                });
                jQuery('.progress-value').css({'transition': 'margin 2s'});

                setTimeout(function() {
                    jQuerythis.appear(function() {
                        jQuerythis.css('width', width + '%');
                    });
                }, 500);

                setTimeout(function() {
                    jQuery('.iq-progressbar-style-2 .progress-value').appear(function() {
                        jQuery('.iq-progressbar-style-2 .progress-value').css('margin-left', width + 'px');
                    });
                }, 500);

                setTimeout(function() {
                    jQuery('.iq-progressbar-style-3 .progress-tooltip').appear(function() {
                        jQuery('.iq-progressbar-style-3 .progress-tooltip').css('margin-left', width + 'px');
                    });
                }, 500);

                
            });
          

            /*------------------------
            Accordion
            --------------------------*/
            jQuery('.iq-accordion .iq-accordion-block .accordion-details').hide();
            jQuery('.iq-accordion .iq-accordion-block:first').addClass('accordion-active').children().slideDown('slow');
            jQuery('.iq-accordion .iq-accordion-block').on("click", function() {
                if (jQuery(this).children('div.accordion-details ').is(':hidden')) {
                    jQuery('.iq-accordion .iq-accordion-block').removeClass('accordion-active').children('div.accordion-details ').slideUp('slow');
                    jQuery(this).toggleClass('accordion-active').children('div.accordion-details ').slideDown('slow');
                }
            });

           /*------------------------
            Accordion
            --------------------------*/
            jQuery('.iq-faq .iq-block .iq-details').hide();
            jQuery('.iq-faq .iq-block:first').addClass('iq-active').children().slideDown('slow');
            jQuery('.iq-faq .iq-block').on("click", function() {
                if (jQuery(this).children('div').is(':hidden')) {
                    jQuery('.iq-faq .iq-block').removeClass('iq-active').children('div').slideUp('slow');
                    jQuery(this).toggleClass('iq-active').children('div').slideDown('slow');
                }
            });
            /*------------------------
            Magnific Popup
            --------------------------*/
            jQuery('.popup-gallery').magnificPopup({
                delegate: 'a.popup-img',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                        return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                    }
                }
            });


            jQuery('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });

             /*------------------------
             Background images  Effect
            --------------------------*/
            jQuery(".iq_background_list_wrapper").each(function() {
                var parentDiv = jQuery(this);

                parentDiv.children('.iq_background_list_column').hover(function() {
                    parentDiv.find('.iq_background_list_column').removeClass('hover');
                    parentDiv.find('.iq_background_img').removeClass('hover');
                    jQuery(this).next('.iq_background_img').addClass('hover');
                    jQuery(this).addClass('hover');

                });


            });
        /*------------------------
        Tabs
        --------------------------*/
        // jQuery(window).on('scroll', function(e) {
        //     var nav = jQuery('#pills-tab');
        //     if (nav.length) {
        //         var contentNav = nav.offset().top - window.outerHeight;
        //         if (jQuery(window).scrollTop() >= (contentNav)) {
        //             e.preventDefault();
        //             jQuery('#pills-tab li a').removeClass('active');
        //             jQuery('#pills-tab li a[aria-selected=true]').addClass('active');
        //         }
        //     }
        // });
        jQuery(window).on('scroll', function(e) {
            var nav = jQuery('#features');
            if (nav.length) {
                var contentNav = nav.offset().top - window.outerHeight;
                if (jQuery(window).scrollTop() >= (contentNav)) {
                    e.preventDefault();
                    jQuery('#features .row li a').removeClass('active');
                    jQuery('#features .row li a[aria-selected=true]').addClass('active');
                }
            }
        });

        /*---------------------------
        Tabs
        ---------------------------*/
        jQuery(document).ready(function(){
            var a=jQuery('.nav.nav-pills').each(function(){
                var b =jQuery(this).find('a.active').attr('href');
                activaTab(b);
            })
        });

        function activaTab(pill){
            jQuery(pill).addClass('active show');
        };

        jQuery('.nav.nav-pills').click(function(){
            jQuery('.nav.nav-pills').children('.tab-title-desc').slideDown('slow');
            
        });

        
             /*----------------
                    Count Down Timer
            ---------------------*/
                jQuery('.iq-data-countdown-timer').each(function() {

                    var future_date = jQuery(this).attr('data-date') ;
                    var label = jQuery(this).attr('data-labels') ;
                    var displayFormat = jQuery(this).attr('data-format') ;
                    var l=true;
                    if(label == "true")
                    {
                        l= true;
                    }
                    else
                    {
                        l = false;
                    }
                        jQuery(this).countdowntimer({
                        dateAndTime : future_date,
                        labelsFormat : l,                
                        displayFormat : displayFormat,

                    });
          
                }); 
        });

    });
          
    
})(jQuery);
