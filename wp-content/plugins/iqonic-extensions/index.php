<?php 
/*
Plugin Name: Iqonic Extensions	
Plugin URI: http://iqonicthemes.com/
Description: iqonic plugin provides custom team post type, gallery post type with related functionality.
Author: iqonicthemes
Version: 2.4
Author URI: http://www.goldenmace.com/
Text Domain: iqonic
*/

if( !defined( 'IQ_TH_ROOT' ) ) 
	define('IQ_TH_ROOT', plugin_dir_path( __FILE__ ));

if( !defined( 'IQ_TH_URL' ) ) 
	define( 'IQ_TH_URL', plugins_url( '', __FILE__ ) );

if( !defined( 'IQ_NAME' ) ) 
	define( 'IQ_NAME', 'iqonic' );




// Register Team Member custom post type
add_action( 'init', 'iqonic_team' );
function iqonic_team() {
	$labels = array(
		'name'                  => esc_html__( 'Team Member', 'post type general name', 'iqonic' ),
		'singular_name'         => esc_html__( 'Team Member', 'post type singular name', 'iqonic' ),
		'featured_image'        => esc_html__( 'Team Member Photo', 'iqonic'  ),
		'set_featured_image'    => esc_html__( 'Set Team Member Photo', 'iqonic'  ),
		'remove_featured_image' => esc_html__( 'Remove Team Member Photo', 'iqonic'  ),
		'use_featured_image'    => esc_html__( 'Use as Team Member Photo', 'iqonic'  ),
		'menu_name'             => esc_html__( 'Our Team', 'admin menu', 'iqonic' ),
		'name_admin_bar'        => esc_html__( 'Team Member', 'add new on admin bar', 'iqonic' ),
		'add_new'               => esc_html__( 'Add New', 'Team Member', 'iqonic' ),
		'add_new_item'          => esc_html__( 'Add New Team Member', 'iqonic' ),
		'new_item'              => esc_html__( 'New Team Member', 'iqonic' ),
		'edit_item'             => esc_html__( 'Edit Team Member', 'iqonic' ),
		'view_item'             => esc_html__( 'View Team Member', 'iqonic' ),
		'all_items'             => esc_html__( 'All Team Members', 'iqonic' ),
		'search_items'          => esc_html__( 'Search Team Member', 'iqonic' ),
		'parent_item_colon'     => esc_html__( 'Parent Team Member:', 'iqonic' ),
		'not_found'             => esc_html__( 'No Classs found.', 'iqonic' ),
		'not_found_in_trash'    => esc_html__( 'No Classs found in Trash.', 'iqonic' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'iqonicteam' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-businessman',
		'supports'           => array( 'title', 'thumbnail' ,'editor' ,'excerpt')
	);

	register_post_type( 'iqonicteam', $args );
}


// Register Portfolio custom post type
add_action( 'init', 'iqonic_portfolio' );
function iqonic_portfolio() {
	$labels = array(
		'name'                  => esc_html__( 'Portfolio', 'post type general name', 'iqonic' ),
		'singular_name'         => esc_html__( 'Portfolio', 'post type singular name', 'iqonic' ),
		'featured_image'        => esc_html__( 'Portfolio Photo', 'iqonic'  ),
		'set_featured_image'    => esc_html__( 'Set Portfolio Photo', 'iqonic'  ),
		'remove_featured_image' => esc_html__( 'Remove Portfolio Photo', 'iqonic'  ),
		'use_featured_image'    => esc_html__( 'Use as Portfolio Photo', 'iqonic'  ),
		'menu_name'             => esc_html__( 'Portfolio', 'admin menu', 'iqonic' ),
		'name_admin_bar'        => esc_html__( 'Portfolio', 'add new on admin bar', 'iqonic' ),
		'add_new'               => esc_html__( 'Add New', 'Portfolio', 'iqonic' ),
		'add_new_item'          => esc_html__( 'Add New Portfolio', 'iqonic' ),
		'new_item'              => esc_html__( 'New Portfolio', 'iqonic' ),
		'edit_item'             => esc_html__( 'Edit Portfolio', 'iqonic' ),
		'view_item'             => esc_html__( 'View Portfolio', 'iqonic' ),
		'all_items'             => esc_html__( 'All Portfolio', 'iqonic' ),
		'search_items'          => esc_html__( 'Search Portfolio', 'iqonic' ),
		'parent_item_colon'     => esc_html__( 'Parent Portfolio :', 'iqonic' ),
		'not_found'             => esc_html__( 'No Classs found.', 'iqonic' ),
		'not_found_in_trash'    => esc_html__( 'No Classs found in Trash.', 'iqonic' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'show_in_nav_menus'   => TRUE,
		'has_archive'        => true,
		'hierarchical'       => false,		
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-category',
		'supports'           => array( 'title', 'editor', 'thumbnail','category','tag')
	);

	register_post_type( 'portfolio', $args );
}


// Custom taxonomy
add_action( 'after_setup_theme', 'iqonic_custom_taxonomy' );
function iqonic_custom_taxonomy() {
	$labels = '';
	register_taxonomy(
		'portfolio-categories',
		'portfolio',
		array(
			'label' => esc_html__( 'Portfolio Category', 'iqonic' ),
			'rewrite' => true,
			'hierarchical' => true,
		)
	);
	register_taxonomy('portfolio-tag','portfolio',array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'tag' ),
	));
}

// Register Testimonial type custom post
add_action( 'init', 'iqonic_testimonial_gallery' );
function iqonic_testimonial_gallery() {
	$labels = array(
		'name'               => esc_html__( 'Testimonial', 'post type general name', 'iqonic' ),
		'singular_name'      => esc_html__( 'Testimonial', 'post type singular name', 'iqonic' ),
		'featured_image'        => esc_html__( 'Photo', 'iqonic'  ),
		'set_featured_image'    => esc_html__( 'Set Photo', 'iqonic'  ),
		'remove_featured_image' => esc_html__( 'Remove Photo', 'iqonic'  ),
		'use_featured_image'    => esc_html__( 'Use as Photo', 'iqonic'  ),
		'menu_name'          => esc_html__( 'Testimonial', 'admin menu', 'iqonic' ),
		'name_admin_bar'     => esc_html__( 'Testimonial', 'add new on admin bar', 'iqonic' ),
		'add_new'            => esc_html__( 'Add New', 'Testimonial', 'iqonic' ),
		'add_new_item'       => esc_html__( 'Add New Testimonial', 'iqonic' ),
		'new_item'           => esc_html__( 'New Testimonial', 'iqonic' ),
		'edit_item'          => esc_html__( 'Edit Testimonial', 'iqonic' ),
		'view_item'          => esc_html__( 'View Testimonial', 'iqonic' ),
		'all_items'          => esc_html__( 'All Testimonial', 'iqonic' ),
		'search_items'       => esc_html__( 'Search Testimonial', 'iqonic' ),
		'parent_item_colon'  => esc_html__( 'Parent Testimonial:', 'iqonic' ),
		'not_found'          => esc_html__( 'No Testimonial found.', 'iqonic' ),
		'not_found_in_trash' => esc_html__( 'No Testimonial found in Trash.', 'iqonic' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'testimonial' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-format-gallery',
		'supports'           => array( 'title', 'editor', 'thumbnail')
	);

	register_post_type( 'testimonial', $args );
}

// Register Team Member custom post type
add_action( 'init', 'iqonic_client' );
function iqonic_client() {
	$labels = array(
		'name'                  => esc_html__( 'Client Member', 'post type general name', 'iqonic' ),
		'singular_name'         => esc_html__( 'Client Member', 'post type singular name', 'iqonic' ),
		'featured_image'        => esc_html__( 'Client Member Photo', 'iqonic'  ),
		'set_featured_image'    => esc_html__( 'Set Client Member Photo', 'iqonic'  ),
		'remove_featured_image' => esc_html__( 'Remove Client Member Photo', 'iqonic'  ),
		'use_featured_image'    => esc_html__( 'Use as Client Member Photo', 'iqonic'  ),
		'menu_name'             => esc_html__( 'Our Client', 'admin menu', 'iqonic' ),
		'name_admin_bar'        => esc_html__( 'Client Member', 'add new on admin bar', 'iqonic' ),
		'add_new'               => esc_html__( 'Add New', 'Client Member', 'iqonic' ),
		'add_new_item'          => esc_html__( 'Add New Client Member', 'iqonic' ),
		'new_item'              => esc_html__( 'New Client Member', 'iqonic' ),
		'edit_item'             => esc_html__( 'Edit Client Member', 'iqonic' ),
		'view_item'             => esc_html__( 'View Client Member', 'iqonic' ),
		'all_items'             => esc_html__( 'All Client Members', 'iqonic' ),
		'search_items'          => esc_html__( 'Search Client Member', 'iqonic' ),
		'parent_item_colon'     => esc_html__( 'Parent Client Member:', 'iqonic' ),
		'not_found'             => esc_html__( 'No Classs found.', 'iqonic' ),
		'not_found_in_trash'    => esc_html__( 'No Classs found in Trash.', 'iqonic' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'client' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-businessman',
		'supports'           => array( 'title', 'thumbnail' ,'editor' ,'excerpt')
	);

	register_post_type( 'client', $args );
}

require_once(IQ_TH_ROOT . 'inc/elementor/init.php');

require_once(IQ_TH_ROOT . 'inc/helper/helperfunction.php');

//Metabox Custom Field Framework
/* if( class_exists( 'RW_Meta_Box' ) ){ */

/*if ( defined( 'ABSPATH' ) && defined( 'RW_Meta_Box' ) ) {*/
	//require_once( IQ_TH_ROOT . '/inc/meta-box/iq_meta_box.php' );
/*}*/

require_once( IQ_TH_ROOT . '/inc/meta-box/iq_meta_box.php' );



/*---------------------------------------
		iqonic admin enque
---------------------------------------*/

function iqonic_plugin_script() {

	
	wp_enqueue_script('popper-min', IQ_TH_URL .'/assest/js/popper.min.js', array(), '1.0.0' , true);
	//wp_enqueue_script('owl.carousel', IQ_TH_URL .'/assest/js/owl.carousel.min.js', array(), '1.0.0' , true);
	wp_enqueue_script('iqonic-extenstion', IQ_TH_URL .'/assest/js/iqonic-extenstion.js', array(), '1.0.0' , true);

	wp_enqueue_style('flaticon', IQ_TH_URL.'/assest/css/flaticon.css',array(), '1.0.0', 'all');
	//wp_enqueue_style('owl.carousel', IQ_TH_URL.'/assest/css/owl.carousel.min.css',array(), '1.0.0', 'all');
	wp_enqueue_style( 'iqonic-extenstion', IQ_TH_URL . '/assest/css/iqonic-extenstion.css', false, '1.0.0' );
}

add_action( 'wp_enqueue_scripts', 'iqonic_plugin_script'  );
