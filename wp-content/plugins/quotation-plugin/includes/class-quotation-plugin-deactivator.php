<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    Quotation_Plugin
 * @subpackage Quotation_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Quotation_Plugin
 * @subpackage Quotation_Plugin/includes
 * @author     Saurab <saurab1234gupt@gmail.com>
 */
class Quotation_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
