<?php

/**
 * Fired during plugin activation
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    Quotation_Plugin
 * @subpackage Quotation_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Quotation_Plugin
 * @subpackage Quotation_Plugin/includes
 * @author     Saurab <saurab1234gupt@gmail.com>
 */
class Quotation_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
