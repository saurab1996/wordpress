<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    Quotation_Plugin
 * @subpackage Quotation_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Quotation_Plugin
 * @subpackage Quotation_Plugin/admin
 * @author     Saurab <saurab1234gupt@gmail.com>
 */
class Quotation_Plugin_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Quotation_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Quotation_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/quotation-plugin-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Quotation_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Quotation_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/quotation-plugin-admin.js', array( 'jquery' ), $this->version, false );

	}
	public function quotation_admin_menu()
	{
		
		add_menu_page('Quotation', 'Quotation Plugin', 'manage_options','Quotation', array($this,'quotation_main_menu'));
     	add_submenu_page('Quotation', 'dashboard','dashboard','manage_options','quotation-dashboard',array($this,'quotation_sub_menu'));
		add_submenu_page('	Quotation', 'dashboard2','dashboard2','manage_options','quotation-dashboard2',array($this,'quotation_sub_menu'));
        
	}
    public function quotation_main_menu()
	{
		include_once(plugin_dir_path( __FILE__ ) .'../extra/html/QuotationSettingPage.php');
	}
	public function quotation_sub_menu()
	{
		 echo "welcome to quotation dashboard";
	}
	public function quotation_admin_custom_setting()
	{   if (false == get_option('quotation_option')) {
        add_option('quotation_option');}
		add_settings_section('quotation_section','Quotation Setting','', 'quotation');
		register_setting('quotation','quotation_option');
		add_settings_field('quotation_field','Quotation Field',array($this,'quotation_setting_field_callback'),'quotation','quotation_section');
	}
	public function quotation_setting_field_callback()
	{  
		include_once(plugin_dir_path( __FILE__ ) .'../extra/html/settingform.php');
	}
	//function to remove wordpress admin footer
	public function remove_admin_footer()
	{
		add_filter('admin_footer_text', array($this,'custom_footer_info')); 
		add_filter('update_footer', array($this,'custom_footer_version'));
	}
    public function custom_footer_info()
	{
		$text = sprintf(
			/* translators: %s: https://wordpress.org/ */
			__( 'Thank for creating with  <a href="%s">Saurab.com</a>.' ),
			__( 'http://wordpress.test/' ) 
		      );
		echo  $text;
	}
	public function custom_footer_version()
	{
		echo "Version 1.0.0";
	}
	
}

