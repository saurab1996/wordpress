<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wordpress.test
 * @since             1.0.0
 * @package           Recent_Product
 *
 * @wordpress-plugin
 * Plugin Name:       recent product 
 * Plugin URI:        http://wordpress.test
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Saurab
 * Author URI:        http://wordpress.test
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       recent-product 
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('PLUGIN_PATH',plugin_dir_path(__file__));

define('PLUGIN_URL',plugin_dir_url(__file__));

define('PLUGIN_NAME','Product Option');

define('PLUGIN_BASE_NAME',plugin_basename(__FILE__));
define( 'RECENT_PRODUCT_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-recent-product -activator.php
 */
function activate_recent_product () {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-recent-product -activator.php';
	Recent_Product_Activator::activate();
}


/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-recent-product -deactivator.php
 */
function deactivate_recent_product () {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-recent-product -deactivator.php';
	Recent_Product_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_recent_product ' );
register_deactivation_hook( __FILE__, 'deactivate_recent_product ' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-recent-product .php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_recent_product () {

	$plugin = new Recent_Product();
	$plugin->run();

}
run_recent_product ();
