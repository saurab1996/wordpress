<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://wordpress.test
 * @since      1.0.0
 *
 * @package    Recent_Product
 * @subpackage Recent_Product/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Recent_Product
 * @subpackage Recent_Product/includes
 * @author     Saurab <saurab1234gupt@gmail.com>
 */
class Recent_Product_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
