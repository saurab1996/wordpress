<?php

/**
 * Fired during plugin activation
 *
 * @link       http://wordpress.test
 * @since      1.0.0
 *
 * @package    Recent_Product
 * @subpackage Recent_Product/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Recent_Product
 * @subpackage Recent_Product/includes
 * @author     Saurab <saurab1234gupt@gmail.com>
 */
class Recent_Product_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
