<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://wordpress.test
 * @since      1.0.0
 *
 * @package    Recent_Product
 * @subpackage Recent_Product/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Recent_Product
 * @subpackage Recent_Product/admin
 * @author     Saurab <saurab1234gupt@gmail.com>
 */
class Recent_Product_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $version ) {

        $this->version = $version;
        $this->plugin_name =PLUGIN_BASE_NAME;

        //add_action( 'init', $this->create_vip_order() );
       // add_action('woocommerce_thankyou', $this->wh_test_1(), 10, 1);




    }
	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Recent_Product_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Recent_Product_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/recent-product -admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Recent_Product_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Recent_Product_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/recent-product -admin.js', array( 'jquery' ), $this->version, false );

	}

	public function product_option_admin_menu(){
	    add_menu_page('product option','Product option','manage_options','product_options_setting',array($this,'plugin_menu_callback_function'));
       add_filter( "plugin_action_links_$this->plugin_name",array($this,'settings_link'));
    }

    function settings_link($links){
        $settings_link = '<a href="'.'admin.php?page=product_options_setting'.'">Settings</a>';
        array_unshift( $links, $settings_link );
        return $links;
    }
    public function plugin_menu_callback_function(){
	  ?>
		<div class="wrap">
		<h1>Welcome Woo Recent Product</h1>

       <?php settings_errors(); ?>
  <form action="options.php" method="post">
  <?php

    settings_fields( 'woo_recent_product');
    do_settings_sections('product_options_setting');
    submit_button('save settingss');
    //submit_button('save settingss');



    ?>
  </form>
        </div>
<?php
    }

    public function product_option_admin_custom_setting()
    {
        {
            if (false == get_option('woo_recent_product')) {
                add_option('woo_recent_product');
            }
            {
                if (false == get_option('woo_recent_product1')) {
                    add_option('woo_recent_product1');
                }
                add_settings_section('recent_product_section', 'recent product section', '', 'product_options_setting');
                register_setting('woo_recent_product', 'woo_recent_product');
                register_setting('woo_recent_product1', 'woo_recent_product1');
                add_settings_field('woo_recent_product_field', '', array($this, 'woo_recent_product_field_callback'), 'product_options_setting', 'recent_product_section');
            }
        }
    }

    public function woo_recent_product_field_callback(){
$options = get_option( 'woo_recent_product' );

$value = array();
if (isset($options['woo_recent_product']) && ! empty($options['woo_recent_product'])) {
    $value = $options['woo_recent_product'];
}
?>
        <tr>
                           <th>
                               <label for="woo_recent_product_label">
                               <?php _e('Recently Viewed Product Label','product_option'); ?>
                               </label>
                           </th>
                           <td>
                               <input id="woo_recent_product_label" name="woo_recent_product[label]" type="text" value="<?php echo $options['label'] ?>" required>
                           </td>
                       </tr>

                        <tr>
                            <th>
                                <label for="woo_recent_product_no_of_product">
                                    <?php _e('Number Of Recently Viewed Product','product_option'); ?>
                                </label>
                            </th>
                            <td>
                                <input id="woo_recent_product_no_of_product" name="woo_recent_product[product_no]" value="<?php echo $options['product_no'] ?>" type="number" required>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label for="woo_recent_product_position">
                                    <?php _e(' Recently Viewed Product Position In Product Page','product_option'); ?>
                                </label>
                            </th>
                            <td>
                                <input id=" woo_recent_product_position" name=" woo_recent_product['before_product']" type="checkbox" value=" <?php if ($options['before_product']==true) echo 'checked="checked" '; ?>" > Before related product
                                    <input id="woo_recent_product_position" name="woo_recent_product['before_product']" type="checkbox"  value=" <?php if ($options['after_product']==true) echo 'checked="checked" '; ?> "> after related product
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label for="woo_recent_product_in_show_page">
                                    <?php _e('Product Option In Show Page','product_option'); ?>
                                </label>
                            </th>
                            <td>
                                <input id="woo_recent_product_in_show_page" name="woo_recent_product['show_page']" value="enabled" type="checkbox" required>
                            </td>
                        </tr>

                        <tr>
                            <th>
                                <label for="product_option_in_cart_page">
                                    <?php _e('Product Option In Cart Page','product_option'); ?>
                                </label>
                            </th>
                            <td>
                                <input id="product_option_in_cart_page" name="woo_recent_product['cart_page']" value="enabled" type="checkbox" required>
                            </td>
                        </tr>


<?php

}
   function addnewoption(){
       masvideos_wp_checkbox(
           array(
               'id'            => '_featured',
               'value'         => is_callable(  'get_featured'  ),
               'label'         => __( 'Featured', 'masvideos' ),
               'description'   => __( 'LiveStream.', 'masvideos' ),
           )
       );
   }

   function addnewoption1($tabs ){
	    $tabs=array('linked_movie'  => array(
           'label'    => __( 'saurab Movies', 'masvideos' ),
           'target'   => 'linked_movie_data',
           'class'    => array(),
           'priority' => 20,
        ));
	    return $tabs;

   }




}