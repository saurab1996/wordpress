/*
Template: Streamit - Responsive Bootstrap 4 Template
Author: iqonicthemes.in
Design and Developed by: iqonicthemes.in
NOTE: This file contains the styling for responsive Template.
*/

/*----------------------------------------------
Index Of Script
------------------------------------------------

:: Sticky Header Animation & Height
:: Back to Top
:: Header Menu Dropdown
:: Slick Slider
:: Owl Carousel
:: Page Loader
:: Mobile Menu Overlay
:: Equal Height of Tab Pane
:: Active Class for Pricing Table
:: Select 2 Dropdown
:: Video Popup
:: Flatpicker
:: Custom File Uploader

------------------------------------------------
Index Of Script
----------------------------------------------*/

(function (jQuery) {
	"use strict";
	jQuery(document).ready(function() {

		window.onscroll = function(){
			jQuery(document).find('.nav-link[aria-selected="true"]').addClass("active");
		};

		/*---------------------------------------------------------------------
			Sticky Header Animation & Height
		----------------------------------------------------------------------- */
		// function headerHeight() {
		// 	var height = jQuery("#main-header").height();
		// 	jQuery('.iq-height').css('height', height + 'px');
		// }
		// jQuery(function() {
		// 	var header = jQuery("#main-header"),
		// 		yOffset = 0,
		// 		triggerPoint = 80;

		// 	headerHeight();

		// 	jQuery(window).resize(headerHeight);
		// 	jQuery(window).on('scroll', function() {

		// 		yOffset = jQuery(window).scrollTop();

		// 		if (yOffset >= triggerPoint) {
		// 			header.addClass("menu-sticky animated slideInDown");
		// 		} else {
		// 			header.removeClass("menu-sticky animated slideInDown");
		// 		}

		// 	});
		// });


		/*---------------------------------------------------------------------
			Slick Slider
		----------------------------------------------------------------------- */
		jQuery('#home-slider').slick({
			autoplay: false,
			cssEase: 'ease-in-out',
            speed: 500,
			// speed: 800,
			lazyLoad: 'progressive',
			arrows: true,
			dots: false,
			prevArrow: '<div class="slick-nav prev-arrow"><i></i><svg><use xlink:href="#circle"></svg></div>',
			nextArrow: '<div class="slick-nav next-arrow"><i></i><svg><use xlink:href="#circle"></svg></div>',
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						dots: true,
						arrows: false,
					}
				}
			]
		}).slickAnimation();
		jQuery('.slick-nav').on('click touch', function (e) {

			e.preventDefault();

			var arrow = jQuery(this);

			if (!arrow.hasClass('animate')) {
				arrow.addClass('animate');
				setTimeout(() => {
					arrow.removeClass('animate');
				}, 1600);
			}

		});
		jQuery('.favorites-slider').slick({
			dots: false,
			arrows: true,
			infinite: false,
			speed: 300,
			autoplay: false,
			slidesToShow: 4,
			nextArrow: '<a href="#" class="slick-arrow slick-next"><i class= "fa fa-chevron-right"></i></a>',
			prevArrow: '<a href="#" class="slick-arrow slick-prev"><i class= "fa fa-chevron-left"></i></a>',
			responsive: [
			{
				breakpoint: 1200,
				settings: {
				slidesToShow: 3,
				infinite: true,
				dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
				slidesToShow: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
				slidesToShow: 2
				}
			}
			]
		});

		jQuery('#top-ten-slider').slick({
			slidesToShow: 1,
			arrows: false,
			fade: true,
			asNavFor: '#top-ten-slider-nav',
			responsive: [
			{
				breakpoint: 992,
				settings: {
				asNavFor: false,
				arrows: true,
				nextArrow: '<button class="NextArrow"><i class="ri-arrow-right-s-line"></i></button>',
				prevArrow: '<button class="PreArrow"><i class="ri-arrow-left-s-line"></i></button>',
				}
			}
			]
		});
		jQuery('#top-ten-slider-nav').slick({
			slidesToShow: 3,
			asNavFor: '#top-ten-slider',
			dots: false,
			arrows: true,
			infinite: true,
			vertical:true,
			verticalSwiping: true,
			centerMode: false,
			nextArrow:'<button class="NextArrow"><i class="ri-arrow-down-s-line"></i></button>',
			prevArrow:'<button class="PreArrow"><i class="ri-arrow-up-s-line"></i></button>',
			focusOnSelect: true,
			responsive: [		    
				{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
				}
				},
				{
					breakpoint: 600,
					settings: {
						asNavFor: false,
					}
				},
			]
		});

		jQuery('#episodes-slider2').slick({
			dots: false,
			arrows: true,
			infinite: false,
			speed: 300,
			autoplay: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				dots: true,
				}
			},
			{
				breakpoint: 600,
				settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				}
			},
			{
				breakpoint: 480,
				settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				}
			}
			]
		});

		jQuery('#episodes-slider3').slick({
			dots: false,
			arrows: true,
			infinite: false,
			speed: 300,
			autoplay: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				dots: true,
				}
			},
			{
				breakpoint: 600,
				settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				}
			},
			{
				breakpoint: 480,
				settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				}
			}
			]
		});

		jQuery('#trending-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,		 
			arrows: true,
			fade: true,
			draggable:false,
			swipe: false,
            touchMove: false,
			asNavFor: '#trending-slider-nav',
			nextArrow: '<a href="#" class="slick-arrow slick-next"><i class= "fa fa-chevron-right"></i></a>',
			prevArrow: '<a href="#" class="slick-arrow slick-prev"><i class= "fa fa-chevron-left"></i></a>',	
		});
		jQuery('#trending-slider-nav').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: '#trending-slider',
			dots: false,
			arrows: false,
			infinite: true,
			centerMode: true,
			centerPadding:0,
			focusOnSelect: true,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				}
			},
			{
				breakpoint: 600,
				settings: {
				slidesToShow: 2,
				slidesToScroll: 1
				}
			},
			{
				breakpoint: 400,
				settings: {
				slidesToShow: 1,
				slidesToScroll: 1
				}
			}
			]
		});
		
		jQuery('#tvshows-slider').slick({
			centerMode: true,
			centerPadding: '200px',
			cssEase: 'ease-in-out',
            speed: 1000,
			slidesToShow: 1,
			nextArrow: '<button class="NextArrow"><i class="ri-arrow-right-s-line"></i></button>',
			prevArrow: '<button class="PreArrow"><i class="ri-arrow-left-s-line"></i></button>',
			arrows:true,
			dots:false,
			responsive: [
				{
					breakpoint: 991,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '20px',
						slidesToShow: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '20px',
						slidesToShow: 1
					}
				}
			]
		});

		jQuery('.season-select').on('change' , function(){
			jQuery('.owl-carousel , .single-season-data').each(function(){
				jQuery(this).removeClass('active show');
			});
			jQuery('[data-display='+jQuery(this).val()+']').addClass('active show');
		});

		/*---------------------------------------------------------------------
			Owl Carousel
		----------------------------------------------------------------------- */
		jQuery('.owl-carousel').owlCarousel({
			loop:true,
			margin:20,
			nav:true,
			navText: ["<i class='ri-arrow-left-s-line'></i>", "<i class='ri-arrow-right-s-line'></i>"],
			dots:false,
			responsive:{
				0:{
					items:1
				},
				500:{
					items:2
				},
				1000:{
					items:4
				}
			}
		});
		
		/*---------------------------------------------------------------------
			Page Loader
		----------------------------------------------------------------------- */
		jQuery("#load").fadeOut();
		jQuery("#loading").delay(0).fadeOut("slow");
		
		jQuery('.widget .fa.fa-angle-down, #main .fa.fa-angle-down').on('click', function () {
			jQuery(this).next('.children, .sub-menu').slideToggle();
		});

		

		/*---------------------------------------------------------------------
		  Equal Height of Tab Pane
		-----------------------------------------------------------------------*/		
		// jQuery('.trending-content').each(function () {			
		// 	var highestBox = 0;			
		// 	jQuery('.tab-pane', this).each(function () {				
		// 		if (jQuery(this).height() > highestBox) {
		// 			highestBox = jQuery(this).height();
		// 		}
		// 	});			 
		// 	jQuery('.tab-pane', this).height(highestBox);
		// }); 

		/*---------------------------------------------------------------------
	 		Active Class for Pricing Table
		   -----------------------------------------------------------------------*/
			if(jQuery('.iq-pricing-card #my-table').length>0){
			   	let activePlan = jQuery('.iq-pricing-card #my-table').data('active');
			   	jQuery('.iq-pricing-card #my-table tr .'+activePlan).children().addClass('active');
			   	var col = jQuery('.iq-pricing-card #my-table tr .'+activePlan).index();
				jQuery(".iq-pricing-card #my-table tr td:nth-child(" + parseInt(col + 1) + ")").addClass('active');
			}
			let actWrap = '.iq-pricing-card-two .iq-price-rate-wrap';
			if(jQuery(actWrap).length>0){
				let planIdOnLoad = jQuery(actWrap+'.active').data('paid-id');
				let planLink = jQuery('.iq-pricing-card-two .iq-button').attr('href');
				jQuery('.iq-pricing-card-two .iq-button').attr('href',planLink+'?subscription_plan='+planIdOnLoad);
				jQuery(document).on('click',actWrap,function(){
					jQuery(actWrap).removeClass('active');
					let planId = jQuery(this).data('paid-id');
					jQuery('.iq-pricing-card-two .iq-button').attr('href',planLink+'?subscription_plan='+planId);
					jQuery(this).addClass('active');
				})

			}
			jQuery(".iq-pricing-card #my-table tr th").on("click", function (){
				jQuery('.iq-pricing-card #my-table tr th').children().removeClass('active');
				jQuery(this).children().addClass('active');
				jQuery(".iq-pricing-card #my-table td").each(function () {
					if (jQuery(this).hasClass('active')) {
						jQuery(this).removeClass('active')
					}
				});
				var col = jQuery(this).index();
				jQuery(".iq-pricing-card #my-table tr td:nth-child(" + parseInt(col + 1) + ")").addClass('active');
			});

		//copy link
		jQuery('.iq-copy-link').on('click',function(){
			var link = jQuery(this).data('link');
			document.addEventListener('copy', function(e) {
				e.clipboardData.setData('text/plain', link);
				e.preventDefault();
			}, true);
			if(document.execCommand('copy')){
				alert('copied');
			}
		});

		if(jQuery('.iq-login-form').length >0)
		{
			var uname = jQuery('.iq-login-form #user_login');
			var pass = jQuery('.iq-login-form #user_pass');
			var email = jQuery('.iq-login-form #user-email');
			uname.attr('placeholder','Username / Email Address');
			pass.attr('placeholder','Password');
			email.attr('placeholder','Email Address');
		}


		jQuery(".custom-file-input").on("change", function() {

			var fileName = jQuery(this).val().split("\\").pop();
			jQuery(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});
		jQuery( window ).resize(function() {

		});
		if(jQuery('.wl-child').length) {
			var count;
			var in_count;
			var width  = jQuery(window).width();
			if(width > 991){
				count = 3;in_count = 4;
			}
			else if(width > 767 && width < 991){
				count = 2;in_count = 3;
			}
			else if(width < 768){
				count = 1;in_count = 2;
			}
			else{
				count = 3;
				in_count = 4;
			}
			var k = 0;
			var j = count;
			var len = jQuery('.wl-child').length;

			jQuery('.wl-child').each(function () {
				if(k === 0)
				{
					jQuery(this).find('.watchlist-img').addClass('watchlist-first');
				}
				if (j == k || k === count || (len.length - 1) === k) {
					j += in_count;
					jQuery(this).find('.watchlist-img').addClass('watchlist-last');
					jQuery(this).next().find('.watchlist-img').addClass('watchlist-first');
				}
				k++;
			})
		}

		//Paid membership plugin input button override
		if(jQuery('#pms_register-form [name=pms_register]').length>0){
			jQuery('#pms_register-form [name=pms_register]').after('<button name="pms_register" type="submit" class="btn btn-hover iq-button" value="Register">Register</button>');
			jQuery('#pms_register-form [name=pms_register]:first').remove();
		}
		if(jQuery('#pms_login [name=wp-submit]').length>0){
			jQuery('#pms_login [name=wp-submit]').after('<button type="submit" name="wp-submit" id="wp-submit" class="btn btn-hover iq-button" value="Lod In">Log In</button>');
			jQuery('#pms_login [name=wp-submit]:first').remove();
		}
		if(jQuery('#pms-upgrade-subscription-form [name=pms_upgrade_subscription]').length>0){
			jQuery('#pms-upgrade-subscription-form [name=pms_upgrade_subscription]').after('<button type="submit" name="pms_upgrade_subscription" class="btn btn-hover iq-button mr-3" value="Upgrade Subscription">Upgrade Subscription</button>');
			jQuery('#pms-upgrade-subscription-form [name=pms_upgrade_subscription]:first').remove();
		}
		if(jQuery('#pms-upgrade-subscription-form [name=pms_redirect_back]').length>0){
			jQuery('#pms-upgrade-subscription-form [name=pms_redirect_back]').after('<button type="submit" name="pms_redirect_back" class="btn btn-hover iq-button" value="Go back">Go back</button>');
			jQuery('#pms-upgrade-subscription-form [name=pms_redirect_back]:first').remove();
		}
		if(jQuery('#pms_edit-profile-form [name=pms_edit_profile]').length>0){
			jQuery('#pms_edit-profile-form [name=pms_edit_profile]').after('<button name="pms_edit_profile" type="submit" class="btn btn-hover iq-button" value="Edit Profile">Edit Profile</button>');
			jQuery('#pms_edit-profile-form [name=pms_edit_profile]:first').remove();
		}
		if(jQuery('#pms_recover_password_form [name=submit]').length>0){
			jQuery('#pms_recover_password_form [name=submit]').after('<button type="submit" name="submit" class="btn btn-hover iq-button" value="Reset Password">Reset Password</button>');
			jQuery('#pms_recover_password_form [name=submit]:first').remove();
		}
		// user forms validation
		if(jQuery('.iq-login-form #loginform').length > 0){
			jQuery('.iq-login-form #loginform #wp-submit').remove();
			jQuery('.iq-login-form #loginform .login-submit').append('<button type="submit" name="wp-submit" id="wp-submit" class="btn btn-hover iq-button" value="Sign In">Sign In</button>');

			jQuery('.iq-login-form #loginform').submit(function() {
				jQuery('.iq-login-form .error-msg').hide();
				var user_id = jQuery.trim(jQuery("#user_login").val());
				var user_pass = jQuery.trim(jQuery("#user_pass").val());
				var error = '';

					//jQuery(".validate-form #comment").removeClass('iq-warning');
					if(user_id === "" || user_pass === "") {
						if (user_id === "") {
							//jQuery(".validate-form #comment").addClass('iq-warning');
							error = '1';
						}
						if (user_pass === "") {
							//jQuery(".validate-form #comment").addClass('iq-warning');
							error = '1';
						}
					}
				if(error !== '' && error === '1')
				{
					jQuery('.iq-login-form .error-msg').html('Require Empty Field.');
					jQuery('.iq-login-form .error-msg').slideDown();
					return false;
				}


			});
		}
		if(jQuery('.iq-login-form #changepass').length > 0) {
			jQuery('.iq-login-form #changepass').submit(function () {
				jQuery('.iq-login-form .error-msg').hide();
				var user_email = jQuery.trim(jQuery("#user_email").val());
				var error = '';

				//jQuery(".validate-form #comment").removeClass('iq-warning');
				if (user_email === "") {
					//jQuery(".validate-form #comment").addClass('iq-warning');
					error = '1';
				}
				if (error !== '' && error === '1') {
					jQuery('.iq-login-form .error-msg').html('Require Empty Field.');
					jQuery('.iq-login-form .error-msg').slideDown();
					return false;
				}


			});
		}

		if(jQuery('#update-avatar').length > 0){
			jQuery(document).on('click','#iq-edit-avatar-btn',function(){
				jQuery('#update-avatar').slideToggle();
			});
			
			jQuery(document).on('click','#cancel-avatar-edit',function(){
				jQuery('#update-avatar').slideToggle();
			});
		}

		
	});
})(jQuery);

jQuery(document).ready(function($) {

	/***************************************
		ajax like shot feature
	***************************************/
	$(".watch-list").stop().click(function(){
		
		var rel = $(this).attr("rel");
		var thisElement = $(this);
		$(".watch-list[rel="+rel+"]").html('<span><i class="fa fa-circle-o-notch fa-spin" style="font-size:24px;line-height: 28px;"></i></span>').addClass("adding");

        //alert(rel)
		var data = {
			data: rel,
			action: 'watchlist_callback'
		}
		$.ajax({
			action: "watchlist_callback",
			type: "GET",
			dataType: "json",
			url: ajaxurl,

			data: data,
			success: function(data){

				//console.log(data.likes);
				//console.log(data.status);

				if(data.status === true){
					$(".watch-list[rel="+rel+"]").addClass("adding");
					$(".watch-list[rel="+rel+"]").html('<span><i class="fa fa-check" aria-hidden="true"></i></span>').addClass("added");
                    //alert();
				}else{
					$(".watch-list[rel="+rel+"]").html('<span><i class="ri-add-line"></i></span>').removeClass("removed");
					//alert('1');
					console.log(thisElement);
					thisElement.closest('.wl-child').remove();
				}

			}
		});

	});

});