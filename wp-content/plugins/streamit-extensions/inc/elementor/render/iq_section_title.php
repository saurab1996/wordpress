<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

    //$this->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );
	
	$settings = $this->get_settings();

	$image_html='';

	$align = $settings['align'];
	if(empty($align))
	{
		$align .= ' text-left';
	}
	
  
	$this->add_render_attribute( 'iq-section', 'class', $align);
	$this->add_render_attribute( 'iq-section', 'class', 'iq-title-box' );
	$this->add_render_attribute( 'iq-section', 'class', $settings['title_style'] );

 ?>
<div <?php echo $this->get_render_attribute_string( 'iq-section' ); ?>>
	

	<<?php echo $settings['title_tag'];  ?> class="iq-title" >
		<?php echo esc_html($settings['section_title'],"streamit-extensions"); ?>
	</<?php echo $settings['title_tag']; ?>>
	
	
	<?php
		if(!empty($settings['description']) && $settings['has_description'] == 'yes')
		{
			echo sprintf('<p class="iq-title-desc">%1$s</p>',$this->parse_text_editor($settings['description']));	
		} 
	?>
</div>