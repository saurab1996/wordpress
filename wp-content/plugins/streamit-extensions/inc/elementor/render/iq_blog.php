<?php
namespace Elementor;

use Elementor\Plugin;
if ( ! defined( 'ABSPATH' ) ) exit; 

    //$this->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );
	
	$settings = $this->get_settings();
	$cat = '';
    if(isset($settings['blog_cat']) && !empty($settings['blog_cat']))
	{
		$cat = implode(',',$settings['blog_cat']);
	}
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
		'post_type'         => 'post',
		'posts_per_page' 	=> $settings['posts_per_page']['size'],
        'paged'				=> $paged,
		'post_status'       => 'publish',
		'category_name'		=> $cat,
		'order'           	=> $settings['order'],       
        'suppress_filters'  => 0            
	);
	$align = $settings['align'];

	// blog layouts	
	$blog_layout = '';
	$blog_art = '';
//	if(isset($settings['blog_layout']) && $settings['blog_layout'] === 'style2')
//	{
//		$blog_layout = ' iq-default-blog-style-2';
//		$blog_art = ' iq-blog-article-style';
//		if(isset($settings['blog_type']) && $settings['blog_type'] === '2')
//		{
//			if(isset($settings['blog_style']) && $settings['blog_style'] > 2)
//			{
//				$blog_layout = ' iq-default-blog-style-2-grid';
//			}
//		}
//		$align .= $blog_layout;
//	}

  	// global $wp_query;
    $wp_query = new \WP_Query($args);

    global $post;
		?>
<div class="iq-blog<?php echo esc_attr($align,"streamit-extensions") ?>">

	<?php
		if($settings['blog_type'] === '1')
		{
			$desk = $settings['desk_number'];
			$lap = $settings['lap_number'];
			$tab = $settings['tab_number'];
			$mob = $settings['mob_number'];

		$this->add_render_attribute( 'slider', 'data-dots', $settings['dots'] );
		$this->add_render_attribute( 'slider', 'data-nav', $settings['nav-arrow'] );
		$this->add_render_attribute( 'slider', 'data-items', $settings['desk_number'] );
		$this->add_render_attribute( 'slider', 'data-items-laptop', $settings['lap_number'] );
		$this->add_render_attribute( 'slider', 'data-items-tab', $settings['tab_number'] );
		$this->add_render_attribute( 'slider', 'data-items-mobile', $settings['mob_number'] );
		$this->add_render_attribute( 'slider', 'data-items-mobile-sm', $settings['mob_number'] );
		$this->add_render_attribute( 'slider', 'data-autoplay', $settings['autoplay'] );
		$this->add_render_attribute( 'slider', 'data-loop', $settings['loop'] );
		$this->add_render_attribute( 'slider', 'data-margin', $settings['margin']['size'] );
		$this->add_render_attribute( 'slider', 'data-padding', $settings['padding']['size'] );

	?>

	 <div class="blog-carousel owl-carousel" <?php echo $this->get_render_attribute_string( 'slider' ) ?>>
	    <?php

		if($wp_query->have_posts())
		{
	    	while ( $wp_query->have_posts() )
	    		{
	    			$wp_query->the_post();
					$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $wp_query->ID  ), "full" );
	    ?>


			<div class="iq-blog-box">
				<div class="iq-blog-image clearfix">
					<?php echo sprintf('<img src="%1$s" alt="iqonic-blog"/>',esc_url($full_image[0],"streamit-extensions"));

					  $postcat = get_the_category();
                                        if ($postcat) {
                                        	?>
                                         <ul class="iq-blogtag">
                                         	<?php
                                        foreach($postcat as $cat) {
                                        	?>
                                          <li><a href="<?php echo get_category_link( $cat->cat_ID ) ;?>"><?php echo $cat->name; ?></a></li>
                                          <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                        }
                                        ?>


				</div>
					<div class="iq-blog-detail">
						<div class="iq-blog-meta">
							<ul class="iq-postdate">
								<?php
								//post date
								$archive_year  = get_the_time('Y',$wp_query->ID);
								$archive_month = get_the_time('m',$wp_query->ID);
								$archive_day   = get_the_time('d',$wp_query->ID);
								$date=date_create($wp_query->post_date); ?>
								<li class="list-inline-item">

										<?php echo sprintf("%s",iqonic_blog_time_link()); ?>
								</li>

							</ul>

						</div>

						<div class="blog-title">
							<a href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>">
							<h4 class="mb-3"><?php echo sprintf("%s",esc_html__(get_the_title( $wp_query->ID ),"streamit-extensions")); ?></h4>
							</a>

						</div>
						<p class=""><?php  echo sprintf("%s",get_the_excerpt( $wp_query->ID ) ); ?></p>
						<div class="blog-button">
	    			<?php
                                        if(!empty($iqonic_option['blog_btn']))
                                        {
                                        ?><a class="iq-btn-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr($iqonic_option['blog_btn']) );?><i class="fa fa-angle-right ml-2" aria-hidden="true"></i></a><?php
                                        }
                                        else
                                        { ?><a class="iq-btn-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr('Read More',"streamit-extensions"));?><i class="fa fa-angle-right ml-2" aria-hidden="true"></i></a>
                                    <?php
                                        }
                                        ?>
                           </div>
					</div>
			</div>


	    <?php
			}

		 }

		 wp_reset_postdata();


		?>
	 </div>


	<?php }
	else
	{
	echo '<div class="row">';
	if($settings['blog_style'] === "2")
	{
		$col = 'col-lg-12 iqonic-space-bottom';
	}
	if($settings['blog_style'] === "3")
	{
		$col = 'col-lg-6 col-md-6 iqonic-space-bottom';
	}
	if($settings['blog_style'] === "4")
	{
		$col = 'col-lg-4 col-md-6 iqonic-space-bottom';
	}
		if($wp_query->have_posts())
		{
			while ( $wp_query->have_posts() )
			{
				$wp_query->the_post();
				$full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $wp_query->ID  ), "full" );

			?>
			<div class="<?php echo esc_attr__($col,"streamit-extensions") ?>">
		<div class="iq-blog-box">
				<div class="iq-blog-image clearfix">
					<?php echo sprintf('<img src="%1$s" alt="iqonic-blog"/>',esc_url($full_image[0],"streamit-extensions"));

					  $postcat = get_the_category();
                                        if ($postcat) {
                                        	?>
                                         <ul class="iq-blogtag">
                                         	<?php
                                        foreach($postcat as $cat) {
                                        	?>
                                          <li><a href="<?php echo get_category_link( $cat->cat_ID );?>"><?php echo $cat->name; ?></a></li>
                                          <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                        }
                                        ?>


				</div>
					<div class="iq-blog-detail">
						<div class="iq-blog-meta">

							<ul class="iq-postdate">
								<?php
								//post date
								$archive_year  = get_the_time('Y',$wp_query->ID);
								$archive_month = get_the_time('m',$wp_query->ID);
								$archive_day   = get_the_time('d',$wp_query->ID);
								$date=date_create($wp_query->post_date); ?>
								<li class="list-inline-item">

										<?php echo sprintf("%s",iqonic_blog_time_link()); ?>
								</li>

							</ul>

						</div>

						<div class="blog-title">
							<a href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>">
							<h4 class="mb-3"><?php echo sprintf("%s",esc_html__(get_the_title( $wp_query->ID ),"streamit-extensions")); ?></h4>
							</a>

						</div>
						<p class=""><?php  echo sprintf("%s",get_the_excerpt( $wp_query->ID ) ); ?></p>
						<div class="blog-button">
	    			<?php
                                        if(!empty($iqonic_option['blog_btn']))
                                        {
                                        ?><a class="iq-btn-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr($iqonic_option['blog_btn']) );?><i class="fa fa-angle-right ml-2" aria-hidden="true"></i></a><?php
                                        }
                                        else
                                        { ?><a class="iq-btn-link" href="<?php echo sprintf("%s",esc_url(get_permalink($wp_query->ID)));?>"><?php echo sprintf("%s",esc_attr('Read More',"streamit-extensions"));?><i class="fa fa-angle-right ml-2" aria-hidden="true"></i></a>
                                    <?php
                                        }
                                        ?>
                           </div>
					</div>
			</div>
			</div>
			<?php
			}

		}
		wp_reset_postdata();
	echo '</div>';
	} ?>
</div>

<?php
 if($settings['blog_type'] != '1'){
        $total_pages = $wp_query->max_num_pages;
     	$total_pages = $wp_query->max_num_pages;
             if ($total_pages > 1) {
                 $current_page = max(1, get_query_var('paged'));
                 echo paginate_links(array(
                     'base' => get_pagenum_link(1) . '%_%',
                     'format' => '/page/%#%',
                     'current' => $current_page,
                     'total' => $total_pages,
                     'type'            => 'list',
                     'prev_text'       => wp_kses('<span aria-hidden="true">' . __('Previous page',"streamit-extensions") . '</span>',"streamit-extensions"),
                     'next_text'       => wp_kses('<span aria-hidden="true">' . __('Next page',"streamit-extensions") . '</span>',"streamit-extensions"),
                 ));
             }
        }
?>
<script>
jQuery(document).ready(function(){
	if(jQuery('.iq-default-blog-style-2 .blog-carousel').length  > 0)
	{
		var scrWidth =  jQuery(window).width();
		jQuery('.iq-default-blog-style-2 .blog-carousel').each(function () {
				if(scrWidth >= 1199 )
				{
					if(jQuery(this).data('items') > 1)
					{

						jQuery(this).parent('.iq-blog').removeClass('iq-default-blog-style-2');
						jQuery(this).parent('.iq-blog').addClass('iq-default-blog-style-2-grid');
					}
				}
				else if(scrWidth >= 1023)
				{
					if(jQuery(this).data('items-laptop') > 1)
					{
						jQuery(this).parent('.iq-blog').removeClass('iq-default-blog-style-2');
						jQuery(this).parent('.iq-blog').addClass('iq-default-blog-style-2-grid');
					}
				}
				else if(scrWidth < 786)
				{

					jQuery(this).parent('.iq-blog').removeClass('iq-default-blog-style-2');
					jQuery(this).parent('.iq-blog').addClass('iq-default-blog-style-2-grid');
				}
		});
	}
});
</script>