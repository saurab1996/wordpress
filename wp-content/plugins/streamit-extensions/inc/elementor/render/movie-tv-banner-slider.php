<?php

namespace Elementor;

$html = '';
if (!defined('ABSPATH')) exit;

$settings = $this->get_settings();
$tabs = $this->get_settings_for_display('tabs');

?>

<div class="iq-main-slider p-0 iq-rtl-direction">
    <div id="tvshows-slider">
        <?php
        foreach ($tabs as $index => $item) {

            $args = array(
                'post_type'         => array('tv_show', 'movie'),
                'post_status'       => 'publish',
            );

            if (!empty($item['iq_tv_show']) && $item['iq_type'] === 'tv_show') {
                $args['p'] = $item['iq_tv_show'];
            }
            if (!empty($item['iq_movie']) && $item['iq_type'] === 'movie') {
                $args['p'] = $item['iq_movie'];
            }
            $wp_query = new \WP_Query($args);
            if ($wp_query->have_posts()) {
                while ($wp_query->have_posts()) {
                    $wp_query->the_post();
                    $back_img = get_field('movie_back_image', get_the_ID());
                    $movie_logo = get_field('name_logo', get_the_ID());
                    $trailer_link = get_field('name_trailer_link', get_the_ID());
                    $movie_run_time = get_post_meta(get_the_ID(), '_movie_run_time');
                    $movie_url_link = get_post_meta(get_the_ID(), '_movie_url_link');
                    $movie_censor_rating = get_post_meta(get_the_ID(), '_movie_censor_rating');
                    $meta = get_post_meta(get_the_ID());
                    $tm_imdb_rating = $meta['_imdb_id'];
                    $imdb_rating = 0;

                    if (isset($tm_imdb_rating[0])) {
                        $imdb_rating = $tm_imdb_rating[0];
                        $imdb_rating = floatval($imdb_rating) / 2;
                    }
                    $movie_release_year = get_post_meta(get_the_ID(), '_movie_release_date');
                    $year = '';
                    if (!empty($movie_release_year)) {
                        $year = date('M Y', $movie_release_year[0]);
                    } else {
                        $year = get_the_date('M Y');
                    }

                    $run_time = '';
                    $url_link = '';
                    $censor_rating = '';
                    if (isset($movie_censor_rating[0])) {
                        $censor_rating = $movie_censor_rating[0];
                    }
                    if (isset($movie_run_time[0])) {
                        $run_time = $movie_run_time[0];
                    }
                    if (isset($movie_url_link[0])) {
                        $url_link = $movie_url_link[0];
                    }

                    $meta = get_post_meta(get_the_ID());
                    if (get_post_type() === 'tv_show')
                        $season_data = unserialize($meta['_seasons'][0]);
        ?>
                    <div class="slick-bg" style="background:url(<?php echo esc_url($item['slider_image']['url']); ?>)">
                        <div class="shows-content h-100">
                            <div class="row align-items-center h-100">
                                <div class="col-xl-7 col-lg-12 col-md-12">
                                    <h1 class="slider-text big-title title text-uppercase" data-animation-in="fadeInLeft" data-delay-in="0.1"><?php the_title(); ?></h1>
                                    <?php if (isset($tm_imdb_rating[0]) && !empty($tm_imdb_rating[0])) { ?>
                                        <div class="slider-ratting d-flex align-items-center" data-animation-in="fadeInLeft">
                                            <ul class="ratting-start p-0 m-0 list-inline text-primary d-flex align-items-center justify-content-left">
                                                <?php
                                                for ($i = 1; $i <= ceil($imdb_rating); $i++) {
                                                    if (($imdb_rating - floor($imdb_rating)) > 0 && $i == ceil($imdb_rating)) {
                                                ?>
                                                        <li>
                                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                                        </li>
                                                    <?php
                                                        continue;
                                                    }
                                                    ?>
                                                    <li>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </li>
                                                <?php
                                                }
                                                ?>
                                            </ul>
                                            <span class="text-white ml-3"><?php echo esc_html(round($imdb_rating, 1).' (lmdb)',"streamit-extensions"); ?></span>
                                        </div>
                                    <?php } ?>
                                    <div class="d-flex align-items-center tv-movie-banner text-detail" data-animation-in="fadeInUp" data-delay-in="0.3">
                                        <span class="badge badge-secondary p-2 mr-3"><?php echo esc_html($censor_rating,"streamit-extensions"); ?></span>
                                        <span class="">
                                            <?php
                                            $display = '';
                                            if (isset($season_data)) {
                                                if (is_array($season_data)) {
                                                    $display = count($season_data) . ' Seasons';
                                                } else {
                                                    $display = '1 Seasons';
                                                }
                                            } elseif (isset($movie_run_time)) {
                                                $display = $movie_run_time[0];
                                            }
                                            echo esc_html__($display,"streamit-extensions");
                                            ?>
                                        </span>
                                        <span class="trending-year"><?php echo esc_html($year,"streamit-extensions"); ?></span>
                                    </div>
                                    <p data-animation-in="fadeInUp" data-delay-in="0.5">
                                        <?php

                                        $iq_excerpt = get_the_excerpt();
                                        if (!empty($iq_excerpt)) {
                                            $iq_remove_tags = array("<p>", "</p>");
                                            $iq_excerpt = str_replace($iq_remove_tags, "", $iq_excerpt);
                                            echo __($iq_excerpt,"streamit-extensions");
                                        } else {
                                            echo esc_html('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s.');
                                        }

                                        ?>
                                    </p>
                                    <?php
                                    if (!empty($settings['play_now_text'])) {
                                    ?>
                                        <div class="d-flex align-items-center r-mb-23" data-animation-in="fadeInUp" data-delay-in="1.2">
                                            <a href="<?php the_permalink(); ?>" class="btn btn-hover iq-button"><i class="fa fa-play mr-2" aria-hidden="true"></i><?php echo esc_html($settings['play_now_text'],"streamit-extensions"); ?></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
        <?php }
            }
        } ?>
    </div>
</div>