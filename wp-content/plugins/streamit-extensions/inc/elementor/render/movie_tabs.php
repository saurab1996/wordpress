<?php

namespace Elementor;

$html = '';
if (!defined('ABSPATH')) exit;

$settings = $this->get_settings();
$args = array();
$tax_query = array();



$args = array(
    'post_type'         => 'Movie',
    'fields'            => 'ids',
    'post_status'       => 'publish',
    'posts_per_page'    => 10,
    'meta_key'          => 'post_views_count',
    'orderby'           => 'meta_value_num',
    'order'             => 'DESC'
);
$wp_query = new \WP_Query($args);
if ($wp_query->post_count < 6) {
    $args2 = array(
        'post_type'         => 'Movie',
        'fields'            => 'ids',
        'post_status'       => 'publish',
        'posts_per_page'    => 10,
        'order'             => 'DESC'
    );

    $wp_query2 = new \WP_Query($args2);
    $allTheIDs = array_merge($wp_query->posts, $wp_query2->posts);
    $id = array_unique($allTheIDs);
    $args3 = array(
        'post_type'         => 'Movie',
        'post__in'          => $allTheIDs,
        'post_status'       => 'publish',
        'posts_per_page'    => 10,
        'order'             => 'DESC'
    );
    $wp_query = new \WP_Query($args3);
}


?>
<div id="iq-trending" class="s-margin iq-movies-tabs iq-rtl-direction">

    <div class="iq-main-header d-flex align-items-center justify-content-between">
        <h4 class="main-title"><?php echo esc_html('Trending',"streamit-extensions"); ?></h4>
    </div>
    <div class="trending-contens">
        <ul id="trending-slider-nav" class="list-inline p-0 mb-0 row align-items-center">
            <?php

            if ($wp_query->have_posts()) {
                while ($wp_query->have_posts()) {
                    $wp_query->the_post();

                    $movie_logo = get_field('name_logo', get_the_ID());


                    $trailer_link = get_field('name_trailer_link', get_the_ID());
                    $movie_run_time = get_post_meta(get_the_ID(), '_movie_run_time');
                    $movie_url_link = get_post_meta(get_the_ID(), '_movie_url_link');

                    $meta = get_post_meta(get_the_ID());

                    $run_time = '';
                    $url_link = '';
                    if (isset($movie_run_time[0])) {
                        $run_time = $movie_run_time[0];
                    }
                    if (isset($movie_url_link[0])) {
                        $url_link = $movie_url_link[0];
                    }
                    $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), "full");

            ?>
                    <li>
                        <a href="javascript:void(0);">
                            <div class="movie-slick position-relative">
                                <img src="<?php echo esc_url($full_image[0]); ?>" class="img-fluid" alt="">
                            </div>
                        </a>
                    </li>
            <?php }
             wp_reset_postdata(); 
            } ?>
        </ul>
        <ul id="trending-slider" class="list-inline p-0 m-0  d-flex align-items-center">
            <?php

            if ($wp_query->have_posts()) {
                $i = 1;
                while ($wp_query->have_posts()) {
                    $wp_query->the_post();
                    $movie_run_time = get_post_meta(  get_the_ID() , '_movie_run_time');
                    $meta = get_post_meta(get_the_ID());
                    $movie_censor_rating = get_post_meta(get_the_ID(), '_movie_censor_rating');
                    $censor_rating = '';
                    if (isset($movie_censor_rating[0])) {
                        $censor_rating = $movie_censor_rating[0];
                    }
                    $_cast = unserialize($meta['_cast'][0]);
                    $cast = '';
                    $genre = '';
                    $tag = '';
                    $run_time = '';
                    if(isset($movie_run_time[0]))
                    {
                       $run_time = $movie_run_time[0];
                    }
                    $year = get_the_date('M Y');
                    if (is_array($_cast)) {
                        $j = 1;
                        foreach ($_cast as $data) {
                            $cast_obj = get_post($data['id']);
                            if ($j === 1)
                                $cast = $cast_obj->post_title;
                            else
                                $cast .= ", " . $cast_obj->post_title;
                            $j++;
                        }
                    }     

                    $wp_object = wp_get_post_terms(get_the_ID(), 'movie_genre');



                    if (!empty($wp_object)) {
                        $k = 1;
                        foreach ($wp_object as $val) {

                            if ($k == 1)
                                $genre = $val->name;
                            else
                                $genre .= ', ' . $val->name;
                            $k++;
                        }
                    }

                    $wp_object = wp_get_post_terms(get_the_ID(), 'movie_tag');



                    if (!empty($wp_object)) {
                        $l = 1;
                        foreach ($wp_object as $val) {

                            if ($l == 1)
                                $tag = $val->name;
                            else
                                $tag .= ', ' . $val->name;
                            $l++;
                        }
                    }


                             
            ?>
                     <li>
                        <div class="tranding-block position-relative" style="background-image: url(<?php echo get_the_post_thumbnail_url($wp_query->ID) ?>);">
                            <div class="trending-custom-tab">
                                <div class="tab-title-info position-relative">
                                    <ul class="trending-pills d-flex nav nav-pills justify-content-center align-items-center text-center" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="pill" href="#overview_<?php echo esc_html($i); ?>" role="tab" aria-selected="true"><?php echo esc_html('Overview',"streamit-extensions"); ?></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#similar_<?php echo esc_html($i); ?>" role="tab" aria-selected="false"><?php echo esc_html('Similar Like This',"streamit-extensions"); ?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="trending-content tab-content">
                                    <div id="overview_<?php echo esc_html($i); ?>" class="overview-tab tab-pane fade active show">
                                        <div class="trending-info align-items-center w-100 animated fadeInUp">
                                            <a href="javascript:void(0);" tabindex="0">
                                                <div class="res-logo">
                                                    <div class="channel-logo">
                                                        <img src="<?php echo esc_url($movie_logo['url']); ?>" class="c-logo" alt="streamit">
                                                    </div>
                                                </div>
                                            </a>
                                            <h1 class="trending-text big-title text-uppercase"><?php the_title(); ?></h1>
                                            
                                            <div class="d-flex align-items-center text-white text-detail mb-4">
                                                <span class="badge badge-secondary p-2 mr-3"><?php echo esc_html($censor_rating,"streamit-extensions"); ?></span>
                                                <span class="text-white"><?php echo esc_html($run_time,"streamit-extensions"); ?></span>
                                                <span class="trending-year"><?php echo esc_html($year); ?></span>
                                            </div>
                                            <div class="d-flex align-items-center series mb-4">
                                                <img src="<?php echo esc_url($settings['trending_top_img']['url']) ?>" class="img-fluid" alt="top10">
                                                <span class="text-gold ml-3">#<?php echo esc_html($i.' in Series Today',"streamit-extensions"); ?></span>
                                            </div>
                                            <div class="trending-dec mb-4">
                                                <?php
                                                $excerpt = get_the_excerpt();
                                                if (!empty($excerpt)) {
                                                    $iq_remove_tags = array("<p>", "</p>");
                                                    $iq_excerpt = str_replace($iq_remove_tags, "", $excerpt);
                                                    echo __($iq_excerpt,"streamit-extensions");
                                                } else {
                                                    echo esc_html(' Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s.');
                                                }
                                                ?>
                                            </div>
                                            <div class="p-btns">
                                                <div class="d-flex align-items-center p-0">
                                                    <a href="<?php the_permalink(); ?>" class="btn btn-hover iq-button mr-2" tabindex="0"><i class="fa fa-play mr-2" aria-hidden="true"></i>Play Now</a>
                                                </div>
                                            </div>
                                            <div class="trending-list mt-4">
                                                <div class="text-primary title starring"><?php echo esc_html('Starring:',"streamit-extensions"); ?> <span class="text-body"><?php echo esc_html(rtrim($cast, ",")); ?></span>
                                                </div>
                                                 <div class="text-primary title genres"><?php echo esc_html('Genres:',"streamit-extensions"); ?> <span class="text-body"><?php echo esc_html(rtrim($genre, ",")); ?></span>
                                                </div>
                                                <div class="text-primary title tag"><?php echo esc_html('Tag:',"streamit-extensions"); ?> <span class="text-body"><?php echo esc_html(rtrim($tag, ",")); ?></span>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <?php if (!empty($genre)) { ?>
                                        <div id="similar_<?php echo esc_html($i); ?>" class="overlay-tab tab-pane fade">
                                            <div class="trending-info align-items-center w-100 animated fadeInUp">
                                                <div class="episodes-contens iq-rtl-direction">
                                                <?php
                                                    $genre = wp_get_post_terms(get_the_ID(), 'movie_genre');
                                                    foreach ($genre as $index => $val) {
                                                        if ($index == 0) {
                                                            $class = ' active show';
                                                        } else {
                                                            $class = '';
                                                        }
                                                ?> 
                                                    <div class="owl-carousel owl-theme episodes-slider1 list-inline p-0 mb-0 animated fadeInUp <?php echo esc_attr($class); ?>" data-display="<?php echo $index; ?>">
                                                    <?php
                                                    foreach ($genre as $epdata ) {
                                                        $args = array(
                                                            'post_type' => 'Movie' ,
                                                            'post_status'       => 'publish',
                                                            'orderby' => 'date' ,
                                                            'order' => 'DESC' ,
                                                            'posts_per_page' => 5,
                                                            'tax_query' => array(
                                                                array(
                                                                    'taxonomy' => 'movie_genre',
                                                                    'field' => 'slug',
                                                                    'terms' => $epdata->slug,
                                                                ),
                                                            ),

                                                        );
                                                        $wp_query1 = new \WP_Query($args);
                                                        if ($wp_query1->have_posts()) {
                                                            while ($wp_query1->have_posts()) {
                                                                $wp_query1->the_post();
                                                                $url = get_the_post_thumbnail_url($wp_query1->ID); 
                                                         
                                                         ?>
                                                            <div class="e-item">
                                                                <div class="block-image position-relative">
                                                                    <a href="<?php the_permalink();  ?>">
                                                                        <img src="<?php echo esc_html($url); ?>" class="img-fluid" alt="">
                                                                    </a>
                                                                    <span class="episode-duration"><?php echo esc_html($run_time,"streamit-extensions"); ?></span>
                                                                    <div class="episode-play-info">
                                                                        <div class="episode-play">
                                                                            <a href="<?php the_permalink();  ?>" tabindex="0"><i class="ri-play-fill"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    <div class="episodes-description text-body iq-ltr-direction">
                                                                        <div class="d-flex align-items-center iq-tab-no-title">
                                                                            <div class="episode-number"><?php echo esc_html($year); ?></div>
                                                                            <a href="<?php the_permalink();  ?>"><?php the_title(); ?></a>
                                                                        </div>
                                                                        <div class="mb-0 mt-2 movies-tab-desc">
                                                                            <?php
                                                                            $thecontent = get_the_excerpt();
                                                                            if (!empty($thecontent)) {
                                                                                echo wp_trim_words($thecontent,20,'...');
                                                                            } else {
                                                                                echo esc_html('Lorem Ipsum is simply dummy text of the printing and typesetting industry.');
                                                                            }

                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                            <?php                                                                
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </li>  
                             
                    <?php $i++;
                }
                wp_reset_postdata();
            } ?>

        </ul>
    </div>

</div>