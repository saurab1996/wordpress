<?php

namespace Elementor;

$html = '';
if (!defined('ABSPATH')) exit;

$settings = $this->get_settings();
$args = array();
$tax_query = array();

$taxargs = array();

$args = array(
    'post_type'         => 'movie',
    'post_status'       => 'publish',

);

if (!empty($settings['iq_movie_genre'])) {
    $tax_query['taxonomy'] = 'movie_genre';
    $tax_query['field'] = 'term_id';
    $tax_query['terms'] = $settings['iq_movie_genre'];
    $tax_query['operator'] = 'IN';
    array_push($taxargs, $tax_query);
}

if (!empty($settings['iq_movie_tag'])) {
    $tax_query['taxonomy'] = 'movie_tag';
    $tax_query['field'] = 'term_id';
    $tax_query['terms'] = $settings['iq_movie_tag'];
    $tax_query['operator'] = 'IN';
    array_push($taxargs, $tax_query);
}

if (!empty($tax_query)) {
    $args['tax_query'] = $taxargs;
    $args['tax_query']['relation'] = 'OR';
}
$wp_query = new \WP_Query($args);
$nav_slider = '';
$slider = '';

if ($wp_query->have_posts()) {
    while ($wp_query->have_posts()) {
        $wp_query->the_post();
        $back_img = get_field('movie_back_image', get_the_ID());
        $movie_logo = get_field('name_logo', get_the_ID());
        $trailer_link = get_field('name_trailer_link', get_the_ID());

        $movie_run_time = get_post_meta(get_the_ID(), '_movie_run_time');
        $movie_url_link = get_post_meta(get_the_ID(), '_movie_url_link');
        $movie_censor_rating = get_post_meta(get_the_ID(), '_movie_censor_rating');
        $movie_choice = get_post_meta(get_the_ID(), '_movie_choice');
        $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), "full");


        $run_time = '';
        $url_link = '';
        $censor_rating = '';

        if (isset($movie_run_time[0])) {
            $run_time = $movie_run_time[0];
        }



        if (isset($movie_censor_rating[0])) {
            $censor_rating = $movie_censor_rating[0];
        }

        if (isset($movie_choice[0])) {
            if ($movie_choice[0] == 'movie_url') {
                $url_link = $movie_url_link[0];
            } else {
                $url_link = get_the_permalink();
            }
        }

        $nav_slider .= '
            <li class="slick-bg">
              
               
            </li>';
        $slider .= '
            <li>
                    <div class="block-images position-relative">
//                    
                   
                    <div class="block-description">
                        <h5>' . get_the_title() . '</h5>
                        <div class="movie-time d-flex align-items-center my-2">
                            <div class="badge badge-secondary p-1 mr-2">' . $censor_rating . '</div>
                            <span class="text-white">' . $run_time . '</span>
                        </div>
                        <div class="hover-buttons">
                            <a href="' . esc_url($url_link) . '" class="btn btn-hover iq-button" tabindex="0">
                            <i class="fa fa-play mr-1" aria-hidden="true"></i><span>' . esc_html($settings['play_now_text'],"streamit-extensions") . '</span>
                            </a>
                        </div>
                    </div>
                    </div>
                </li>
            ';
    }
    wp_reset_postdata();
}
?>
<div id="iq-topten" class="position-relative iq-rtl-direction">
    <h4 class="iq-title"><?php echo __($settings['movie_section_title'],"streamit-extensions"); ?></h4>
    <div class="topten-contens">
        <ul id="top-ten-slider" class="list-inline p-0 m-0  d-flex align-items-center">
            <?php echo $nav_slider; ?>
        </ul>
        <div class="vertical_s">
            <ul id="top-ten-slider-nav" class="list-inline p-0 m-0  d-flex align-items-center">
                <?php echo $slider; ?>
            </ul>
        </div>
    </div>
</div>