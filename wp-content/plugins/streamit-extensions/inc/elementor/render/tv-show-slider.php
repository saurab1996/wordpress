<?php

namespace Elementor;

$html = '';
if (!defined('ABSPATH')) exit;

$settings = $this->get_settings();
$args = array();
$tax_query = array();
$slider_title = '';
$view_cat = '';
$iq_option = get_option('streamit_options');

$taxargs = array();

$args = array(
   'post_type'         => 'tv_show',
   'post_status'       => 'publish',

);

if (!empty($settings['iq_select_tv_shows'])) {
   $args['post__in'] = $settings['iq_select_tv_shows'];
}
if (empty($settings['iq_select_tv_shows']) && $settings['post_view_type'] === 'most_liked') {
   $args['post__in'] = iq_get_most_liked('tv_show', array(), 'id');
   $args['orderby'] = 'post__in';
}

if (isset($settings['slider_title'])) {
   $slider_title = $settings['slider_title'];
   $slider_title_key  = str_replace(" ", "_", $slider_title);

   $title_option = '_m12iqt' . $slider_title_key;
   if (!get_option($title_option)) {
      update_option($title_option, $slider_title);
   }
   $view_cat = $title_option;
}

if (!empty($settings['iq_tv_show_genre'])) {
   $tax_query['taxonomy'] = 'tv_show_genre';
   $tax_query['field'] = 'slug';
   $tax_query['terms'] = $settings['iq_tv_show_genre'];
   $tax_query['operator'] = 'IN';
   array_push($taxargs, $tax_query);
   $option = '';
   foreach ($settings['iq_tv_show_genre'] as $val) {
      $option .= substr($val, 0, 3);
   }
   $option = 't12iq_' . $option;
   $option_value = implode(',', $settings['iq_tv_show_genre']);
   if (!get_option($option)) {
      update_option($option, $option_value);
   }
   $view_cat .= '=' . $option;
}
$view_all = '';
if ($settings['view_all_switch'] === 'yes') {
   $view_all_text = $iq_option['streamit_viewall_text'];
   $view_all = '<a class="iq-view-all" href="' . esc_url(get_page_link($iq_option['streamit_viewall_link'])) . '?tv_shows=' . __($view_cat,"streamit-extensions") . '">'.__($view_all_text,"streamit-extensions").'</a>';
}
if (!empty($settings['iq_tv_show_tag'])) {
   $tax_query['taxonomy'] = 'tv_show_tag';
   $tax_query['field'] = 'slug';
   $tax_query['terms'] = $settings['iq_tv_show_tag'];
   $tax_query['operator'] = 'IN';
   array_push($taxargs, $tax_query);
}

if (!empty($settings['posts_per_page'])) {
   $args['posts_per_page'] = $settings['posts_per_page']['size'];
}

if (!empty($settings['post_view_type']) && $settings['post_view_type'] !== 'none') {
   if ($settings['post_view_type'] === 'latest') {
      $args['orderby'] = 'publish_date';
      $args['order'] = 'DESC';
   }
}
if (!empty($settings['order']) && $settings['post_view_type'] !== 'latest') {
   $args['order'] = $settings['order'];
}

if (!empty($tax_query)) {
   $args['tax_query'] = $taxargs;
   $args['tax_query']['relation'] = 'OR';
}
$wp_query = new \WP_Query($args);

// $t=iq_get_most_like();


?>

<section class="s-margin iq-rtl-direction">
   <div class="iq-main-header d-flex align-items-center justify-content-between iq-ltr-direction">
      <?php
      if (!empty($slider_title)) {
      ?>
         <h4 class="main-title"><?php echo esc_html($slider_title,"streamit-extensions"); ?></h4>
      <?php } ?>
      <?php echo wp_kses_post($view_all); ?>
   </div>

   <div class="tvthrillers-contens">
      <ul class="favorites-slider list-inline row p-0 mb-0">
         <?php
         if ($wp_query->have_posts()) {
            while ($wp_query->have_posts()) {
               $wp_query->the_post();
               $back_img = get_field('movie_back_image', get_the_ID());
               $movie_logo = get_field('name_logo', get_the_ID());
               $trailer_link = get_field('name_trailer_link', get_the_ID());
               $movie_run_time = get_post_meta(get_the_ID(), '_movie_run_time');
               $movie_url_link = get_post_meta(get_the_ID(), '_movie_url_link');
               $meta = get_post_meta(get_the_ID());

               $run_time = '';
               $url_link = '';
               if (isset($movie_run_time[0])) {
                  $run_time = $movie_run_time[0];
               }
               if (isset($movie_url_link[0])) {
                  $url_link = $movie_url_link[0];
               }
               $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), "full");
               $meta = get_post_meta(get_the_ID());
               $season_data = unserialize($meta['_seasons'][0]);

         ?>
               <li class="slide-item">
                  <div class="block-images position-relative">
                     <div class="img-box">
                        <img src="<?php echo esc_html($full_image[0]); ?>" class="img-fluid" alt="" />
                     </div>
                     <div class="block-description">
                        <h6>
                           <a href="<?php echo esc_url(the_permalink(),"streamit-extensions"); ?>">
                              <?php the_title(); ?>
                           </a>
                        </h6>
                        <div class="movie-time d-flex align-items-center my-2">
                           <div class="badge badge-secondary p-1 mr-2"><?php echo esc_html('15+',"streamit-extensions"); ?></div>
                           <span class="text-white">
                              <?php if (is_array($season_data)) 
                                       echo esc_html(count($season_data));
                                    else 
                                       echo __('1',"streamit-extensions"); ?> 
                                 <?php echo __('Seasons',"streamit-extensions"); ?></span>
                        </div>
                        <div class="hover-buttons">
                           <a href="<?php echo esc_url(the_permalink(),"streamit-extensions"); ?>" class="btn btn-hover iq-button">
                              <i class="fa fa-play mr-1" aria-hidden="true"></i>
                              <?php echo esc_html($settings['play_now_text']); ?>
                           </a>
                        </div>
                     </div>
                     <div class="block-social-info">
                        <ul class="list-inline p-0 m-0 music-play-lists">
                           <li class="share">
                              <span><i class="ri-share-fill"></i></span>
                              <div class="share-box">
                                 <div class="d-flex align-items-center">
                                    <a href="https://www.facebook.com/sharer?u=;" target="_blank" rel="noopener noreferrer" class="share-ico"><i class="ri-facebook-fill"></i></a>
                                    <a href="http://twitter.com/intent/tweet?text=Currentlyreading" target="_blank" rel="noopener noreferrer" class="share-ico"><i class="ri-twitter-fill"></i></a>
                                    <a href="#" data-link='<?php the_permalink(); ?>' class="share-ico iq-copy-link"><i class="ri-links-fill"></i></a>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="iq-like-btn"><?php echo do_shortcode('[wp_ulike for="movie" id="' . get_the_ID() . '" style="wpulike-heart"]'); ?></div>
                           </li>
                           <li>
                              <?php
                              if (!is_user_logged_in()) {
                                 if (isset($iq_option['streamit_signin_link'])) {
                                    $iqonic_signin_link = get_page_link($iq_option['streamit_signin_link']);

                              ?>
                                    <a class="watch-list-not" href="<?php echo esc_url($iqonic_signin_link) ?>">
                                       <span><i class="ri-add-line"></i></span>
                                    </a>
                                 <?php }
                              } else {
                                 ?>
                                 <a class="watch-list" rel="<?php echo get_the_ID(); ?>">
                                    <?php
                                    echo add_to_watchlist(get_the_ID());
                                    ?>
                                 </a>
                              <?php } ?>
                           </li>
                        </ul>
                     </div>
                  </div>
               </li>

         <?php }
         }
         wp_reset_postdata();
         ?>
      </ul>
   </div>
</section>