<?php
namespace Elementor; 

$html = '';
if ( ! defined( 'ABSPATH' ) ) exit;

$settings = $this->get_settings();
$args = array();
$tax_query = array();
$tabs = $this->get_settings_for_display( 'tabs' ); ?>

<div id="parallaxslider" class="slider m-0 p-0"> <?php

    foreach ( $tabs as $index => $item ){
        $args = array(
            'post_type'         => 'movie',
            'post_status'       => 'publish',  
        );
        if(!empty($item['iq_movie'])) {
            $args['p'] = $item['iq_movie'];
        }
        $wp_query = new \WP_Query($args);
        if($wp_query->have_posts()) {
            while ( $wp_query->have_posts() ) {
                $wp_query->the_post();

                $movie_logo = get_field('name_logo' , get_the_ID());
                $trailer_link = get_field('name_trailer_link' , get_the_ID());
                
                $movie_run_time = get_post_meta(  get_the_ID() , '_movie_run_time');
                $movie_url_link = get_post_meta(  get_the_ID() , '_movie_url_link');
                $movie_avg_rate = get_post_meta(  get_the_ID() , '_masvideos_average_rating');      
               
                $avg_rate = '';
                $run_time = '';
                $url_link = '';
                $censor_rating = '';               
                if(isset($movie_avg_rate[0]))
                {
                    $avg_rate = $movie_avg_rate[0];
                }
                $movie_run_time = get_post_meta(  get_the_ID() , '_movie_run_time');
                $movie_url_link = get_post_meta(  get_the_ID() , '_movie_url_link');
                $movie_censor_rating = get_post_meta(  get_the_ID() , '_movie_censor_rating');
                $movie_choice = get_post_meta(  get_the_ID() , '_movie_choice');
                if(isset($movie_run_time[0])) {
                    $run_time = $movie_run_time[0];
                }
                if(isset($movie_censor_rating[0])) {
                    $censor_rating = $movie_censor_rating[0];
                }
                if(isset($movie_choice[0])) {
                    if($movie_choice[0] == 'movie_url') {
                        $url_link = $movie_url_link[0];
                    } else {
                        $url_link = get_the_permalink();
                    }
                }
                update_post_meta( get_the_ID(), 'iq_slider_back_image' , $item['slider_image']['url']); ?>
                <div id="parallex" class="parallax-window slide slick-bg" style="background:url('<?php echo esc_url($item['slider_image']['url']); ?>')">
    
                    <div class="row align-items-center justify-content-center h-100 parallaxt-details">
                        <div class="col-lg-4 r-mb-23">
                            <div class="text-left">
                                <a href="javascript:void(0);">
                                    <h2 class="slider-text big-title title text-uppercase" ><?php the_title(); ?></h2>
                                </a>
                                <div class="parallax-ratting d-flex align-items-center mt-3 mb-3">
                                    <ul class="ratting-start p-0 m-0 list-inline text-primary d-flex align-items-center justify-content-left"> <?php
                                        for($i=1;$i<=5;$i++)
                           {
                               if($i <= $avg_rate)
                               {
                                   $class = 'fa fa-star';
                               }
                               else
                               {
                                $class = 'fa fa-star-o';
                               }
                           ?>
                              <li><a href="javascript:void(0);" class=""><i class="<?php echo esc_attr($class); ?>"
                                 aria-hidden="true"></i></a></li>
                           <?php } ?>
                             
                           </ul>
                           <span class="text-white ml-3"><?php echo esc_html($avg_rate,"streamit-extensions"); ?></span>
                        </div>
                        <div class="movie-time d-flex align-items-center mb-3">
                           <div class="badge badge-secondary mr-3"><?php echo esc_html($censor_rating); ?></div>
                           <h6 class="text-white"><?php echo esc_html($run_time); ?></h6>
                        </div>
                        
                        <div class="iq_movie_desc"><?php the_excerpt(); ?></div>
                        <div class="parallax-buttons">
                           <a href="<?php echo esc_url($url_link); ?>" class="btn btn-hover iq-button"><?php echo esc_html($settings['play_now_text'],"streamit-extensions"); ?></a>
                           <a href="<?php echo esc_url($url_link); ?>" class="btn btn-link"><?php echo esc_html($settings['moredetail_text'],"streamit-extensions"); ?></a>
                          
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-8">
                     <div class="parallax-img">
                        <a href="<?php echo esc_url($url_link); ?>">
                        	<img src="<?php echo esc_html($item['slider_image']['url']); ?>" class="img-fluid w-100" alt="bailey">
                        </a>
                       
                     </div>
                  </div>
               </div>
         
</div>
<?php } } } ?>
</div>