<?php

namespace Elementor;

$html = '';
if (!defined('ABSPATH')) exit;

$settings = $this->get_settings();
$args = array();
$tax_query = array();
$tabs = $this->get_settings_for_display('tabs');
?>

<div id="home" class="iq-main-slider p-0 iq-rtl-direction">
    <div id="home-slider" class="slider m-0 p-0">
        <?php
        foreach ($tabs as $index => $item) {

            $args = array(
                'post_type'         => 'movie',
                'post_status'       => 'publish',
            );

            if (!empty($item['iq_movie'])) {
                $args['p'] = $item['iq_movie'];
            }
            $wp_query = new \WP_Query($args);
            if ($wp_query->have_posts()) {

                while ($wp_query->have_posts()) {
                    $wp_query->the_post();

                    $movie_logo = get_field('name_logo', get_the_ID());
                    $trailer_link = get_field('name_trailer_link', get_the_ID());
                    $movie_run_time = get_post_meta(get_the_ID(), '_movie_run_time');
                    $movie_url_link = get_post_meta(get_the_ID(), '_movie_url_link');
                    $movie_censor_rating = get_post_meta(get_the_ID(), '_movie_censor_rating');
                    $movie_choice = get_post_meta(get_the_ID(), '_movie_choice');
                    $movie_cast = get_post_meta(get_the_ID(), '_cast');
                    $movie_imdb_rating = get_post_meta(get_the_ID(), '_imdb_id');


                    $run_time = '';
                    $url_link = '';
                    $censor_rating = '';
                    $imdb_rating = '';

                    if (isset($movie_run_time[0])) {
                        $run_time = $movie_run_time[0];
                    }

                    if (isset($movie_censor_rating[0])) {
                        $censor_rating = $movie_censor_rating[0];
                    }
                    if (isset($movie_imdb_rating[0])) {
                        $imdb_rating = $movie_imdb_rating[0];
                        $imdb_rating = floatval($imdb_rating) / 2;
                    }
                    if (isset($movie_choice[0])) {
                        if ($movie_choice[0] == 'movie_url') {
                            $url_link = $movie_url_link[0];
                        } else {
                            $url_link = get_the_permalink();
                        }
                    }

                    $m_cast = $movie_cast[0];
                    $cast = '';
                    $genre = '';
                    $tag = '';
                    if (is_array($m_cast)) {
                        $j = 1;
                        foreach ($m_cast as $data) {
                            $cast_obj = get_post($data['id']);
                            if ($j === 1)
                                $cast = $cast_obj->post_title;
                            else
                                $cast .= ", " . $cast_obj->post_title;
                            $j++;
                        }
                    }

                    $wp_object = wp_get_post_terms(get_the_ID(), 'movie_genre');

                    if (!empty($wp_object)) {
                        $k = 1;
                        foreach ($wp_object as $val) {

                            if ($k == 1)
                                $genre = $val->name;
                            else
                                $genre .= ', ' . $val->name;
                            $k++;
                        }
                    }

                    $wp_object = wp_get_post_terms(get_the_ID(), 'movie_tag');



                    if (!empty($wp_object)) {
                        $l = 1;
                        foreach ($wp_object as $val) {

                            if ($l == 1)
                                $tag = $val->name;
                            else
                                $tag .= ', ' . $val->name;
                            $l++;
                        }
                    }


                    update_post_meta(get_the_ID(), 'iq_slider_back_image', $item['slider_image']['url']);
        ?>
                    <div class="slide slick-bg" style="background:url('<?php echo esc_url($item['slider_image']['url']); ?>')">
                        <div class="container-fluid position-relative h-100">
                            <div class="slider-inner h-100">
                                <div class="row align-items-center  h-100">
                                    <div class="col-xl-7 col-lg-12 col-md-12">
                                        <a href="javascript:void(0);">
                                            <div class="channel-logo" data-animation-in="fadeInLeft">
                                                <img src="<?php echo esc_url($movie_logo['url']); ?>" class="c-logo" alt="streamit">
                                            </div>
                                        </a>
                                        <h1 class="slider-text big-title title text-uppercase" data-animation-in="fadeInLeft"><?php the_title(); ?></h1>
                                        <div class="d-flex flex-wrap align-items-center" data-animation-in="fadeInLeft">
                                            <?php if (isset($movie_imdb_rating[0]) && !empty($movie_imdb_rating[0])) { ?>
                                                <div class="slider-ratting d-flex align-items-center mr-4 mt-2 mt-md-3">
                                                    <ul class="ratting-start p-0 m-0 list-inline text-primary d-flex align-items-center justify-content-left">
                                                        <?php
                                                        for ($i = 1; $i <= ceil($imdb_rating); $i++) {
                                                            if (($imdb_rating - floor($imdb_rating)) > 0 && $i == ceil($imdb_rating)) {
                                                        ?>
                                                                <li>
                                                                    <i class="fa fa-star-half" aria-hidden="true"></i>
                                                                </li>
                                                            <?php
                                                                continue;
                                                            }
                                                            ?>
                                                            <li>
                                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                            </li>
                                                        <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                    <span class="text-white ml-2"><?php echo esc_html(round($imdb_rating, 1).'(lmdb)',"streamit-extensions"); ?></span>
                                                </div>
                                            <?php } ?>
                                            <div class="d-flex align-items-center mt-2 mt-md-3">
                                                <span class="badge badge-secondary p-2"><?php echo esc_html($censor_rating); ?></span>
                                                <span class="ml-3"><?php echo esc_html($run_time); ?></span>
                                            </div>
                                        </div>
                                        <p data-animation-in="fadeInUp" data-delay-in="1.2">
                                            <?php

                                            $iq_excerpt = get_the_excerpt();
                                            if (!empty($iq_excerpt)) {
                                                $iq_remove_tags = array("<p>", "</p>");
                                                $iq_excerpt = str_replace($iq_remove_tags, "", $iq_excerpt);
                                                echo __($iq_excerpt,"streamit-extensions");
                                            } else {
                                                echo esc_html('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s.');
                                            }

                                            ?>
                                        </p>
                                        <div class="trending-list" data-wp_object-in="fadeInUp" data-delay-in="1.2">
                                            <div class="text-primary title starring">
                                                <?php echo __('Starring:',"streamit-extensions") ?> <span class="text-body"><?php echo esc_html(rtrim($cast, ",")); ?></span>
                                            </div>
                                            <div class="text-primary title genres">
                                                <?php echo __('Genres:',"streamit-extensions") ?> <span class="text-body"><?php echo esc_html(rtrim($genre, ",")); ?></span>
                                            </div>
                                            <div class="text-primary title tag">
                                                <?php echo __('Tag:',"streamit-extensions") ?> <span class="text-body"><?php echo esc_html(rtrim($tag, ",")); ?></span>
                                            </div>
                                        </div>
                                        <?php
                                        if (!empty($settings['play_now_text'])) {
                                        ?>
                                            <div class="d-flex align-items-center r-mb-23" data-animation-in="fadeInUp" data-delay-in="1.3">
                                                <a href="<?php echo esc_url($url_link); ?>" class="btn btn-hover iq-button"><i class="fa fa-play mr-2" aria-hidden="true"></i><?php echo esc_html($settings['play_now_text'],"streamit-extensions"); ?></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xl-5 col-lg-12 col-md-12 trailor-video">

                                        <a href="<?php echo esc_url($trailer_link); ?>" class="video-open playbtn">
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px" height="80px" viewBox="0 0 213.7 213.7" enable-background="new 0 0 213.7 213.7" xml:space="preserve">
                                                <polygon class='triangle' fill="none" stroke-width="7" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="73.5,62.5 148.5,105.8 73.5,149.1 " />
                                                <circle class='circle' fill="none" stroke-width="7" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="106.8" cy="106.8" r="103.3" />
                                            </svg>
                                            <?php
                                            if (!empty($settings['trailer_text'])) {
                                            ?>
                                                <span class="w-trailor"><?php echo esc_html($settings['trailer_text'],"streamit-extensions") ?></span>
                                            <?php } ?>
                                        </a>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

        <?php }
            }
        } ?>

    </div>
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44" width="44px" height="44px" id="circle" fill="none" stroke="currentColor">
            <circle r="20" cy="22" cx="22" id="test"></circle>
        </symbol>
    </svg>
</div>