<?php
   namespace Elementor; 
   $html = '';
   if ( ! defined( 'ABSPATH' ) ) exit;
   
   $settings = $this->get_settings();
   $args = array();
   $tax_query = array();
   
   $taxargs = array();
   
   $user = wp_get_current_user();
   $user_id = $user->ID;
   $post_ids = get_user_meta( $user_id, '_user_watchlist', true );
   $post_id_array = explode(', ', $post_ids);
   $args = array(
       'post_type'         => array('movie','tv_show'),
       'post__in'          => $post_id_array,
       'post_status'       => 'publish',  
      
   );
   
   if(!empty($settings['iq_movie_genre']))
   {
       $tax_query['taxonomy'] = 'movie_genre';
       $tax_query['field'] = 'term_id';
       $tax_query['terms'] = $settings['iq_movie_genre'];
       $tax_query['operator'] = 'IN';
       array_push($taxargs, $tax_query);
   
   }
   
   if(!empty($settings['iq_movie_tag']))
   {
       $tax_query['taxonomy'] = 'movie_tag';
       $tax_query['field'] = 'term_id';
       $tax_query['terms'] = $settings['iq_movie_tag'];
       $tax_query['operator'] = 'IN';
       array_push($taxargs, $tax_query);
   
   }
   
   if(!empty($tax_query))
   {
       $args['tax_query'] = $taxargs;
       $args['tax_query']['relation'] = 'OR';
   }
   $wp_query = new \WP_Query($args);
   ?>
<div class="watchlist-contens">
<div class="row"> 
    <?php 
         if($wp_query->have_posts()) 
         {   
            	while ( $wp_query->have_posts() ) 
           		{
                    $wp_query->the_post();
                    
                    $movie_logo = get_field('name_logo' , get_the_ID());
                    $trailer_link = get_field('name_trailer_link' , get_the_ID());
                    $movie_run_time = get_post_meta(  get_the_ID() , '_movie_run_time');
                    $movie_url_link = get_post_meta(  get_the_ID() , '_movie_url_link');
                    $movie_choice = get_post_meta(  get_the_ID() , '_movie_choice');
                    $meta = get_post_meta(  get_the_ID());
                    
                    $run_time = '';
                    $url_link = '';
                    $censor_rating = '';
                    if(isset($movie_run_time[0]))
                    {
                        $run_time = $movie_run_time[0];
                    }
                    if(isset($movie_censor_rating[0]))
                    {
                        $censor_rating = $movie_censor_rating[0];
                    }

                    if(isset($movie_choice[0]))
                    {
                        if($movie_choice[0] == 'movie_url')
                        {
                                $url_link = $movie_url_link[0];
                        }
                        else
                        {
                                $url_link = get_the_permalink();
                        }
                    }
                    $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "full" );
    ?>
                                 
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="block-images position-relative">
                        <div class="img-box">
                        <img src="<?php echo esc_url($full_image[0]) ?>" class="img-fluid" alt="">
                        </div>
                        <div class="block-description">
                        <h6 class="iq-title"><?php the_title(); ?></h6>
                        <div class="movie-time d-flex align-items-center my-2">
                            <div class="badge badge-secondary p-1 mr-2"><?php echo esc_html($censor_rating); ?></div>
                            <span class="text-white"><?php echo esc_html($run_time); ?></span>
                        </div>
                        <div class="hover-buttons">
                            
                            <a href="<?php echo esc_url($url_link); ?>"  class="btn btn-hover">
                                <i class="fa fa-play mr-1" aria-hidden="true"></i>
                            
                                <?php echo esc_html($settings['play_now_text']); ?> 
                                </a>
                            
                        </div>
                        </div>
                        <div class="block-social-info">
                        <ul class="list-inline p-0 m-0 music-play-lists">
                            <li>
                                <span>
                                <a href="<?php echo esc_url($trailer_link); ?>" class="video-open playbtn">
                                    <i class="ri-volume-mute-fill"></i>
                                </a>
                                    
                                    
                                </span>
                            </li>
                            <li><span><?php echo do_shortcode('[wp_ulike for="movie" id="'.get_the_ID().'" style="wpulike-heart"]'); ?></span></li>
                            <li>
                                <a class="watch-list" rel="<?php echo get_the_ID(); ?>">
                                    <?php 
                                    echo add_to_watchlist(get_the_ID()); 
                                    ?>    
                                </a>
                            </li>
                        </ul>
                        </div>
                        </div> 
                    </div>           
    <?php } } ?>
</div> 
</div>