<?php

namespace Elementor;

if (!defined('ABSPATH')) exit;

$html = '';

//$this->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );
$settings = $this->get_settings();
// $align = $settings['align'];

// if ($settings['iqonic_has_box_shadow'] == 'yes') {

//     $align .= ' iq-box-shadow';
// }
// echo $settings['icon_for_true']['value'];
$active = '';
if (isset($settings['iq_is_active']) && !empty($settings['iq_is_active'])) {
    $this->add_render_attribute('active_data', 'data-active', $settings['iq_is_active']);
    $active = $settings['iq_is_active'];
}

//price header render attribute
$args = array(
    'post_type'         => 'pricing',
    'post_status'       => 'publish',
);
$term_by = 'name';
if (isset($settings['select_price_todisplay']) && !empty($settings['select_price_todisplay'])) {
    $args['post__in'] = $settings['select_price_todisplay'];
}
if (isset($settings['select_pricecategory_todisplay']) && !empty($settings['select_pricecategory_todisplay'])) {
    $term_by = 'slug';
    $price_cat = $settings['select_pricecategory_todisplay'];
} else {
    $price_cat = iq_get_custom_texonomy('pricing_categories', array(), 'id');
}
$wp_query = new \WP_Query($args);
$cat = $price_cat;
$header = [];
$footer = [];
$body = [];
$link_id = '';
foreach ($cat as $key => $val) {
    $body[$key] = [];
}
if ($wp_query->have_posts()) {
    while ($wp_query->have_posts()) {
        $wp_query->the_post();
        $meta = get_post_meta(get_the_ID());
        $header[] = [
            "title" => get_the_title(),
            "period" => !empty($meta['name_period'][0]) ? $meta['name_period'][0] : '',
            "price" => !empty($meta['name_price'][0]) ? $meta['name_price'][0] : '',
            "paid_sub_ids" => !empty($meta['name_paid_sub_id'][0]) ? $meta['name_paid_sub_id'][0] : ''

        ];
        $link_id = $meta['name_pricing_redirect_link'][0];
        foreach ($cat as $key => $val) {
            $body[$key][] = !empty($meta[$key]) && count($meta[$key]) > 0 ? $meta[$key][0] : '';
        }

        $footer[] = [
            "btn_link" => !empty($meta['name_pricing_redirect_link'][0]) ? (get_permalink($meta['name_pricing_redirect_link'][0]) . '?subscription_plan=' . $meta['name_paid_sub_id'][0]) : '#',
        ];
    }
}
$style2 = 'iq-pricing-card';
$text_align = 'text-center';
if ($settings['price_layout'] === 'style2') {
    $style2 = 'iq-pricing-card-two';
    $text_align = 'text-left';
}
?>
<div class="<?php echo esc_html($style2, "streamit-extensions"); ?>">
    <div class="table-responsive iq-pricing pt-2">
        <table id="my-table" class="table" <?php echo $this->get_render_attribute_string('active_data') ?>>
            <thead>
                <tr>
                    <th class="text-center iq-price-head"></th>
                    <?php
                    foreach ($header as $head) {
                        $class_active = str_replace(' ', '-', strtolower($head['title']));
                    ?>
                        <th class="text-center iq-price-head <?php echo esc_html($class_active, "streamit-extensions"); ?>">
                            <div class="iq-price-box">
                                <?php if ($settings['price_layout'] === 'style1') { ?>
                                    <<?php echo $settings['title_tag']; ?> class="iq-price-rate text-white"><?php echo esc_attr($head['price']); ?><small> / <?php echo esc_attr($head['period']); ?></small></<?php echo esc_attr($settings['title_tag']); ?>>
                                <?php } ?>
                                <span class="type"><?php echo esc_attr($head['title']); ?></span>
                            </div>
                        </th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($cat as $key => $cat_list) {
                    $category_details = get_term_by($term_by, $cat_list, 'pricing_categories');
                    $c_id = $category_details->term_id;
                    $type = get_term_meta($c_id, 'name_cat_type', true);
                    $type = !empty($type) ? $type : 'checkbox';
                ?>
                    <tr>
                        <th class="<?php echo esc_html($text_align, "streamit-extensions") ?>" scope="row"> <?php echo esc_attr($cat_list); ?></th>
                        <?php foreach ($header as $index => $val) { ?>
                            <td class="text-center iq-child-cell">
                                <?php
                                switch ($type) {
                                    case 'checkbox':
                                        if ($body[$key][$index] === 'yes')
                                            echo '<i class="' . $settings['icon_for_true']['value'] . '"></i>';
                                        else
                                            echo '<i class="' . $settings['icon_for_false']['value'] . '"></i>';
                                        break;
                                    case 'text':
                                    default:
                                        echo esc_html($body[$key][$index]);
                                        break;
                                }
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                <?php if ($settings['price_layout'] === 'style1') { ?>
                    <tr>
                        <th class="text-center iq-price-footer"></th>
                        <?php foreach ($footer as $foot) { ?>
                            <td class="text-center iq-price-footer">
                                <?php if (!empty($settings['pricing_btn_text'])) { ?>
                                    <div class="align-items-center r-mb-23" data-animation-in="fadeInUp" data-delay-in="1.3">
                                        <a href="<?php echo esc_url($foot['btn_link']); ?>" class="btn btn-hover iq-button"><?php echo esc_html($settings['pricing_btn_text']); ?></a>
                                    </div>
                                <?php } ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php if ($settings['price_layout'] === 'style2') { ?>
        <div class="iq-price-bottom">
            <div class="iq-select-price row">
                <?php
                $i = 0;
                $count = 12;
                if (count($header) === 2) {
                    $count = 6;
                } else if (count($header) === 3) {
                    $count = 4;
                } else if (count($header) === 4) {
                    $count = 3;
                } else if (count($header) === 5) {
                    $count = 2;
                }

                foreach ($header as $head) {
                    $class_active = str_replace(' ', '-', strtolower($head['title']));
                    $active_class = '';
                    if ($active === $class_active) {
                        $active_class = 'active';
                    }
                ?>
                    <div class="col-lg-<?php echo esc_attr($count); ?> col-md-6 col-sm-12">
                        <div class="iq-price-rate-wrap <?php echo esc_attr($active_class); ?>" data-paid-id="<?php echo esc_attr($head['paid_sub_ids']) ?>">
                            <i class="fa fa-check-square"></i>
                            <div class="iq-price-label">
                                <span class="type"><?php echo esc_attr($head['title']); ?></span>
                                <<?php echo esc_attr($settings['title_tag']); ?> class="iq-price-rate text-white"><?php echo esc_attr($head['price']); ?><small> / <?php echo esc_attr($head['period']); ?></small></<?php echo esc_attr($settings['title_tag']); ?>>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php if (!empty($settings['pricing_btn_text'])) { ?>
                <div class="align-items-center r-mb-23" data-animation-in="fadeInUp" data-delay-in="1.3">
                    <a href="<?php echo esc_url(get_permalink($link_id)); ?>" class="btn btn-hover iq-button"><?php echo esc_html($settings['pricing_btn_text']); ?></a>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
    <?php wp_reset_postdata(); ?>
</div>