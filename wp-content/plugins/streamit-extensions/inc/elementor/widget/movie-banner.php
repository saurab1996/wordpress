<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Movie_Banner extends Widget_Base {

	public function get_name() {
		return __( 'Iq_Movie_Banner',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Movie Banner',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-person';
	}

	

	protected function _register_controls() {

		
		$this->start_controls_section(
			'section_Team',
			[
				'label' => __( 'Movie Banner',"streamit-extensions" ),
			]
		);
	
		$this->add_control(
			'play_now_text', 
			[
				'label' => __( 'Play Now Text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Play Now' , 'plugin-domain' ),
				'label_block' => true,
			]
		);
      $this->add_control(
			'moredetail_text', 
			[
				'label' => __( 'More Details Text',"streamit-extensions" ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'More Details' ,"streamit-extensions" ),
				'label_block' => true,
			]
		);
        		
        $repeater = new Repeater();
       
		$repeater->add_control(
			'iq_movie',
			[
				'label' => __( 'Select Movie',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_custom_post_data('movie' , array() , 'id'),
				
			]
		);
		$repeater->add_control(
			'slider_image',
			[
				'label' => __( 'Slider Image',"streamit-extensions" ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				
				
			]
		);
		

        $this->add_control(
			'tabs',
			[
				'label' => __( 'Movie List',"streamit-extensions" ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
			]
        );
       
        

        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/movie-banner.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

		<script>	
		jQuery('#parallaxslider').slick({
			dots: false,
			arrows: false,
			infinite: true,
			speed: 300,
			autoplay: true,
			slidesToShow: 1,
			slidesToScroll: 1,
		
		});


		</script>
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Movie_Banner() );