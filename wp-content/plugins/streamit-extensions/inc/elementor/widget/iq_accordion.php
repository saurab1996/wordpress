<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Accordion extends Widget_Base {

	public function get_name() {
		return __( 'iqonic_accordion',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Iqonic Accordion',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-help-o';
	}

	
	public function get_script_depends() {
		return [ 'iqonic-custom' ];
	 }

	protected function _register_controls() {

        
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Accordion',"streamit-extensions" ),
			]
		);

        $this->add_control(
            'design_style',
            [
                'label'      => __( 'Design Style',"streamit-extensions" ),
                'type'       => Controls_Manager::SELECT,
                'default'    => '1',
                'options'    => [

                    '1'          => __( 'Style 1',"streamit-extensions" ),
                    '2'          => __( 'Style 2',"streamit-extensions" ),
                    '3'          => __( 'Style 3',"streamit-extensions" ),


                ],
            ]
        );
        
        $repeater = new Repeater();
        $repeater->add_control(
			'tab_title',
			[
				'label' => __( 'Question',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'What is Lorem Ipsum?',"streamit-extensions" ),
				'placeholder' => __( 'Tab Title',"streamit-extensions" ),
				'label_block' => true,
			]
        );
        
        $repeater->add_control(
			'tab_content',
			[
				'label' => __( 'Answer',"streamit-extensions" ),
				'default' => __( 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',"streamit-extensions" ),
				'placeholder' => __( 'Tab Content',"streamit-extensions" ),
				'type' => Controls_Manager::TEXTAREA,
				'show_label' => false,
			]
        );
        

        
        $this->add_control(
			'tabs',
			[
				'label' => __( 'Tabs Items',"streamit-extensions" ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'tab_title' => __( 'Tab #1',"streamit-extensions" ),
                        'tab_content' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',"streamit-extensions" ),
                       
					]
					
				],
				'title_field' => '{{{ tab_title }}}',
			]
        );
        $this->add_control(
			'has_icon',
			[
				'label' => __( 'Use Icon?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
		);
		
		$this->add_control(
			'iqonic_has_box_shadow',
			[
				'label' => __( 'Box Shadow?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        
        $this->add_control(
			'active_icon',
			[
				'label' => __( 'Active Icon',"streamit-extensions" ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star'
					
				],
				'condition' => [
					'has_icon' => 'yes',
				],
				'label_block' => false,
				'skin' => 'inline',

				
			]
		);
		$this->add_control(
			'inactive_icon',
			[
				'label' => __( 'Inactive Icon',"streamit-extensions" ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star'
					
				],
				'condition' => [
					'has_icon' => 'yes',
				],
				'label_block' => false,
				'skin' => 'inline',

				
			]
		);
		$this->add_responsive_control(
			'icon_position',
			[
				'label' => __( 'Icon Position',"streamit-extensions" ),
                'type' => Controls_Manager::CHOOSE,
                'default' => 'right',
				'options' => [
					'left' => [
						'title' => __( 'Left',"streamit-extensions" ),
						'icon' => 'eicon-text-align-left',
					],
					
					'right' => [
						'title' => __( 'Right',"streamit-extensions" ),
						'icon' => 'eicon-text-align-right',
					],
					
                ],
                'condition' => [
					'has_icon' => 'yes',
				],
			]
		);

		$this->add_control(
			'title_tag',
			[
				'label'      => __( 'Title Tag',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'h4',
				'options'    => [
					
					'h1'          => __( 'h1',"streamit-extensions" ),
					'h2'          => __( 'h2',"streamit-extensions" ),
					'h3'          => __( 'h3',"streamit-extensions" ),
					'h4'          => __( 'h4',"streamit-extensions" ),
					'h5'          => __( 'h5',"streamit-extensions" ),
					'h6'          => __( 'h6',"streamit-extensions" ),
					
					
				],
			]
		);


        $this->end_controls_section();

        

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Text Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-title .accordion-title' => 'color: {{VALUE}};',
					
				],
			]
		);

		$this->add_control(
			'title_active_color',
			[
				'label' => __( 'Text Active Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-active .iq-accordion-title .accordion-title' => 'color: {{VALUE}};',
					
				],
			]
		);

		$this->add_control(
			'title_back_color',
			[
				'label' => __( 'Background Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-title' => 'background: {{VALUE}};',
					
				],
			]
		);

		$this->add_control(
			'title_back_active_color',
			[
				'label' => __( 'Active Background Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-active .iq-accordion-title' => 'background: {{VALUE}};',
					
				],
			]
		);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'content_color',
			[
				'label' => __( 'Content Text Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-details' => 'color: {{VALUE}};',
					
				],
			]
		);

		$this->add_control(
			'content_back_color',
			[
				'label' => __( 'Background Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-details' => 'background: {{VALUE}};',
					
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'icon_active_color',
			[
				'label' => __( 'Active Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-block.iq-active .iq-accordion-title i.active' => 'color: {{VALUE}};',
					
				],
			]
		);

		$this->add_control(
			'icon_inactive_color',
			[
				'label' => __( 'Inactive Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-block .iq-accordion-title i.inactive' => 'color: {{VALUE}};',
					
				],
			]
		);

		$this->end_controls_section();
		
		$this->start_controls_section(
			'section_border_style',
			[
				'label' => __( 'Border',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'has_border',
			[
				'label' => __( 'Border?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'label_off',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        $this->add_control(
			'border_style',
				[
					'label' => __( 'Border Style',"streamit-extensions" ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid',"streamit-extensions" ),
						'dashed' => __( 'Dashed',"streamit-extensions" ),
						'dotted' => __( 'Dotted',"streamit-extensions" ),
						'double' => __( 'Double',"streamit-extensions" ),
						'outset' => __( 'outset',"streamit-extensions" ),
						'groove' => __( 'groove',"streamit-extensions" ),
						'ridge' => __( 'ridge',"streamit-extensions" ),
						'inset' => __( 'inset',"streamit-extensions" ),
						'hidden' => __( 'hidden',"streamit-extensions" ),
						'none' => __( 'none',"streamit-extensions" ),
						
					],
					'condition' => [
					'has_border' => 'yes',
					],
					'selectors' => [
						'{{WRAPPER}} .iq-accordion .iq-accordion-block' => 'border-style: {{VALUE}};',
						
					],
				]
			);

		$this->add_control(
			'border_active_color',
			[
				'label' => __( 'Active Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-block.iq-active' => 'border-color: {{VALUE}};',
					
				],
				'condition' => [
					'has_border' => 'yes',
				],
			]
		);

		$this->add_control(
			'border_inactive_color',
			[
				'label' => __( 'Inactive Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-block' => 'border-color: {{VALUE}};',
					
				],
				'condition' => [
					'has_border' => 'yes',
				],
			]
		);

		
		$this->add_control(
			'border_width',
			[
				'label' => __( 'Border Width',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-accordion .iq-accordion-block' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'has_border' => 'yes',
				],
			]
		);

		$this->end_controls_section();

        


	}
	
	protected function render() {
		$settings = $this->get_settings();
        require  IQ_TH_ROOT . '/inc/elementor/render/iq_accordion.php';

        ?>
        	<script type="text/javascript">
        		 /*------------------------
                Accordion
                --------------------------*/
                jQuery('.iq-accordion .iq-accordion-block .iq-accordion-details').hide();
                jQuery('.iq-accordion .iq-accordion-block:first').addClass('iq-active').children().slideDown('slow');
                jQuery('.iq-accordion .iq-accordion-block').on("click", function() {
                    if (jQuery(this).children('div.iq-accordion-details').is(':hidden')) {
                        jQuery('.iq-accordion .iq-accordion-block').removeClass('iq-active').children('div.iq-accordion-details').slideUp('slow');
                        jQuery(this).toggleClass('iq-active').children('div.iq-accordion-details').slideDown('slow');
                    }
                });

        	</script>
        <?php 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Accordion() );
