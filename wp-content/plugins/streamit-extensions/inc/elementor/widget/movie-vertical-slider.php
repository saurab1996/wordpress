<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Movie_Vertical_Slider extends Widget_Base {

	public function get_name() {
		return __( 'Iq_Movie_Vertical_Slider',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Movie  Vertical Slider',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-person';
	}

	

	protected function _register_controls() {

		
		$this->start_controls_section(
			'section_Team',
			[
				'label' => __( 'Movie Baner Slider',"streamit-extensions" ),
			]
		);
		  $this->add_control(
			'movie_section_title',
			[
				'label' => __( 'Title',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
                'label_block' => true,
                'default' => __( 'Title',"streamit-extensions" ),
			]
		);
		$this->add_control(
			'play_now_text', 
			[
				'label' => __( 'Play Now Text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Play Now' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

        $this->add_control(
			'iq_movie_genre',
			[
				'label' => __( 'Display Movie From Specific Genre',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT2,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_get_custom_texonomy('movie_genre'),
				
			]
		);
        
        $this->add_control(
			'iq_movie_tag',
			[
				'label' => __( 'Display Movie From Specific Tags',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT2,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_get_custom_texonomy('movie_tag'),
				
			]
		);

        

        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/movie-vertical-slider.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

		<script>	
		jQuery('#top-ten-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '#top-ten-slider-nav',
			responsive: [
			{
				breakpoint: 992,
				settings: {
				asNavFor: false,
				arrows: true,
				nextArrow: '<button class="NextArrow"><i class="ri-arrow-right-s-line"></i></button>',
				prevArrow: '<button class="PreArrow"><i class="ri-arrow-left-s-line"></i></button>',
				}
			}
			]
		});
		jQuery('#top-ten-slider-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '#top-ten-slider',
			dots: false,
			arrows: true,
			infinite: true,
			vertical:true,
			verticalSwiping: true,
			centerMode: false,
			nextArrow:'<button class="NextArrow"><i class="ri-arrow-down-s-line"></i></button>',
			prevArrow:'<button class="PreArrow"><i class="ri-arrow-up-s-line"></i></button>',
			focusOnSelect: true,
			responsive: [		    
				{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
				}
				},
				{
					breakpoint: 600,
					settings: {
						asNavFor: false,
					}
				},
			]
		});

		</script>
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Movie_Vertical_Slider() );