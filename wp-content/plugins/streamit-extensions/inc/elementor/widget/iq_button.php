<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

/**
 * Elementor Blog widget.
 *
 * Elementor widget that displays an eye-catching headlines.
 *
 * @since 1.0.0
 */

class Iq_Button extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve heading widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */

	public function get_name() {
		return __( 'iq_button',"streamit-extensions" );
	}

	/**
	 * Get widget Title.
	 *
	 * Retrieve heading widget Title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget Title.
	 */
	
	public function get_title() {
		return __( 'Iqonic Button',"streamit-extensions" );
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the heading widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */


	public function get_categories() {
		return [ 'streamit-extensions' ];
	}


	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */

	public function get_icon() {
		return 'eicon-button';
	}

	protected function _register_controls() {

			$this->start_controls_section(
			'section_21eZ2eh1Myn3Vx5qrK29',
			[
				'label' => __( 'Button',"streamit-extensions" ),
			]
		);

        
        $this->add_control(
			'button_text',
			[
				'label' => __( 'Text',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
				'default' => __( 'Read More',"streamit-extensions" ),
			]
        );

        
        
        $this->add_control(
			'has_icon',
			[
				'label' => __( 'Use Icon?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        
        $this->add_control(
			'button_icon',
			[
				'label' => __( 'Icon',"streamit-extensions" ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star'
					
                ],
                'condition' => [
					'has_icon' => 'yes',
				],
			]
        );
        

		$this->add_responsive_control(
			'icon_position',
			[
				'label' => __( 'Icon Position',"streamit-extensions" ),
                'type' => Controls_Manager::CHOOSE,
                'default' => 'right',
				'options' => [
					'left' => [
						'title' => __( 'Left',"streamit-extensions" ),
						'icon' => 'eicon-text-align-left',
					],
					
					'right' => [
						'title' => __( 'Right',"streamit-extensions" ),
						'icon' => 'eicon-text-align-right',
					],
					
                ],
                'condition' => [
					'has_icon' => 'yes',
				],
			]
		);

		$this->add_control(
			'button_action',
			[
				'label' => __( 'Action',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'movie_tv'  => __( 'Open Mmovie / Tv Show',"streamit-extensions" ),
					'popup'  => __( 'Open Popup',"streamit-extensions" ),
					'link'  => __( 'Open Link',"streamit-extensions" ),
					'none'  => __( 'none',"streamit-extensions" ),
					
				],
			]
		);
		
		$this->add_control(
			'iq_movie_tv_id',
			[
				'label' => __( 'Select Movie / Tv Show',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_custom_post_data(array('movie','tv_show') , array() , 'id'),
				'condition' => ['button_action' => 'movie_tv']
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link',"streamit-extensions" ),
				'type' => Controls_Manager::URL,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => __( 'https://your-link.com',"streamit-extensions" ),
				'default' => [
					'url' => '#',
				],
				'condition' => ['button_action' => 'link']
			]
		);



        $this->end_controls_section();

        $this->start_controls_section(
			'section_header',
			[
				'label' => __( 'Model Header',"streamit-extensions" ),
				'condition' => ['button_action' => 'popup']
			]
		);

		$this->add_control(
			'model_title',
			[
				'label' => __( 'Title',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
				'default' => __( 'Model Title',"streamit-extensions" ),
			]
        );


		$this->add_control(
			'model_selected_icon',
			[
				'label' => __( 'Icon',"streamit-extensions" ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star'
					
                ],
                
			]
        );


        $this->end_controls_section();

        $this->start_controls_section(
			'section_body',
			[
				'label' => __( 'Model Body',"streamit-extensions" ),
				'condition' => ['button_action' => 'popup']
			]
		);
		$this->add_control(
			'model_body',
			[
				'label' => __( 'Description',"streamit-extensions" ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => __( 'Default description',"streamit-extensions" ),
				'placeholder' => __( 'Type your description here',"streamit-extensions" ),
			]
		);


        $this->end_controls_section();
        

      	
        $this->start_controls_section(
			'section_uxf6ePJ1WkNzb9E5h72H',
			[
				'label' => __( 'Button Container',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
				
			]
		);
		$this->add_control(
			'btn_has_box_shadow',
			[
				'label' => __( 'Use Box Shadow?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        $this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'btn_box_shadow',
				'label' => __( 'Box Shadow',"streamit-extensions" ),
				'selector' => '{{WRAPPER}} .iq-button',
				'condition' => ['has_box_shadow' => 'yes']
			]
		);
		$this->add_responsive_control(
			'container_padding',
			[
				'label' => __( 'Padding',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'container_margin',
			[
				'label' => __( 'Margin',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);
		$this->add_responsive_control(
			'align_btn',
			[
				'label' => __( 'Alignment',"streamit-extensions" ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left',"streamit-extensions" ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center',"streamit-extensions" ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right',"streamit-extensions" ),
						'icon' => 'eicon-text-align-right',
					],
					
				],
				'selectors' => [
					'{{WRAPPER}} .iq-btn-container' => 'text-align: {{value}};',
					
				]
			]
		);

		$this->end_controls_section();
		
		$this->start_controls_section(
			'section_d6iw352ACG3c5r1zvhYp',
			[
				'label' => __( 'Button Style',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
				
			]
		);

		$this->add_control(
			'button_size',
			[
				'label' => __( 'Size',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'iq-btn-small'  => __( 'Small',"streamit-extensions" ),
					'iq-btn-medium' => __( 'Medium',"streamit-extensions" ),
					'iq-btn-large' => __( 'Large',"streamit-extensions" ),					
					'iq-btn-extra-large' => __( 'Extra Large',"streamit-extensions" ),					
					'default' => __( 'Default',"streamit-extensions" ),
				],
			]
        );
        
        $this->add_control(
			'button_shape',
			[
				'label' => __( 'Shape',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'iq-btn-round'  => __( 'Round',"streamit-extensions" ),
					'iq-btn-semi-round' => __( 'Semi Round',"streamit-extensions" ),
					'iq-btn-circle' => __( 'Circle',"streamit-extensions" ),					
					'default' => __( 'Default',"streamit-extensions" ),
				],
			]
        );
       
        
        $this->add_control(
			'button_style',
			[
				'label' => __( 'Button Style',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'iq-btn-flat'  => __( 'Flat',"streamit-extensions" ),
					'iq-btn-outline' => __( 'Outline',"streamit-extensions" ),					
					'iq-btn-link' => __( 'Link Button',"streamit-extensions" ),					
					'default' => __( 'Default',"streamit-extensions" ),
				],
			]
        );

        $this->end_controls_section();
        // Button style End

        // Button Text Style
        $this->start_controls_section(
			'section_d1da6dnvYM43C71weL29',
			[
				'label' => __( 'Button Text Color',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
				
			]
		);
		
		$this->start_controls_tabs( 'contact_tabs' );
		$this->start_controls_tab(
			'tabs_o8I22AKRc2bJa7BgdwHW',
			[
				'label' => __( 'Normal',"streamit-extensions" ),
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Choose Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-button' => 'color: {{VALUE}};',
		 		],
				
				
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab(
			'tabs_1322c8M564ER8L6I65U0',
			[
				'label' => __( 'Hover',"streamit-extensions" ),
			]
		);

		$this->add_control(
			'data_hover_text',
			[
				'label' => __( 'Choose Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				
				'selectors' => [
					'{{WRAPPER}} .iq-button:hover' => 'color: {{VALUE}};',
		 		],
				
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'btn_text_typography',
				'label' => __( 'Typography',"streamit-extensions" ),				
				'selector' => '{{WRAPPER}} .iq-button',
			]
		);

		$this->end_controls_section();
		// Button Text Style

		// Background Style Start
		
        $this->start_controls_section(
			'section_0s6Y4c68qoBcctzHf68f',
			[
				'label' => __( 'Button Background',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
				
			]
		);
		$this->start_controls_tabs( '_dr6Yu5af63L5yHm3cGc1' );
		$this->start_controls_tab(
			'tabs_z5VRHMPjDcr6wJb0a4vF',
			[
				'label' => __( 'Normal',"streamit-extensions" ),
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'data_background',
				'label' => __( 'Background',"streamit-extensions" ),
				'types' => [ 'classic', 'gradient'],
				'selector' => '{{WRAPPER}} .iq-button',
			]
		);
		
		
		$this->end_controls_tab();

		$this->start_controls_tab(
			'tabs_Xa27O3BGf5k23KqHfeNM',
			[
				'label' => __( 'Hover',"streamit-extensions" ),
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'data_hover',
				'label' => __( 'Background',"streamit-extensions" ),
				'types' => [ 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .iq-button:hover',
			]
		);
	
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();

        // Border Style Start
        $this->start_controls_section(
			'section_iD8bVLQc8q83f4j5cnJk',
			[
				'label' => __( 'Button Border',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
				
			]
		);
		$this->add_control(
			'has_custom_border',
			[
				'label' => __( 'Use Custom Border?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
		$this->add_control(
			'data_border',
			[
				'label' => __( 'Border Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-button' => 'border-color: {{VALUE}};',
		 		],
		 		'condition' => ['has_custom_border' => 'yes'],
				
			]
		);
		$this->add_control(
			'data_hover_border_outline',
			[
				'label' => __( 'Hover Border Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				
				'selectors' => [
					'{{WRAPPER}} .iq-button:hover' => 'border-color: {{VALUE}};',
		 		],
		 		'condition' => ['has_custom_border' => 'yes'],
				
			]
		);

		

		$this->add_control(
			'border_style',
				[
					'label' => __( 'Border Style',"streamit-extensions" ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid',"streamit-extensions" ),
						'dashed' => __( 'Dashed',"streamit-extensions" ),
						'dotted' => __( 'Dotted',"streamit-extensions" ),
						'double' => __( 'Double',"streamit-extensions" ),
						'outset' => __( 'outset',"streamit-extensions" ),
						'groove' => __( 'groove',"streamit-extensions" ),
						'ridge' => __( 'ridge',"streamit-extensions" ),
						'inset' => __( 'inset',"streamit-extensions" ),
						'hidden' => __( 'hidden',"streamit-extensions" ),
						'none' => __( 'none',"streamit-extensions" ),
						
					],
					'condition' => ['has_custom_border' => 'yes'],
					
					'selectors' => [
						'{{WRAPPER}} .iq-button' => 'border-style: {{VALUE}};',
						
					],
				]
			);

		$this->add_control(
			'border_width',
			[
				'label' => __( 'Border Width',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-button' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => ['has_custom_border' => 'yes'],
				
			]
		);

		$this->add_control(
			'border_radius',
			[
				'label' => __( 'Border Radius',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => ['has_custom_border' => 'yes'],
				
			]
		);
        $this->end_controls_section();
        // Border Style Start

        // Icon Style Start
        $this->start_controls_section(
			'section_qfCKlSw4To1FsPY6B33a',
			[
				'label' => __( 'Icon',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => ['has_icon' => 'yes']
				
			]
		);

		$this->add_control(
			'button_set_icon_size',
			[
				'label' => __( 'Set Icon Size?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'yes' => __( 'Yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
				'return_value' => 'yes',
				'default' => 'no',
			]
		);

		$this->add_control(
			'button_icon_size',
			[
				'label' => __( 'Icon Size',"streamit-extensions" ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'condition' => ['button_set_icon_size' => 'yes'],
				'selectors' => [
					'{{WRAPPER}} .iq-button i' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'icon_spacing_left',
			[
				'label' => __( 'Icon Spacing',"streamit-extensions" ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'condition' => ['icon_position' => 'left'],
				'selectors' => [
					'{{WRAPPER}} .iq-button.has-icon.btn-icon-left i' => 'margin-right: {{SIZE}}{{UNIT}};'
					
				],
			]
		);
		$this->add_control(
			'icon_spacing_right',
			[
				'label' => __( 'Icon Spacing',"streamit-extensions" ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'condition' => ['icon_position' => 'right'],
				'selectors' => [
					
					'{{WRAPPER}} .iq-button.has-icon.btn-icon-right i' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
        require  IQ_TH_ROOT . '/inc/elementor/render/iq_button.php';
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Button() );