<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Title extends Widget_Base {

	public function get_name() {
		return __( 'section_title',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Section Title',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];

		
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-site-title';
	}

	protected function _register_controls() {
	

	
		$this->start_controls_section(
			'section',
			[
				'label' => __( 'Section Title',"streamit-extensions" ),
			]
		);

        $this->add_control(
			'section_title',
			[
				'label' => __( 'Section Title',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
                'label_block' => true,
                'default' => __( 'Section Title',"streamit-extensions" ),
			]
		);

		

		$this->add_control(
			'has_description',
			[
				'label' => __( 'Has Description?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );


		$this->add_control(
			'description',
			[
				'label' => __( 'Description',"streamit-extensions" ),
				'type' => Controls_Manager::TEXTAREA,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => __( 'Enter Title Description',"streamit-extensions" ),
				'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',"streamit-extensions" ),
				'condition' => ['has_description' => 'yes']
			]
		);

		$this->add_control(
			'title_style',
			[
				'label'      => __( 'Title Style',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'iq-title-default',
				'options'    => [
					
					'iq-title-default'          => __( 'Default',"streamit-extensions" ),
					'iq-title-white'          => __( 'White',"streamit-extensions" ),
					
				],
			]
		);

		$this->add_control(
			'title_tag',
			[
				'label'      => __( 'Title Tag',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'h4',
				'options'    => [
					
					'h1'          => __( 'h1',"streamit-extensions" ),
					'h2'          => __( 'h2',"streamit-extensions" ),
					'h3'          => __( 'h3',"streamit-extensions" ),
					'h4'          => __( 'h4',"streamit-extensions" ),
					'h5'          => __( 'h5',"streamit-extensions" ),
					'h6'          => __( 'h6',"streamit-extensions" ),					
				],
			]
		);

				

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment',"streamit-extensions" ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left',"streamit-extensions" ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center',"streamit-extensions" ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right',"streamit-extensions" ),
						'icon' => 'eicon-text-align-right',
					]
				]
			]
		);
		
        $this->end_controls_section();
      
/* Price Table Start*/

        $this->start_controls_section(
			'section_NIfezt7YM7feDaT9vP8J',
			[
				'label' => __( 'Title Box',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

        $this->start_controls_tabs( 'titlebox_tabs' );
		$this->start_controls_tab(
            'tabs_c8fpaelTGDkf951QeYf2',
            [
                'label' => __( 'Normal', 'elementor' ),
            ]
        );
        
		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'titlebox_background',
                'label' => __( 'Background',"streamit-extensions" ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-title-box',
            ]
        );

         
		$this->add_control(
			'titlebox_has_border',
			[
				'label' => __( 'Set Custom Border?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        $this->add_control(
			'titlebox_border_style',
				[
					'label' => __( 'Border Style',"streamit-extensions" ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'condition' => [
					'titlebox_has_border' => 'yes',
					],
					'options' => [
						'solid'  => __( 'Solid',"streamit-extensions" ),
						'dashed' => __( 'Dashed',"streamit-extensions" ),
						'dotted' => __( 'Dotted',"streamit-extensions" ),
						'double' => __( 'Double',"streamit-extensions" ),
						'outset' => __( 'outset',"streamit-extensions" ),
						'groove' => __( 'groove',"streamit-extensions" ),
						'ridge' => __( 'ridge',"streamit-extensions" ),
						'inset' => __( 'inset',"streamit-extensions" ),
						'hidden' => __( 'hidden',"streamit-extensions" ),
						'none' => __( 'none',"streamit-extensions" ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-title-box ' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'titlebox_border_color',
			[
				'label' => __( 'Border Color',"streamit-extensions" ),
				'condition' => [
					'titlebox_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-title-box' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'titlebox_border_width',
			[
				'label' => __( 'Border Width',"streamit-extensions" ),
				'condition' => [
					'titlebox_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'titlebox_border_radius',
			[
				'label' => __( 'Border Radius',"streamit-extensions" ),
				'condition' => [
					'titlebox_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
		
		$this->end_controls_tab();
		$this->start_controls_tab(
            'tabs_49pcfagYof19beG4w8Ee',
            [
                'label' => __( 'Hover', 'elementor' ),
            ]
        );
       
		$this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'titlebox_hover_background',
                'label' => __( 'Hover Background',"streamit-extensions" ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .iq-title-box:hover ',
            ]
        );

       
        $this->add_control(
			'titlebox_hover_has_border',
			[
				'label' => __( 'Set Custom Border?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        $this->add_control(
			'titlebox_hover_border_style',
				[
					'label' => __( 'Border Style',"streamit-extensions" ),
					'condition' => [
					'titlebox_hover_has_border' => 'yes',
					],
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid',"streamit-extensions" ),
						'dashed' => __( 'Dashed',"streamit-extensions" ),
						'dotted' => __( 'Dotted',"streamit-extensions" ),
						'double' => __( 'Double',"streamit-extensions" ),
						'outset' => __( 'outset',"streamit-extensions" ),
						'groove' => __( 'groove',"streamit-extensions" ),
						'ridge' => __( 'ridge',"streamit-extensions" ),
						'inset' => __( 'inset',"streamit-extensions" ),
						'hidden' => __( 'hidden',"streamit-extensions" ),
						'none' => __( 'none',"streamit-extensions" ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-title-box:hover' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'titlebox_hover_border_color',
			[
				'label' => __( 'Border Color',"streamit-extensions" ),
				'condition' => [
					'titlebox_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .iq-title-box:hover' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'titlebox_hover_border_width',
			[
				'label' => __( 'Border Width',"streamit-extensions" ),
				'condition' => [
					'titlebox_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box:hover' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'titlebox_hover_border_radius',
			[
				'label' => __( 'Border Radius',"streamit-extensions" ),
				'condition' => [
					'titlebox_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
		
		$this->end_controls_tab();
		$this->end_controls_tabs();



		$this->add_responsive_control(
			'titlebox_padding',
			[
				'label' => __( 'Padding',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_responsive_control(
			'titlebox_margin',
			[
				'label' => __( 'Margin',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);


		$this->end_controls_section();

		 /* Price table End*/



        // Title Style Section
        $this->start_controls_section(
			'section_f4aS9uHc50Of5eNP8jbc',
			[
				'label' => __( 'Title',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'mobile_typography',
				'label' => __( 'Typography',"streamit-extensions" ),				
				'selector' => '{{WRAPPER}} .iq-title-box .iq-title',
			]
		);
		
		$this->start_controls_tabs( 'title_tabs' );

		$this->start_controls_tab(
			'title_color_tab_normal',
			[
				'label' => __( 'normal',"streamit-extensions" ),
			]
		);

		$this->add_control(
			'title_normal_color',
			[
				'label' => __( 'Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title' => 'color: {{VALUE}};',
		 		],
				
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'title_back_color',
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-title-box .iq-title',				
			]
		);

		$this->add_control(
			'iq_title_has_border',
			[
				'label' => __( 'Set Custom Border?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        $this->add_control(
			'iq_title_border_style',
				[
					'label' => __( 'Border Style',"streamit-extensions" ),
					'condition' => [
					'iq_title_has_border' => 'yes',
					],
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid',"streamit-extensions" ),
						'dashed' => __( 'Dashed',"streamit-extensions" ),
						'dotted' => __( 'Dotted',"streamit-extensions" ),
						'double' => __( 'Double',"streamit-extensions" ),
						'outset' => __( 'outset',"streamit-extensions" ),
						'groove' => __( 'groove',"streamit-extensions" ),
						'ridge' => __( 'ridge',"streamit-extensions" ),
						'inset' => __( 'inset',"streamit-extensions" ),
						'hidden' => __( 'hidden',"streamit-extensions" ),
						'none' => __( 'none',"streamit-extensions" ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-title-box .iq-title' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'iq_title_border_color',
			[
				'label' => __( 'Border Color',"streamit-extensions" ),
				'condition' => [
					'iq_title_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box .iq-title' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'iq_title_border_width',
			[
				'label' => __( 'Border Width',"streamit-extensions" ),
				'condition' => [
					'iq_title_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box .iq-title' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'iq_title_border_radius',
			[
				'label' => __( 'Border Radius',"streamit-extensions" ),
				'condition' => [
					'iq_title_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box .iq-title' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	

		$this->end_controls_tab();

		$this->start_controls_tab(
			'title_color_tab_hover',
			[
				'label' => __( 'Hover',"streamit-extensions" ),
			]
		);

		$this->add_control(
			'title_hover_color',
			[
				'label' => __( 'Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title:hover' => 'color: {{VALUE}};',
		 		],
				
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'title_hover_back_color',
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .iq-title-box:hover .iq-title',				
			]
		);


		$this->add_control(
			'iq_title_hover_has_border',
			[
				'label' => __( 'Set Custom Border?',"streamit-extensions" ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'no',
				'yes' => __( 'yes',"streamit-extensions" ),
				'no' => __( 'no',"streamit-extensions" ),
			]
        );
        $this->add_control(
			'iq_title_hover_border_style',
				[
					'label' => __( 'Border Style',"streamit-extensions" ),
					'condition' => [
					'iq_title_hover_has_border' => 'yes',
					],
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'solid'  => __( 'Solid',"streamit-extensions" ),
						'dashed' => __( 'Dashed',"streamit-extensions" ),
						'dotted' => __( 'Dotted',"streamit-extensions" ),
						'double' => __( 'Double',"streamit-extensions" ),
						'outset' => __( 'outset',"streamit-extensions" ),
						'groove' => __( 'groove',"streamit-extensions" ),
						'ridge' => __( 'ridge',"streamit-extensions" ),
						'inset' => __( 'inset',"streamit-extensions" ),
						'hidden' => __( 'hidden',"streamit-extensions" ),
						'none' => __( 'none',"streamit-extensions" ),
						
					],
					
					'selectors' => [
						'{{WRAPPER}} .iq-title-box:hover .iq-title' => 'border-style: {{VALUE}};',
						
					],
				]
		);

		$this->add_control(
			'iq_title_hover_border_color',
			[
				'label' => __( 'Border Color',"streamit-extensions" ),
				'condition' => [
					'iq_title_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box:hover .iq-title' => 'border-color: {{VALUE}};',
		 		],
				
				
			]
		);

		$this->add_control(
			'iq_title_hover_border_width',
			[
				'label' => __( 'Border Width',"streamit-extensions" ),
				'condition' => [
					'iq_title_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box:hover .iq-title' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);

		$this->add_control(
			'iq_title_hover_border_radius',
			[
				'label' => __( 'Border Radius',"streamit-extensions" ),
				'condition' => [
					'iq_title_hover_has_border' => 'yes',
					],
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}  .iq-title-box:hover .iq-title' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->add_responsive_control(
			'title_margin',
			[
				'label' => __( 'Margin',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	

		$this->add_responsive_control(
			'title_padding',
			[
				'label' => __( 'Padding',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
        $this->end_controls_section();
        // Title Style Section End

      
        // Description Style Section
        $this->start_controls_section(
			'section_ZcASngaa14lc8er55aND',
			[
				'label' => __( 'Description',"streamit-extensions" ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => ['has_description' => 'yes']
			]
		);

		$this->add_control(
			'description_heading_color',
			[
				'label' => __( 'Color',"streamit-extensions" ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		
		$this->start_controls_tabs( 'description_tabs' );


		$this->start_controls_tab(
			'description_color_tab_normal',
			[
				'label' => __( 'normal',"streamit-extensions" ),
			]
		);

		$this->add_control(
			'description_normal_color',
			[
				'label' => __( 'Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title-desc' => 'color: {{VALUE}};',
		 		],
				
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'description_color_tab_hover',
			[
				'label' => __( 'Hover',"streamit-extensions" ),
			]
		);

		$this->add_control(
			'description_hover_color',
			[
				'label' => __( 'Color',"streamit-extensions" ),
				'type' => Controls_Manager::COLOR,
				
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title-desc:hover' => 'color: {{VALUE}};',
		 		],
				
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();
	

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'description_typography',
				'label' => __( 'Typography',"streamit-extensions" ),				
				'selector' => '{{WRAPPER}} .iq-title-box .iq-title-desc',
			]
		);
        
        $this->add_responsive_control(
			'desciption_marging',
			[
				'label' => __( 'Margin',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title-desc' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	

		$this->add_responsive_control(
			'desciption_padding',
			[
				'label' => __( 'Padding',"streamit-extensions" ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .iq-title-box .iq-title-desc' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				
			]
		);	
		$this->end_controls_section();
        // Descrition Style Section End

       


	}
	
	protected function render() {
		$settings = $this->get_settings();
        require  IQ_TH_ROOT . '/inc/elementor/render/iq_section_title.php';
    }	    
		
}
Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Title() );