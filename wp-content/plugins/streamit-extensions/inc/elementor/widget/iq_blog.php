<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

/**
 * Elementor Blog widget.
 *
 * Elementor widget that displays an eye-catching headlines.
 *
 * @since 1.0.0
 */

// Blog Category
if ( !function_exists('iq_by_blog_cat')) {
	function iq_by_blog_cat() {
		$taxonomy = 'category';
		$iq_blog_cat = array();
		$terms = get_terms($taxonomy);

			foreach ( $terms as $term ) {			   
			    $iq_blog_cat[$term->slug] = $term->name;
		    }
		    return $iq_blog_cat;
	}	
}

class IQONIC_Blog extends Widget_Base {
	
	/**
	 * Get widget name.
	 *
	 * Retrieve heading widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */

	public function get_name() {
		return __( 'iqonic_blog',"streamit-extensions" );
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve heading widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	
	public function get_title() {
		return __( 'Iqonic Blog',"streamit-extensions" );
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the heading widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */

	public function get_icon() {
		return 'eicon-slider-push';
	}

	/**
	 * Register heading widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */

	protected function _register_controls() {

		$this->start_controls_section(
			'section_blog',
			[
				'label' => __( 'Blog Post',"streamit-extensions" ),
				
			]
		);


		$this->add_control(
			'blog_type',
			[
				'label'      => __( 'Blog Type',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => '1',
				'options'    => [
					
					'1'          => __( 'Blog Slider',"streamit-extensions" ),
					'2'          => __( 'Blog Grid',"streamit-extensions" ),
				],
			]
		);

        $this->add_control(
			'blog_style',
			[
				'label'      => __( 'Grid',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'condition' => [
					'blog_type' => '2',
				],
				'options'    => [
					'2'          => __( 'Blog 1 Columns',"streamit-extensions" ),
					'3'          => __( 'Blog 2 Columns',"streamit-extensions" ),
					'4'          => __( 'Blog 3 Columns',"streamit-extensions" ),
				],
				'default'    => '1',
			]
		);
		
		$this->add_control(
            'blog_cat',
            [
                'label' => esc_html__( 'Select blog category',"streamit-extensions" ),
                'type' => Controls_Manager::SELECT2,
                'return_value' => 'true',
                'multiple' => true,
                'options' => iq_by_blog_cat(),
            ]
        );
		$this->add_responsive_control(
			'posts_per_page',
			[
				'label' => __( 'Posts Per Page',"streamit-extensions" ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'unit' => '%',
					'size' => -1,
				],
				
			]
		);
        $this->add_control(
			'desk_number',
			[
				'label' => __( 'Desktop view',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '3',
				'classes' => "desk-ele-item",
			]
		);

		$this->add_control(
			'lap_number',
			[
				'label' => __( 'Laptop view',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '3',
				'classes' => "lap-ele-item",
			]
		);

		$this->add_control(
			'tab_number',
			[
				'label' => __( 'Tablet view',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '2',
				'classes' => "tab-ele-item",
			]
		);

		$this->add_control(
			'mob_number',
			[
				'label' => __( 'Mobile view',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '1',
				'classes' => "mob-ele-item",
			]
		);	

		$this->add_control(
			'autoplay',
			[
				'label'      => __( 'Autoplay',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True',"streamit-extensions" ),
					'false'      => __( 'False',"streamit-extensions" ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_control(
			'loop',
			[
				'label'      => __( 'Loop',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True',"streamit-extensions" ),
					'false'      => __( 'False',"streamit-extensions" ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_control(
			'dots',
			[
				'label'      => __( 'Dots',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True',"streamit-extensions" ),
					'false'      => __( 'False',"streamit-extensions" ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_control(
			'nav-arrow',
			[
				'label'      => __( 'Arrow',"streamit-extensions" ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True',"streamit-extensions" ),
					'false'      => __( 'False',"streamit-extensions" ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label' => __( 'Margin',"streamit-extensions" ),
				'type' => Controls_Manager::SLIDER,
				
				'condition' => [
					'blog_type' => '1',
				]
				
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label' => __( 'Padding',"streamit-extensions" ),
				'type' => Controls_Manager::SLIDER,
				
				'condition' => [
					'blog_type' => '1',
				]
				
			]
		);


		

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order By',"streamit-extensions" ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'ASC',
				'options' => [
						'DESC' => esc_html__('Descending',"streamit-extensions"), 
						'ASC' => esc_html__('Ascending',"streamit-extensions") 
				],

			]
		);
		
		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment',"streamit-extensions" ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left',"streamit-extensions" ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center',"streamit-extensions" ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right',"streamit-extensions" ),
						'icon' => 'eicon-text-align-right',
					],
					'text-justify' => [
						'title' => __( 'Justified',"streamit-extensions" ),
						'icon' => 'eicon-text-align-justify',
					],
				]
			]
		);

		
        $this->end_controls_section();  

		


	}
	/**
	 * Render Blog widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	protected function render() {
		
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_blog.php';
		
		if ( Plugin::$instance->editor->is_edit_mode() ) :
			
		?>

<script>	
				
		jQuery('.owl-carousel').each(function() {
                    
                    var jQuerycarousel = jQuery(this);
                    jQuerycarousel.owlCarousel({
                        items: jQuerycarousel.data("items"),                        
                        loop: jQuerycarousel.data("loop"),
                        margin: jQuerycarousel.data("margin"),
                        stagePadding: jQuerycarousel.data("padding"),
                        nav: jQuerycarousel.data("nav"),
                        dots: jQuerycarousel.data("dots"),
                        autoplay: jQuerycarousel.data("autoplay"),
                        autoplayTimeout: jQuerycarousel.data("autoplay-timeout"),
                        navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
                        responsiveClass: true,
                        responsive: {
                            // breakpoint from 0 up
                            0: {
                                items: jQuerycarousel.data("items-mobile-sm"),
                                nav: false,
                                dots: true
                            },
                            // breakpoint from 480 up
                            480: {
                                items: jQuerycarousel.data("items-mobile"),
                                nav: false,
                                dots: true
                            },
                            // breakpoint from 786 up
                            786: {
                                items: jQuerycarousel.data("items-tab")
                            },
                            // breakpoint from 1023 up
                            1023: {
                                items: jQuerycarousel.data("items-laptop")
                            },
                            1199: {
                                items: jQuerycarousel.data("items")
                            }
                        }
                    });
                });

		</script>


		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\IQONIC_Blog() );
?>