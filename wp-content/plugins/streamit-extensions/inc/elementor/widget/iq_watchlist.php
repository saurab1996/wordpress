<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Watchlist extends Widget_Base {

	public function get_name() {
		return __( 'Iq_Watchlist',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Watch List',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-person';
	}

	

	protected function _register_controls() {

		
		$this->start_controls_section(
			'section_Team',
			[
				'label' => __( 'Watch List',"streamit-extensions" ),
			]
		);
		$this->add_control(
			'play_now_text', 
			[
				'label' => __( 'Play Now Text', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Play Now' , 'plugin-domain' ),
				'label_block' => true,
			]
		);

        $this->add_control(
			'iq_movie_genre',
			[
				'label' => __( 'Display Movie From Specific Genre',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT2,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_get_custom_texonomy('movie_genre'),
				
			]
		);
        
        $this->add_control(
			'iq_movie_tag',
			[
				'label' => __( 'Display Movie From Specific Tags',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT2,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_get_custom_texonomy('movie_tag'),
				
			]
		);

		require_once IQ_TH_ROOT . '/inc/elementor/include/post-control.php';
        

        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/iq_watchlist.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

		<script>	
		

		</script>
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Watchlist() );