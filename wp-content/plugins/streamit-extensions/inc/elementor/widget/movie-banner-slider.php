<?php
namespace Elementor; 

if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Movie_Banner_Slider extends Widget_Base {

	public function get_name() {
		return __( 'Iq_Movie_Banner_Slider',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Movie Banner Slider',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-person';
	}

	

	protected function _register_controls() {

		
		$this->start_controls_section(
			'section_zhyghasgahs',
			[
				'label' => __( 'Movie Banner Slider',"streamit-extensions" ),
			]
		);

		$this->add_control(
			'play_now_text', 
			[
				'label' => __( 'Play Now Text',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Play Now' ,"streamit-extensions" ),
				'label_block' => true,
			]
		);
		
		$this->add_control(
            'show_trailer_btn',
            [
                'label'   => __( 'Show Trailer Button',"streamit-extensions" ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} #home-slider .trailor-video' => 'display: {{value}};',
                ],

            ]
		);
		
		$this->add_control(
			'view_logo',
			[
				'label' => __( 'Show logo?', 'streamit-extensions' ),
				'type' => Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} #home-slider .channel-logo' => 'display: {{value}};',
                ],
			]
		);

		$this->add_control(
			'view_all_starring',
			[
				'label' => __( 'Show Starring?', 'streamit-extensions' ),
				'type' => Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} #home-slider .text-primary.title.starring' => 'display: {{value}};',
                ],
			]
		);

		$this->add_control(
			'view_all_genres',
			[
				'label' => __( 'Show Genres?', 'streamit-extensions' ),
				'type' => Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} #home-slider .text-primary.title.genres' => 'display: {{value}};',
                ],
			]
		);

		$this->add_control(
			'view_all_tag',
			[
				'label' => __( 'Show Tags?', 'streamit-extensions' ),
				'type' => Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} #home-slider .text-primary.title.tag' => 'display: {{value}};',
                ],
			]
		);

		$this->add_control(
			'trailer_text', 
			[
				'label' => __( 'Trailer Text',"streamit-extensions" ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Watch Trailer' ,"streamit-extensions" ),
				'label_block' => true,
				'condition' => ['show_trailer_btn'=>['block']]
			]
		);
       
		
        $repeater = new Repeater();
       
		$repeater->add_control(
			'iq_movie',
			[
				'label' => __( 'Select Movie',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_custom_post_data('movie' , array() , 'id'),
				
			]
		);
		$repeater->add_control(
			'slider_image',
			[
				'label' => __( 'Slider  Image',"streamit-extensions" ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				
				
			]
		);
		

        $this->add_control(
			'tabs',
			[
				'label' => __( 'Movie List',"streamit-extensions" ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
			]	
		);
		
        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/movie-banner-slider.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

		<script>	
		jQuery('#home-slider').slick({
			autoplay: false,
			speed: 800,
			lazyLoad: 'progressive',
			arrows: true,
			dots: false,
			prevArrow: '<div class="slick-nav prev-arrow"><i></i><svg><use xlink:href="#circle"></svg></div>',
			nextArrow: '<div class="slick-nav next-arrow"><i></i><svg><use xlink:href="#circle"></svg></div>',
			responsive: [
				{
					breakpoint: 992,
					settings: {
						dots: true,
						arrows: false,
					}
				}
			]
		}).slickAnimation();
		jQuery('.slick-nav').on('click touch', function (e) {

			e.preventDefault();

			var arrow = jQuery(this);

			if (!arrow.hasClass('animate')) {
				arrow.addClass('animate');
				setTimeout(() => {
					arrow.removeClass('animate');
				}, 1600);
			}

		});

		</script>
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Movie_Banner_Slider() );