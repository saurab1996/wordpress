<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Movie_Tab extends Widget_Base {

	public function get_name() {
		return __( 'Iq_Movie_Tab',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Movie Tab',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-person';
	}

	

	protected function _register_controls() {

		
		$this->start_controls_section(
			'section_Team',
			[
				'label' => __( 'Movie Banner',"streamit-extensions" ),
			]
        );
        $this->add_control(
            'trending_top_img',
            [
                'label' => __( 'Trending Order Image',"streamit-extensions" ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],

            ]
        );
		$this->add_control(
			'view_logo',
			[
				'label' => __( 'Show logo?', 'streamit-extensions' ),
				'type' => Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-movies-tabs .channel-logo' => 'display: {{value}};',
                ],
			]
		);
		$this->add_control(
			'view_all_starring',
			[
				'label' => __( 'Show Starring?', 'streamit-extensions' ),
				'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-movies-tabs .text-primary.title.starring' => 'display: {{value}};',
                ],
			]
		);

		$this->add_control(
			'view_all_genres',
			[
				'label' => __( 'Show Genres?', 'streamit-extensions' ),
				'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-movies-tabs .text-primary.title.genres' => 'display: {{value}};',
                ],
			]
		);

		$this->add_control(
			'view_all_tag',
			[
				'label' => __( 'Show Tags?', 'streamit-extensions' ),
				'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'block',
                'options' => [
                    'block' => esc_html__('yes',"streamit-extensions"),
                    'none' => esc_html__('no',"streamit-extensions")
                ],
                'selectors' => [
                    '{{WRAPPER}} .iq-movies-tabs .text-primary.title.tag' => 'display: {{value}};',
                ],
			]
		);
	
       
        

        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/movie_tabs.php';
		if ( !Plugin::$instance->editor->is_edit_mode() ) : ?>

		<style type="text/css">
			.owl-carousel.episodes-slider1{
				display: none;
			}
			.active.show {
				display: block;
			}
		</style>
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Movie_Tab() );
