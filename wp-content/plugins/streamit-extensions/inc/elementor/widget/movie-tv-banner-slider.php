<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

class Iq_Movie_TV_Banner_Slider extends Widget_Base {

	public function get_name() {
		return __( 'Iq_Movie_TV_Banner_Slider',"streamit-extensions" );
	}
	
	public function get_title() {
		return __( 'Movie / Tv Show Banner Slider',"streamit-extensions" );
	}

	public function get_categories() {
		return [ 'streamit-extensions' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-person';
	}

	

	protected function _register_controls() {

		
		$this->start_controls_section(
			'section_Team',
			[
				'label' => __( 'Movie / Tv Show Baner Slider',"streamit-extensions" ),
			]
        );
		
		$this->add_control(
			'play_now_text', 
			[
				'label' => __( 'Play Now Text',"streamit-extensions" ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Play Now' ,"streamit-extensions" ),
				'label_block' => true,
			]
		);
        
		$repeater = new Repeater();
		$repeater->add_control(
			'iq_type',
			[
				'label' => __( 'Display Specific Movie',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'label_block' => true,
				'multiple' => true,
				'options' => [
					'movie' => __('Movie',"streamit-extensions"),
					'tv_show' => __('Tv Show',"streamit-extensions") 
				],
				'default' => 'movie'
				
			]
		);
		$repeater->add_control(
			'iq_tv_show',
			[
				'label' => __( 'Display Specific Movie',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_custom_post_data('tv_show' , array() , 'id'),
				'condition' => ['iq_type'=>['tv_show']],
			]
		);
		$repeater->add_control(
			'iq_movie',
			[
				'label' => __( 'Display Specific Movie',"streamit-extensions" ),
				'type' => Controls_Manager::SELECT,
				'label_block' => true,
				'multiple' => true,
				'options' => iq_custom_post_data('movie' , array() , 'id'),
				'condition' => ['iq_type'=>['movie']],
			]
		);
		$repeater->add_control(
			'slider_image',
			[
				'label' => __( 'Slider  Image',"streamit-extensions" ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				
				
			]
		);
		

        $this->add_control(
			'tabs',
			[
				'label' => __( 'Lists Items',"streamit-extensions" ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
			]
        );

        

        $this->end_controls_section();

	}
	
	protected function render() {
		$settings = $this->get_settings();
		require  IQ_TH_ROOT . '/inc/elementor/render/movie-tv-banner-slider.php';
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

		<script>	
		

		</script>
		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Iq_Movie_TV_Banner_Slider() );