<?php 
require  IQ_TH_ROOT . '/inc/elementor/register-custom-control/iqonic_register_control.php';
require  IQ_TH_ROOT . '/inc/elementor/icon/custom-icon.php';
add_action( 'elementor/widgets/widgets_registered', 'iqonic_register_elementor_widgets' );
function iqonic_register_elementor_widgets() {
	
	if ( defined( 'ELEMENTOR_PATH' ) && class_exists('Elementor\Widget_Base') ) {
	
		
		//require  IQ_TH_ROOT . '/inc/elementor/widget/iq_team.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_button.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_section_title.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/movie-banner-slider.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/movie-slider.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/movie-vertical-slider.php';
		//require  IQ_TH_ROOT . '/inc/elementor/widget/movie-banner.php';
        require  IQ_TH_ROOT . '/inc/elementor/widget/iq_accordion.php';
        require  IQ_TH_ROOT . '/inc/elementor/widget/iq_blog.php';
        require  IQ_TH_ROOT . '/inc/elementor/widget/iq_price.php';

		require  IQ_TH_ROOT . '/inc/elementor/widget/tv-show-slider.php';

		require  IQ_TH_ROOT . '/inc/elementor/widget/tv_show-tab.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/movie_tabs.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/movie-tv-banner-slider.php';
		require  IQ_TH_ROOT . '/inc/elementor/widget/iq_watchlist.php';
		

 	}
}

add_action( 'elementor/init', function() {
	\Elementor\Plugin::$instance->elements_manager->add_category( 
		'streamit-extensions',
		[
			'title' => __( 'Element',"streamit-extensions" ),
			'icon' => 'fa fa-plug',
		]
	);
});

add_action( 'wp_footer', function() {
	if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
		return;
	}
	?>

	<script>
		jQuery( function( $ ) {
			// Add space for Elementor Menu Anchor link
			if ( window.elementorFrontend ) {
				
				jQuery("#load").fadeOut();
				jQuery("#loading").delay(0).fadeOut("slow");
				
				if(jQuery('header').hasClass('has-sticky'))
                {         
                    jQuery(window).on('scroll', function() {
                        if (jQuery(this).scrollTop() > 10) {
                            jQuery('header').addClass('menu-sticky');
                            jQuery('.has-sticky .logo').addClass('logo-display');
                        } else {
                            jQuery('header').removeClass('menu-sticky');
                            jQuery('.has-sticky .logo').removeClass('logo-display');
                        }
                    });

				}
				
				
			}
			
		} );
	</script>
	<?php
} );
?>