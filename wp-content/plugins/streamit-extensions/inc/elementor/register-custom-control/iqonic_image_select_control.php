<?php
/**
 * Elementor emoji one area control.
 *
 * A control for displaying a textarea with the ability to add emojis.
 *
 * @since 1.0.0
 */

if(class_exists('\Elementor\Base_Data_Control'))
{
class Iqonic_Image_Select_Control extends \Elementor\Base_Data_Control {

	/**
	 * Get emoji one area control type.
	 *
	 * Retrieve the control type, in this case `emojionearea`.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Control type.
	 */
	public function get_type() {
		return 'iqonic_image_select_control';
	}

	/**
	 * Enqueue emoji one area control scripts and styles.
	 *
	 * Used to register and enqueue custom scripts and styles used by the emoji one
	 * area control.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function enqueue() {
		// Styles
		// wp_register_style( 'emojionearea', 'https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.css', [], '3.4.1' );
		// wp_enqueue_style( 'emojionearea' );

		// // Scripts
		 //wp_register_script( 'emojionearea', 'https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.js', [], '3.4.1' );
		 wp_register_script( 'emojionearea-control', IQ_TH_URL.'/assest/js/iqonic-extension.js', [ 'emojionearea', 'jquery' ], '1.0.0' );
		// wp_enqueue_script( 'emojionearea-control' );
	}

	/**
	 * Get emoji one area control default settings.
	 *
	 * Retrieve the default settings of the emoji one area control. Used to return
	 * the default settings while initializing the emoji one area control.
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @return array Control default settings.
	 */
	protected function get_default_settings() {
		return [
			'label_block' => true,			
			'option' => [],
		];
	}

	/**
	 * Render emoji one area control output in the editor.
	 *
	 * Used to generate the control HTML in the editor using Underscore JS
	 * template. The variables for the class are available using `data` JS
	 * object.
	 *
	 * @since 1.0.0
	 * @access public
	 */

	
	public function content_template() {
		$control_uid = $this->get_control_uid();

		

		?>
		<style type="text/css">
			/* HIDE RADIO */
			[class=iq-radio] { 
			  position: absolute;
			  opacity: 0;
			  width: 0;
			  height: 0;			  

			}

			/* IMAGE STYLES */
			[class=iq-radio] + img {
			  cursor: pointer;
			  padding: 10px;
			}

			/* CHECKED STYLES */
			[class=iq-radio]:checked + img {
			  outline: 2px solid #0d1e67;
			}

			.emt-box-main {flex-wrap: wrap;display: -webkit-box;display: -moz-box;display: -ms-flexbox;display: -webkit-flex;display: flex;}
			.emt-box-main .emt-box-item {width: 50%;display: block;position: relative;}
			main .emt-box-item img {height: 95px;object-fit: cover;width: 100%;}
		</style>
		<div class="elementor-control-field">
			<label for="<?php echo esc_attr( $control_uid ); ?>" class="elementor-control-title">{{{ data.label }}}</label>
			<div class="elementor-control-input-wrapper">
				<div class="emt-box-main">
				<# _.each( data.option, function( item, index ) { #>
					
				<label class="emt-box-item">
					<input type="radio" data-setting="{{ data.name }}" id="<?php echo esc_attr( $control_uid ); ?>" class="iq-radio" value="{{  index }}" name="{{ data.name }}">
					  <img src="{{ item }}" class="emt-hover-img">
				</label>
				<#
					} );
			 	#>
				</div>
			</div>
		</div>
		
		<# if ( data.description ) { #>
		<div class="elementor-control-field-description">{{{ data.description }}}</div>
		<# } #>

		<?php
	}

}
}