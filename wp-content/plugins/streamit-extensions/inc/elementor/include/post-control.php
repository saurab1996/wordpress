<?php 
namespace Elementor; 

		$this->add_responsive_control(
			'posts_per_page',
			[
				'label' => __( 'Posts Per Page', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'unit' => '%',
					'size' => -1,
				],
				
			]
		);
		$this->add_control(
			'post_view_type',
			[
				'label'   => __( 'Type', 'streamit' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'none',
				'options' => [
						'none' => esc_html__('None', 'streamit'), 
						'latest' => esc_html__('Latest', 'streamit'), 
						'upcoming' => esc_html__('Upcoming', 'streamit'), 
						'most_viewd' => esc_html__('Most Viewd', 'streamit'), 
						'most_liked' => esc_html__('Most Liked', 'streamit') 
				],

			]
		);
		$this->add_control(
			'order',
			[
				'label'   => __( 'Order By', 'streamit' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'ASC',
				'options' => [
						'DESC' => esc_html__('Descending', 'streamit'), 
						'ASC' => esc_html__('Ascending', 'streamit') 
				],
		 		'condition' => ['post_view_type'=>['none','upcoming']],

			]
		);