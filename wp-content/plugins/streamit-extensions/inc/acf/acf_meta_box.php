<?php
if (function_exists('acf_add_local_field_group')):

    // Page Options
    acf_add_local_field_group(array(
        'key' => 'group_46Cg7N74r8t811VLFfR6',
        'title' => 'Extra Data',
        'fields' => array(
             array(
                'key' => 'field_sth54fsf22fsd',
                'label' => 'Extra Data',
                'name' => 'body_set',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'placement' => 'left',
                'endpoint' => 0,
            ) ,
            
            array(
                'key' => 'key_logo', //key_movie_logo
                'label' => 'Logo',
                'name' => 'name_logo', //movie_logo
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'placement' => 'left',
                'endpoint' => 0,
            ) ,

            array(
                'key' => 'key_trailer_link', //key_movie_trailer_link
                'label' => 'Trailer Link',
                'name' => 'name_trailer_link', //movie_trailer_link
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'placement' => 'left',
                'endpoint' => 0,
            ) ,

            array(
                'key' => 'key_upcoming',
                'label' => 'Upcomming',
                'name' => 'name_upcoming',
                'type' => 'checkbox',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => Array(
                   'yes' => __('this is upcomming movie/tv show',"streamit-extensions")
                ),
                'layout' => 'vertical',
                'toggle' => false,
                'return_format' => 'array',
            ),

        ) ,
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'movie',
                ) ,
            ) ,
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'tv_show',
                ) ,

            ) ,
          
           
            

            
        ) ,
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
endif;
