<?php
if (function_exists('acf_add_local_field_group')):

    // Page Options
    acf_add_local_field_group(array(
        'key' => 'group_banner',
        'title' => 'Page Options',
        'fields' => array(
            array(
                'key' => 'key_header_display',
                'label' => 'Display Header',
                'name' => 'name_header_display',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'choices' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                ) ,
                'wrapper' => array(
                    'width' => '33.33%',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'yes',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,
            array(
                'key' => 'key_banner_display',
                'label' => 'Display Banner',
                'name' => 'name_banner_display',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'choices' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                ) ,
                'wrapper' => array(
                    'width' => '33.33%',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'yes',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,
            array(
                'key' => 'key_footer_display',
                'label' => 'Display Footer',
                'name' => 'name_footer_display',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'choices' => array(
                    'yes' => 'Yes',
                    'no' => 'No',
                ) ,
                'wrapper' => array(
                    'width' => '33.33%',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'yes',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,

        ) ,
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ) ,

            ) ,

        ) ,
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
endif;