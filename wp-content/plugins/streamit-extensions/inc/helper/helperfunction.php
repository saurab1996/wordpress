<?php
if (!function_exists('iqonic_blog_time_link')) :
    /**
     * Gets a nicely formatted string for the published date.
     */
    function iqonic_blog_time_link()
    {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        /*if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }*/

        $time_string = sprintf(
            $time_string,
            get_the_date(DATE_W3C),
            get_the_date(),
            get_the_modified_date(DATE_W3C),
            get_the_modified_date()
        );

        // Wrap the time string in a link, and preface it with 'Posted on'.
        return sprintf(
            /* translators: %s: post date */
            __('<span class="screen-reader-text">Posted on</span> %s',"streamit-extensions"),
            '<a href="' . esc_url(get_permalink()) . '" rel="bookmark">' . $time_string . '</a>'
        );
    }
endif;

function iq_custom_post_data($post_type = '', $args = array(), $return = '')
{
    $array = array();
    global $post;
    $args = array(
        'post_type'         => $post_type,
        'post_status'       => 'publish',
        'posts_per_page' => -1
    );
    $wp_query = new \WP_Query($args);
    if ($wp_query->have_posts()) {

        while ($wp_query->have_posts()) {
            $wp_query->the_post();
            if ($return == 'id') {
                $array[get_the_ID()] = get_the_title();
            }
            if ($return == 'slug') {
                $array[$post->post_name] = get_the_title();
            }
        }
        return $array;
    } else {
        return $wp_query;
    }
}

function iq_get_most_liked($post_type = '', $args = array(), $return = '')
{
    $liked = wp_ulike_get_most_liked_posts(10, array($post_type), 'post', 'all', 'like');
    if ($liked) {
        foreach ($liked as $post) {
            $most_liked_ids[] = $post->ID;
        }
        return $most_liked_ids;
    }
}

function iq_get_custom_texonomy($taxo = '')
{
    if (empty($taxo)) {
        return;
    }


    $show_count = 0; // 1 for yes, 0 for no
    $pad_counts = 0; // 1 for yes, 0 for no
    $hierarchical = 1; // 1 for yes, 0 for no
    $title = '';
    $empty = 0;
    $array = array();
    $args = array(
        'taxonomy' => $taxo,
        'show_count' => $show_count,
        'pad_counts' => $pad_counts,
        'hierarchical' => $hierarchical,
        'hide_empty' => false,
        'parent' => 0
    );
    $wp_object =  get_categories($args);

    if (!empty($wp_object)) {
        foreach ($wp_object as $val) {
            $array[$val->slug] = $val->name;
        }
    }

    return $array;
}

function iq_get_pages()
{

    $wp_object =  get_all_page_ids();

    if (!empty($wp_object)) {
        foreach ($wp_object as $val) {
            $array[$val] = get_the_title($val);
        }
    }

    return $array;
}
//user profile picture update
if (!function_exists('iq_get_attachment_id')) {
    function iq_get_attachment_id($img)
    {

        $userdata = wp_get_current_user();
        $profile_image = $img;
        $url = '';
        $type      =  $profile_image['type'];

        $extension = pathinfo($profile_image['name'], PATHINFO_EXTENSION);

        // Upload dir.
        $upload_dir  = wp_upload_dir();
        $upload_path = str_replace('/', DIRECTORY_SEPARATOR, $upload_dir['path']) . DIRECTORY_SEPARATOR;

        // Save the image in the uploads directory.

        $upload_file = move_uploaded_file($profile_image["tmp_name"], $upload_path . $profile_image["name"]);

        $attachment = array(
            'post_mime_type' => $extension,
            'post_title'     => preg_replace('/\.[^.]+$/', '', $profile_image["name"]),
            'post_content'   => '',
            'post_status'    => 'inherit',
            'guid'           => $upload_dir['url'] . '/' . $profile_image["name"]
        );

        $attach_id = wp_insert_attachment($attachment, $upload_dir['path'] . '/' . $profile_image["name"]);

        // Regenerate Thumbnail
        global $wpdb;
        $images = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'image/%' AND ID ='$attach_id'");

        foreach ($images as $image) {
            $id = $image->ID;
            $fullsizepath = get_attached_file($id);

            if (false === $fullsizepath || !file_exists($fullsizepath))
                return;
        }
        $url = wp_get_attachment_url($attach_id);

        $update = update_user_meta($userdata->ID, 'streamit_profile_image', $url);

        if (!is_wp_error($update)) {
            $img = get_user_meta($userdata->ID, 'streamit_profile_image');
            $data['profile_image'] = $img[0];
        }
        return $url;
    }
}
if (!function_exists('iqonic_random_strings')) {
    function iqonic_random_strings($random = '')
    {

        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . $random;

        // Shufle the $str_result and returns substring 
        // of specified length 
        return substr(str_shuffle($str_result), 0, 5);
    }
}