<?php
add_shortcode('iqonic_view_all', 'iqonic_get_view_all');

if (!function_exists('iqonic_get_view_all')) {
    function iqonic_get_view_all()
    {
        return iqonic_show_view_all();
    }
}

if (!function_exists('iqonic_show_view_all')) {
    function iqonic_show_view_all()
    {
        ob_start();

        global $wp , $paged;
        $current_url = home_url(add_query_arg(array($_REQUEST), $wp->request));
        $option = '';
        $title = '';
        $parts = [];
        if (strpos($current_url, 'movies=') || strpos($current_url, 'tv_shows=')) {

            $pos = strpos($current_url, 'movies=');
            if(strpos($current_url, 'tv_shows='))
            {
                $pos = strpos($current_url, 'tv_shows=');
            }
            $len = strlen($current_url);
            $val = substr($current_url, $pos, $len);
            $parts = explode('=', $val);
            $title = $parts[1];
            if (count($parts) > 2)
                $option = $parts[2];
        }
        $paged = get_query_var('paged') ? get_query_var('paged') : 1; 
        $args = array(
            'post_status'       => 'publish',
            'posts_per_page'    => 12,
            'paged' => $paged 
        );
        $args['post_type'] = array('movie', 'tv_show');
        if (!empty($title)) {
            $title = get_option($title);
        }
        if (!empty($parts[0])) {
            if ($parts[0] === 'movies') {
                $args['post_type'] = array('movie');
                if (!empty(get_option($option))) {
                    $args['tax_query'] = array(
                        array(
                            'taxonomy' => 'movie_genre',
                            'field' => 'slug',
                            'operator' => 'IN',
                            'terms' => explode(',', get_option($option)),
                        ),
                    );
                }
            } elseif ($parts[0] === 'tv_shows') {
                $args['post_type'] = array('tv_show');
                if (!empty(get_option($option))) {
                    $args['tax_query'] = array(
                        array(
                            'taxonomy' => 'tv_show_genre',
                            'field' => 'slug',
                            'operator' => 'IN',
                            'terms' => explode(',', get_option($option)),
                        ),
                    );
                }
            }
        }
        //print_r($args);
        $wp_query = new \WP_Query($args);
?>
        <div class="watchlist-contens">
            <div class="iq-main-header d-flex align-items-center justify-content-between">
                <h4 class="iq-title"><?php echo esc_html($title, "streamit-extensions"); ?></h4>
            </div>
            <div class="row wl">
                <?php
                if ($wp_query->have_posts()) {
                    while ($wp_query->have_posts()) {
                        $wp_query->the_post();

                        $movie_run_time = get_post_meta(get_the_ID(), '_movie_run_time');

                        $run_time = '';
                        $url_link = '';
                        $censor_rating = '';
                        if (isset($movie_run_time[0])) {
                            $run_time = $movie_run_time[0];
                        }
                        if (isset($movie_censor_rating[0])) {
                            $censor_rating = $movie_censor_rating[0];
                        }

                        $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), "full");
                ?>

                        <div class="col-lg-3 col-md-4 col-sm-6 wl-child">
                            <div class="block-images position-relative watchlist-img">
                                <div class="img-box">
                                    <img src="<?php echo esc_url($full_image[0]) ?>" class="img-fluid" alt="">
                                </div>
                                <div class="block-description">
                                    <h6 class="iq-title">
                                        <a href="<?php echo esc_url(get_the_permalink()); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h6>
                                    <div class="movie-time d-flex align-items-center my-2">
                                        <div class="badge badge-secondary p-1 mr-2"><?php echo esc_html($censor_rating); ?></div>
                                        <span class="text-white"><?php echo esc_html($run_time); ?></span>
                                    </div>
                                    <div class="hover-buttons">

                                        <a href="<?php echo esc_url(get_the_permalink()); ?>" class="btn btn-hover iq-button">
                                            <i class="fa fa-play mr-1" aria-hidden="true"></i>
                                            <?php _e('Play Now', 'streamit-extension') ?>
                                        </a>

                                    </div>
                                </div>
                                <div class="block-social-info">
                                    <ul class="list-inline p-0 m-0 music-play-lists">
                                        <li>
                                            <span><i class="ri-share-fill"></i></span>
                                            <div class="share-box">
                                                <div class="d-flex align-items-center">
                                                    <a href="https://www.facebook.com/sharer?u=" target="_blank" rel="noopener noreferrer" class="share-ico"><i class="ri-facebook-fill"></i></a>
                                                    <a href="http://twitter.com/intent/tweet?text=Currentlyreading" target="_blank" rel="noopener noreferrer" class="share-ico"><i class="ri-twitter-fill"></i></a>
                                                    <a href="#" data-link='<?php the_permalink(); ?>' class="share-ico iq-copy-link"><i class="ri-links-fill"></i></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="iq-like-btn"><?php echo do_shortcode('[wp_ulike for="movie" id="' . get_the_ID() . '" style="wpulike-heart"]'); ?></div>
                                        </li>
                                        <li>
                                            <a class="watch-list" rel="<?php echo get_the_ID(); ?>">
                                                <?php
                                                echo add_to_watchlist(get_the_ID());
                                                ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                <?php }
                    /** PAGINATION */
                    $big = 999999999; // need an unlikely integer
                                            
                    $pagination_args = array(
                    'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $wp_query->max_num_pages,
                    'prev_next'       => True,
                    'prev_text'       => '<span aria-hidden="true">'. esc_html__( 'Previous page', 'streamit' ) .'</span>',
                    'next_text'       => '<span aria-hidden="true">'. esc_html__( 'Next page', 'streamit' ) .'</span>',
                    'type'            => 'list',
                    'add_args'        => false,
                    'add_fragment'    => ''
                    );
                    $paginate_links = paginate_links($pagination_args);
                    if ($paginate_links) {
                                echo '<div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="pagination justify-content-center">
                                            <nav aria-label="Page navigation">';
                                                        printf( esc_html__('%s','streamit'),$paginate_links);
                                            echo '</nav>
                                    </div>
                                </div>';
                    }

                    wp_reset_postdata();
                } ?>
            </div>
        </div>
<?php
        return ob_get_clean();
    }
}

?>