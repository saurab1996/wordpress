<?php
add_action('init', 'iqonic_send_pass');
add_shortcode('iqonic_forgot_pass', 'iqonic_forgot_form');

if( !function_exists('iqonic_forgot_form') )
{
    function iqonic_forgot_form() {
        // get registration form if user not logged in
        if(!is_user_logged_in()) {
            return iqonic_edit_pass_form();
        }
    }
}

if( !function_exists('iqonic_edit_pass_form') )
{
    function iqonic_edit_pass_form() {
        ob_start();
//        streamit_profile_image

        if(class_exists('ReduxFramework')) {
            $iq_option = get_option('streamit_options');
        }
        ?>
        <div class="iqonic-user-forms">
        <div class="iq-login-form">
        <div class = "iq-login-title mb-3"><h4><?php echo esc_html('Forgot Password',"streamit-extensions") ?></h4></div>
            <div class="error-msg"></div>
        <form method="post" id="changepass" class="w-100 float-left" name="forgot" action="<?php the_permalink(); ?>">
            <p class="form-email">
<!--                <label for="user_email">--><?php ////_e('Enter Email', 'profile'); ?><!--</label>-->
                <input class="input" id="user_email" name="user_email" placeholder="Email Address" type="text" id="user-email" value="" />
            </p>


            <p class="form-submit">
                <button name="sendpass" type="submit" id="change-pass" class="btn btn-hover iq-button" value="<?php _e('Get New Password',"streamit-extensions"); ?>" ><?php _e('Get New Password',"streamit-extensions"); ?></button>
                <?php wp_nonce_field( 'forgot-password' ) ?>
                <input name="action" type="hidden" id="action" value="forgot-password" />
            </p><!-- .form-submit -->
        </form>
            <div class="links float-left w-100">
                <?php
                if(isset($iq_option['streamit_signin_link'])) {
                    $iqonic_signin_link = get_page_link($iq_option['streamit_signin_link']);
                    $iqonic_signin_title = $iq_option['streamit_signin_title']; ?>
                    Did your remembered your password?<a href="<?php echo esc_url( $iqonic_signin_link ) ?>" class="iq-sub-card setting-dropdown">

                        <h6 class="mb-0"> <?php
                            if(!empty($iqonic_signin_title)) {
                                echo esc_attr($iqonic_signin_title,'streamit');
                            } else {
                                echo esc_html__('Sign In','streamit');
                            } ?>
                        </h6>

                    </a> <?php
                }
                ?>
            </div>
        </div>
        </div>
        <?php
        return ob_get_clean();
    }
}

if( !function_exists('iqonic_send_pass') )
{
    function iqonic_send_pass() {

        /* If profile was saved, update profile. */
        if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'forgot-password' ) {
            /* Update user information. */
            $email = $_REQUEST['user_email'];
            $user = get_user_by('email', $email);
            $message = null;

            if($user) {

                $title = 'New Password';
                $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                $password = substr(str_shuffle($str_result),0, 10);
                $message = '<label><b>Hello,</b></label>';
                $message.= '<p>Your recently requested to reset your password. Here is the new password for your App</p>';
                $message.='<p><b>New Password </b> : '.$password.'</p>';
                $message.='<p>Thanks,</p>';

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $is_sent_wp_mail = wp_mail($email,$title,$message,$headers);

                if($is_sent_wp_mail) {
                    wp_set_password( $password, $user->ID);
                    $message = __('Password has been sent successfully to your email address.');
                } elseif (mail( $email, $title, $message, $headers )) {
                    wp_set_password( $password, $user->ID);
                    $message = __('Password has been sent successfully to your email address.');
                } else {
                    $message = __('Email not sent');
                }
            } else {
                $message = __('User not found with this email address');
            }
           echo $message;

        }
    }
}
