<?php
add_action('init', 'iqonic_edit_user_data');
add_shortcode('iqqonic_edit_user', 'iqonic_edit_form');

if (!function_exists('iqonic_edit_form')) {
    function iqonic_edit_form()
    {
        // get registration form if user not logged in
        if (is_user_logged_in()) {
            return iqonic_edit_user_form();
        }
    }
}

if (!function_exists('iqonic_edit_user_form')) {
    function iqonic_edit_user_form()
    {
        ob_start();
        $current_user = wp_get_current_user();
        //        streamit_profile_image
?>
        <div class="container-fluid m-profile pt-0 pb-0">
            <div class="sign-user_card">
                <form method="post" id="adduser" action="<?php get_permalink(); ?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="form-username">
                                <label for="first-name"><?php _e('First Name',"streamit-extensions"); ?></label>
                                <input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta('first_name', $current_user->ID); ?>" />
                            </p><!-- .form-username -->
                        </div>
                        <div class="col-sm-6">
                            <p class="form-username">
                                <label for="last-name"><?php _e('Last Name',"streamit-extensions"); ?></label>
                                <input class="text-input" name="last-name" type="text" id="last-name" value="<?php the_author_meta('last_name', $current_user->ID); ?>" />
                            </p><!-- .form-username -->
                        </div>
                        <div class="col-sm-6">
                            <p class="form-email">
                                <label for="email"><?php _e('E-mail*',"streamit-extensions"); ?></label>
                                <input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta('user_email', $current_user->ID); ?>" />
                            </p><!-- .form-email -->
                        </div>
                        <div class="col-sm-6">
                            <p class="form-dob">
                                <label for="dob"><?php _e('Date of birth',"streamit-extensions"); ?></label>
                                <input class="text-input" name="dob" type="date" id="dob" value="<?php the_author_meta('user_dob', $current_user->ID); ?>" />

                            </p><!-- .form-dob -->
                        </div>
                        <div class="col-sm-6">
                            <p class="form-img custom-file">
                                <label for="img"><?php _e('Upload Profile',"streamit-extensions"); ?></label>
                                <!--                        <input class="text-input file-upload" name="user_img" type="file" id="user-img" />-->

                                <label class="custom-file-label" for="customFile">Choose file</label>
                                <input type="file" class="custom-file-input" name="user_img" id="customFile">
                            </p><!-- .form-dob -->
                        </div>
                        <div class="col-sm-6">
                            <p class="form-gender">
                                <?php
                                $selected = get_the_author_meta('user_gender', $current_user->ID);
                                $m_selected = '';
                                $f_selected = '';
                                if ($selected === 'Male')
                                    $m_selected  = 'selected';
                                else
                                    $f_selected = 'selected';
                                ?>
                                <label for="gender"><?php _e('Gender',"streamit-extensions"); ?></label>
                                <select name="user_gender" id="gender">
                                    <option value="">Select Gender</option>
                                    <option value="Male" <?php echo esc_html($m_selected); ?>>Male</option>
                                    <option value="Female" <?php echo esc_html($f_selected); ?>>Female</option>
                                </select>
                                <!--                <input class="text-input" name="gender" type="date" id="gender" value="--><?php //the_author_meta( 'user_gender', $current_user->ID ); 
                                                                                                                                ?>
                                <!--" />-->
                            </p><!-- .form-gender -->
                        </div>
                        <div class="col-md-12">
                            <p class="form-textarea mt-0">
                                <label for="description"><?php _e('Biographical Information',"streamit-extensions") ?></label>
                                <textarea name="description" id="description" rows="3" cols="50"><?php the_author_meta('description', $current_user->ID); ?></textarea>
                            </p><!-- .form-textarea -->
                        </div>
                        <?php
                        //action hook for plugin and extra fields
                        //do_action('edit_user_profile',$current_user);
                        ?>
                        <div class="col-md-12">
                            <div class="d-flex align-items-center">
                                <p class="form-submit mb-0">

                                    <button name="updateuser" type="submit" id="updateuser" class="btn btn-hover iq-button" value="<?php _e('Update',"streamit-extensions"); ?>"><?php _e('Update',"streamit-extensions"); ?></button>
                                    <?php wp_nonce_field('update-user') ?>
                                    <input name="action" type="hidden" id="action" value="update-user" />
                                </p><!-- .form-submit -->
                                <p class="form-submit mb-0">

                                    <a href="" class="btn btn-link btn-cancel ml-3"><?php _e('Cancel',"streamit-extensions"); ?></a>
                                </p>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    <?php
        return ob_get_clean();
    }
}

if (!function_exists('iqonic_edit_user')) {
    function iqonic_edit_user_data()
    {
        $current_user = wp_get_current_user();

        $error = array();
        /* If profile was saved, update profile. */
        if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'update-user') {
            /* Update user information. */

            if (!empty($_POST['email'])) {
                if (!is_email(esc_attr($_POST['email']))) {
                    $error[] = __('The Email you entered is not valid.  please try again.',"streamit-extensions");
                } elseif (email_exists(esc_attr($_POST['email']))) {
                    $error[] = __('This email is already used by another user.  try a different one.',"streamit-extensions");
                } else {
                    wp_update_user(array('ID' => $current_user->ID, 'user_email' => esc_attr($_POST['email'])));
                }
            }

            if (isset($_FILES['user_img']['name']) && !empty($_FILES['user_img']['name'])) {
                iq_get_attachment_id($_FILES["user_img"]);
            }

            if (!empty($_POST['first-name']))
                update_user_meta($current_user->ID, 'first_name', esc_attr($_POST['first-name']));
            if (!empty($_POST['last-name']))
                update_user_meta($current_user->ID, 'last_name', esc_attr($_POST['last-name']));
            if (!empty($_POST['dob']))
                update_user_meta($current_user->ID, 'user_dob', esc_attr($_POST['dob']));
            if (!empty($_POST['user_gender']))
                update_user_meta($current_user->ID, 'user_gender', esc_attr($_POST['user_gender']));
            if (!empty($_POST['description']))
                update_user_meta($current_user->ID, 'description', esc_attr($_POST['description']));


            if (count($error) == 0) {
                //action hook for plugins and extra fields saving
                do_action('edit_user_profile_update', $current_user->ID);
                wp_redirect(get_permalink());
            }
        }
    }
}

//only avatar change shortcode
add_action('init', 'iqonic_edit_avatar_data');
add_shortcode('iqqonic_edit_user_avatar', 'iqonic_edit_avatar');

if (!function_exists('iqonic_edit_avatar')) {
    function iqonic_edit_avatar()
    {
        // get registration form if user not logged in
        if (is_user_logged_in()) {
            return iqonic_edit_avatar_form();
        }
    }
}

if (!function_exists('iqonic_edit_avatar_form')) {
    function iqonic_edit_avatar_form()
    {
        ob_start();
        $current_user = wp_get_current_user();
        //        streamit_profile_image
        $user_full_name = esc_html($current_user->user_firstname) . ' ' . esc_html($current_user->user_lastname);
        $get_avatar = get_the_author_meta('streamit_profile_image', $current_user->ID);
        if (empty($get_avatar)) {
            $get_avatar = get_avatar_url($current_user->ID);
        }
    ?>
        <div class="iq-avtar-box m-profile pt-0 pb-0">
            <div class="sign-user_card">
                <img src="<?php echo esc_url($get_avatar,"streamit-extensions"); ?>" class="img-fluid rounded-circle" />
                <a href="javascript:void(0)" id='iq-edit-avatar-btn'><i class="fa fa-edit"></i> <?php echo __('Edit',"streamit-extensions"); ?></a>
                <form method="post" id="update-avatar" style="display: none;" action="<?php get_permalink(); ?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="form-img custom-file">
                                <label for="img"><?php _e('Upload Profile',"streamit-extensions"); ?></label>
                                <!--                        <input class="text-input file-upload" name="user_avatar" type="file" id="user-img" />-->

                                <label class="custom-file-label" for="customFile">Choose file</label>
                                <input type="file" class="custom-file-input" name="user_avatar" id="customFile">
                            </p><!-- .form-dob -->
                        </div>
                        <div class="col-md-12">
                            <div class="avatar-btn">
                                <p class="form-submit mb-0">

                                    <button name="update_avatar" type="submit" id="update_avatar" class="btn btn-hover iq-button" value="<?php _e('Change',"streamit-extensions"); ?>"><?php _e('Update',"streamit-extensions"); ?></button>
                                    <?php wp_nonce_field('update-avatar') ?>
                                    <input name="action" type="hidden" id="action" value="update-avatar" />
                               
                                    <a href="javascript:void(0)" id="cancel-avatar-edit" class="btn btn-link btn-cancel ml-3">Cancel</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
                <h4 class="mt-3"><?php echo esc_html($user_full_name); ?></h4>
            </div>
        </div>
<?php
        return ob_get_clean();
    }
}

if (!function_exists('iqonic_edit_user')) {
    function iqonic_edit_avatar_data()
    {
        $current_user = wp_get_current_user();

        $error = array();
        /* If profile was saved, update profile. */
        if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'update-avatar') {
            /* Update user information. */

            if (isset($_FILES['user_avatar']['name']) && !empty($_FILES['user_avatar']['name'])) {
                iq_get_attachment_id($_FILES["user_avatar"]);
            }

            if (count($error) == 0) {
                wp_redirect(get_permalink());
            }
        }
    }
}
