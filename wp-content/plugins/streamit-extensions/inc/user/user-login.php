<?php
    add_shortcode('iqonic_login', 'iqonic_login_form');
    add_action( 'wp_login_failed', 'user_login_fail' );

    if(!function_exists('user_login_fail')){
        function user_login_fail( $username ) {
            $referrer = $_SERVER['HTTP_REFERER'];
            if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
                wp_redirect( $referrer . '?login=failed' );
                exit;
            }
        }
    }
    if( !function_exists('iqonic_login_form') )
    { 
        function iqonic_login_form() {
            // get registration form if user not logged in
            if(!is_user_logged_in()) {

                global $wp;
                global $current_url;
                $current_url = home_url(add_query_arg(array($_REQUEST), $wp->request));
                define('CURL',$current_url);
                $page = explode('?',$current_url);
                $get_page = end($page);
                if($get_page === 'forgot-password' || (isset($_POST['action']) && $_POST['action'] == 'forgot-password') )
                    return iqonic_edit_pass_form();
                else
                    return iqonic_user_login();
            }
            else{
                if(!is_super_admin(get_current_user_id()))
                wp_redirect(home_url());
            }
            
        }
    }
    if(!function_exists('iqonic_user_login'))
    {
        function iqonic_user_login()
        {
            ob_start();
             ?>
             <div class="iqonic-user-forms">
            <div class="iq-login-form">
                <div class = "iq-login-title mb-3"><h4><?php echo esc_html('Sign In',"streamit-extensions"); ?></h4></div>
                <div class="error-msg"></div>
            <?php
            if(isset($_REQUEST['login']) && $_REQUEST['login'] === 'failed'){ ?>
                <div class="error-login"><?php echo esc_html('Please Enter Valid Username / Email And Password',"streamit-extensions"); ?></div>
           <?php }
            $args = array(  'echo'  => false,
                'label_log_in'      =>__('Sign In',"streamit-extensions"),
                'label_username'    =>'',
                'label_password'    =>'',
                'redirect'          =>esc_url(home_url(),"streamit-extensions")
            );
            $login_form = wp_login_form($args);
            echo $login_form;
            if(class_exists('ReduxFramework')) {
                $iq_option = get_option('streamit_options');
            }
            if(isset($iq_option['streamit_signup_link'])) {
                $iqonic_signup_link = get_page_link($iq_option['streamit_signup_link']);
                $iqonic_signup_title = $iq_option['streamit_signup_title']; ?>
                <div class="d-flex justify-content-center links">
                Don't have an account?<a href="<?php echo esc_url($iqonic_signup_link) ?>" class="iq-sub-card setting-dropdown">
                    <h6 class="mb-0 "> <?php
                        if(!empty($iqonic_signup_title)) {
                            echo esc_attr($iqonic_signup_title,'streamit');
                        } else {
                            echo esc_html__('Sign Up','streamit');
                        } ?>
                    </h6>
                </a>
                </div>
                <div class="d-flex justify-content-center links mt-0">
                    <a href="<?php echo esc_url(CURL ).'?forgot-password' ?>" class="iq-sub-card setting-dropdown">
                        <h6 class="mb-0 "><?php echo esc_html('Forgot Password?',"streamit-extensions"); ?></h6>
                    </a>
                </div>
            <?php
             }
            ?>
            </div>
             </div>
                <?php
            return ob_get_clean();
        }
    }
?>