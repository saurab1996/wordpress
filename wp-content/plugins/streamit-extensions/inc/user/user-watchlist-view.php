<?php
add_shortcode('iqonic_user_watchlist', 'iqonic_watchlist_view');

if (!function_exists('iqonic_watchlist_view')) {
    function iqonic_watchlist_view()
    {
        if (is_user_logged_in()){
            return iqonic_get_watchlist_view();
        }
        else{
            $iqonic_signin_link = home_url();
            if(class_exists('ReduxFramework')) {
                $iq_option = get_option('streamit_options');
                if(isset($iq_option['streamit_signin_link']) && !empty($iq_option['streamit_signin_link'])) {
                    $iqonic_signin_link = get_page_link($iq_option['streamit_signin_link']);
                }
            }
            wp_redirect($iqonic_signin_link);
        }
    }
}

if (!function_exists('iqonic_get_watchlist_view')) {
    function iqonic_get_watchlist_view()
    {
        ob_start();
        global $paged;
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $paged = get_query_var('paged') ? get_query_var('paged') : 1; 
        $watchlist = get_user_meta($user_id, '_user_watchlist', true);
        $args = array(
            'post_type'         => array('movie', 'tv_show'),
            'post_status'       => 'publish',
            'post__in'          => explode(',', $watchlist),
            'posts_per_page'    => 12,
            'paged' => $paged 
        );
        $wp_query = new \WP_Query($args);

?>
        <div class="watchlist-contens">
            <div class="row wl">
                <?php
                if ($wp_query->have_posts()) {
                    while ($wp_query->have_posts()) {
                        $wp_query->the_post();

                        $movie_logo = get_field('movie_logo', get_the_ID());
                        $trailer_link = get_field('movie_trailer_link', get_the_ID());
                        $movie_run_time = get_post_meta(get_the_ID(), '_movie_run_time');
                        $movie_url_link = get_post_meta(get_the_ID(), '_movie_url_link');
                        $movie_choice = get_post_meta(get_the_ID(), '_movie_choice');
                        $meta = get_post_meta(get_the_ID());

                        $run_time = '';
                        $url_link = '';
                        $censor_rating = '';
                        if (isset($movie_run_time[0])) {
                            $run_time = $movie_run_time[0];
                        }
                        if (isset($movie_censor_rating[0])) {
                            $censor_rating = $movie_censor_rating[0];
                        }


                        if (isset($movie_choice[0])) {
                            if ($movie_choice[0] == 'movie_url') {
                                $url_link = $movie_url_link[0];
                            } else {
                                $url_link = get_the_permalink();
                            }
                        } else {
                            $url_link = get_the_permalink();
                        }
                        $full_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), "full");
                ?>

                        <div class="col-lg-3 col-md-4 col-sm-6 wl-child">
                            <div class="block-images position-relative watchlist-img">
                                <div class="img-box">
                                    <img src="<?php echo esc_url($full_image[0]) ?>" class="img-fluid" alt="">
                                </div>
                                <div class="block-description">
                                    <h6 class="iq-title">
                                        <a href="<?php echo esc_url($url_link); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h6>
                                    <div class="movie-time d-flex align-items-center my-2">
                                        <div class="badge badge-secondary p-1 mr-2"><?php echo esc_html($censor_rating); ?></div>
                                        <span class="text-white"><?php echo esc_html($run_time); ?></span>
                                    </div>
                                    <div class="hover-buttons">

                                        <a href="<?php echo esc_url($url_link); ?>" class="btn btn-hover iq-button">
                                            <i class="fa fa-play mr-1" aria-hidden="true"></i>
                                            <?php echo esc_html('Play Now',"streamit-extensions"); ?>    
                                        </a>

                                    </div>
                                </div>
                                <div class="block-social-info">
                                    <ul class="list-inline p-0 m-0 music-play-lists">

                                        <li class="share">
                                            <span><i class="ri-share-fill"></i></span>
                                            <div class="share-box">
                                                <div class="d-flex align-items-center">
                                                    <a href="https://www.facebook.com/sharer?u=" target="_blank" rel="noopener noreferrer" class="share-ico"><i class="ri-facebook-fill"></i></a>
                                                    <a href="http://twitter.com/intent/tweet?text=Currentlyreading" target="_blank" rel="noopener noreferrer" class="share-ico"><i class="ri-twitter-fill"></i></a>
                                                    <a href="#" data-link='<?php the_permalink(); ?>' class="share-ico iq-copy-link"><i class="ri-links-fill"></i></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="iq-like-btn"><?php echo do_shortcode('[wp_ulike for="movie" id="' . get_the_ID() . '" style="wpulike-heart"]'); ?></div>
                                        </li>
                                        <li>
                                            <a class="watch-list" rel="<?php echo get_the_ID(); ?>">
                                                <?php
                                                echo add_to_watchlist(get_the_ID());
                                                ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php  }
                    /** PAGINATION */
                    $big = 999999999; // need an unlikely integer
                                            
                    $pagination_args = array(
                    'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $wp_query->max_num_pages,
                    'prev_next'       => True,
                    'prev_text'       => '<span aria-hidden="true">'. esc_html__( 'Previous page', 'streamit' ) .'</span>',
                    'next_text'       => '<span aria-hidden="true">'. esc_html__( 'Next page', 'streamit' ) .'</span>',
                    'type'            => 'list',
                    'add_args'        => false,
                    'add_fragment'    => ''
                    );
                    $paginate_links = paginate_links($pagination_args);
                    if ($paginate_links) {
                                echo '<div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="pagination justify-content-center">
                                            <nav aria-label="Page navigation">';
                                                        printf( esc_html__('%s','streamit'),$paginate_links);
                                            echo '</nav>
                                    </div>
                                </div>';
                    }
                    wp_reset_postdata();
                } else {
                    ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 wl-child">
                        <?php
                        echo esc_html('Your Watclist is empty...',"streamit-extensions");
                        ?>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
<?php
        return ob_get_clean();
    }
}

?>