<?php
    if(!function_exists('user_post_view_count'))
    {
        function user_post_view_count($tv_show_id = '')
        {
            global $wpdb;
            $post_id = get_the_ID();
            $ipaddress = $_SERVER['REMOTE_ADDR'] ;
            $table_name = $wpdb->prefix . 'streamit_postview';
            $already_exist = $wpdb->get_row("SELECT * FROM $table_name WHERE ip_address = '$ipaddress' AND post_id = '$post_id'");
            if ( gettype($already_exist) !== 'object') {
                $wpdb->insert($table_name, array(
                            'ip_address' => $ipaddress,
                            'post_id' => $post_id
                        ));

               set_post_view();
                if($tv_show_id !== '')
                {
                    $key = 'tv_show_views_count';
                    $post_id = $tv_show_id[0];
                        $seasons = get_post_meta($post_id,'_seasons');
                        $ep_id = array_column($seasons[0],'episodes');
                        $id = call_user_func_array('array_merge',$ep_id);
                            $count = count($id);
                    $sum = 0;
                    $args = array(
                        'post_type' => 'episode',
                        'post__in' => $id,
                    );
                    $query = new WP_Query($args);
                    if ( $query->have_posts() ) {
                        while( $query->have_posts() ) {
                            $query->the_post();
                            $view = get_post_meta( get_the_ID(), 'post_views_count', true );

                            if($view)
                            {
                                $sum += $view;
                            }
                        }
                    }

                    update_post_meta( $post_id, $key, ceil($sum/$count) );
                }
            }

        }
    }
    function get_post_view() {
        $count = get_post_meta( get_the_ID(), 'post_views_count', true );
        return "$count views";
    }
    function set_post_view() {
        $key = 'post_views_count';
        $post_id = get_the_ID();
        $count = (int) get_post_meta( $post_id, $key, true );
        $count++;
        update_post_meta( $post_id, $key, $count );
    }
    function posts_column_views( $columns ) {
        $columns['post_views'] = 'Views';
        return $columns;
    }
    function posts_custom_column_views( $column ) {
        if ( $column === 'post_views') {
            echo get_post_view();
        }
    }
    add_filter( 'manage_posts_columns', 'posts_column_views' );
    add_action( 'manage_posts_custom_column', 'posts_custom_column_views' );