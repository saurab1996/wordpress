<?php
//post like count
/*******************************
 * make sure ajax is working otherwise the like button won't work
*******************************/

function add_ajax_url_watchlist() {
    echo '<script type="text/javascript">var ajaxurl = "' . admin_url('admin-ajax.php') . '";</script>';
}
// Add hook for admin <head></head>
add_action('wp_head', 'add_ajax_url_watchlist');



/*******************************
 * likeCount:
 * Get current post is added to watch list or not
*******************************/

function add_to_watchlist($post_id){
   $user = wp_get_current_user();
   $user_id = $user->ID;
   
   $posts = get_user_meta( $user_id, '_user_watchlist', true );
   $post_array = explode(', ', $posts);
   if(in_array($post_id,$post_array)){
      return '<span><i class="fa fa-check" aria-hidden="true"></i></span>';
   }else{
      return '<span><i class="ri-add-line"></i></span>';
   }

}

	
/*******************************
 * like_callback:
 * add or remove post from watch list 
*******************************/
	
add_action('wp_ajax_watchlist_callback', 'watchlist_callback');
add_action('wp_ajax_nopriv_watchlist_callback', 'watchlist_callback');

function watchlist_callback() {

   	$post_id = json_decode($_GET['data']); // Get the ajax call
   	$feedback = array("watchlist" => "");
   	$user = wp_get_current_user();
	   $user_id = $user->ID;

   // Get metabox values
   $currentvalue = get_user_meta( $user_id, '_user_watchlist', true );


   // Convert likers string to an array
   $post_array = explode(', ', $currentvalue);


   


   // Check if the IP address is already present, if not, add it
   if(!in_array($post_id, $post_array)){
      // Check if the likers metabox already has a value to determine if the new entry has to be prefixed with a comma or not
      if(!empty($currentvalue)){
         $newvalue = $currentvalue .', '. $post_id;
      }else{
         $newvalue = $post_id;
      }
      if(update_user_meta($user_id, '_user_watchlist', $newvalue, $currentvalue)){
         $feedback = array("watchlist" => add_to_watchlist($post_id), "status" => true);
      }
   }else{

      $key = array_search($post_id, $post_array);
      unset($post_array[$key]);

      if(update_user_meta($user_id, '_user_watchlist', implode(", ", $post_array), $currentvalue)){
         $feedback = array("watchlist" => add_to_watchlist($post_id), "status" => false);
      }

   }

   echo json_encode($feedback);

   die(); // A kitten gif will be removed from the interwebs if you delete this line

}
?>