<?php
    add_shortcode('iqonic_register_form', 'iqonic_registration_form');


    if( !function_exists('iqonic_registration_form') )
    { 
        function iqonic_registration_form() {
            // get registration form if user not logged in
            if(!is_user_logged_in()) {
            return iqonic_registration_form_fields();
            }
            else
            {
                if(!is_super_admin(get_current_user_id()))
                    wp_redirect(home_url());
            }
        }
    }

    if( !function_exists('iqonic_registration_form_fields') )  {
        // registration form fields
        function iqonic_registration_form_fields() {
            ob_start();
            if(class_exists('ReduxFramework')) {
                $iq_option = get_option('streamit_options');
            }
            ?>
            <div class="iqonic-user-forms">
            <div class="iq-signup-form">
                <h4 class="iqonic_header"><?php _e('Sign Up'); ?></h4> <?php
                // show any error messages after form submission ?>
                <?php
                if (isset( $_POST["iqonic_user_login"] ) && wp_verify_nonce($_POST['iqonic_register_nonce'], 'iqonic-register-nonce')) {
                    $user_login       = $_POST["iqonic_user_login"];
                    $user_email       = $_POST["iqonic_user_email"];
                    $user_first       = $_POST["iqonic_user_first"];
                    $user_last        = $_POST["iqonic_user_last"];
                    $user_description = $_POST["iqonic_user_description"];
                    $user_pass        = $_POST["iqonic_user_pass"];
                    $pass_confirm     = $_POST["iqonic_user_pass_confirm"];
                    $term_condition = '';
                    if(isset($_POST["iqonic_term_condition"])) {
                        $term_condition = $_POST["iqonic_term_condition"];
                    }


                    $aValid = array('-', '_');
                    $errors = array();

                    if(email_exists($user_email)) {
                        $errors['email_error'] = 'Email already exists.';
                    }
                    if(username_exists($user_login))
                    {
                        $errors['uname_error'] = 'Username already exists.';
                    }
                    elseif(!ctype_alnum(str_replace($aValid, '', $user_login))) {
                        $errors['uname_error'] = 'Your username is not properly formatted.';
                    }

                    if(empty($user_pass) || empty($pass_confirm))
                    {
                        $errors['pass_error'] = 'Password fields should not be empty';
                    }
                    elseif($user_pass !== $pass_confirm)
                    {
                        $errors['pass_error'] = 'Password and confirm password did not match';
                    }

                    if(empty($term_condition))
                    {
                        $errors['terms_conditions'] = 'Please select terms & Conditions';
                    }
                    // only create the user in if there are no errors
                    if(empty($errors)) {

                        $new_user_id = wp_insert_user(array(
                                'user_login'    => $user_login,
                                'user_pass'     => $user_pass,
                                'user_email'    => $user_email,
                                'first_name'    => $user_first,
                                'last_name'     => $user_last,
                                'description'   => $user_description,
                                'user_registered' => date('Y-m-d H:i:s'),
                                'role'        => 'subscriber'
                            )
                        );
                        if($new_user_id) {
                            // send an email to the admin alerting them of the registration
                            wp_new_user_notification($new_user_id);

                            update_user_meta($new_user_id, 'user_terms_conditions', esc_attr( $term_condition ) );
                            // log the new user in
                            wp_set_current_user($new_user_id, $user_login);
                            wp_set_auth_cookie($user_login);
                            do_action('wp_login', $user_login,$new_user_id);

                            // send the newly created user to the home page after logging them in
                            wp_redirect(home_url());
                            exit;
                        }

                    }
                    else
                    {
                        ?>
                        <div class="error-msg">

                            <?php
                            if(isset($errors['terms_conditions']))
                                echo '<li>'.esc_html($errors['terms_conditions'],"streamit-extensions").'</li>';
                            if(isset($errors['email_error']))
                                echo '<li>'.esc_html($errors['email_error'],"streamit-extensions").'</li>';
                            if(isset($errors['uname_error']))
                                echo '<li>'.esc_html($errors['uname_error'],"streamit-extensions").'</li>';
                            if(isset($errors['pass_error']))
                                echo '<li>'.esc_html($errors['pass_error'],"streamit-extensions").'</li>';
                            ?>
                        </div>
                        <?php
                    }

                }
                ?>
                <form id="iqonic_registration_form" class="iqonic_form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
                    <fieldset>
                        <div class="row">
                        <div class="col-md-6">
                        <p>
                            <!-- <label for="iqonic_user_first"><?php // _e('First Name'); ?></label> -->
                            <input name="iqonic_user_first" id="iqonic_user_first" type="text" placeholder="First Name"/>
                        </p>
                        </div>
                        <div class="col-md-6">
                        <p>
                            <!-- <label for="iqonic_user_last"><?php // _e('Last Name'); ?></label> -->
                            <input name="iqonic_user_last" id="iqonic_user_last" type="text" placeholder="<?php _e('Last Name',"streamit-extensions"); ?>"/>
                        </p>
                        </div>
                        <div class="col-md-6">
                        <p>
                            <!-- <label for="iqonic_user_Login"><?php // _e('Username'); ?></label> -->
                            <input name="iqonic_user_login" id="iqonic_user_login" class="required" type="text" placeholder="Username"/>
                        </p>
                        </div>
                        <div class="col-md-6">
                        <p>
                            <!-- <label for="iqonic_user_email"><?php // _e('Email'); ?></label> -->
                            <input name="iqonic_user_email" id="iqonic_user_email" class="required" type="email" placeholder="Email"/>
                        </p>
                        </div>
                        <div class="col-md-6">
                        <p>
                            <!-- <label for="password"><?php // _e('Password'); ?></label> -->
                            <input name="iqonic_user_pass" id="password" class="required" type="password" placeholder="Password"/>
                        </p>
                        </div>
                        <div class="col-md-6">
                        <p>
                            <!-- <label for="password_again"><?php // _e('Confirm Password'); ?></label> -->
                            <input name="iqonic_user_pass_confirm" id="password_again" class="required" type="password" placeholder="Confirm Password"/>
                        </p>
                        </div>
                        <div class="col-xl-12">
                        <p>
                            <!-- <label for="iqonic_user_description"><?php // _e('Biographical Info'); ?></label> -->
                            <textarea name="iqonic_user_description" id="iqonic_user_description" rows="3" cols="50" placeholder="Biographical Info"></textarea>
                        </p>
                        </div>
                        <div class="col-xl-12">
                            <p class="login-remember">
                                <!-- <label for="iqonic_user_description"><?php // _e('Biographical Info'); ?></label> -->
                                <input type="checkbox" class="iq-term" name="iqonic_term_condition" id="iqonic_term_condition" value="accepted"> I've read and accept the
                                <a href="<?php echo esc_html(get_page_link($iq_option['streamit_term_condition'])) ?>">terms & conditions*</a>
                            </p>
                        </div>
                        <div class="col-xl-12 hover-buttons">
                        <p class="signup-submit mb-0">
                            <input type="hidden" name="iqonic_register_nonce" class="btn btn-hover" value="<?php echo wp_create_nonce('iqonic-register-nonce'); ?>"/>
                            <button type="submit" class="btn btn-hover iq-button" value="<?php _e('Sign Up'); ?>"><?php _e('Sign Up'); ?></button>
                        </p>
                        </div>
                            <div class="col-xl-12">
                            <div class="d-flex justify-content-center links">
                                <?php
                                if(isset($iq_option['streamit_signin_link'])) {
                                    $iqonic_signin_link = get_page_link($iq_option['streamit_signin_link']);
                                    $iqonic_signin_title = $iq_option['streamit_signin_title']; ?>
                                    Already have an account?<a href="<?php echo esc_url( $iqonic_signin_link ) ?>" class="iq-sub-card setting-dropdown">

                                        <h6 class="mb-0"> <?php
                                            if(!empty($iqonic_signin_title)) {
                                                echo esc_attr($iqonic_signin_title,'streamit');
                                            } else {
                                                echo esc_html__('Sign In','streamit');
                                            } ?>
                                        </h6>

                                    </a> <?php
                                }
                                ?>
                            </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            </div><?php
            return ob_get_clean();
        }
    }


