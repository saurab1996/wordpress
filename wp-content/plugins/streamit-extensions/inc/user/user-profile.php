<?php
add_shortcode('iqonic_user_profile', 'iqonic_get_user_profile');

if(!function_exists('iqonic_get_user_profile'))
{
    function iqonic_get_user_profile()
    {
        if(is_user_logged_in()) {
            global $wp;
            global $current_url;
            $current_url = home_url(add_query_arg(array($_REQUEST), $wp->request));
            define('CPROFILEURL',$current_url);
            $page = explode('?',$current_url);
            $get_page = end($page);
            if($get_page === 'profile' || (isset($_POST['action']) && $_POST['action'] == 'Update') )
                return iqonic_edit_user_form();
            else
                return iqonic_user_profile_view();

        }
    }
}

if( !function_exists('iqonic_user_profile_view') )
{ 
    function iqonic_user_profile_view() {
        ob_start();
        // get registration form if user not logged in

            $current_user = wp_get_current_user();

            $get_avatar = get_the_author_meta( 'streamit_profile_image', $current_user->ID );
            if(empty($get_avatar))
            {
                $get_avatar = get_avatar_url($current_user->ID);
            }

            $user_name = esc_html( $current_user->user_login );
            $user_email = esc_html( $current_user->user_email );
            $iqonic_user_description = esc_html( $current_user->description );
            $user_full_name = esc_html( $current_user->user_firstname ).' '.esc_html( $current_user->user_lastname );
            $user_dob = esc_html( get_the_author_meta( 'user_dob', $current_user->ID ));
            if(empty($user_dob)) {
                $user_dob = '0000-00-00';
            }
            $user_gender = esc_html( get_the_author_meta( 'user_gender', $current_user->ID ));
            if(empty($user_gender)) {
                $user_gender = '----';
            }
            ?>
            
            <div class="container-fluid m-profile pt-0 pb-0">
            <div class="row">
                <div class="col-lg-4">
                    <div class="sign-user_card text-center">
                        <img src="<?php echo esc_url($get_avatar,"streamit-extensions"); ?>" class="img-fluid rounded-circle"/>
                        <h4 class="mb-3 mt-3"><?php echo esc_html($user_full_name); ?></h4>
                        <p><?php echo esc_html($iqonic_user_description); ?></p>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="sign-user_card">
                        <div class="d-flex justify-content-between links mt-0 mb-3 pb-3 a-border">
                        <h4 class=""><?php echo esc_html('Personal Details',"streamit-extensions"); ?></h4>
                        </div>
                        <div class="row align-items-center justify-content-between mb-3">
                            <div class="col-md-8">
                                <span class="font-size-13"><?php echo esc_html('Email',"streamit-extensions"); ?></span>
                                <p class="text-light  m-0"><?php echo esc_html($user_email); ?></p>
                                
                            </div>
                            <div class="col-md-4 text-md-right text-left">
                                <a href="<?php echo esc_url(CPROFILEURL).'?profile' ?>" class="iq-sub-card setting-dropdown">
                                <?php echo esc_html('Change',"streamit-extensions"); ?>
                                </a>
                            </div> 
                        </div>
                        <div class="row align-items-center justify-content-between mb-3">  
                            <div class="col-md-8">
                                <span class="font-size-13"><?php echo esc_html('User Name',"streamit-extensions"); ?></span>
                                <p class="text-light m-0"><?php echo esc_html($user_name); ?></p>
                                
                            </div>
                            <div class="col-md-4 text-md-right text-left">
                                <a href="<?php echo esc_url(CPROFILEURL).'?profile' ?>" class="iq-sub-card setting-dropdown">
                                <?php echo esc_html('Change',"streamit-extensions"); ?>
                                </a>
                            </div>
                            </div>
                        <div class="row align-items-center justify-content-between mb-3">  
                            <div class="col-md-8">
                                <span class="font-size-13">Date of Birth</span>
                                <p class="text-light m-0"><?php echo esc_html($user_dob); ?></p>
                                
                            </div>
                            <div class="col-md-4 text-md-right text-left">
                                <a href="<?php echo esc_url(CPROFILEURL).'?profile' ?>" class="iq-sub-card setting-dropdown">
                                <?php echo esc_html('Change',"streamit-extensions"); ?>
                                </a>
                            </div>
                            </div>
                        <div class="row align-items-center justify-content-between mb-3">  
                            <div class="col-md-8">
                                <span class="font-size-13"><?php echo esc_html('Gender',"streamit-extensions"); ?></span>
                                <p class="text-light m-0"><?php echo esc_html($user_gender); ?></p>
                                
                            </div>
                            <div class="col-md-4 text-md-right text-left">
                                <a href="<?php echo esc_url(CPROFILEURL).'?profile' ?>" class="iq-sub-card setting-dropdown">
                                <?php echo esc_html('Change',"streamit-extensions"); ?>
                                </a>
                            </div>
                        </div>
                </div>
            </div>
        </div> <?php


        return ob_get_clean();
    }
}