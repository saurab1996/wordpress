<?php 
/*
Plugin Name: StreamIt Extensions	
Plugin URI: http://iqonicthemes.com/
Description: iqonic plugin provides custom team post type, gallery post type with related functionality.
Author: iqonicthemes
Version: 1.2.1
Author URI: http://www.goldenmace.com/
Text Domain: streamit-extensions
Domain Path: /languages
*/

    if( !defined( 'IQ_TH_ROOT' ) )
        define('IQ_TH_ROOT', plugin_dir_path( __FILE__ ));

    if( !defined( 'IQ_TH_URL' ) )
        define( 'IQ_TH_URL', plugins_url( '', __FILE__ ) );

    if( !defined( 'IQ_NAME' ) )
        define( 'IQ_NAME', 'streamit-extensions' );



    require_once(IQ_TH_ROOT . 'widget/social_media.php');

    require_once(IQ_TH_ROOT . 'widget/recent-post.php');


    require_once(IQ_TH_ROOT . 'inc/elementor/init.php');

    require_once(IQ_TH_ROOT . 'inc/helper/helperfunction.php');
    // user account
    require_once(IQ_TH_ROOT . 'inc/user/user-register.php');
    require_once(IQ_TH_ROOT . 'inc/user/user-login.php');
    require_once(IQ_TH_ROOT . 'inc/user/user-forgot-password.php');
    require_once(IQ_TH_ROOT . 'inc/user/user-profile.php');
    require_once(IQ_TH_ROOT . 'inc/user/user-profile-edit.php');
    require_once(IQ_TH_ROOT . 'inc/user/user-watchlist.php');
    require_once(IQ_TH_ROOT . 'inc/user/user-watchlist-view.php');
    require_once(IQ_TH_ROOT . 'inc/user/iq-view-all.php');
    require_once(IQ_TH_ROOT . 'inc/user/iq-view-count.php');

    require IQ_TH_ROOT . 'inc/acf/acf_meta_box.php';
    require IQ_TH_ROOT . 'inc/acf/acf_page_option.php';
    require IQ_TH_ROOT . 'inc/acf/acf_pricing_option.php';
    require IQ_TH_ROOT . 'inc/post-like/post-like.php';




    /*---------------------------------------
            iqonic admin enque
    ---------------------------------------*/

    function iqonic_plugin_script()
    {
        $iqonic_options = get_option('streamit_options');
        $previewpreset = (isset($_REQUEST['preset']) ? $_REQUEST['preset'] : null);
        if($previewpreset){
            $_SESSION["preset"] = $previewpreset;
        }
        if(!isset($_SESSION["preset"])){
            $_SESSION["preset"] = 1;
        }
        if($_SESSION["preset"] != 1) {
            $presetopt = $_SESSION["preset"];
            $presetopt1 = $_SESSION["preset"];
        } else {
            /* if no preset varialbe found in url, use from theme options */
            $presetopt = $iqonic_options['streamit_layout_mode_options'];
            $presetopt1 = $iqonic_options['streamit_layout_mode_options'];
        }	
        
        if(!isset($presetopt)) $presetopt = 1; /* in case first time install theme, no options found ...custom*/
        if(!isset($presetopt1)) $presetopt1 = 1; /* in case first time install theme, no options found...responsive */       

        wp_enqueue_script('slick-min', IQ_TH_URL .'/assest/js/slick.min.js', array(), '1.0.0' , true);
        wp_enqueue_script('slick-animation', IQ_TH_URL .'/assest/js/slick-animation.min.js', array(), '1.0.0' , true);
        wp_enqueue_script('magnific-popup', IQ_TH_URL .'/assest/js/jquery.magnific-popup.min.js', array(), '1.0.0' , true);

        wp_enqueue_script('owl-carousel', IQ_TH_URL .'/assest/js/owl.carousel.min.js', array(), '1.0.0' , true);
        wp_enqueue_script('select2', IQ_TH_URL .'/assest/js/select2.min.js', array(), '1.0.0' , true);
        wp_enqueue_script('streamit-extenstion', IQ_TH_URL .'/assest/js/custom.js', array(), '1.0.0' , true);
        wp_enqueue_style('ionicons', IQ_TH_URL.'/assest/css/ionicons.min.css',array(), '2.0.0', 'all');
	    wp_enqueue_style('flaticon', IQ_TH_URL.'/assest/css/flaticon.css',array(), '1.0.0', 'all');


        wp_enqueue_style('slick-theme', IQ_TH_URL .'/assest/css/slick-theme.css',array(), '1.0', 'all');
        wp_enqueue_style('slick-animation', IQ_TH_URL .'/assest/css/slick-animation.css',array(), '1.0', 'all');
        wp_enqueue_style('all', IQ_TH_URL .'/assest/css/all.min.css',array(), '1.0', 'all');
        wp_enqueue_style('animate', IQ_TH_URL .'/assest/css/animate.min.css',array(), '1.0', 'all');
        wp_enqueue_style('dark', IQ_TH_URL .'/assest/css/dark.css',array(), '1.0', 'all');
        wp_enqueue_style('slick', IQ_TH_URL .'/assest/css/slick.css',array(), '1.0', 'all');
        wp_enqueue_style('select2', IQ_TH_URL .'/assest/css/select2.css',array(), '1.0', 'all');
        wp_enqueue_style('owl-carousel', IQ_TH_URL .'/assest/css/owl.carousel.min.css',array(), '1.0', 'all');

        wp_enqueue_style('magnific-popup', IQ_TH_URL .'/assest/css/magnific-popup.css',array(), '1.0', 'all');

        wp_enqueue_style('remixicon', IQ_TH_URL .'/assest/css/remixicon.css',array(), '1.0', 'all');

	    // Load main strap css style
	    wp_enqueue_style( 'streamit-extenstion', IQ_TH_URL . '/assest/css/style'.$presetopt.'.css', false, '1.0.0' );
        wp_enqueue_style( 'streamit-plugin-responsive', IQ_TH_URL . '/assest/css/responsive'.$presetopt1.'.css', false, '1.0.0' );
    }

    add_action( 'wp_enqueue_scripts', 'iqonic_plugin_script' );

    add_action( 'init', 'iqonic_pricing' );
function iqonic_pricing() {
    $url = get_template_directory_uri();
	$labels = array(
		'name'                  => esc_html__( 'Pricing Plan', 'post type general name',"streamit-extensions" ),
		'singular_name'         => esc_html__( 'Pricing Plan', 'post type singular name',"streamit-extensions" ),
		'menu_name'             => esc_html__( 'Streamit Pricing', 'admin menu',"streamit-extensions" ),
		'name_admin_bar'        => esc_html__( 'Streamit Pricing', 'add new on admin bar',"streamit-extensions" ),
		'add_new'               => esc_html__( 'Add New', 'Pricing',"streamit-extensions" ),
		'add_new_item'          => esc_html__( 'Title',"streamit-extensions" ),
		'new_item'              => esc_html__( 'New Price',"streamit-extensions" ),
		'edit_item'             => esc_html__( 'Edit Price',"streamit-extensions" ),
		'view_item'             => esc_html__( 'View Pricing',"streamit-extensions" ),
		'all_items'             => esc_html__( 'All Pricing',"streamit-extensions" ),
		'search_items'          => esc_html__( 'Search Price',"streamit-extensions" ),
		'parent_item_colon'     => esc_html__( 'Parent Price :',"streamit-extensions" ),
		'not_found'             => esc_html__( 'No Classs found.',"streamit-extensions" ),
		'not_found_in_trash'    => esc_html__( 'No Classs found in Trash.',"streamit-extensions" )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'show_in_nav_menus'  => TRUE,
		'has_archive'        => true,
		'hierarchical'       => false,		
		'menu_position'      => null,
		'menu_icon'			 => $url.'/assets/images/options.png',
		'supports'           => array( 'title')
	);

	register_post_type( 'pricing', $args );
}

add_action( 'after_setup_theme', 'iqonic_custom_taxonomy' );
function iqonic_custom_taxonomy() {
	$labels = '';

	register_taxonomy(
		'pricing_categories',
		'pricing',
		array(
			'label'         => esc_html__( 'Pricing Categories',"streamit-extensions" ),
			'rewrite'       => true,
            'hierarchical'  => true,
            'meta_box_cb' => false,
		)
	);

}

    if(!function_exists('streamit_db_setup')){
        function streamit_db_setup() {
                global $wpdb;
                $charset = $wpdb->get_charset_collate();
                $table_name = $wpdb->prefix . 'streamit_postview';
                $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );
                if ($wpdb->get_results( $query ) !== $table_name ) {
                    $sql = "CREATE TABLE $table_name (
                            id  bigint(20) NOT NULL AUTO_INCREMENT,
                            ip_address longtext NOT NULL,
                            UNIQUE KEY id (id),
                            post_id  bigint(20) NOT NULL
                        )$charset;";
                    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
                    dbDelta($sql);
                }
        }
        add_action( 'init', 'streamit_db_setup' );
    }

/**
 * Activate the plugin.
 */
function streamit_extension_activate() {
    // Trigger our function that registers the custom post type plugin.
    streamit_db_setup();
    // Clear the permalinks after the post type has been registered.
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'streamit_extension_activate' );

add_filter('views_edit-pricing','my_filter');
function my_filter($views){
    $views['import'] = '<form method="post" action="'.admin_url('edit.php?post_type=pricing').'">
                            <button style="color: rgb(0, 113, 161);
                            border:1px solid rgb(0, 113, 161);
                            cursor:pointer;
                            background: rgb(243, 245, 246);
                            vertical-align: top;border-radius:5px;" name="import_pms_plans" value="Import">Import PMS Plans</button> 
                            <label style="background:white;color:green;cursor: default;">Click on import to get all subscriptions from Paid Member Subscription plugin</label>
                        </form>';
    return $views;
}
    
if(!function_exists('iqonic_import_pms_plans')){

    function iqonic_import_pms_plans(){
        if(isset($_POST['import_pms_plans']) && $_POST['import_pms_plans'] === 'Import'){
            global $wpdb;
            $pms_args=array(
                'post_type'         => 'pms-subscription',
                'post_status'       => 'active',
                'posts_per_page'    => -1
            );
            $pricing_args=array(
                'post_type'         => 'pricing',
                'post_status'       => 'publish',
                'posts_per_page'    => -1,
                'meta_key'          => 'name_paid_sub_id'
            );
            $pms_subscription = new \WP_Query($pms_args);
            $pricing_query = new \WP_Query($pricing_args);
            $existing_ids = [];
            if ($pricing_query->posts) {
                foreach ($pricing_query->posts as $key => $value) {
                    $existing_ids[] = get_post_meta($value->ID,'name_paid_sub_id',true);
                }
            }
            if ($pms_subscription->posts) {
                foreach ($pms_subscription->posts as $key => $value) {
                    if(!in_array ( $value->ID, $existing_ids )){ 
                        $id = $value->ID;

                        $price = get_post_meta($id,'pms_subscription_plan_price',true);
                        $price = !empty($price) ? '$'.$price : '';

                        $duration = get_post_meta($id,'pms_subscription_plan_duration',true);
                        $duration = !empty($duration) ? $duration : '';

                        $duration_unit = get_post_meta($id,'pms_subscription_plan_duration_unit',true);
                        $duration_unit = !empty($duration_unit) ? $duration_unit : '';
                        $duration = $duration.' '.$duration_unit;

                        $title = $value->post_title;
                        $title = !empty($title) ? $title : '';

                        $post = array(
                            'post_title' => $title,
                            'post_status' => 'publish',
                            'post_type' => 'pricing',
                        );
                        
                        if($pricing_id = wp_insert_post($post))
                        {
                            update_post_meta($pricing_id,'name_price', $price);
                            update_post_meta($pricing_id,'name_paid_sub_id', $id);
                            update_post_meta($pricing_id,'name_period', $duration);
                            $page_redirect = get_option('pms_general_settings')['register_page'];
                            $page_redirect = !empty($page_redirect) ? $page_redirect : '';
                            update_post_meta($pricing_id,'name_pricing_redirect_link', $page_redirect);
                        }
                    }
                }
            }
        }
    }
add_action('admin_init', 'iqonic_import_pms_plans');
}
?>