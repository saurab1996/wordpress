<?php

/**
 * Fired during plugin activation
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    Woo_More_Option
 * @subpackage Woo_More_Option/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woo_More_Option
 * @subpackage Woo_More_Option/includes
 * @author     Saurab <saurab1234gupt@gmail.com>
 */
class Woo_More_Option_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
