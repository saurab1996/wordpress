<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    Woo_More_Option
 * @subpackage Woo_More_Option/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
