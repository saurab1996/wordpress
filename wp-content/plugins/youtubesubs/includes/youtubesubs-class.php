<?php
/**
 * Adds youtubesubs_Widget widget.
 */
class youtubesubs_Widget extends WP_Widget
{

/**
 * Register widget with WordPress.
 */
    public function __construct()
    {
        parent::__construct(
            'youtubesubs_Widget', // Base ID
            esc_html__('youtube subs', 'yts_domain'), // Name
            array('description' => esc_html__('Widget to display youtube subs', 'yts_domain')) // Args
        );
    }

/**
 * Front-end display of widget.
 *
 * @see WP_Widget::widget()
 *
 * @param array $args     Widget arguments.
 * @param array $instance Saved values from database.
 */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        //if( empty( $instance['channelname'] )) {
        //echo $instance->$title;
        if ($instance['channelname'] == 'channelname' || $instance['channelname'] == '') {
            echo "channelid is ", $instance['channelid'];
            ?>

        <div  class="g-ytsubscribe" data-channelid="<?php echo $instance['channelid']; ?>"
        data-layout="full" data-count="default" >
        </div>
        <?php
//$instance['']
        } else {
            echo "channelname", $instance['channelname'];
            ?>
        <div class="g-ytsubscribe" data-channel="<?php echo $instance['channelname']; ?>"
        data-layout="full" data-count="hidden">
        </div>
         <?php
}

        // }
        echo $args['after_widget'];
    }

/**
 * Back-end widget form.
 *
 * @see WP_Widget::form()
 *
 * @param array $instance Previously saved values from database.
 */
    public function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : esc_html__('Youtube subs', 'yts_domain');
        $channelid = !empty($instance['channelid']) ? $instance['channelid'] : esc_html__('channelid ', 'yts_domain');
        $channelname = !empty($instance['channelname']) ? $instance['channelname'] : esc_html__('channelname', 'yts_domain');
         $temp = $channelname;
        echo $temp;
        ?>
    <!-- title  -->
    <p >
    <label class="ptag" for="<?php echo esc_attr($this->get_field_id('title')); ?>">
    <?php esc_attr_e('Title:', 'yts_domain');?>
    </label>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
     name="<?php echo esc_attr($this->get_field_name('title')); ?>"
     type="text" value="<?php echo esc_attr($title); ?>">
    </p>
    <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('select#select').change( function() {
                    $( '#hide_show div' ).css('display', 'none');
                    $( '#hide_show #' + this.value ).css('display', 'block');
                    console.log('<?php echo  $channelname; ?>');
                   
                     if(this.value == 'name')
                     {
                        
                         $( '#hide_show #' + this.value+ ' input').val('<?php echo esc_attr($channelname); ?>');
                         //$( '#hide_show #id input').val('channelid');
                        }
                     else if(this.value =='id')
                     {
                        $( '#hide_show #' + this.value+ ' input').val('<?php echo esc_attr($channelid); ?>');
                        //$( '#hide_show #name input').val('channelname');
                     }
                    
                });
            });
        </script>
        <style type="text/css">
            #hide_show div  {display: none;}
        </style>
         <p>
        <select class="widefat" id="select">
            <option value="unknown" selected>choose by youtube channelname or Id</option>
            <option value="name">Channelname</option>
            <option value="id">ChannelId</option>

        </select>
         </p>

    <div id="hide_show">
    <div id="id">
    <p>
    <label for="<?php echo esc_attr($this->get_field_id('channelid')); ?>">
    <?php esc_attr_e('channelid:', 'yts_domain');?>
    </label>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('channelid')); ?>"
     name="<?php echo esc_attr($this->get_field_name('channelid')); ?>"
     type="text" >
    </p>
    </div>
    <div id="name">
    <p >
    <label for="<?php echo esc_attr($this->get_field_id('channelname')); ?>">
    <?php esc_attr_e('channelname:', 'yts_domain');?>
    </label>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('channelname')); ?>"
     name="<?php echo esc_attr($this->get_field_name('channelname')); ?>"
     type="text" >
    </p>
    </div>
    </div>
    <?php
    
    echo $instance['channelid'];
   

}

/**
 * Sanitize widget form values as they are saved.
 *
 * @see WP_Widget::update()
 *
 * @param array $new_instance Values just sent to be saved.
 * @param array $old_instance Previously saved values from database.
 *
 * @return array Updated safe values to be saved.
 */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? sanitize_text_field($new_instance['title']) : '';
        $instance['channelid'] = (!empty($new_instance['channelid'])) ? sanitize_text_field($new_instance['channelid']) : '';
        
        $instance['channelname'] = (!empty($new_instance['channelname'])) ? sanitize_text_field($new_instance['channelname']) : '';

        return $instance;
    }

} // class youtubesubs_Widget
?>