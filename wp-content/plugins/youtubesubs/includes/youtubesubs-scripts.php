<?php
function yts_add_scripts()
{
    //add main css file
    wp_enqueue_style('yts-main-style', plugin_dir_url( __DIR__ ) . 'assests/css/style.css');
    //add main js file
    wp_enqueue_script('yts-main-script',plugin_dir_url( __DIR__ ) . 'assests/js/main.js');
    //add youtube api js
    wp_register_script('google','https://apis.google.com/js/platform.js');
    wp_enqueue_script('google');
   
}
add_action('wp_enqueue_scripts','yts_add_scripts');
?>