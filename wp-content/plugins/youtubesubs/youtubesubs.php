<?php
/*
Plugin Name: YouTube Subs
Plugin URI: http://wp.test
Description: Widget to add subscribe button on your site
Version: 1.0.0
Author: Saurab Gupta
Author URI: http://wp.test

*/
//exit if accessed directly 
if(!defined('ABSPATH'))
{
    exit;
} 
//load scripts
include_once(plugin_dir_path(__FILE__).'includes/youtubesubs-scripts.php');

//load class
include_once(plugin_dir_path(__FILE__).'includes/youtubesubs-class.php');

//register widget
function register_youtubesubs_widget()
{
    register_widget('youtubesubs_Widget');
}
add_action('widgets_init','register_youtubesubs_widget');
?>
