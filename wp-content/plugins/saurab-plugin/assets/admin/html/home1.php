<?php
$options=get_option('product1');
//global $current_screen;
//echo $current_screen;

?>
<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
    <a class="nav-tab nav-tab-active" href="http://wordpress.test/wp-admin/admin.php?page=product_option_setting">Product Options</a>
    <a class="nav-tab nav-tab-active" href="http://wordpress.test/wp-admin/admin.php?page=product_search_option_setting">Search Product </a>
</h2>
<form method="post" action="options.php">
    <table id="product_table" class="wc_gateways widefat thpladmin_fields_table" cellspacing="0">
        <thead>
        <tr>
            <th colspan="5">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">+ Add field</button>
                <button type="button" onclick="thwepofRemoveSelectedFields()" class="btn btn-small">Remove</button>
                <button type="button" onclick="thwepofEnableSelectedFields()" class="btn btn-small">Enable</button>
                <button type="button" onclick="thwepofDisableSelectedFields()" class="btn btn-small">Disable</button>
            </th>
            <th colspan="5">
                <input type="submit" name="save_fields" class="btn btn-small btn-primary" value="Save changes" style="float:right">
                <input type="submit" name="reset_fields" class="btn btn-small" value="Reset to default options" style="float:right; margin-right: 5px;" onclick="return confirm('Are you sure you want to reset to default fields? all your changes will be deleted.');">
            </th>
        </tr>
        <tr>
            <th class="sort"></th>
            <th class="check-column">
                <input type="checkbox" style="margin:0px 4px -1px -1px;" onclick="thwepofSelectAllProductFields(this)">
            </th>
            <td class="name">Name</td>
            <th class="id" ></th>
            <th>Label</th>

            <th class="status">Required</th>
            <th class="status">Enabled</th>
            <th class="status">Actions</th>
        </tr>
        </thead>
          <div id="tablecontent">
             <tbody>
             <tr>
                  <th class="sort"></th>
                  <th class="check-column">
                      <input type="checkbox" style="margin:0px 4px -1px -1px;" onclick="thwepofSelectAllProductFields(this)">
                  </th>
                  <td class="name"><?php echo $options['name']; ?></td>
                  <th class="id" ></th>
                  <td><?php echo $options['label']; ?></td>

                  <td class="status"><?php echo $options['required'] == 'checked'?'<span class="dashicons dashicons-yes tips"></span>' :''; ?></td>
                  <td class="status"><?php echo $options['enabled'] == 'checked' ?'<span class="dashicons dashicons-yes tips"></span>' :''; ?></td>
                  <th class="status">Actions</th>
              </tr>
             </tbody>
          </div>

        <tfoot>

        <tr>        <th colspan="5">
                <button type="button" onclick="thwepofOpenNewFieldForm()" class="btn btn-small btn-primary">+ Add field</button>
                <button type="button" onclick="thwepofRemoveSelectedFields()" class="btn btn-small">Remove</button>
                <button type="button" onclick="thwepofEnableSelectedFields()" class="btn btn-small">Enable</button>
                <button type="button" onclick="thwepofDisableSelectedFields()" class="btn btn-small">Disable</button>
            </th>
            <th colspan="5">
                <input type="submit" name="save_fields" class="btn btn-small btn-primary" value="Save changes" style="float:right">
                <input type="submit" name="reset_fields" class="btn btn-small" value="Reset to default options" style="float:right; margin-right: 5px;" onclick="return confirm('Are you sure you want to reset to default fields? all your changes will be deleted.');">
            </th>
        </tr>
        </tfoot>
        <?php if( empty($options['name'])) {?>
        <tbody class="ui-sortable">
        <tr><td colspan="10" align="center" class="empty-msg-row">No custom fields found. Click on <b>Add field</b> button to create new fields.</td></tr>
        </tbody>
    <?php }?>
    </table>
</form>


<?php
require(PLUGIN_PATH.'assets/admin/html/modal.php');
?>