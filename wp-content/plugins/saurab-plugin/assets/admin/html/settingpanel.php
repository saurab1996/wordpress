
<?php

$options=add_option('product');
$getoption=get_option('product');
?>
<table>
    <tr>
        <th>
            <label for="product_fieldtype">
                <?php _e('Fieldtype','product_option'); ?><span>*</span>
            </label>
        </th>
        <td>
            <select name="product1[fieldtype]" id="field" value="" style="width:260px;"  >
                <option value="inputtext">Text</option>
                <option value="hidden">Hidden</option>
                <option value="number">Number</option>
                <option value="tel">Telephone</option>
                <option value="password">Password</option>
                <option value="textarea">Textarea</option>
                <option value="select">Select</option>
                <option value="checkbox">Checkbox</option>
                <option value="checkboxgroup">Checkbox Group</option>
                <option value="radio">Radio Button</option>
                <option value="datepicker">Date Picker</option>
                <option value="heading">Heading</option>
                <option value="paragraph">Paragraph</option>
            </select>
        </td>
    </tr>
    <br>
    <tr>
        <th>
            <label for="product_name">
                <?php _e('Name','product_option'); ?><span>*</span>
            </label>
        </th>
        <td>
            <input id="product_name" name="product1[name]" type="text" value="" required>
        </td>
    </tr>
    <br>
    <tr>
        <th>
            <label for="product_label">
                <?php _e('Label','product_option'); ?>
            </label>
        </th>
        <td>
            <input id="product_label" name="product1[label]" value="" type="text" required>
        </td>
    </tr>
    <br>
    <tr>
        <th>
            <label for="product_required">
                <?php _e('Required','product_option'); ?>
            </label>
        </th>
        <td>
            <input type="hidden" id="product_required" name="product1[required]" value="unchecked" type="checkbox">
            <input id="product_required" name="product1[required]" value="checked" type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value">
        </td>
    </tr>
    <br>
    <tr>
        <th>
            <label for="product_enabled">
                <?php _e('Enabled','product_option'); ?>
            </label>
        </th>
        <td>
            <input type="hidden" id="product_enabled" name="product1[enabled]" value="unchecked" type="checkbox">
            <input id="product_enabled" name="product1[enabled]" value="checked" type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value">
        </td>
    </tr>
</table>

<?php

?>