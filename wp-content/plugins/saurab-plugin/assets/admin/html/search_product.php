
<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
    <a class="nav-tab nav-tab-active" href="http://wordpress.test/wp-admin/admin.php?page=product_option_setting">Product Options</a>
    <a class="nav-tab nav-tab-active" href="http://wordpress.test/wp-admin/admin.php?page=product_search_option_setting">Search Product </a>
</h2>
<?php

settings_errors();
$options=get_option('admin_product_search');
?>

<form action="options.php" method="post">
   <?php
    settings_fields('admin_product_search');
    do_settings_sections('product_search_option_setting');
    ?>
    <table>

        <tr><th></th><td><br></td></tr>

        <tr valign="top" class="search-products">
            <th>Products</th>
            <td>
                <input  id="product_include" type="text" class="wbdv wc-product-search" data-multiple="true" style="width: 50%;" name="admin_product_search[include_product]" data-placeholder="Search for a product&hellip;" data-action="wbdv_ajax_products" data-selected="[]" value="<?php echo $options['include_product']; ?>" />
                <div id="show"></div>

            </td>
        </tr>
        <tr><th></th><td><br></td></tr>
        <tr valign="top" class="search-products">
            <th>Exclude Products</th>
            <td>
                <input type="text" id="product_exclude" class="wbdv wc-product-search" data-multiple="true" style="width: 50%;" name="admin_product_search[exclude_product]" data-placeholder="Search for a product&hellip;" data-action="wbdv_ajax_products" data-selected="[]" value="<?php echo $options['exclude_product']; ?>" />
            </td>
        </tr>

        <tr><th></th><td><br></td></tr>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="wbdv_categories">Categories <span class="woocommerce-help-tip" data-tip="A product must be in this category for the coupon to remain valid or, for &quot;Product Discounts&quot;, products in these categories will be discounted."></span></label>
            </th>
            <td class="forminp forminp-multiselect">
                <select
                        name="admin_product_search[include_category]"
                        id="product_categories"
                        style="width:300px"
                        class="wbdv_cats"
                        multiple="multiple">
                    <option value="15" selected>
                        Uncategorized</option>
                </select>
            </td>
        </tr>
        <tr><th></th><td><br></td></tr>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="wbdv_exclude_categories">Exclude Categories <span class="woocommerce-help-tip" data-tip="Product must not be in this category for the coupon to remain valid or, for &quot;Product Discounts&quot;, products in these categories will not be discounted."></span></label>
            </th>
            <td class="forminp forminp-multiselect">
                <select name="admin_product_search[exclude_category]"
                        id="product_exclude_categories"
                        style="width:300px"
                        class="wbdv_cats"
                        multiple="multiple">
                    <option value="15" selected>Uncategorized</option>
                </select>
            </td>
        </tr>

    </table>


    <?php   submit_button('save settings');?>
</form>