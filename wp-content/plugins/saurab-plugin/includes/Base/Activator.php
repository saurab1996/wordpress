<?php
/**
 * BaseController class file
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    saurab_plugin
 * @subpackage Saurab_Plugin/includes
 */
//prevent from direct access
namespace includes\Base;
if(!defined('ABSPATH'))
{
    die;
}

//activator class
class Activator{
   
 function activate(){

      flush_rewrite_rules();
    }

    function quotation_setting_field_callback(){

         $options = get_option( 'product_option_settings' );

         $value = array();
         if (isset($options['product_option_settings']) && ! empty($options['product_option_settings'])) {
             $value = $options['product_option_settings'];
         }
        ?>
        <input type="checkbox" name="product_option_settings[name]" value="name <?PHP in_array('name', $value) ? 'checked' : '' ; ?> " class="regular-text"/> Show Username
        <br>
        <br>
        <br>
        <input type="checkbox" name="product_option_settings[email]" value="email <?php in_array('email', $value) ? 'checked' : '' ; ?>" class="regular-text"/> Show Email
       <?php
        }

    function register()
    {
        if(!get_option('product_option_setting'))
        {

            add_option('product_option_settings');
            add_settings_section('recent_view_section','recent view Setting','', 'product_option_setting');
            register_setting('product_option_settings','product_option_settings');
            add_settings_field('product_option_setting_field','product option setting Field',array($this,'quotation_setting_field_callback'),'product_option_setting','recent_view_section');
        }
       //Check if woocommerce plugin is installed.
       add_action( 'admin_notices', array( $this, 'check_required_plugins' ) );


    }

    public function check_required_plugins() {
        if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) { ?>
          <div id="message" class="error">
            <p><?php echo PLUGIN_NAME; ?> requires <a href="http://www.woothemes.com/woocommerce/" target="_blank">WooCommerce</a> to be activated in order to work. Please install and activate <a href="<?php echo admin_url('/plugin-install.php?tab=search&amp;type=term&amp;s=WooCommerce'); ?>" target="">WooCommerce</a> first.</p>
          </div>
    
          <?php
          deactivate_plugins( '/saurab-plugin/saurab-plugin.php' );
        }
      }









}