<?php
/**
 * Fired during plugin deactivation
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    saurab_plugin
 * @subpackage Saurab_Plugin/includes
 */

 namespace includes\Base;

 //prevent from direct access
 if(!defined('ABSPATH'))
{
    die;
}

//deactivator class
class Deactivator{
   
   static function deactivate(){
        flush_rewrite_rules();
    }

}