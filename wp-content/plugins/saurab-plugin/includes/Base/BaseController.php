<?php
/**
 * Fired during plugin activation
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    saurab_plugin
 * @subpackage Saurab_Plugin/includes
 */
//prevent from direct access
namespace includes\Base;
if(!defined('ABSPATH'))
{
    die;
}

//activator class
class BaseController{

}