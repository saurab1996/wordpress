<?php
/**
 * admin class
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    saurab_plugin
 * @subpackage Saurab_Plugin/includes/admin
 */
//prevent from direct access
namespace includes\Admin;
use includes\Admin\VoucherClass;

if(!defined('ABSPATH'))
{
    die;
}
define( 'plugin_name', 'saurab_plugin' );
class AdminClass{

    public $plugin_name1=PLUGIN_BASE_NAME;

    function __construct(){
        add_action('init', array($this, 'custom_post_type'));

       //$this->adminside_enqueue_script();
        //add_action( 'init', array($this,'create_product_tax' ));
    }
    
    function custom_post_type(){

    }

    function register(){

        add_action('admin_menu',array($this,'add_admin_menu'));
        add_filter( "plugin_action_links_$this->plugin_name1",array($this,'settings_link'));
        //add_action( 'wp_enqueue_scripts',array($this,'adminside_enqueue_script' ));
        add_action( 'admin_enqueue_scripts',array($this,'adminside_enqueue_script'));
        add_action( 'admin_enqueue_scripts',array($this,'adminside_enqueue_style'));
        add_action('admin_init',array($this,'custom_setting'));
        $value=get_option('product1');
        add_action( 'wp_ajax_search_product',array($this, 'search_product') );
        add_action( 'wp_ajax_nopriv_search_product', array($this, 'search_product') );


 if($value['enabled']=='checked') {
     add_action('woocommerce_before_add_to_cart_button', array($this, 'add_field_before_cart'));
     if($value['required']=='checked') {
         add_filter('woocommerce_add_to_cart_validation', array($this, 'add_the_date_validation'), 10, 5);
     }

 }
   }
   function add_field_before_cart(){
       $value=get_option('product1'); ?>
           <div class="">
               <label for="pr-field"><?php echo  $value['label']; ?></label>
               <input type="<?php echo  $value['fieldtype'];  ?>" name='pr-field' id='pr-field' value=''>
           </div>
       <?php
   }


        function add_the_date_validation( $passed, $product_id, $quantity, $variation_id=null ) {
            if( empty( $_POST['pr-field'] ) ) {
                $passed = false;
                wc_add_notice( __( 'Your name is a required field.', 'plugin-republic' ), 'error' );
            }
            return $passed;

    }


    function custom_setting(){
       {
           $default_options=array(
               'fieldtype'=>'',
               'name'=>'',
               'label'=>'',
               'required'=>'unchecked',
               'enabled'=>'unchecked'
           );

           $product_search_default_options=array(
               'include_product'=>'',
               'exclude_product'=>'',
               'include_category'=>'',
               'exclude_category'=>'',

           );
           add_option('admin_product_search',serialize($product_search_default_options));
           add_option('product1', serialize($default_options));
           register_setting('admin_product_search','admin_product_search');
           register_setting('product1', 'product1');
           add_settings_section('product_section', 'product section', '', 'product_option_setting');
           add_settings_field('product_field', '', array($this, 'product_field_callback'), 'product_option_setting', 'product_section');
          // add_setting_section('product_search_section','product search section','','product_search_option_setting');
           add_settings_field('product_search_field','',array($this,'product_search_field_callback'),'product_search_option_setting','product_search_section');

       }
   }
   function product_search_field_callback(){

   }
   function product_field_callback()
   {


   }

    function settings_link($links){

       $settings_link = '<a href="'.admin_url('edit.php?post_type=book').'">Settings</a>';
       array_unshift( $links, $settings_link );
       return $links;
   }



    function add_admin_menu(){
       add_submenu_page('woocommerce',__('WC recently viewed products','product_option_setting'),__('WC recently viewed products','product_option_setting'),'manage_options',"product_option_setting",array($this,'admin_index'));
        add_submenu_page('woocommerce','','','manage_options',"product_search_option_setting",array($this,'admin_product_search'));

   }
   function admin_product_search(){
       require_once(PLUGIN_PATH.'assets/admin/html/search_product.php');
   }
   function admin_index(){
        require_once(PLUGIN_PATH.'assets/admin/html/home1.php');
   }

    function adminside_enqueue_script(){

        wp_enqueue_script('mypluginmainjs', PLUGIN_URL.'assets/admin/js/main.js');
        wp_enqueue_script('mypluginbootrapjs', PLUGIN_URL.'assets/admin/js/bootstrap.min.js');
        wp_register_script('jquery1','https://apis.google.com/js/platform.js');
        wp_enqueue_script('jquery1');
        wp_localize_script(
            'mypluginmainjs',
            'opt',
            array(
                'ajaxUrl'   => admin_url('admin-ajax.php'),
                'noResults' => esc_html__( 'No products found', 'textdomain' ),
            )
        );

    }
    public function adminside_enqueue_style(){

        wp_enqueue_style('mypluginmaincss', PLUGIN_URL.'assets/admin/css/style.css');
        wp_enqueue_style('mypluginbootsrapcss', PLUGIN_URL.'assets/admin/css/bootstrap.min.css');

    }

    function search_product()
    {
        global $wpdb, $woocommerce;
        if (isset($_POST['search_term']) && !empty($_POST['search_term'])) {

            $keyword = $_POST['search_term'];
            $response['custom'] = "do something custom";
            $response['success'] = true;
            $response = json_encode($response);
            $objProduct = wc_get_product('101');
            // $name = $objProduct->get_name();
            $querystr = "SELECT DISTINCT $wpdb->posts.*
                FROM $wpdb->posts, $wpdb->postmeta
                WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id
                AND (
                    ($wpdb->postmeta.meta_key = '_sku' AND $wpdb->postmeta.meta_value LIKE '%{$keyword}%')
                    OR
                    ($wpdb->posts.post_content LIKE '%{$keyword}%')
                    OR
                    ($wpdb->posts.post_title LIKE '%{$keyword}%')
                )
                AND $wpdb->posts.post_status = 'publish'
                AND $wpdb->posts.post_type = 'product'
                ORDER BY $wpdb->posts.post_date DESC";
            $query_results = $wpdb->get_results($querystr);

            $all_product_data = $wpdb->get_results("SELECT post_title FROM `" . $wpdb->prefix . "posts` where post_type='product' and post_status = 'publish' ");

            if (!empty($query_results)) {

                $output = '';
                $valuearray=array();
                foreach ($query_results as $result) {
                   $value = $result->post_title;
                  array_push($valuearray,$value);
                }

                echo "<br>";
                // print_r($all_product_data);
                print_r($valuearray);

                echo 'hello';
               // return $valuearray;
                die();
            }
        }
    }

}






?>