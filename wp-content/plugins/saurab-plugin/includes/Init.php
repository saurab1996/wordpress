<?php
/**
 * Init class
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    saurab_plugin
 * @subpackage Saurab_Plugin/includes
 */
namespace includes;
final class Init {
    /**
     * store all the class
     * @return array full list of class
     */
    public static function get_services(){
     return[
        Admin\AdminClass::class,
        Publics\PublicClass::class,
        Base\Activator::class
     ];
    }
     /**
     * loop through  all the class
     * @param class $class class from services array
     */
    public static function register_services(){
       foreach (self::get_services() as $class )
       {
           $service =self::instantiate($class);
           if(method_exists($service,'register')){
               $service->register();
           }
       }
    }
   
    /**
     * instantiate  the class
     * @return array full list of class
     * @return class instace new instance of the class
     */

    public static function instantiate($class){
        $service =new $class();
        return $service;
    }
}