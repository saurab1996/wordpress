<?php
/**
 * public class
 *
 * @link       http://wp.test
 * @since      1.0.0
 *
 * @package    saurab_plugin
 * @subpackage Saurab_Plugin/includes/admin
 */
//prevent from direct access
namespace includes\Publics;
if(!defined('ABSPATH'))
{
    die;
}

class PublicClass{

    function register(){
        add_action('wp_enqueue_scripts', array($this,'public_enqueue_style'));
        add_action('wp_enqueue_scripts', array($this,'public_enqueue_script'));
     }

    function public_enqueue_script(){
        wp_enqueue_script('mypluginscript',  PLUGIN_URL.'assets/public/js/main.js');
    }

    function public_enqueue_style(){
        wp_enqueue_style('mypluginstyle', PLUGIN_URL.'assets/public/css/style.css');
    }

}