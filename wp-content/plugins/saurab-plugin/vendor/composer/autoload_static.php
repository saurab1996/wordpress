<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitacf4ca939f000b25bd37a542cbd9a7c1
{
    public static $prefixLengthsPsr4 = array (
        'i' => 
        array (
            'includes\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'includes\\' => 
        array (
            0 => __DIR__ . '/../..' . '/includes',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitacf4ca939f000b25bd37a542cbd9a7c1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitacf4ca939f000b25bd37a542cbd9a7c1::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitacf4ca939f000b25bd37a542cbd9a7c1::$classMap;

        }, null, ClassLoader::class);
    }
}
