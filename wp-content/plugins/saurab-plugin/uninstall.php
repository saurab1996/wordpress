<?php
/**
 * Fired when the plugin is uninstalled.
 * @package           saurab_plugin
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

//clear database when uninstall plugin
$book=get_posts(array(
    'numberposts' => -1,
    'post_type'   => 'book'
));

