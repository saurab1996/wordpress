<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wp.test
 * @since             1.0.0
 * @package           saurab_plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Saurab plugin
 * Plugin URI:        http://wp.test
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Saurab
 * Author URI:        http://wp.test
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 
 */

// If this file is called directly, abort.
//exit if accessed directly 
if(!defined('ABSPATH'))
{
    die;
}
use includes\Base\Activator;
//add_action( 'after_setup_theme', function() {
//    do_action('check_action_of_hook');
//    //remove_action('check_action_of_hook','sauarab_plugin1');
//} );
//
//
//
//
//add_action('check_action_of_hook','sauarab_plugin');
//add_action('init','sauarab_plugin1');
////remove_action('check_action_of_hook','sauarab_plugin1');
//
//function sauarab_plugin1(){
//    remove_action('check_action_of_hook','sauarab_plugin2',11);
//}
//function sauarab_plugin2(){
//    echo "morning";
//    die;
//}
//
//function sauarab_plugin(){
//echo  "hello";
//    die;
//}

define('PLUGIN_PATH',plugin_dir_path(__file__));

define('PLUGIN_URL',plugin_dir_url(__file__));

define('PLUGIN_NAME','Saurab Plugin');

define('PLUGIN_BASE_NAME',plugin_basename(__FILE__));

if(file_exists(dirname(__FILE__).'/vendor/autoload.php')){
    require_once dirname(__FILE__).'/vendor/autoload.php';
}


if(class_exists('includes\\Init')){
    includes\Init::register_services();
}

function activate_saurab_plugin()
{
    $activateobject=new Activator();
    $activateobject -> activate();

}

function deactivate_saurab_plugin()
{
    includes\Base\Deactivator::deactivate();
}

register_activation_hook(__FILE__,'activate_saurab_plugin');

register_deactivation_hook(__FILE__,'deactivate_saurab_plugin');