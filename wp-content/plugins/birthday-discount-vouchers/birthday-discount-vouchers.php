<?php
/*
Plugin Name: Birthday Discount Vouchers
Plugin URI: https://codecanyon.net/item/woocommerce-birthday-discount-vouchers/19822188
Description: This plugin allows you to send discount vouchers to your customers on their birthday
Version: 1.0.3
Author: WooExtension
Author URI: http://wooextension.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// No direct file access
! defined( 'ABSPATH' ) AND exit;

define( 'BDV_FILE', __FILE__ );
define( 'BDV_PATH', plugin_dir_path(__FILE__) );
define( 'BDV_BASE', plugin_basename(__FILE__) );
define( 'BDV_PLUGIN_NAME', 'Birthday and Discount Vouchers' );


add_action('init', 'allow_admin_to_pay_for_order');


function allow_admin_to_pay_for_order(){

    $administrator = get_role('administrator');
    $administrator->add_cap( 'pay_for_order' );
}
/**
 * Enable Localization
 */
add_action( 'plugins_loaded', 'wbdv_load_textdomain' );

function wbdv_load_textdomain() {
	load_plugin_textdomain( 'wbdv', false, basename( dirname( __FILE__ ) ) . '/lang' );
}
//add_action( 'admin_notices',  'check_required_plugins' ) ;

if ( ! in_array( 'woocommerce/woocommerce.php', (array) get_option( 'active_plugins', array() ), true )) { ?>
<div id="message" class="error">
    <p><?php echo BDV_PLUGIN_NAME; ?> requires <a href="http://www.woothemes.com/woocommerce/" target="_blank">WooCommerce</a> to be activated in order to work. Please install and activate <a href="<?php echo admin_url('/plugin-install.php?tab=search&amp;type=term&amp;s=WooCommerce'); ?>" target="">WooCommerce</a> first.</p>
</div> <?php
 }
else{
  echo  "successfull";
}


require BDV_PATH . '/includes/class-bdv.php';

new Birthday_Discount_Vouchers_Lite();