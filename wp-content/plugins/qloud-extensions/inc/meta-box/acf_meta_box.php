<?php
if (function_exists('acf_add_local_field_group')):

    // Page Options
    acf_add_local_field_group(array(
        'key' => 'group_46Cg7N74r8t811VLFfR6',
        'title' => 'Page Options',
        'fields' => array(


            // Banner Settings
            array(
                'key' => 'field_7a2p3jBTfCbZ17c4cciu',
                'label' => 'Banner Settings',
                'name' => 'banner',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'placement' => 'left',
                'endpoint' => 0,
            ),

            array(
                'key' => 'field_QnF1',
                'label' => 'Display Banner',
                'name' => 'display_banner',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'choices' => array(
                            'default' => 'Default',
                            'yes' => 'yes',
                            'no' => 'no',                            
                        ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),

            array(
                'key' => 'banner_remove_options',
                'label' => 'Remove Options',
                'name' => 'remove_options',
                'type' => 'radio',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_QnF1',
                            'operator' => '==',
                            'value' => 'no',
                        ) ,
                    ) ,
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'full-remove' => 'Remove Full Breadcrumb',
                    'with-padding' => 'Remove With Padding',
                ),
                'allow_custom' => 0,
                'default_value' => array(
                ),
                'layout' => 'horizontal',
                'toggle' => 0,
                'return_format' => 'value',
                'save_custom' => 0,
            ),

            array(
                'key' => 'key_pjros',
                'label' => 'Breadcumbs Layout',
                'name' => 'breadcumb_layout',
                'type' => 'group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_QnF1Ebcc8OXfqaebPj7H',
                            'operator' => '==',
                            'value' => 'yes',
                        ) ,
                    ) ,
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'layout' => 'table',
                'sub_fields' => array(
                    array(
                        'key' => 'field_WGCt5cd3bf759qMh8gRk',
                        'label' => 'Display Title',
                        'name' => 'display_title',
                        'type' => 'button_group',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'choices' => array(
                            'default' => 'Default',
                            'yes' => 'yes',
                            'no' => 'no',                            
                        ) ,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'message' => '',
                        'default_value' => 'default',
                        'ui' => 1,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ) ,

                    array(
                        'key' => 'field_3PnJp21d93eM5Nrs8422',
                        'label' => 'Display Breadcumbs',
                        'name' => 'display_breadcumb',
                        'type' => 'button_group',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'message' => '',
                        'choices' => array(
                            'default' => 'Default',
                            'yes' => 'yes',
                            'no' => 'no',                            
                        ) ,
                        'default_value' => 'default',
                        'ui' => 1,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ) ,

                    

                ) ,
            ) ,

            array(
                'key' => 'key_banner_back',
                'label' => 'Banner Background',
                'name' => 'banner_back_option',
                'type' => 'group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_QnF1',
                            'operator' => '==',
                            'value' => 'yes',
                        ) ,
                    ) ,
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'layout' => '',
                'sub_fields' => array(
                    array(
                        'key' => 'field_ybmis',
                        'label' => 'Background',
                        'name' => 'banner_background_type',
                        'type' => 'button_group',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '50',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'choices' => array(
                            'default' => 'Default',
                            'color' => 'Color',
                            'image' => 'Image'
                        ) ,
                        'allow_null' => 0,
                        'default_value' => 'default',
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                    ) ,
                    array(
                        'key' => 'field_egeo',
                        'label' => 'Background Color',
                        'name' => 'banner_background_color',
                        'type' => 'color_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_ybmis',
                                    'operator' => '==',
                                    'value' => 'color',
                                ) ,
                            ) ,
                        ) ,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'default_value' => '',
                    ) ,

                    array(
                            'key' => 'field_5d6d06b7dca4c',
                            'label' => 'Background Image',
                            'name' => 'banner_background_image',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => array(
                                    array(
                                        array(
                                            'field' => 'field_ybmis',
                                            'operator' => '==',
                                            'value' => 'image',
                                        ) ,
                                    ) ,
                                ) ,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'array',
                            'preview_size' => 'medium',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),

                    array(
                        'key' => 'field_ybmisxy',
                        'label' => 'Background Size',
                        'name' => 'banner_background_size',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                         'conditional_logic' => array(
                                    array(
                                        array(
                                            'field' => 'field_ybmis',
                                            'operator' => '==',
                                            'value' => 'image',
                                        ) ,
                                    ) ,
                                ) ,
                        'wrapper' => array(
                            'width' => '100',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'choices' => array(
                            'auto' => 'auto',
                            'cover' => 'cover',
                            'contain' => 'contain'
                        ) ,
                        'allow_null' => 0,
                        'default_value' => '',
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                    ) ,

                     array(
                        'key' => 'field_ybmiskr',
                        'label' => 'Background Repeat',
                        'name' => 'banner_background_repeat',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                         'conditional_logic' => array(
                                    array(
                                        array(
                                            'field' => 'field_ybmis',
                                            'operator' => '==',
                                            'value' => 'image',
                                        ) ,
                                    ) ,
                                ) ,
                        'wrapper' => array(
                            'width' => '100',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'choices' => array(
                            'no-repeat' => 'no-repeat',
                            'repeat' => 'repeat',
                            'repeat-y' => 'repeat-y',
                            'repeat-x' => 'repeat-x',
                            'initial' => 'initial',
                            'inherit' => 'inherit'
                        ) ,
                        'allow_null' => 0,
                        'default_value' => '',
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                    ) ,

                ) ,
            ) ,

            // Header Opion
    
            array(
                'key' => 'field_TfCbZ17c4cciu',
                'label' => 'Header Options',
                'name' => 'header',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'placement' => 'left',
                'endpoint' => 0,
            ) ,


            //header layout

            array(
                'key' => 'header_menu_style',
                'label' => 'Header Menu Style',
                'name' => 'header_menu_variation',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'choices' => array(
                    'default' => 'Default',
                    '1' => 'Style 1',
                    '2' => 'Style 2',                            
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,

            array(
                'key' => 'header_menu_color_style',
                'label' => 'Menu Background Color',
                'name' => 'menu_color',
                'type' => 'radio',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'header_menu_style',
                            'operator' => '==',
                            'value' => '1',
                        ) ,
                    ) ,
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'transparent' => 'Transparent',
                    'dark' => 'Dark',
                ),
                'allow_custom' => 0,
                'default_value' => array(
                ),
                'layout' => 'horizontal',
                'toggle' => 0,
                'return_format' => 'value',
                'save_custom' => 0,
            ),

            array(
                'key' => 'header_logovariation',
                'label' => 'Set Header Logo as',
                'name' => 'header_logo_variation',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'choices' => array(
                    'default' => 'Default',
                    '1' => 'Image',
                    '2' => 'Text',                            
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,


            array(
                'key' => 'key_header',
                'label' => '',
                'name' => 'header_layout',
                'type' => 'group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'header_logovariation',
                            'operator' => '!=',
                            'value' => 'default',
                        ) ,
                    ) ,
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'layout' => 'table',
                'sub_fields' => array(
            
                    array(
                        'key' => 'field_logo',
                        'label' => 'Logo as image',
                        'name' => 'header_as_logo',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'header_logovariation',
                                    'operator' => '==',
                                    'value' => '1',
                                ) ,
                            ) ,
                        ) ,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'medium',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),


                    array (
                        'key' => 'field_header_text',
                        'label' => 'Logo as Text',
                        'name' => 'logo_as_text',
                        'type' => 'text',
                        'prefix' => '',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'header_logovariation',
                                    'operator' => '==',
                                    'value' => '2',
                                ) ,
                            ) ,
                        ) ,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array(
                        'key' => 'field_logo_tag',
                        'label' => 'Text Tag',
                        'name' => 'logo_text_tag',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'header_logovariation',
                                    'operator' => '==',
                                    'value' => '2',
                                ) ,
                            ) ,
                        ) ,
                        'wrapper' => array(
                            'width' => '25',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'choices' => array(
                            'h1' => 'h1',
                            'h2' => 'h2',
                            'h3' => 'h3',
                            'h4' => 'h4',
                            'h5' => 'h5',
                            'h6' => 'h6'
                        ) ,
                        'default_value' => 'h2',
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ) ,
                    array(
                        'key' => 'field_logo_color',
                        'label' => 'Logo Color',
                        'name' => 'logo_color_text',
                        'type' => 'color_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'header_logovariation',
                                    'operator' => '==',
                                    'value' => '2',
                                ) ,
                            ) ,
                        ) ,
                        'wrapper' => array(
                            'width' => '25',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'default_value' => '',
                    ) ,

                ) ,
            ) ,


                    // Sticky Header

                    array(
                        'key' => 'header_stick_logovariation',
                        'label' => 'Set Sticky Header Logo as',
                        'name' => 'header_stick_logo_variation',
                        'type' => 'button_group',
                        'instructions' => '',
                        'required' => 0,
                        'choices' => array(
                            'default' => 'Default',
                            '1' => 'Image',
                            '2' => 'Text',                            
                        ) ,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'message' => '',
                        'default_value' => 'default',
                        'ui' => 1,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ) ,

                    array(
                        'key' => 'key_stick_header',
                        'label' => 'Logo',
                        'name' => 'header_stick_layout',
                        'type' => 'group',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ) ,
                        'layout' => 'table',
                        'sub_fields' => array(
                    
                            array(
                                'key' => 'field_stick_logo',
                                'label' => 'Logo as image',
                                'name' => 'header_stick_as_logo',
                                'type' => 'image',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => array(
                                    array(
                                        array(
                                            'field' => 'header_stick_logovariation',
                                            'operator' => '==',
                                            'value' => '1',
                                        ) ,
                                    ) ,
                                ) ,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'return_format' => 'array',
                                'preview_size' => 'medium',
                                'library' => 'all',
                                'min_width' => '',
                                'min_height' => '',
                                'min_size' => '',
                                'max_width' => '',
                                'max_height' => '',
                                'max_size' => '',
                                'mime_types' => '',
                            ),
                            array (
                                'key' => 'field_stick_header_text',
                                'label' => 'Logo as Text',
                                'name' => 'logo_stick_as_text',
                                'type' => 'text',
                                'prefix' => '',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'conditional_logic' => array(
                                    array(
                                        array(
                                            'field' => 'header_stick_logovariation',
                                            'operator' => '==',
                                            'value' => '2',
                                        ) ,
                                    ) ,
                                ) ,
                                'wrapper' => array (
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                                'readonly' => 0,
                                'disabled' => 0,
                            ),
                            array(
                                'key' => 'field_stick_logo_tag',
                                'label' => 'Text Tag',
                                'name' => 'logo_stick_text_tag',
                                'type' => 'select',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => array(
                                    array(
                                        array(
                                            'field' => 'header_stick_logovariation',
                                            'operator' => '==',
                                            'value' => '2',
                                        ) ,
                                    ) ,
                                ) ,
                                'wrapper' => array(
                                    'width' => '25',
                                    'class' => '',
                                    'id' => '',
                                ) ,
                                'choices' => array(
                                    'h1' => 'h1',
                                    'h2' => 'h2',
                                    'h3' => 'h3',
                                    'h4' => 'h4',
                                    'h5' => 'h5',
                                    'h6' => 'h6'
                                ) ,
                                'default_value' => 'h2',
                                'allow_null' => 0,
                                'multiple' => 0,
                                'ui' => 0,
                                'return_format' => 'value',
                                'ajax' => 0,
                                'placeholder' => '',
                            ) ,
                            array(
                                'key' => 'field_stick_logo_color',
                                'label' => 'Logo Color',
                                'name' => 'logo_stick_color_text',
                                'type' => 'color_picker',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => array(
                                    array(
                                        array(
                                            'field' => 'header_stick_logovariation',
                                            'operator' => '==',
                                            'value' => '2',
                                        ) ,
                                    ) ,
                                ) ,
                                'wrapper' => array(
                                    'width' => '25',
                                    'class' => '',
                                    'id' => '',
                                ) ,
                                'default_value' => '',
                            ) ,

                         ) ,
                    ) ,

            // Footer Options
            array(
                'key' => 'field_1gY7e',
                'label' => 'Footer Settings',
                'name' => 'footer',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'placement' => 'left',
                'endpoint' => 0,
            ) ,

            array(
                'key' => 'acf_key_footer_switchdf',
                'label' => 'Display Footer',
                'name' => 'display_footer',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'choices' => array(
                            'default' => 'Default',
                            'yes' => 'yes',
                            'no' => 'no',                            
                        ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,

            array(
                'key' => 'acf_key_footer',
                'label' => 'Customize Footer',
                'name' => 'acf_footer_options',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_key_footer_switchdf',
                            'operator' => '==',
                            'value' => 'yes',
                        ) ,
                    ) ,
                ) ,
                'choices' => array(
                            'default' => 'Default',
                            '1' => 'One Column',
                            '2' => 'Two Column',                            
                            '3' => 'Three Column',                            
                            '4' => 'Four Column',                            
                            '5' => 'Five Column',                            
                        ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,

            array(
                'key' => 'field_WGC',
                'label' => 'Display top footer',
                'name' => 'display_top_footer',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_key_footer_switchdf',
                            'operator' => '==',
                            'value' => 'yes',
                        ) ,
                    ) ,
                ) ,
                'choices' => array(
                    'default' => 'Default',
                    'yes' => 'yes',
                    'no' => 'no',                            
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,

            array(
                'key' => 'field_WGCmid',
                'label' => 'Display middel footer',
                'name' => 'display_middel_footer',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_key_footer_switchdf',
                            'operator' => '==',
                            'value' => 'yes',
                        ) ,
                    ) ,
                ) ,
                'choices' => array(
                    'default' => 'Default',
                    'yes' => 'yes',
                    'no' => 'no',                            
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,

            array(
                'key' => 'field_WGCbott',
                'label' => 'Display bottom footer',
                'name' => 'display_bottom_footer',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'acf_key_footer_switchdf',
                            'operator' => '==',
                            'value' => 'yes',
                        ) ,
                    ) ,
                ) ,
                'choices' => array(
                    'default' => 'Default',
                    'yes' => 'yes',
                    'no' => 'no',                            
                ) ,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ) ,
                'message' => '',
                'default_value' => 'default',
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ) ,

        ) ,
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ) ,

            ) ,
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ) ,

            ) ,

            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'iqonicteam',
                ) ,

            ) ,

             array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'portfolio',
                ) ,

            ) ,

              array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'testimonial',
                ) ,

            ) ,

              array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'client',
                ) ,

            ) ,
            
        ) ,
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
endif;
