<?php
namespace Elementor; 
if ( ! defined( 'ABSPATH' ) ) exit; 

// Blog Category
if ( !function_exists('iq_by_blog_cat_2')) {
	function iq_by_blog_cat_2() {
		$taxonomy = 'category';
		$iq_blog_cat = array();
		$terms = get_terms($taxonomy);

			foreach ( $terms as $term ) {			   
			    $iq_blog_cat[$term->slug] = $term->name;
		    }
		    return $iq_blog_cat;
	}	
}

/**
 * Elementor Blog widget.
 *
 * Elementor widget that displays an eye-catching headlines.
 *
 * @since 1.0.0
 */

class Qloud_Blog extends Widget_Base {

	public function __construct($data = [], $args = null) {

		parent::__construct($data, $args);
		wp_register_script('owl-carousel-qloud', IQ_TH_URL .'/assest/js/owl.carousel.min.js', [ 'elementor-frontend' ], '1.0.0' , true);
		wp_register_script('iq_owl-qloud', IQ_TH_URL .'/assest/js/widget/iq_owl.js', [ 'elementor-frontend', 'owl-carousel-qloud' ], '1.0.0' , true);
		wp_register_style( 'owl-carousel-qloud', IQ_TH_URL .'/assest/css/owl.carousel.min.css');

		
	}

	public function get_script_depends() {
        return [ 'owl-carousel-qloud', 'iq_owl-qloud' ];
    }

    public function get_style_depends() {
        return [ 'owl-carousel-qloud' ];
    }
	
	/**
	 * Get widget name.
	 *
	 * Retrieve heading widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */

	public function get_name() {
		return __( 'Qloud_blog', 'qloud' );
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve heading widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	
	public function get_title() {
		return __( 'Blog', 'qloud' );
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the heading widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */

	public function get_categories() {
		return [ 'qloud' ];
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve heading widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */

	public function get_icon() {
		return 'eicon-slider-push';
	}

	/**
	 * Register heading widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */

	protected function _register_controls() {
		$this->start_controls_section(
			'section_blog',
			[
				'label' => __( 'Blog Post', 'qloud' ),
				
			]
		);

		$this->add_control(
			'blog_type',
			[
				'label'      => __( 'Blog Style', 'qloud' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => '1',
				'options'    => [
					
					'1'          => __( 'Blog Slider', 'qloud' ),
					'2'          => __( 'Blog Gride', 'qloud' ),
				],
			]
		);

        $this->add_control(
			'blog_style',
			[
				'label'      => __( 'Blog Style', 'qloud' ),
				'type'       => Controls_Manager::SELECT,
				'condition' => [
					'blog_type' => '2',
				],
				'options'    => [
					'2'          => __( 'Blog 1 Columns', 'qloud' ),
					'3'          => __( 'Blog 2 Columns', 'qloud' ),
					'4'          => __( 'Blog 3 Columns', 'qloud' ),
				],
				'default'    => '1',
			]
		);

		$this->add_control(
            'blog_cat_2',
            [
                'label' => esc_html__( 'Select blog category', 'iqonic' ),
                'type' => Controls_Manager::SELECT2,
                'return_value' => 'true',
                'multiple' => true,
                'options' => iq_by_blog_cat_2(),
            ]
        );
        
        $this->add_control(
			'desk_number',
			[
				'label' => __( 'Desktop view', 'qloud' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '3',
			]
		);

		$this->add_control(
			'lap_number',
			[
				'label' => __( 'Laptop view', 'qloud' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '3',
			]
		);

		$this->add_control(
			'tab_number',
			[
				'label' => __( 'Tablet view', 'qloud' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '2',
			]
		);

		$this->add_control(
			'mob_number',
			[
				'label' => __( 'Mobile view', 'qloud' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'condition' => [
					'blog_type' => '1',
				],
				'label_block' => true,
				'default'    => '1',
			]
		);	

		$this->add_control(
			'autoplay',
			[
				'label'      => __( 'Autoplay', 'qloud' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'qloud' ),
					'false'      => __( 'False', 'qloud' ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_control(
			'loop',
			[
				'label'      => __( 'Loop', 'qloud' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'qloud' ),
					'false'      => __( 'False', 'qloud' ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_control(
			'dots',
			[
				'label'      => __( 'Dots', 'qloud' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'qloud' ),
					'false'      => __( 'False', 'qloud' ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_control(
			'nav-arrow',
			[
				'label'      => __( 'Arrow', 'qloud' ),
				'type'       => Controls_Manager::SELECT,
				'default'    => 'true',
				'options'    => [
					'true'       => __( 'True', 'qloud' ),
					'false'      => __( 'False', 'qloud' ),
					
				],
				'condition' => [
					'blog_type' => '1',
				]
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label' => __( 'Margin', 'qloud' ),
				'type' => Controls_Manager::SLIDER,
				
				'condition' => [
					'blog_type' => '1',
				]
				
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label' => __( 'Padding', 'qloud' ),
				'type' => Controls_Manager::SLIDER,
				
				'condition' => [
					'blog_type' => '1',
				]
				
			]
		);


		

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order By', 'qloud' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'ASC',
				'options' => [
						'DESC' => esc_html__('Descending', 'qloud'), 
						'ASC' => esc_html__('Ascending', 'qloud') 
				],

			]
		);
		
		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'qloud' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'text-left' => [
						'title' => __( 'Left', 'qloud' ),
						'icon' => 'eicon-text-align-left',
					],
					'text-center' => [
						'title' => __( 'Center', 'qloud' ),
						'icon' => 'eicon-text-align-center',
					],
					'text-right' => [
						'title' => __( 'Right', 'qloud' ),
						'icon' => 'eicon-text-align-right',
					],
					'text-justify' => [
						'title' => __( 'Justified', 'qloud' ),
						'icon' => 'eicon-text-align-justify',
					],
				]
			]
		);

		
        $this->end_controls_section();  

		


	}
	/**
	 * Render Blog widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	
	protected function render() {
		
		require  QLOUD_TH_ROOT . '/inc/elementor/render/blog.php';
		
		if ( Plugin::$instance->editor->is_edit_mode() ) : ?>

            <script>
				
		        jQuery('.owl-carousel').each(function() {
                    
                    var jQuerycarousel = jQuery(this);
                    jQuerycarousel.owlCarousel({
                        items: jQuerycarousel.data("items"),                        
                        loop: jQuerycarousel.data("loop"),
                        margin: jQuerycarousel.data("margin"),
                        stagePadding: jQuerycarousel.data("padding"),
                        nav: jQuerycarousel.data("nav"),
                        dots: jQuerycarousel.data("dots"),
                        autoplay: jQuerycarousel.data("autoplay"),
                        autoplayTimeout: jQuerycarousel.data("autoplay-timeout"),
                        navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
                        responsiveClass: true,
                        responsive: {
                            // breakpoint from 0 up
                            0: {
                                items: jQuerycarousel.data("items-mobile-sm"),
                                nav: false,
                                dots: true
                            },
                            // breakpoint from 480 up
                            480: {
                                items: jQuerycarousel.data("items-mobile"),
                                nav: false,
                                dots: true
                            },
                            // breakpoint from 786 up
                            786: {
                                items: jQuerycarousel.data("items-tab")
                            },
                            // breakpoint from 1023 up
                            1023: {
                                items: jQuerycarousel.data("items-laptop")
                            },
                            1199: {
                                items: jQuerycarousel.data("items")
                            }
                        }
                    });
                });

		    </script>


		
		<?php endif; 
    }	    
		
}

Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\Qloud_Blog() );
