<?php
add_action( 'elementor/widgets/widgets_registered', 'QLOUD_register_elementor_widgets' );
function QLOUD_register_elementor_widgets() {
	
	if ( defined( 'ELEMENTOR_PATH' ) && class_exists('Elementor\Widget_Base') ) {
		
		
		require  QLOUD_TH_ROOT . '/inc/elementor/widget/blog.php';
		require  QLOUD_TH_ROOT . '/inc/elementor/widget/portfolio.php';
		
		
 	}
}

add_action( 'elementor/init', function() {
	\Elementor\Plugin::$instance->elements_manager->add_category( 
		'qloud',
		[
			'title' => __( 'Qloud Elements', 'qloud' ),
			'icon' => 'fa fa-plug',
		]
	);
});

// Add Custom Icon 


add_action( 'wp_footer', function() {
	if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
		return;
	}
	?>
	<script>
		jQuery( function( $ ) {
			// Add space for Elementor Menu Anchor link
			if ( window.elementorFrontend ) {
				jQuery("#load").fadeOut();
				jQuery("#loading").delay(0).fadeOut("slow");
				
				if(jQuery('header').hasClass('has-sticky'))
                {         
                    jQuery(window).on('scroll', function() {
                        if (jQuery(this).scrollTop() > 10) {
                            jQuery('header').addClass('menu-sticky');
                            jQuery('.has-sticky .logo').addClass('logo-display');
                        } else {
                            jQuery('header').removeClass('menu-sticky');
                            jQuery('.has-sticky .logo').removeClass('logo-display');
                        }
                    });

				}
				
			}
			
		} );
		
	</script>
	<?php
} );
