<?php 
/*
Plugin Name: Qloud Extensions	
Plugin URI: http://iqonic.design.com/
Description: Qloud plugin provides custom team post type, gallery post type with related functionality.
Author: iqonic.design
Version: 2.4
Author URI: http://www.goldenmace.com/
Text Domain: QLOUD_NAME
*/

if( !defined( 'QLOUD_TH_ROOT' ) ) 
	define('QLOUD_TH_ROOT', plugin_dir_path( __FILE__ ));

if( !defined( 'QLOUD_TH_URL' ) ) 
	define( 'QLOUD_TH_URL', plugins_url( '', __FILE__ ) );

if( !defined( 'QLOUD_NAME' ) ) 
	define( 'QLOUD_NAME', 'qloud' );

$qloud_option = get_option('qloud_options');

require_once(QLOUD_TH_ROOT . 'widget/footer-contact.php');

require_once(QLOUD_TH_ROOT . 'widget/subscribe.php');

require_once(QLOUD_TH_ROOT . 'widget/social_media.php');

require_once(QLOUD_TH_ROOT . 'widget/recent-post.php');

require_once(QLOUD_TH_ROOT . 'widget/testimonial.php');

require_once(QLOUD_TH_ROOT . 'widget/pdf.php');

require_once(QLOUD_TH_ROOT . 'inc/elementor/init.php');

if( function_exists( 'get_field' ) ){
	require_once( QLOUD_TH_ROOT. 'inc/meta-box/init.php' );
}
