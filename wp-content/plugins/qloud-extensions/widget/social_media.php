<?php

function qloud_social_media_widgets() {
	register_widget( 'iq_socail_media' );
}
add_action( 'widgets_init', 'qloud_social_media_widgets' );

/*-------------------------------------------
		iqonic Contact Information widget 
--------------------------------------------*/
class iq_socail_media extends WP_Widget {
 
	function __construct() {
		parent::__construct(
 
			// Base ID of your widget
			'iq_socail_media', 
			
			// Widget name will appear in UI
			esc_html('Qloud Social Media', 'qloud'), 
 
			// Widget description
			array( 'description' => esc_html( 'Qloud social media. ', 'qloud' ), ) 
		);
	}
 
	// Creating widget front-end
	
	public function widget( $args, $instance ) {

        if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html( 'Social Media', 'qloud' );
		
		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		/* here add extra display item  */ 
	
		$iq_contact = isset($instance[ 'iq-contact' ]) ? $instance['iq-contact'] : '';
		$qloud_option = get_option('qloud_options'); 

		if(isset($qloud_option['social-media-iq'])) {
			$top_social = $qloud_option['social-media-iq'];  ?>
		    <ul class="info-share social-icone d-inline list-inline"> <?php 
			    $i = 1;
			    foreach($top_social as $key=>$value) {
					if($value) {
						echo '<li class="list-inline-item"><a href="'.$value.'"><i class="fa fa-'.$key.'"></i></a></li>';
					}
				    $i++;
		        } ?>
		    </ul> <?php
		}		

	}
         
	// Widget Backend 
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
				
		?>
		
		<p><label for="<?php echo esc_html($this->get_field_id( 'title','qloud' )); ?>"><?php esc_html_e( 'Title:','qloud' ); ?></label>
		<input class="widefat" id="<?php echo esc_html($this->get_field_id( 'title','qloud' )); ?>" name="<?php echo esc_html($this->get_field_name( 'title','qloud')); ?>" type="text" value="<?php echo esc_html($title,'qloud'); ?>" /></p>
		
		<?php 
							
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
        return $instance;
	}
} 
/*---------------------------------------
		Class wpb_widget ends here
----------------------------------------*/	
