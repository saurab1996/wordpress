<?php

function qloud_pdf() {
    register_widget( 'iq_pdf' );
}
add_action( 'widgets_init', 'qloud_pdf' );

/*-------------------------------------------
		iqonic Contact Information widget
--------------------------------------------*/
class iq_pdf extends WP_Widget {

	function __construct() {
		parent::__construct(

			// Base ID of your widget
			'iq_pdf',

			// Widget name will appear in UI
			esc_html('Qloud PDF', 'qloud'),

			// Widget description
			array( 'description' => esc_html( 'Qloud PDF download. ', 'qloud' ), )
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {

		global $wp_registered_sidebars;

        if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : '';
		$pdftitle = ( ! empty( $instance['pdftitle'] ) ) ? $instance['pdftitle'] : esc_html( 'PDF download', 'qloud' );
		$pdflink = ( ! empty( $instance['pdflink'] ) ) ? $instance['pdflink'] : esc_html( 'PDF download', 'qloud' );

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'title', $title, $instance, $this->id_base );
		$pdftitle = apply_filters( 'pdftitle', $pdftitle, $instance, $this->id_base );
		$pdflink = apply_filters( 'pdflink', $pdflink, $instance, $this->id_base );

		//$args['after_widget'];
		?>

		<div class="widget get-file">
			<?php
			if ( $title ) {
				echo ($args['before_title'] . $title . $args['after_title']);
			}
			?>

			<a href="<?php echo $pdflink; ?>">
				<ul class="list-inline download-item">
				<li class="list-inline-item"><i class="fa fa-file-pdf-o"></i><span class="ml-3"><?php echo $pdftitle; ?></span></li>
				</ul>
			</a>
		</div>

	<?php
	}

	// Widget Backend
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$pdftitle     = isset( $instance['pdftitle'] ) ? esc_attr( $instance['pdftitle'] ) : '';
		$pdflink     = isset( $instance['pdflink'] ) ? esc_attr( $instance['pdflink'] ) : '';
		?>
		<p><label for="<?php echo esc_html($this->get_field_id( 'title','qloud' )); ?>"><?php esc_html_e( 'Title:','qloud' ); ?></label>
		<input class="widefat" id="<?php echo esc_html($this->get_field_id( 'title','qloud' )); ?>" name="<?php echo esc_html($this->get_field_name( 'title','qloud')); ?>" type="text" value="<?php echo esc_html($title,'qloud'); ?>" /></p>

		<p><label for="<?php echo esc_html($this->get_field_id( 'pdftitle','qloud' )); ?>"><?php esc_html_e( 'File Title:','qloud' ); ?></label>
		<input class="widefat" id="<?php echo esc_html($this->get_field_id( 'pdftitle','qloud' )); ?>" name="<?php echo esc_html($this->get_field_name( 'pdftitle','qloud')); ?>" type="text" value="<?php echo esc_html($pdftitle,'qloud'); ?>" /></p>

		<p><label for="<?php echo esc_html($this->get_field_id( 'pdflink','qloud' )); ?>"><?php esc_html_e( 'File Link:','qloud' ); ?></label>
		<input class="widefat" id="<?php echo esc_html($this->get_field_id( 'pdflink','qloud' )); ?>" name="<?php echo esc_html($this->get_field_name( 'pdflink','qloud')); ?>" type="text" value="<?php echo esc_html($pdflink,'qloud'); ?>" /></p>

		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['pdftitle'] = sanitize_text_field( $new_instance['pdftitle'] );
		$instance['pdflink'] = sanitize_text_field( $new_instance['pdflink'] );
        return $instance;
	}
}
/*---------------------------------------
		Class wpb_widget ends here
----------------------------------------*/
