<?php

function qloud_subscribe_widgets() {
	register_widget( 'iq_contact_info' );
}
add_action( 'widgets_init', 'qloud_subscribe_widgets' );

/*-------------------------------------------
		iqonic Contact Information widget
--------------------------------------------*/
class iq_contact_info extends WP_Widget {

	function __construct() {
		parent::__construct(

			// Base ID of your widget
			'iq_contact_info',

			// Widget name will appear in UI
			esc_html('Qloud Subscribe', 'qloud'),

			// Widget description
			array( 'description' => esc_html( 'Qloud subscribe. ', 'qloud' ), )
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
        if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : esc_html( 'qloud Subscribe', 'qloud' );

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number ) {
			$number = 5;
		}
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$show_author = isset( $instance['show_author'] ) ? $instance['show_author'] : false;



		//$args['after_widget'];

		/* here add extra display item  */
		$iq_contact = isset($instance[ 'iq-contact' ]) ? $instance['iq-contact'] : '';

		?>
		<div class="widget">
			<div class="container">
				<div class="row  align-items-center qloud-subscribe">
					<div class="col-lg-5   col-sm-12 title-fancy mb-5 mb-lg-0">
						<?php
						if ( $title ){
							echo ($args['before_title'] . $title . $args['after_title']);
						}
						?>
						<?php
						$qloud_option = get_option('qloud_options');
						if(!empty($qloud_option['qloud_subscribe_contant']))
						{
						?>
						<p class="mb-0"><?php echo html_entity_decode($qloud_option['qloud_subscribe_contant']); ?></p>
						<?php
						}
						?>
					</div>
					<div class="col-lg-7 col-sm-12 text-center">
						<?php
						$qloud_option = get_option('qloud_options');
						if(isset($qloud_option['qloud_subscribe'])){
							$qloud_subscribe = $qloud_option['qloud_subscribe'];
						}
						echo do_shortcode($qloud_subscribe);
						?>
					</div>
				</div>
			</div>
		</div>
		<?php

	}

	// Widget Backend
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		?>

		<p><label for="<?php echo esc_html($this->get_field_id( 'title','qloud' )); ?>"><?php esc_html_e( 'Title:','qloud' ); ?></label>
		<input class="widefat" id="<?php echo esc_html($this->get_field_id( 'title','qloud' )); ?>" name="<?php echo esc_html($this->get_field_name( 'title','qloud')); ?>" type="text" value="<?php echo esc_html($title,'qloud'); ?>" /></p>

		<?php

	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
        return $instance;
	}
}
/*---------------------------------------
		Class wpb_widget ends here
----------------------------------------*/
